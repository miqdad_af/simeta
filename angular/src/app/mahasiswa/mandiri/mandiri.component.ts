import { Component, ViewEncapsulation, NgZone, OnInit, OnDestroy } from "@angular/core";
import { Response } from '@angular/http';
import { ISubscription } from "rxjs/Subscription";
import { AuthHttp } from "angular2-jwt";

import { ToastrService } from 'toastr-ng2';
import { DataService } from '../../data/data.service';
import { IMyOptions } from 'mydatepicker';
import { NgUploaderOptions } from 'ngx-uploader';

@Component({
  selector: "mandiri",
  encapsulation: ViewEncapsulation.None,
  providers: [DataService],
  styles: [require("./mandiri.scss")],
  template: require("./mandiri.html")
})
export class Mandiri implements OnInit, OnDestroy {
  // status
  statusDaftar;
  statusKolokium;
  statusPraseminar;
  statusProfile;
  statusSeminar;
  statusSidang;
  statusSkl;
  statusTa;
  statusForm: number;
  statusTempat: number;
  statusAdmin: number;

  public statusPembimbing1: number;
  public statusPembimbing2: number;
  public statusPenguji1: number;
  public statusPenguji2: number;

  // cek koneksi
  noConn;
  status;

  private dosen1;
  private dosen_1;
  private dosen2;
  private dosen_2;
  private topik;

  private logPDF = this.data.urlLogPDF;
  private seminarPDF = this.data.urlSeminarPDF;

  private subscription: ISubscription;

  constructor(
    public authHttp: AuthHttp,
    public toastr: ToastrService,
    public data: DataService
  ) {
    this.d = new Date();
    this.dateFormat = {
      date: {
        year: this.d.getFullYear(),
        month: this.d.getMonth() + 1,
        day: this.d.getDate()
      }
    };
  }

  private myDatePickerOptions: IMyOptions = {
    dateFormat: "yyyy-mm-dd",
    editableDateField: false,
    width: "220px"
  };

  d;
  dateFormat;

  onDateChanged(e) {
    this.tanggal = e.formatted;
  }

  delete() {
    this.subscription = this.authHttp
      .get(this.data.urlDeleteMandiri)
      .map((res: Response) => res.json())
      .subscribe(data => {
        if (data.status) {
          this.refresh();
        }
      });
  }

  submit(nim) {
    this.status = false;
    console.log(this.nim);
    let creds = JSON.stringify({
      nim,
      nama: this.nama,
      dosen1: this.dosen1,
      topik: this.topik,
      tempat: this.tempat,
      jam: this.jam,
      tanggal: this.tanggal,
      status_formulir: this.statusForm
    });
    console.log(creds);
    this.authHttp
      .post(this.data.urlSeminarMandiri, creds)
      .map(res => res.json())
      .subscribe(data => {
        if (data.status) {
          this.showSuccess();
        } else {
          this.toastr.error('Gagal update seminar mandiri. Silahkan coba lagi.', 'Error');
        }
        this.status = true;
        this.ngOnInit();
      });
  }

  submitDua() {
    let creds = JSON.stringify({
      nim: this.nim,
      pembahas_1: this.pembahas_1,
      pembahas_2: this.pembahas_2,
      pembahas_3: this.pembahas_3,
      status_formulir: this.statusForm,
      form: 2
    });
    this.subscription = this.authHttp.post(this.data.urlSetPembahas, creds)
      .map((res: Response) => res.json())
      .subscribe(
        data => {
          if (data.status) {
            this.upload = 1;
            this.getDataSeminar();
            this.showSuccess();
          } else {
            this.toastr.error('Gagal update status permohonan', 'Error');
          }
        }, 
        err => console.log(err)
      )
  }

  showSuccess() {
    this.toastr.success("Berhasil Update Seminar Mandiri", "Success!");
  }

  dataSeminar;
  show = false;
  pembahas_1;
  pembahas_2;
  pembahas_3;
  tempat;
  jam;
  tanggal;
  berkas;
  makalah;
  upload = 0;
  pengumuman = 0;
  log;
  nim;
  nama;

  getDataSeminar() {
    this.authHttp
      .get(this.data.urlSeminarData)
      .map((res: Response) => res.json())
      .subscribe(data => {
        this.dataSeminar = data;
        this.log = data.log;

        if (this.dataSeminar.seminar.jenis_seminar == 3) {
          if (data.data.pembahas_1 || data.data.pembahas_2 || data.data.pembahas_3) this.upload = 1;
          this.show = true;
          this.nim = data.data.nim;
          this.pembahas_1 = data.data.pembahas_1;
          this.pembahas_2 = data.data.pembahas_2;
          this.pembahas_3 = data.data.pembahas_3;
          this.tempat = data.data.tempat;
          this.jam = data.data.jam;
          this.tanggal = data.data.tanggal;
          this.statusForm = data.data.status_formulir;
          this.statusTempat  = data.data.status_tempat;
          this.statusAdmin = data.data.status_admin;
          this.dateFormat = new Date(data.data.tanggal);
          this.dateFormat = <Object>{
            date: {
              year: this.dateFormat.getFullYear(),
              month: this.dateFormat.getMonth() + 1,
              day: this.dateFormat.getDate()
            }
          };
          this.getStatusMandiri();
          this.berkas = data.data.berkas;
          if (data.data.makalah) {
            this.pengumuman = 1;
            this.makalah =
              "http://simeta.apps.cs.ipb.ac.id/uploads/fileSeminar/mandiri/" +
              data.data.makalah;
          }

          console.log(this.statusAdmin !== 1 && this.statusTempat !== 1);
        }
      });
  }

  public getStatusMandiri() {
    this.authHttp.post(this.data.urlGetStatusMandiri, JSON.stringify({nim: this.nim}))
      .map((res: Response) => res.json())
      .subscribe(
        data => {
          if (data.status) {
            this.statusPembimbing1 = data.data[0].konfirmasi_pembimbing1;
            this.statusPembimbing2 = data.data[0].konfirmasi_pembimbing2;
            this.statusPenguji1 = data.data[0].konfirmasi_penguji1;
            this.statusPenguji2 = data.data[0].konfirmasi_penguji2;
            console.log(this.statusPembimbing1, this.statusPembimbing2, this.statusPenguji1, this.statusPenguji2);
          } else {
            this.toastr.error('Tidak dapat mendapatkan status konfirmasi', 'Error');
          }
        }
      )
  }

  // ---------------------------
  // FILE UPLOAD

  private zone: NgZone;

  uploadFile: any;
  hasBaseDropZoneOver: boolean = false;

  private progress: number = 0;
  private response: any = {};

  options: NgUploaderOptions = {
    url: this.data.urlUploadMandiri,
    authToken: localStorage.getItem("id_token"),
    authTokenPrefix: ""
  };
  sizeLimit = 3000000;

  preview = "";
  handleUpload(data: any): void {
    if (data && data.response) {
      let data1 = JSON.parse(data.response);
      this.uploadFile = data1;
      this.pengumuman = 1;
      this.makalah =
        "http://simeta.apps.cs.ipb.ac.id/uploads/fileSeminar/mandiri/" +
        this.uploadFile[0].filename;
      this.showSelesai();
    }

    this.zone.run(() => {
      this.response = data;
      this.progress = Math.floor(data.progress.percent);
    });
  }

  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  beforeUpload(uploadingFile): void {
    if (uploadingFile.size > this.sizeLimit) {
      uploadingFile.setAbort();
      alert("File harus kurang dari 3 MB");
    }

    if (uploadingFile.originalName.search(".pdf") == -1) {
      uploadingFile.setAbort();
      alert("File Harus Berekstensi PDF");
    }
  }

  showSelesai() {
    this.toastr.success("Berhasil Upload Makalah Kolokium", "Success!");
  }

  // -----------------------------
  // TEMPLATE

  // DASHBOARD SERVICE
  getStatus() {
    this.subscription = this.authHttp
      .get(this.data.urlStatus)
      .map(res => res.json())
      .subscribe(data => {
        this.statusDaftar = data[0].statusDaftar;
        this.statusKolokium = data[0].statusKolokium;
        this.statusPraseminar = data[0].statusPrasminar;
        this.statusTa = data[0].statusTa;
        this.statusSeminar = data[0].statusSeminar;
        this.statusSidang = data[0].statusSidang;
        this.statusSkl = data[0].statusSkl;
        this.statusProfile = data[0].statusProfile;
        if (this.statusTa) {
          this.getDataMahasiswa();
        }
      });
  }

  ngOnInit() {
    this.zone = new NgZone({ enableLongStackTrace: false });
    this.getStatus();
    this.getConnection();
    this.getDataSeminar();
  }

  getConnection() {
    this.noConn = 0;

    this.subscription = this.authHttp
      .get(this.data.urlTest)
      .map(res => res.json())
      .subscribe(data => {
        this.status = data["status"];
      });

    setTimeout(() => {
      if (!this.status) {
        this.status = 0;
        this.noConn = 1;
        this.showNoConn();
      }
    }, 5000);
  }

  penguji1;
  penguji2;
  getDataMahasiswa() {
    this.subscription = this.authHttp
      .get(this.data.urlTa)
      .map(res => res.json())
      .subscribe(data => {
        this.nim = data[0]["nim"];
        this.nama = data[0]["nama"];
        this.topik = data[0]["topik"];
        this.dosen1 = data[0]["dosen1"];
        this.dosen2 = data[0]["dosen2"];
        this.dosen_1 = data[0]["dosen_1"];
        this.dosen_2 = data[0]["dosen_2"];
        this.penguji1 = data[0]["penguji1"];
        this.penguji2 = data[0]["penguji2"];
      });
  }

  refresh() {
    this.getStatus();
    this.getConnection();
    this.getConnection();
    this.getDataSeminar();
  }

  showNoConn() {
    this.toastr.warning("Error Connecting to Server", "Error");
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
