import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule }  from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { DataService } from '../../data/data.service';
import { PembimbingKedua } from './pembimbing-kedua.component';
import { routing }       from './pembimbing-kedua.routing';
import { AlertModule } from 'ng2-bootstrap/alert';
import { TypeaheadModule } from 'ng2-bootstrap/typeahead';
import { TypeaheadMatch } from 'ng2-bootstrap/typeahead/typeahead-match.class';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        NgaModule,
        AlertModule.forRoot(),
        TypeaheadModule.forRoot(),
        routing
    ],
    declarations: [
        PembimbingKedua
    ],
    providers: [
        DataService
    ]
})

export default class PembimbingKeduaModule {}

