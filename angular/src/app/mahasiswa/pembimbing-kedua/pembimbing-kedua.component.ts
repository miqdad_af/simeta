// SIMETA v2
// Komponen untuk mengatur logic dan distribusi data pada modul pengajuan pembimbing kedua

import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import {
    FormGroup,
    FormControl,
    FormBuilder,
    Validators,
    AbstractControl,
    ValidatorFn } from '@angular/forms';

import { Response } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { ToastrService } from 'toastr-ng2';
import { DataService } from '../../data/data.service';
import { TypeaheadMatch } from 'ng2-bootstrap/typeahead';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'pengajuan-pembimbing',
    encapsulation: ViewEncapsulation.None,
    template: require('./pembimbing-kedua.html'),
    styles: [require('./pembimbing-kedua.scss')]
})

export class PembimbingKedua implements OnInit {

    // Vairabel status
    public statusDaftar;
    public statusKolokium;
    public statusPraseminar;
    public statusProfile;
    public statusSeminar;
    public statusSidang;
    public statusSkl;
    public statusTa;
    public konfirmasiNull;
    public konfirmasiLima;

    // Variabel cek koneksi DB
    public status;
    public noConn;

    // Form
    public pembimbingKeduaForm: FormGroup;
    public checkBoxVal = false;
    public namaDosen: string;
    public creds: Object;
    // public skills = [
    //     'Software Engineering and Informations System',
    //     'Computational Intelligence Optimization',
    //     'Net-Centric Computation'
    // ];

    // Nama dosen untuk typeaheadmatching
    public dosenRes = '';
    public dosenRaw;
    public dosen: Array<string> = [];
    public typeaheadNoResults;

    // Data mahasiswa
    public nim: string;

    ngOnInit() {
        this.getStatus();
        this.getConnection();
        this.getDataMahasiswa();
        this.getStatusPengajuan();
        this.getDataDosen();
        this.getDataPengajuanPembimbingKedua();
    }

    constructor(
        private authHttp: AuthHttp,
        private data: DataService,
        private fb: FormBuilder,
        private toastr: ToastrService
    ) {}

    public showError() {
        this.toastr.error('Pengajuan Pembimbing Kedua Gagal', 'Error!');
    }

    public showSuccess() {
        this.toastr.success('Pengajuan Pembimbing Kedua Gagal Berhasil', 'Success !');
    }

    public showNoConn() {
        this.toastr.warning('Error Connecting to Server', 'Error');
    }

    public getStatus() {
        this.authHttp.get(this.data.urlStatus)
          .map(res => res.json())
          .subscribe(data => {
            this.statusDaftar = data[0].statusDaftar;
            this.statusKolokium = data[0].statusKolokium;
            this.statusPraseminar = data[0].statusPrasminar;
            this.statusTa = data[0].statusTa;
            this.statusSeminar = data[0].statusSeminar;
            this.statusSidang = data[0].statusSidang;
            this.statusSkl = data[0].statusSkl;
            this.statusProfile = data[0].statusProfile;
          });
    }

    public getDataPengajuanPembimbingKedua() {
        this.pembimbingKeduaForm = this.fb.group({
            namaPembimbingKedua: ['', Validators.required],
            bidangKeahlian: ['', Validators.required],
            namaInstansi: '',
            alamatInstansi: '',
            nomorTelpDosen: ['', Validators.minLength(11)],
            emailPembimbing: ['', this.emailValidator()],
            nipPembimbing: ['', Validators.minLength(18)]
        });
    }

    public getDataDosen() {
        this.authHttp.get(this.data.urlDosen)
            .map((res: Response) => res.json())
            .subscribe(data => {
                this.dosenRaw = data;
                this.dosenRaw.forEach(elem => {
                    this.dosen.push(elem.nama);
                });
            });
    }

    public getIdDosen(nama) {
        let id;
        this.dosenRaw.forEach(elem => {
            if (nama === elem.nama) {
                id = elem.id;
            }
        });
        return id;
    }

    public getConnection() {
        this.noConn = 0;
        this.authHttp.get(this.data.urlTest)
          .map((res: Response) => res.json())
          .subscribe(data => {
            this.status = data['status'];
          });
        setTimeout(() => {
          if (!this.status) {
            this.status = 0;
            this.noConn = 1;
            this.showNoConn();
          }
        }, 10000);
    }

    public typeaheadOnSelect(e: TypeaheadMatch): void {
        this.dosenRes = this.getIdDosen(e.value);
    }

    public changeTypeaheadNoResults(e: boolean): void {
        this.typeaheadNoResults = e;
    }

    public refresh() {
        this.getDataPengajuanPembimbingKedua();
        this.getDataDosen();
        this.getStatus();
        this.getConnection();
    }

    public onSubmit() {
        this.status = false;
        if (!this.checkBoxVal) {
            this.creds = {
                id: this.getIdDosen(this.pembimbingKeduaForm.value.namaPembimbingKedua),
                nim: this.nim,
                statReq: 1,
                dosen_2: this.pembimbingKeduaForm.value.namaPembimbingKedua,
                bidangKeahlian: this.pembimbingKeduaForm.value.bidangKeahlian
            };
        } else {
            this.creds = {
                id : this.getIdDosen(this.pembimbingKeduaForm.value.namaPembimbingKedua),
                nim: this.nim,
                statReq: 2,
                dosen_2: this.pembimbingKeduaForm.value.namaPembimbingKedua,
                bidangKeahlian: this.pembimbingKeduaForm.value.bidangKeahlian,
                namaInstansi: this.pembimbingKeduaForm.value.namaInstansi,
                alamatInstansi: this.pembimbingKeduaForm.value.alamatInstansi,
                nomorTelp: this.pembimbingKeduaForm.value.nomorTelpDosen,
                email: this.pembimbingKeduaForm.value.emailPembimbing,
                nip: this.pembimbingKeduaForm.value.nipPembimbing
            };
        }
        console.log(this.creds);
        this.authHttp.post(this.data.urlSetPembimbingKedua, JSON.stringify(this.creds))
            .map((res: Response) => res.json())
            .subscribe(
                (data) => {
                    console.log(data);
                    this.status = true;
                    if (data.status) {
                        this.toastr.success(
                        'Pengajuan berhasil telah dikirim, tunggu pemberitahuan selanjutnya',
                        'Success');
                    } else {
                        this.toastr.error(data.message, 'Error');
                    }
                    this.ngOnInit();
                },
                err => console.log(err)
            );
    }
    
    public getStatusPengajuan() {
        this.authHttp.get(this.data.urlGetStatus)
            .map((res: Response) => res.json())
            .subscribe((data)=> {
                console.log(data);
                if (data.status) {
                    this.konfirmasiNull = data.data[0];
                    this.konfirmasiLima = data.data[5];
                } else {
                    this.konfirmasiNull = true;
                    this.konfirmasiLima = false;
                    this.toastr.info('Anda belum sama sekali mengajukan pembimbing kedua', 'Info');
                }
                console.log(this.konfirmasiLima, this.konfirmasiNull);
            }, err => console.log(err));
    }

    private emailValidator(): ValidatorFn {
        const regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (this.checkBoxVal) {
            return (control: AbstractControl): { [key: string]: any} => {
                const value = control.value;
                if (value === '') return null;
                return !regexp.test(value) ? { 'patternInvalid': {regexp} } : null;
            };
        } else {
            return null;
        }
    }

    private getDataMahasiswa() {
        this.authHttp.get(this.data.urlTa).
            map((res: Response) => res.json()).
            subscribe(
                (data) => {
                    this.nim = data[0].nim;
                },
                err => console.log(err)
            );
    }
}
