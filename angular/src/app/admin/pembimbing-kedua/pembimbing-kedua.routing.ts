import { Routes, RouterModule } from '@angular/router';

import { PembimbingKedua } from './pembimbing-kedua.component';

const routes: Routes = [
    { 
        path: '', 
        component: PembimbingKedua 
    },
];

export const routing = RouterModule.forChild(routes);