import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Response } from '@angular/http';

import { ISubscription } from "rxjs/Subscription";
import { AuthHttp } from 'angular2-jwt';
import { DataService } from '../../data/data.service';
import { ToastrService } from 'toastr-ng2';
import swal from 'sweetalert2';


@Component({
    selector: 'pembimbing-kedua',
    encapsulation: ViewEncapsulation.None,
    styles: [require('./pembimbing-kedua.scss')],
    template: require('./pembimbing-kedua.html'),
})

export class PembimbingKedua implements OnInit, OnDestroy {
    // Cek koneksi
    public noConn;
    public status;

     // Data pembimbing kedua
    public items;
    public konfirmasiKetiga;
    public konfirmasiKeempat;

    // Subs (rxjs)
    private subscription: ISubscription;

    constructor(
        private authHttp: AuthHttp,
        private dataService: DataService,
        private toastr: ToastrService
    ) { }

    ngOnInit() {
        this.getConnection();
        this.getDataAdmin();
        this.getDataStatus();
    }

    public onSubmit(i: number) {
        this.status = false;
        this.subscription = this.authHttp.post(this.dataService.urlSetKonfirmasiAdmin, this.items[i])
            .map((res:Response) => res.json())
            .subscribe(data => {
                if (data.status) {
                    this.toastr.success('Berhasil melakukan konfirmasi', 'Berhasil');
                    this.ngOnInit();
                } else {
                    this.toastr.error('Gagal memberikan konfirmasi, silahkan ulangi lagi.', 'Error');
                }
                setTimeout(() => {
                    location.reload();
                }, 3000); 
            });
    }

    public onDelete(i: number) {
        // this.status = false;
        swal({
            title: 'Anda yakin?',
            text: 'Berikan alasan sabagai konfirmasi bahwa permohonan ini ditolak.',
            type: 'warning',
            input: 'text',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            showLoaderOnConfirm: true,
            confirmButtonText: 'Ditolak',
            preConfirm: (text) => {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        if (text === '') {
                            reject('Anda harus memberikan alasan.');
                        } else {
                            resolve();
                        }
                    }, 2000);
                });
            }
        }).then((text) => {
            if (text.value) {
                this.items[i].alasan = text.value;
                this.subscription = this.authHttp.post(this.dataService.urlRejectKonfirmasi, this.items[i])
                    .map((res: Response) => res.json())
                    .subscribe(
                        data => {
                            if (data.status) {
                                swal({
                                    type: 'success',
                                    title: 'Berhasil',
                                    html: 'Permohonan ditolak. Alasan anda: ' + text.value
                                });
                                this.toastr.success('Konfirmasi berhasil dilakukan', 'Berhasil');   
                                setTimeout(() => {
                                    location.reload();
                                }, 3000); 
                            } else {
                                swal({
                                    type: 'error',
                                    title: 'Gagal',
                                    html: 'Konfirmasi gagal dilakukan, silahkan coba lagi.'
                                });
                                this.toastr.error('Konfirmasi gagal dilakukan, silahkan coba lagi.', 'Error');
                           
                                this.ngOnInit();
                            }
                        },
                        err => console.log(err)
                    );
            } 
        }).catch(e => {
            this.toastr.error(e, 'Error');
        });
    }

    private showNoConn() {
        this.toastr.warning('Error Connecting to Server', 'Error');
    }

    private getConnection() {
        this.noConn = 0;
        this.subscription = this.authHttp.get(this.dataService.urlTest)
          .map((res: Response) => res.json())
          .subscribe(data => {
            this.status = data['status'];
          });
        setTimeout(() => {
          if (!this.status) {
            this.status = 0;
            this.noConn = 1;
            this.showNoConn();
          }
        }, 100000);
    }

    private getDataAdmin() {
        this.subscription = this.authHttp.get(this.dataService.urlGetDataAdmin)
            .map((res: Response) => res.json())
            .subscribe(data => {
                if (data.status) {
                    this.items = data.data;
                    console.log(this.items);
                } else {
                    if (data.code === 404) {
                        console.log('Tidak ada mahasiswa yang mengajukan');
                    } else {
                        this.toastr.error(data.message, 'Error');
                    }
                }
            });
    }

    private getDataStatus() {
        this.subscription = this.authHttp.get(this.dataService.urlGetStatus)
            .map((res: Response) => res.json())
            .subscribe(data => {
                if (data.status) {
                    console.log(data.data);
                    this.konfirmasiKetiga = data.data[3];
                    this.konfirmasiKeempat = data.data[4];
                } else {
                    this.konfirmasiKetiga = false;
                    this.konfirmasiKeempat = false;
                    console.log('Gagal mendapatkan status');
                }
                console.log(this.konfirmasiKeempat, this.konfirmasiKetiga);
            }, err => console.log(err));
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}