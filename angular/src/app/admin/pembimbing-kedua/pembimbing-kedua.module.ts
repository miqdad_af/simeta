import { NgModule } from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { routing } from './pembimbing-kedua.routing';
import { AlertModule } from 'ng2-bootstrap/alert';
import { PembimbingKedua } from './pembimbing-kedua.component';
import { DataService } from '../../data/data.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgaModule,
        routing,
        AlertModule.forRoot(),
    ],
    declarations: [ PembimbingKedua ],
    providers: [ DataService ],
})
export default class PembimbingKeduaModule { }
