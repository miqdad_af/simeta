import { Routes, RouterModule }  from '@angular/router';

import { KonfirmasiSeminarComponent } from './konfirmasi-seminar.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: KonfirmasiSeminarComponent,
    children: [
    ]
  }
];

export const routing = RouterModule.forChild(routes);
