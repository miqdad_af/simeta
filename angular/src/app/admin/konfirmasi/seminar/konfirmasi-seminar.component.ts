// SIMETA v2
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Response } from '@angular/http';
import { ISubscription } from 'rxjs/Subscription';
import { ToastrService } from 'toastr-ng2';
import { DataService } from '../../../data/data.service';
import { AuthHttp } from 'angular2-jwt';
import { TypeaheadMatch } from 'ng2-bootstrap/typeahead';
import swal from 'sweetalert2';

@Component({
    selector: 'selector-name',
    encapsulation: ViewEncapsulation.None,
    template: require('./konfirmasi-seminar.html'),
    styles: [require('./konfirmasi-seminar.scss')]
})

export class KonfirmasiSeminarComponent implements OnInit, OnDestroy {
    // cek koneksi
    public noConn;
    public status;

    // Typeahead purposes
    public selected = '';
    public pengajuan;
    public click = false;
    public mhsDetail;
    public statusTempat;
    public statusForm;
    public statusPembimbing1;
    public statusPembimbing2;
    public statusPenguji1;
    public statusPenguji2;
    public statusAdmin = 0;

    // Form purposes
    public tempat: string;
    public urlBeritaAcaraPdf;
    public urlAbsenPdf;
    
    private subscription: ISubscription;

    constructor(
        private authHttp: AuthHttp,
        private data: DataService,
        private toastr: ToastrService
    ) { }

    ngOnInit() {
        this.getConnection();
        this.getMahasiswa();
    }

    public getConnection() {
        this.noConn = 0;
        this.subscription = this.authHttp.get(this.data.urlTest)
          .map(res => res.json())
          .subscribe(data => {
            this.status = data['status'];
          });
        setTimeout(() => {
          if (!this.status) {
            this.status = 0;
            this.noConn = 1;
            this.showNoConn();
          }
        }, 5000);
    }

    public typeaheadOnSelect(e: TypeaheadMatch) {
        let creds = JSON.stringify({nama: e.value});
        this.subscription = this.authHttp.post(this.data.urlGetMandiri, creds)
            .map((res: Response) => res.json())
            .subscribe(data => {
                if (data.status) {
                    this.click = true;
                    this.mhsDetail = data.data[0];
                    this.tempat = data.data[0].tempat;
                    this.urlBeritaAcaraPdf = this.data.urlBeritaAcaraMandiriPDF + `/${this.mhsDetail.nim}` + `/${localStorage.getItem('id_token')}`;
                    this.urlAbsenPdf = this.data.urlAbsenMandiriPDF + `/${this.mhsDetail.nim}` + `/${localStorage.getItem('id_token')}`;
                    this.getStatus(this.mhsDetail.nim);
                } else {
                    this.toastr.error('Gagal mendapatkan data pengajuan', 'Error');
                }
            });
    }

    public getMahasiswa() {
        this.subscription = this.authHttp.get(this.data.urlMahasiswaAll)
            .map((res: Response) => res.json())
            .subscribe( data => {
                this.pengajuan = data;
            });
    }

    public getStatus(nim) {
        this.subscription = this.authHttp.post(this.data.urlGetStatusMandiri, JSON.stringify({nim}))
            .map((res: Response) => res.json())
            .subscribe( data => {
                if (data.status) {
                    this.statusTempat = data.data[0].status_tempat;
                    this.statusForm = data.data[0].status_formulir;
                    this.statusAdmin = data.data[0].status_admin;
                    this.statusPembimbing1 = data.data[0].konfirmasi_pembimbing1;
                    this.statusPembimbing2 = data.data[0].konfirmasi_pembimbing2;
                    this.statusPenguji1 = data.data[0].konfirmasi_penguji1;
                    this.statusPenguji2 = data.data[0].konfirmasi_penguji2;
                } else {
                    this.toastr.error('Gagal mendapatkan data status', 'Error');
                }
            })
    }
    
    public refresh() {
        this.ngOnInit();
    }

    onSubmit(nim, nama) {
        console.log(this.tempat);
        this.status = false;
        this.subscription = this.authHttp.post(this.data.urlSetKonfirmasiAdminMandiri, JSON.stringify({nim, nama, tempat: this.tempat}))
            .map((res: Response) => res.json())
            .subscribe(
                data => {
                    if (data.status) {
                        this.toastr.success('Berhasil memperbaharui status seminar mahasiswa', 'Success');
                        this.statusTempat = 1;
                        this.statusForm = 2;
                    } 
                    else {
                        this.toastr.error('Gagal memperbaharui status semimar mahasiswa', 'Error');
                    }
                    this.statusAdmin = 1;
                    this.ngOnInit();
                },
                err => console.log(err)
            );
    }

    onDecline(nim) {
        // this.status = false;
        swal({
            title: 'Anda yakin?',
            text: 'Berikan alasan sabagai konfirmasi bahwa permohonan ini ditolak.',
            type: 'warning',
            input: 'text',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            showLoaderOnConfirm: true,
            confirmButtonText: 'Ditolak',
            preConfirm: (text) => {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        if (text === '') {
                            reject('Anda harus memberikan alasan.');
                        } else {
                            resolve();
                        }
                    }, 2000);
                });
            }
        }).then((text) => {
            if (text.value) {
                this.subscription = this.authHttp.post(this.data.urlDeleteAdminMandiri, JSON.stringify({nim, alasan: text.value}))
                    .map((res: Response) => res.json())
                    .subscribe(
                        data => {
                            if (data.status) {
                                this.toastr.success('Berhasil', 'Success');
                                swal({
                                    type: 'success',
                                    title: 'Berhasil',
                                    html: 'Permohonan ditolak. Alasan anda: ' + text.value
                                });
                            } else {
                                swal({
                                    type: 'error',
                                    title: 'Gagal',
                                    html: 'Gagal memperbaharui status'
                                });
                                this.toastr.error('Gagal memperbaharui status', 'Error');
                                
                            }
                            setTimeout(() => {
                                location.reload();
                            }, 3000);
                        }, err => console.log(err)
                    );
            } 
        }).catch(e => {
            this.toastr.error(e, 'Error');
        });
    }

    public showNoConn() {
        this.toastr.warning('Error Connecting to Server', 'Error');
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}