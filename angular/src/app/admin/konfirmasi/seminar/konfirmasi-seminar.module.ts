import { NgModule } from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TypeaheadModule } from 'ng2-bootstrap/typeahead';
import { NgaModule } from '../../../theme/nga.module';
import { KonfirmasiSeminarComponent } from './konfirmasi-seminar.component';
import { DataService } from '../../../data/data.service';
import { routing } from './konfirmasi-seminar.routing';

@NgModule({
  imports: [
    CommonModule,
    NgaModule,
    routing,
    TypeaheadModule.forRoot(),
    FormsModule
  ],
  declarations: [
    KonfirmasiSeminarComponent
  ],
  providers: [
    DataService
  ]
})

export default class KonfirmasiSeminarModule {}
