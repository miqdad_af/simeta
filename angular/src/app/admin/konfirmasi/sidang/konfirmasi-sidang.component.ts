// SIMETA v2
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Response } from '@angular/http';
import { ISubscription } from 'rxjs/Subscription';
import { ToastrService } from 'toastr-ng2';
import { DataService } from '../../../data/data.service';
import { AuthHttp } from 'angular2-jwt';
import { TypeaheadMatch } from 'ng2-bootstrap/typeahead';
import swal from 'sweetalert2';

@Component({
    selector: 'selector-name',
    encapsulation: ViewEncapsulation.None,
    template: require('./konfirmasi-sidang.component.html'),
    styles: [require('./konfirmasi-sidang.component.scss')]
})

export class KonfirmasiSidangComponent implements OnInit, OnDestroy {

    // cek koneksi
    public noConn;
    public status;

    // Form purposes
    public tempat: string;

    // Typeahead purposes
    public selected = '';
    public pengajuan;
    public click = false;
    public mhsDetail;
    public statusTempat;
    public statusForm;
    public statusPembimbing1;
    public statusPembimbing2;
    public statusPenguji1;
    public statusPenguji2;
    public statusAdmin = 0;
    
    public urlPenilaianPdf: string;
    public urlBeritaAcaraPdf: string;
    public urlPerbaikanPdf: string;

    private subscription: ISubscription;
    constructor(
        private toastr: ToastrService,
        private authHttp: AuthHttp,
        private data: DataService
    ) {

    }

    ngOnInit() {
        this.getConnection();
        this.getMahasiswa();
    }

    public getMahasiswa() {
        this.subscription = this.authHttp.get(this.data.urlMahasiswaAll)
            .map((res: Response) => res.json())
            .subscribe( data => {
                this.pengajuan = data;
            });
    }

    public typeaheadOnSelect(e: TypeaheadMatch) {
        let creds = JSON.stringify({nama: e.value});
        this.subscription = this.authHttp.post(this.data.urlGetSidangAdm, creds)
            .map((res: Response) => res.json())
            .subscribe(data => {
                if (data.status) {
                    this.click = true;
                    this.mhsDetail = data.data[0];
                    this.tempat = data.data[0].tempat;

                    this.urlPenilaianPdf= this.data.urlPenialainSidangPdf + `${this.mhsDetail.nim}/${localStorage.getItem('id_token')}`;  
                    this.urlBeritaAcaraPdf = this.data.urlBeritaAcaraSidangPdf + `${this.mhsDetail.nim}/${localStorage.getItem('id_token')}`;
                    this.urlPerbaikanPdf = this.data.urlPerbaikanSidangPdf + `${this.mhsDetail.nim}/${localStorage.getItem('id_token')}`;

                    this.getStatus(this.mhsDetail.nim);
                } else {
                    this.toastr.error('Gagal mendapatkan data pengajuan', 'Error');
                }
            });
    }


    public getStatus(nim) {
        this.subscription = this.authHttp.post(this.data.urlGetStatusSidang, JSON.stringify({nim}))
            .map((res: Response) => res.json())
            .subscribe( data => {
                if (data.status) {
                    console.log(data.data);
                    this.statusTempat = data.data[0].status_tempat;
                    this.statusForm = data.data[0].status_formulir;
                    this.statusAdmin = data.data[0].status_admin;
                    this.statusPembimbing1 = data.data[0].konfirmasi_pembimbing1;
                    this.statusPembimbing2 = data.data[0].konfirmasi_pembimbing2;
                    this.statusPenguji1 = data.data[0].konfirmasi_penguji1;
                    this.statusPenguji2 = data.data[0].konfirmasi_penguji2;
                    console.log(this.statusPembimbing1, this.statusPembimbing2, this.statusPenguji1, this.statusPenguji2, this.statusAdmin);
                } else {
                    this.toastr.error('Gagal mendapatkan data status', 'Error');
                }
            })
    }
    

    public getConnection() {
        this.noConn = 0;
    
        this.subscription = this.authHttp.get(this.data.urlTest)
          .map(res => res.json())
          .subscribe(data => {
            this.status = data['status'];
          });
    

        setTimeout(() => {
          if (!this.status) {
            this.status = 0;
            this.noConn = 1;
            this.showNoConn();
          }
        }, 5000);
    }
    
    public onSubmit(nim, nama) {
        this.status = false;
        this.subscription = this.authHttp.post(this.data.urlSetKonfirmasiAdminSidang, JSON.stringify({nim, nama, tempat: this.tempat}))
        .map((res: Response) => res.json())
        .subscribe(
            data => {
                if (data.status) {
                    this.toastr.success('Berhasil memperbaharui status seminar mahasiswa', 'Success')
                } 
                else {
                    this.toastr.error('Gagal memperbaharui status semimar mahasiswa', 'Error');
                }
                this.ngOnInit();
                this.status = true;
                this.statusAdmin = 1;
                this.statusTempat = 1;
            },
            err => console.log(err)
        );
    }

    onDecline(nim) {
        // this.status = false;
        console.log(nim)
        swal({
            title: 'Anda yakin?',
            text: 'Berikan alasan sabagai konfirmasi bahwa permohonan ini ditolak.',
            type: 'warning',
            input: 'text',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            showLoaderOnConfirm: true,
            confirmButtonText: 'Ditolak',
            preConfirm: (text) => {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        if (text === '') {
                            reject('Anda harus memberikan alasan.');
                        } else {
                            resolve();
                        }
                    }, 2000);
                });
            }
        }).then((text) => {
            if (text.value) {
                this.subscription = this.authHttp.post(this.data.urlDeleteSidang, JSON.stringify({nim, alasan: text.value}))
                    .map((res: Response) => res.json())
                    .subscribe(
                        data => {
                            if (data.status) {
                                this.toastr.success('Berhasil', 'Success');
                                swal({
                                    type: 'success',
                                    title: 'Berhasil',
                                    html: 'Permohonan ditolak. Alasan anda: ' + text.value
                                });
                            } else {
                                swal({
                                    type: 'error',
                                    title: 'Gagal',
                                    html: 'Gagal memperbaharui status'
                                });
                                this.toastr.error('Gagal memperbaharui status', 'Error');
                                
                            }
                            setTimeout(() => {
                                location.reload();
                            }, 3000);
                        }, err => console.log(err)
                    );
            } 
        }).catch(e => {
            this.toastr.error(e, 'Error');
        });
    }

    public showNoConn() {
        this.toastr.warning('Error Connecting to Server', 'Error');
    }

    public refresh() {
        this.ngOnInit();
    }
    
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}