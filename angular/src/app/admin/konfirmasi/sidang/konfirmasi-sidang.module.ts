import { NgModule } from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TypeaheadModule } from 'ng2-bootstrap/typeahead';
import { NgaModule } from '../../../theme/nga.module';
import { KonfirmasiSidangComponent } from './konfirmasi-sidang.component';
import { DataService } from '../../../data/data.service';
import { routing } from './konfirmasi-sidang.routing';

@NgModule({
  imports: [
    CommonModule,
    NgaModule,
    routing,
    TypeaheadModule.forRoot(),
    FormsModule
  ],
  declarations: [
    KonfirmasiSidangComponent
  ],
  providers: [
    DataService
  ]
})

export default class KonfirmasiSidangModule {}
