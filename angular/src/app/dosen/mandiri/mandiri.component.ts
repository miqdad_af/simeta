// SIMETA v2
import { Component, ViewEncapsulation, OnInit, OnDestroy } from "@angular/core";
import { Response } from '@angular/http';
import { ISubscription } from "rxjs/Subscription";
import { AuthHttp } from "angular2-jwt";

import { ToastrService } from "toastr-ng2";
import { DataService } from "../../data/data.service";
import swal from 'sweetalert2';

@Component({
  selector: "mandiri",
  encapsulation: ViewEncapsulation.None,
  providers: [DataService],
  styles: [require("./mandiri.component.scss")],
  template: require("./mandiri.component.html")
})
export class MandiriComponent implements OnInit, OnDestroy {
  // Check con
  public status;
  public noConn;

  public mahasiswa: Array<any> = [];
  private subscription: ISubscription;
  
  constructor(
    private authHttp: AuthHttp,
    private toaster: ToastrService,
    private data: DataService
  ) {}


  ngOnInit() {
    this.getMahsiswa();
    this.getConnection();
    this.getStatus();
  }

  public getConnection() {
    this.noConn = 0;

    this.subscription = this.authHttp.get(this.data.urlTest)
      .map(res => res.json())
      .subscribe(data => {
        this.status = data['status'];
      })

    setTimeout(() => {
      if (!this.status) {
        this.status = 0;
        this.noConn = 1;
        this.showNoConn();
      }
    }, 5000)
  }

  private getMahsiswa() {
    this.subscription = this.authHttp.get(this.data.urlGetMandiriMhs)
      .map((res: Response) => res.json())
      .subscribe(
        data => {
          if (!data.status) {
            this.toaster.error('Tidak ada data mahasiswa', 'Error');
          } else {
            this.mahasiswa = data.data;
            console.log(this.mahasiswa);
          }
        },
        err => console.log(err)
      )
  }

  private getStatus() {
    this.subscription = this.authHttp.post(this.data.urlGetStatusMandiri, JSON.stringify({nim: null}))
      .map((res: Response) => res.json())
      .subscribe(
        data => {
          console.log(data);
        }
      )
  }

  public onSubmit(nim, statusDosen) {
    this.status = false;
    let url;
    if (statusDosen === 'Dosen pembimbing ketua') {
      url = this.data.urlSetKonfirmasiPertamaMandiri;
    } else {
      url = this.data.urlSetKonfirmasiMandiri;
    }
    console.log(nim, statusDosen);
    this.subscription = this.authHttp.post(url, JSON.stringify({nim, status_dosen: statusDosen}))
      .map((res: Response) => res.json())
      .subscribe(
        data => {
          if (data.status) {
            this.toaster.success('Berhasil memperbaharui status permohonan', 'Success');
          } else {
            this.toaster.error(data.message, 'Error');
          }
          this.ngOnInit();
          this.status = true;
        },
        err => console.log(err)
      )
  }

  public onDecline(nim, statusDosen) {
    console.log('nim: ', nim, 'status dosen: ', statusDosen);
    let url;
    if (statusDosen === 'Dosen pembimbing ketua') {
      url = this.data.urlDeleteMandiriPertama;
    } else {
      url = this.data.urlDeleteMandiri;
    }
    swal({
      title: 'Anda yakin?',
      text: 'Berikan alasan sabagai konfirmasi bahwa permohonan ini ditolak.',
      type: 'warning',
      input: 'text',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      showLoaderOnConfirm: true,
      confirmButtonText: 'Ditolak',
      preConfirm: (text) => {
          return new Promise(function (resolve, reject) {
              setTimeout(function () {
                  if (text === '') {
                      reject('Anda harus memberikan alasan.');
                  } else {
                      resolve();
                  }
              }, 2000);
          });
      }
    }).then((text) => {
        if (text.value) {
          let creds = {
            nim,
            type: 'konfirmasi',
            statusDosen,
            alasan: text.value
          };
          this.subscription = this.authHttp.post(url, JSON.stringify(creds))
              .map((res: Response) => res.json())
              .subscribe(
                data => {
                    if (data.status) {
                      this.toaster.success('Berhasil', 'Success');
                      swal({
                        type: 'success',
                        title: 'Berhasil',
                        html: 'Permohonan ditolak. Alasan anda: ' + text.value
                      });
                      setTimeout(() => {
                          location.reload();
                      }, 3000);
                    } else {
                      this.toaster.error(data.message, 'Error');
                      swal({
                        type: 'error',
                        title: 'Gagal',
                        html: data.message
                      })
                    }
                }, err => console.log(err)
              );
        } 
    }).catch(e => {
        this.toaster.error(e, 'Error');
    });
  }

  public refresh() {
    this.ngOnInit();
  }

  public showNoConn() {
    this.toaster.warning("Error Connecting to Server", 'Error');
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

