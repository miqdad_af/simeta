import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { MandiriComponent } from './mandiri.component';
import { routing }       from './mandiri.routing';
import { AlertModule } from 'ng2-bootstrap/alert';
import { DataService } from '../../data/data.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    AlertModule.forRoot(),
    routing
  ],
  declarations: [
    MandiriComponent
  ],
  providers: [
    DataService
  ]
})
export default class MandiriModule {}
