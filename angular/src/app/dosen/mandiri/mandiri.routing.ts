import { Routes, RouterModule }  from '@angular/router';

import { MandiriComponent } from './mandiri.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: MandiriComponent,
    children: [
    ]
  }
];

export const routing = RouterModule.forChild(routes);