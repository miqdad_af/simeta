import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

import { ISubscription } from 'rxjs/Subscription';
import { ToastrService } from 'toastr-ng2';
import { DataService } from '../../data/data.service';
import swal from 'sweetalert2';
let Chart = require('chart.js');

@Component({
  selector: 'konfirmasi-sidang',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./konfirmasi-sidang.component.scss')],
  template: require('./konfirmasi-sidang.component.html')
})
export class KonfirmasiSidangComponent implements OnInit, OnDestroy{
  // Check con
  public status;
  public noConn;

  // Form puposes
  public mahasiswa: Array<any> = [];
  
  private subscription: ISubscription;

  constructor(
    private authHttp: AuthHttp,
    private data: DataService,
    private toastr: ToastrService
  ) {}
  
  ngOnInit() {
    this.getMahasiswaSidang();
    this.getConnection();
  }

  public getMahasiswaSidang() {
    this.subscription = this.authHttp.get(this.data.urlGetSidangMhs)
      .map((res: Response) => res.json())
      .subscribe(
        data => {
          if (data.status) {
            this.mahasiswa = data.data;
            console.log(this.mahasiswa);
          } else {
            this.toastr.error('Gagal mendapatkan data mahasiswa', 'Error');
          }
        }
      );
  }

  public getConnection() {
    this.noConn = 0;

    this.subscription = this.authHttp.get(this.data.urlTest)
      .map(res => res.json())
      .subscribe(data => {
        this.status = data['status'];
      })

    setTimeout(() => {
      if (!this.status) {
        this.status = 0;
        this.noConn = 1;
        this.showNoConn();
      }
    }, 5000)
  }

  public showNoConn() {
    this.toastr.warning("Error Connecting to Server", 'Error');
  }

  public onSubmit(nim, statusDosen) {
    this.status = false;
    let url;
    console.log(statusDosen)
    if (statusDosen === 'Dosen pembimbing ketua') {
      url = this.data.urlSetKonfirmasiPertamaSidang;
    } else {
      url = this.data.urlSetKonfirmasiSidang;
    }
    // console.log(url);
    this.subscription = this.authHttp.post(url, JSON.stringify({nim, statusDosen}))
      .map((res: Response) => res.json())
      .subscribe(
        data => {
          if (data.status) {
            this.toastr.success('Berhasil memperbaharui status permohonan', 'Success');
          } else {
            this.toastr.error(data.message, 'Error');
          }
          this.ngOnInit();
          this.status = true;
        },
        err => console.log(err)
      )
  }

  public onDecline(nim, statusDosen) {
    console.log('nim: ', nim, 'status dosen: ', statusDosen);
    let url;
    if (statusDosen === 'Dosen pembimbing ketua') {
      url = this.data.urlDeleteSidangPertama;
    } else {
      url = this.data.urlDeleteSidang;
    }
    console.log(url);
    swal({
      title: 'Anda yakin?',
      text: 'Berikan alasan sabagai konfirmasi bahwa permohonan ini ditolak.',
      type: 'warning',
      input: 'text',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      showLoaderOnConfirm: true,
      confirmButtonText: 'Ditolak',
      preConfirm: (text) => {
          return new Promise(function (resolve, reject) {
              setTimeout(function () {
                  if (text === '') {
                      reject('Anda harus memberikan alasan.');
                  } else {
                      resolve();
                  }
              }, 2000);
          });
      }
    }).then((text) => {
        if (text.value) {
          let creds = {
            nim,
            type: 'konfirmasi',
            statusDosen,
            alasan: text.value
          };
          this.subscription = this.authHttp.post(url, JSON.stringify(creds))
              .map((res: Response) => res.json())
              .subscribe(
                data => {
                    if (data.status) {
                      this.toastr.success('Berhasil', 'Success');
                      swal({
                        type: 'success',
                        title: 'Berhasil',
                        html: 'Permohonan ditolak. Alasan anda: ' + text.value
                      });
                      setTimeout(() => {
                          location.reload();
                      }, 3000);
                    } else {
                      this.toastr.error(data.message, 'Error');
                      swal({
                        type: 'error',
                        title: 'Gagal',
                        html: data.message
                      })
                    }
                }, err => console.log(err)
              );
        } 
    }).catch(e => {
        this.toastr.error(e, 'Error');
    });
  }

  public refresh() {
    this.ngOnInit();
  }
  
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
