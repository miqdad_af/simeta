import { Routes, RouterModule }  from '@angular/router';

import { KonfirmasiSidangComponent } from './konfirmasi-sidang.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: KonfirmasiSidangComponent,
    children: [
    ]
  }
];

export const routing = RouterModule.forChild(routes);