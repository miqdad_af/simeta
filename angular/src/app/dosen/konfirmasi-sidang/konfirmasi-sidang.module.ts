import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { KonfirmasiSidangComponent } from './konfirmasi-sidang.component';
import { routing }       from './konfirmasi-sidang.routing';
import { AlertModule } from 'ng2-bootstrap/alert';
import { DataService } from '../../data/data.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    AlertModule.forRoot(),
    routing
  ],
  declarations: [
    KonfirmasiSidangComponent
  ],
  providers: [
    DataService
  ]
})
export default class KonfirmasiSidangModule {}
