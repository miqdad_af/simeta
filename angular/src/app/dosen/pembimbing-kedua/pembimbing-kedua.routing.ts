import { Routes, RouterModule }  from '@angular/router';

import { PembimbingKedua } from './pembimbing-kedua.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: PembimbingKedua,
    children: [
    ]
  }
];

export const routing = RouterModule.forChild(routes);