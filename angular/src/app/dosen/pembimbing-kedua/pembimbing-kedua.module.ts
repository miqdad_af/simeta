import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PembimbingKedua } from './pembimbing-kedua.component';
import { NgaModule } from '../../theme/nga.module';
import { AlertModule } from 'ng2-bootstrap/alert';
import { routing } from './pembimbing-kedua.routing';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgaModule,
        AlertModule,
        routing
    ],
    declarations: [
        PembimbingKedua
    ],
    providers: []
})

export default class PembimbingKeduaModule {}