// SIMETA v2

import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Response } from '@angular/http';
import { ISubscription } from "rxjs/Subscription";

import swal from 'sweetalert2';
import { ToastrService } from 'toastr-ng2';
import { DataService } from '../../data/data.service';

@Component({
    selector: 'pembimbing-kedua',
    encapsulation: ViewEncapsulation.None,
    providers: [DataService],
    styles: [require('./pembimbing-kedua.scss')],
    template: require('./pembimbing-kedua.html')
})

export class PembimbingKedua implements OnInit, OnDestroy {
    // Cek koneksi
    public noConn;
    public status;

    // Data pembimbing kedua
    public itemsPertama: Array<any>;
    public itemsKedua: Array<any>;
    public itemsKepalaBagian: Array<any>;
    public konfirmasiSatu;
    public konfirmasiDua;

    // Request
    public alasan;

    private subscription: ISubscription;

    constructor(
        private authHttp: AuthHttp,
        private dataService: DataService,
        private toastr: ToastrService
    ) {}
    
    ngOnInit() {
        this.getConnection();
        this.getDataStatus();
        this.getDataPertama();
        this.getDataKedua();
        this.getDataKepalaBagian();
    }

    public showNoConn() {
        this.toastr.warning('Error Connecting to Server', 'Error');
    }

    public getConnection() {
        this.noConn = 0;
        this.subscription = this.authHttp.get(this.dataService.urlTest)
          .map((res: Response) => res.json())
          .subscribe(data => {
            this.status = data['status'];
          });
        setTimeout(() => {
          if (!this.status) {
            this.status = 0;
            this.noConn = 1;
            this.showNoConn();
          }
        }, 100000);
    }

    public getDataPertama() {
        this.subscription = this.authHttp.get(this.dataService.urlGetDataPertama)
            .map((res: Response) => res.json())
            .subscribe(data => {
                this.itemsPertama = data.data;
                if (this.itemsPertama) {
                    this.itemsPertama.map(item => item.whichData = 1);
                }
                console.log(this.itemsPertama);
            });    
    }

    public getDataKedua() {
        this.subscription = this.authHttp.get(this.dataService.urlGetDataKedua)
            .map((res: Response) => res.json())
            .subscribe(data => {
                this.itemsKedua = data.data;
                if (this.itemsKedua) {
                    this.itemsKedua.map(item => item.whichData = 2);
                }
                console.log(this.itemsKedua);
            });
    }

    public getDataKepalaBagian() {   
        console.log('masuk data admin');
        this.subscription = this.authHttp.get(this.dataService.urlGetDataKepalaBagian)
            .map((res: Response) => res.json())
            .subscribe( data => {
                if (data.status) {
                    this.itemsKepalaBagian = data.data;
                    if (this.itemsKepalaBagian) {
                        this.itemsKepalaBagian.map(item => item.whichData = 3);
                    }
                    console.log(this.itemsKepalaBagian);
                }
            });
    }

    public onSubmitPertama(i: number) {
        this.status = false;
        this.subscription = this.authHttp.post(this.dataService.urlSetKonfirmasiPertama, this.itemsPertama[i])
            .map((res: Response) => res.json())
            .subscribe(data => {
                if (data.status) {
            
                    this.toastr.success('Konfirmasi berhasil dilakukan', 'Berhasil');
                
                } else {
              
                    this.toastr.error('Konfirmasi gagal dilakukan, silahkan coba lagi.', 'Error');
                }
                this.status = true;
                this.ngOnInit();
            }, err => console.log(err));
        
    }

    public onSubmitKedua(i: number) {
        this.status = false;
        this.subscription = this.authHttp.post(this.dataService.urlSetKonfirmasiKedua, this.itemsKedua[i])
            .map((res: Response) => res.json())
            .subscribe(data => {
                if (data.status) {
             
                    this.toastr.success('Konfirmasi berhasil dilakukan', 'Berhasil');
                
                } else {
                 
                    this.toastr.error('Konfirmasi gagal dilakukan, silahkan coba lagi.', 'Error');
                }
                this.status = true;
                this.ngOnInit();
            }, err => console.log(err));
    }

    public onSubmitKepalaBagian(i: number) {
        console.log(this.itemsKepalaBagian)
        this.status = false;
        this.subscription = this.authHttp.post(this.dataService.urlSetKonfirmasiKepalaBagian, this.itemsKepalaBagian[i])
            .map((res: Response) => res.json())
            .subscribe(data => {
                if (data.status) {
                    this.toastr.success('Konfirmasi berhasil dilakukan', 'Berhasil');
                } else {
                    this.toastr.error('Konfirmasi gagal dilakukan, silahkan coba lagi.', data.message);
                }
                this.status = true;
                this.ngOnInit();
            })
    }

    public onDeletePertama(i: number) {
        swal({
            title: 'Anda yakin?',
            text: 'Berikan alasan sabagai konfirmasi bahwa permohonan ini ditolak.',
            type: 'warning',
            input: 'text',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            showLoaderOnConfirm: true,
            confirmButtonText: 'Ditolak',
            preConfirm: (text) => {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        if (text === '') {
                            reject('Anda harus memberikan alasan.');
                        } else {
                            resolve();
                        }
                    }, 2000);
                });
            }
        }).then((text) => {
            if (text.value) {
                this.itemsPertama[i].alasan = text.value;
                this.subscription = this.authHttp.post(this.dataService.urlRejectKonfirmasi, this.itemsPertama[i])
                    .map((res: Response) => res.json())
                    .subscribe(
                        data => {
                            if (data.status) {
                                this.toastr.success('Konfirmasi berhasil dilakukan', 'Berhasil'); 
                                swal({
                                    type: 'success',
                                    title: 'Berhasil',
                                    html: 'Permohonan ditolak. Alasan anda: ' + text.value
                                });  
                                setTimeout(() => {
                                    location.reload();
                                }, 3000);   
                            } else {
                                this.toastr.error('Konfirmasi gagal dilakukan, silahkan coba lagi.', 'Error');
                                swal({
                                    type: 'error',
                                    title: 'Gagal',
                                    html: 'Konfirmasi gagal dilakukan, silahkan coba lagi.'
                                });
                            }
                        },
                        err => console.log(err)
                    );
            } 
        }).catch(e => {
            swal({
                type: 'error',
                title: 'Gagal',
                html: e
            });
            this.toastr.error(e, 'Error');
        });
    }

    public onDeleteKedua(i: number) {
        swal({
            title: 'Anda yakin?',
            text: 'Berikan alasan sabagai konfirmasi bahwa permohonan ini ditolak.',
            type: 'warning',
            input: 'text',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            showLoaderOnConfirm: true,
            confirmButtonText: 'Ditolak',
            preConfirm: (text) => {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        if (text === '') {
                            reject('Anda harus memberikan alasan.');
                        } else {
                            resolve();
                        }
                    }, 2000);
                });
            }
        }).then((text) => {
            if (text.value) {
                this.itemsKedua[i].alasan = text.value;
                this.subscription = this.authHttp.post(this.dataService.urlRejectKonfirmasi, this.itemsKedua[i])
                    .map((res: Response) => res.json())
                    .subscribe(
                        data => {
                            if (data.status) {
                                swal({
                                    type: 'success',
                                    title: 'Berhasil',
                                    html: 'Permohonan ditolak. Alasan anda: ' + text.value
                                });
                                this.toastr.success('Konfirmasi berhasil dilakukan', 'Berhasil');
                                setTimeout(() => {
                                    location.reload();
                                }, 3000);   
                            } else {
                                swal({
                                    type: 'error',
                                    title: 'Gagal',
                                    html: 'Konfirmasi gagal dilakukan, silahkan coba lagi.'
                                });
                                this.toastr.error('Konfirmasi gagal dilakukan, silahkan coba lagi.', 'Error');
                            }
                        },
                        err => console.log(err)
                    );
            } 
        }).catch(e => {
            swal({
                type: 'error',
                title: 'Gagal',
                html: e
            });
            this.toastr.error(e, 'Error');
        });
    }

    public onDeleteKepalaBagian(i: number) {
        console.log('masuk delete admin')
        swal({
            title: 'Anda yakin?',
            text: 'Berikan alasan sabagai konfirmasi bahwa permohonan ini ditolak.',
            type: 'warning',
            input: 'text',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            showLoaderOnConfirm: true,
            confirmButtonText: 'Ditolak',
            preConfirm: (text) => {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        if (text === '') {
                            reject('Anda harus memberikan alasan.');
                        } else {
                            resolve();
                        }
                    }, 2000);
                });
            }
        }).then((text) => {
            if (text.value) {
                this.itemsKepalaBagian[i].alasan = text.value;
                this.subscription = this.authHttp.post(this.dataService.urlRejectKonfirmasi, this.itemsKepalaBagian[i])
                    .map((res: Response) => res.json())
                    .subscribe(
                        data => {
                            if (data.status) {
                                this.toastr.success('Konfirmasi berhasil dilakukan', 'Berhasil');
                                swal({
                                    type: 'success',
                                    title: 'Berhasil',
                                    html: 'Permohonan ditolak. Alasan anda: ' + text.value
                                });   
                                setTimeout(() => {
                                    location.reload();
                                }, 3000);  
                            } else {
                                this.toastr.error('Konfirmasi gagal dilakukan, silahkan coba lagi.', 'Error');
                                swal({
                                    type: 'error',
                                    title: 'Gagal',
                                    html: 'Konfirmasi gagal dilakukan, silahkan coba lagi.'
                                });
                                this.ngOnInit();
                            }
                        },
                        err => console.log(err)
                    );
            } 
        }).catch(e => {
            swal({
                type: 'error',
                title: 'Gagal',
                html: e
            });
            this.toastr.error(e, 'Error');
        });
    }

    public getDataStatus() {
        this.subscription = this.authHttp.get(this.dataService.urlGetStatus)
            .map((res: Response) => res.json())
            .subscribe(
                data => {
                    console.log(data);
                    if (data.status) {
                        this.konfirmasiSatu = data.data[1];
                        this.konfirmasiDua = data.data[2];
                        
                    } else {
                        this.konfirmasiSatu = false;
                        this.konfirmasiDua = false;
                        console.log('gagal mendapatkan status');
                    }
                },  
                err => console.log(err)
            );
    }

    public refresh() {
        this.ngOnInit();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}