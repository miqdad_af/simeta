export const DOSEN_SIDEBAR_MENU = [
  {
    path: 'dosen',
    children: [
      {
        path: 'dashboard',
        data: {
          menu: {
            title: 'Dashboard',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'pembimbing-kedua',
        data: {
          menu: {
            title: 'Pembimbing Kedua',
            icon: 'ion-android-contacts',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'log',
        data: {
          menu: {
            title: 'Log Bimbingan',
            icon: 'ion-chatboxes',
            selected: false,
            expanded: false,
            order: 0
          }
        },
        
      },
      {
        path: 'kolokium',
        data: {
          menu: {
            title: 'Kolokium',
            icon: 'ion-document-text',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'praseminar',
        data: {
          menu: {
            title: 'Praseminar',
            icon: 'ion-code-working',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'seminar',
        data: {
          menu: {
            title: 'Seminar',
            icon: 'ion-person-stalker',
            selected: false,
            expanded: false,
            order: 200
          }
        },
        children: [
          {
            path: '',
            data: {
              menu: {
                title: 'Rekapitulasi Seminar'
              }
            }
          },
          {
            path: 'mandiri',
            data: {
              menu: {
                title: 'Seminar Mandiri'
              }
            }
          }
        ]
      },
      {
        path: 'sidang',
        data: {
          menu: {
            title: 'Sidang',
            icon: 'ion-ribbon-b',
            selected: false,
            expanded: false,
            order: 300
          }
        },
        children: [
          {
            path: '',
            data: {
              menu: {
                title: 'Rekapitulasi Sidang'
              }
            }
          },
          {
            path: 'konfirmasi',
            data: {
              menu: {
                title: 'Konfirmasi Sidang'
              }
            }
          }
        ]
      },
      {
        path: 'skl',
        data: {
          menu: {
            title: 'SKL',
            icon: 'ion-university',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      }
    ]
  },
];
