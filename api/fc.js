var _ = require('./simak');
var connection = require('./connection');

function test1(test) {
  return 1;
}

function Auth() {

  this.getDosen = function(nim, res) {
    connection.acquire(function(err, con) {
      con.query('SELECT * FROM dosen_mahasiswa_view WHERE nim = ?', [nim], function(err, result) {
        con.release();
        if(err) {
          throw err;
        }
        else {
          var dosen1 = result[0].dosen1;
          var dosen2 = result[0].dosen2;
          var penguji1 = result[0].penguji1;
          var penguji2 = result[0].penguji2;
          var data = {dosen1: dosen1, dosen2: dosen2, penguji1: penguji1, penguji2: penguji2};

        }
      })
    })
  };

  this.createSeminar = function(jenis,res, tanggal) {
    connection.acquire(function(err, con) {
      con.query('select * from ta_seminar where nim = ?', [_.decoded.nim], function(err, result) {
        if(err) {
          con.release();
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          if(result.length > 0) {
            var query = 'update ta_seminar set tanggal = ?, jenis_seminar = ?, konfirmasi = 1 where nim = ?';
            var data = [tanggal, jenis, _.decoded.nim];
            con.query(query, data, function(err, result1) {
              con.release();
              if (err) {
                console.log('mesuk error 1');
                res.json({status: 0, message: 'Update Gagal'});
                throw err;
              } else {
              };
            })
          }
          else {
            var query = 'insert into ta_seminar (nim, tanggal, jenis_seminar, konfirmasi, konfirmasi_pembimbing1, konfirmasi_pembimbing2, konfirmasi_penguji1, konfirmasi_penguji2) values (?, ?, ?, 1, 0, 0, 0,0)';
            var data = [_.decoded.nim, tanggal, jenis];
            con.query(query, data, function(err, result1) {
              con.release();
              if (err) {
                console.log('mesuk error 2');
                res.json({status: 0, message: 'Update Gagal'});
                throw err;
              } else {
              };
            })
          }
        }
      })
    })
  }

  this.checkAdminDosen = function() {
    if(_.decoded.role == 2 || _.decoded.role == 1 || _.decoded.role == 4) {
      return true;
    }
    else {
      return false;
    }
  };

  this.checkAdmin = function() {
    if(_.decoded.role == 1 || _.decoded.role == 4) {
      return true;
    }
    else {
      return false;
    }
  };

  this.countSeminar = function(data) {
    var send = {};
    var temp = new Date();
    var tahun_berjalan = temp.getFullYear() - 4;
    var tahun_awal = data[0].tahun_masuk;

    var tahun = [];
    for(tahun_awal; tahun_awal <= tahun_berjalan; tahun_awal++) {
      tahun.push(tahun_awal);
    }

    var rangkuman = {};
    var dataSeminar = {};
    for (let i = 0; i < tahun.length; i++) {
      let temp = 0;
      let Cmakalah = 0;

      let mandiri = 0;
      let micon = 0;
      let konferensi = 0;


      for (let j = 0; j < data.length; j++) {
        if (tahun[i] == data[j].tahun_masuk) {
          temp++;

          if (data[j].jenis_seminar != null) {
            Cmakalah++;
          }

          if(data[j].jenis_seminar == 1) {
            if(data[j].berkas_konferensi != null) {
              konferensi++;
            }
          }
          else if(data[j].jenis_seminar == 2) {
            if(data[j].berkas_micon != null) {
              micon++;
            }
          }
          else if(data[j].jenis_seminar == 3) {
            if(data[j].makalah_mandiri != null) {
              mandiri++;
            }
          }
        }
      }
      rangkuman[tahun[i]] = [Cmakalah, temp - Cmakalah];
      dataSeminar[tahun[i]] = [temp - Cmakalah, konferensi,micon, mandiri];
    }

    return {'tahun': tahun, 'data': rangkuman, 'jenis': dataSeminar};

  }

  this.countKolokiumPraseminar = function(data) {
    var send = {};
    var temp = new Date();
    var tahun_berjalan = temp.getFullYear() - 4;
    var tahun_awal = data[0].tahun_masuk;

    var tahun = [];
    for(tahun_awal; tahun_awal <= tahun_berjalan; tahun_awal++) {
      tahun.push(tahun_awal);
    }

    var rangkuman = {};
    for (let i = 0; i < tahun.length; i++) {
      let temp = 0;
      let Cmakalah = 0;
      for (let j = 0; j < data.length; j++) {
        if (tahun[i] == data[j].tahun_masuk) {
          temp++;

          if (data[j].makalah != null) {
            Cmakalah++;
          }
        }
      }
      rangkuman[tahun[i]] = [Cmakalah, temp - Cmakalah];
    }

    return {'tahun': tahun, 'data':rangkuman};
  }

  this.countSKL = function(data) {
    var send = {};
    var temp = new Date();
    var tahun_berjalan = temp.getFullYear() - 4;
    var tahun_awal = data[0].tahun_masuk;

    var tahun = [];
    for(tahun_awal; tahun_awal <= tahun_berjalan; tahun_awal++) {
      tahun.push(tahun_awal);
    }

    var rangkuman = {};
    for (let i = 0; i < tahun.length; i++) {
      let temp = 0;
      let Cmakalah = 0;
      for (let j = 0; j < data.length; j++) {
        if (tahun[i] == data[j].tahun_masuk) {
          temp++;

          if (data[j].berkas != null) {
            Cmakalah++;
          }
        }
      }
      rangkuman[tahun[i]] = [Cmakalah, temp - Cmakalah];
    }

    return {'tahun': tahun, 'data':rangkuman};
  }

  this.countSummary = function(data) {
    var send = {};
    var temp = new Date();
    var tahun_berjalan = temp.getFullYear() - 4;
    var tahun_awal = data[0].tahun_masuk;

    var tahun = [];
    for(tahun_awal; tahun_awal <= tahun_berjalan; tahun_awal++) {
      tahun.push(tahun_awal);
    }

    var rangkuman = {};
    var total = {};
    for (let i = 0; i < tahun.length; i++) {
      let temp = 0;
      let Cbelum = 0;
      let Cta = 0;
      let Ckolokium = 0;
      let Cpraseminar = 0;
      let Cseminar = 0;
      let Csidang = 0;
      let Cskl = 0;

      for (let j = 0; j < data.length; j++) {
        if (tahun[i] == data[j].tahun_masuk) {
          temp++;

          if (data[j].topik != null) {
            Cta++;
          }
          if (data[j].topik == null) {
            Cbelum++;
          }
          if (data[j].tanggal_kolokium != null) {
            Ckolokium++;
          }
          if (data[j].tanggal_praseminar != null) {
            Cpraseminar++;
          }
          if (data[j].tanggal_seminar != null) {
            Cseminar++;
          }
          if (data[j].tanggal_sidang != null) {
            Csidang++;
          }
          if (data[j].tanggal_skl != null) {
            Cskl++;
          }

        }
      }

      total[tahun[i]] = [temp];

      let skl = Cskl;
      let sidang = Csidang - Cskl;
      let seminar = Cseminar - Csidang;
      let praseminar = Cpraseminar - Cseminar;
      let kolokium = Ckolokium - Cpraseminar;
      let ta = Cta - Ckolokium;

      rangkuman[tahun[i]] = [Cbelum, ta, kolokium, praseminar, seminar, sidang, skl];
    }

    return {'tahun': tahun, 'total': total, 'data': rangkuman};
  }

  this.countSummaryMahasiswa = function(data) {
    var send = {};
    var temp = new Date();
    var tahun_berjalan = temp.getFullYear() - 4;
    var tahun_awal = data[0].tahun_masuk;

    var tahun = [];
    for(tahun_awal; tahun_awal <= tahun_berjalan; tahun_awal++) {
      tahun.push(tahun_awal);
    }

    var rangkuman = {};
    var total = {};
    for (let i = 0; i < tahun.length; i++) {
      let temp = 0;
      let Cta = 0;
      let Ckolokium = 0;
      let Cpraseminar = 0;
      let Cseminar = 0;
      let Csidang = 0;
      let Cskl = 0;

      for (let j = 0; j < data.length; j++) {
        if (tahun[i] == data[j].tahun_masuk) {
          temp++;

          if (data[j].topik != null) {
            Cta++;
          }
          if (data[j].makalah_kolokium != null) {
            Ckolokium++;
          }
          if (data[j].makalah_praseminar != null) {
            Cpraseminar++;
          }
          if (data[j].jenis_seminar != null) {
            Cseminar++;
          }
          if (data[j].makalah_sidang != null) {
            Csidang++;
          }
          if (data[j].berkas_skl != null) {
            Cskl++;
          }

        }
      }

      total[tahun[i]] = [temp];

      let skl = Cskl;
      let sidang = Csidang - Cskl;
      let seminar = Cseminar - Csidang;
      let praseminar = Cpraseminar - Cseminar;
      let kolokium = Ckolokium - Cpraseminar;
      let ta = Cta - Ckolokium;

      rangkuman[tahun[i]] = [ta, kolokium, praseminar, seminar, sidang, skl];
    }

    return {'tahun': tahun, 'total': total, 'data': rangkuman};
  }

};

module.exports = new Auth();
