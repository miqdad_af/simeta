var approve = require('./direct/approve');
var generate = require('./direct/generateJWT');
var pengajuanExcel = require('./generateExcel/pengajuan');
var summaryExcel = require('./generateExcel/summary');
var seminarExcel = require('./generateExcel/seminar-sidang');
var generatePDF = require('./generatePDF/demo');
var logPDF = require('./generatePDF/logPDF');

var undanganSeminar = require('./generatePDF/undanganSeminar');
var undanganSidang = require('./generatePDF/undanganSidang');

var testEmail = require('./direct/testMail');
var testSoap = require('./direct/soap');
var querystring = require('querystring');

module.exports = {
  configure: function(app) {

    app.get('/pdf/mandiri/berita-acara/:id/:token', function(req, res) {
      generatePDF.getBeritaAcaraMandiri(req.params.id, req.params.token, res);
    });

    app.get('/pdf/mandiri/absen/:id/:token', function(req, res) {
      generatePDF.getAbsenMandiri(req.params.id, req.params.token, res);
    })

    app.get('/undangan/seminar/:nim/:id', function(req, res) {
      undanganSeminar.seminarUndangan(req.params.nim, req.params.id, res);
    });

    app.get('/undangan/sidang/:nim/:id', function(req, res) {
      undanganSidang.sidangUndangan(req.params.nim, req.params.id, res);
    });

    app.get('/pdf/sidang/lembar-penilaian/:nim/:token', function(req, res) {
      generatePDF.getPenilaianSidang(req.params.nim, req.params.token, res);
    });

    app.get('/pdf/sidang/berita-acara/:nim/:token', function(req, res) {
      generatePDF.getBeritaAcaraSidang(req.params.nim, req.params.token, res);
    });

    app.get('/pdf/sidang/perbaikan/:nim/:token', function(req, res) {
      generatePDF.getPerbaikanSidang(req.params.nim, req.params.token, res);
    });

    app.get('/mail/test', function(req, res) {
      testEmail.test(res);
    });

    app.get('/test', function(req, res) {
      res.json({status: 1, message: 'Connection Established'});
    })

    app.get('/pdf/log/:id', function(req, res) {
      logPDF.logpdf(req.params.id, res);
    });

    app.get('/seminar/excel/:id', function(req, res) {
      seminarExcel.get(req.params.id, res);
    });

    app.get('/generate/pdf/:id', function(req, res) {
      generatePDF.getSeminar(req.params.id, res);
    });

    app.get('/sidang/pdf/:id', function(req, res) {
      generatePDF.getSiapSidang(req.params.id, res);
    });

    app.get('/keterangan/pdf/:id', function(req, res) {
      generatePDF.getKelengkapan(req.params.id, res);
    });

    // GENERATE EXCEL
    app.get('/excel/:id', function(req, res) {
      pengajuanExcel.get(req.params.id, res);
    });

    // GENERATE EXCEL
    app.get('/summary/all/:id', function(req, res) {
      summaryExcel.get(req.params.id, res);
    });

    // GENERATE EXCEL
    app.get('/summary/dosen/:id', function(req, res) {
      summaryExcel.getDosen(req.params.id, res);
    });

    app.get('/log/approve/:id/:token', function(req, res){
      approve.get(req.params.id, req.params.token, res);
    });

    app.get('/generate/token/', function(req, res) {
      generate.create(req, res);
    });

    // SIMETA v2
    // Konfirmasi untuk pengajuan pembimbing kedua
    app.get('/pembimbing-kedua/konfirmasi/pertama/set/:id/:token', function(req,res) {
      approve.setAcceptedPertama(req.params.id, req.params.token, res);
    });
    app.get('/pembimbing-kedua/konfirmasi/kedua/set/:id/:token', function(req, res) {
      approve.setAcceptedKedua(req.params.id, req.params.token, res);
    });
    app.get('/pembimbing-kedua/konfirmasi/delete/:id/:token', function(req,res) {
      console.log(req.params.token);
      approve.setRejected(req.params.id, req.params.token, res);
    });
    app.get('/pembimbing-kedua/konfirmasi/kepala-bagian/set/:id/:dosen/:token', function(req, res) {
      approve.setAcceptedKepalaBagian(req.params.id, req.params.dosen, req.params.token, res);
    });

    // Konfirmasi untuk pengajuan pelaksanaan seminar mandiri
    app.get('/seminar/mandiri/konfirmasi/pertama/:id/:token', function(req, res) {
      approve.setAcceptedPertamaMandiri(req.params.id, req.params.token, res);
    });
    app.get('/seminar/mandiri/delete/pertama/:id/:token', function(req, res) {
      approve.declinePertamaMandiri(req.params.id, req.params.token, res);
    });
    app.get('/seminar/mandiri/konfirmasi/:id/:email/:token', function(req, res) {
      const email = querystring.parse(req.params.email);
      console.log(email);
      approve.setAcceptedMandiri(req.params.id, req.params.token, email, res);
    });
    app.get('/seminar/mandiri/delete/:id/:email/:token', function(req, res) {
      const email = querystring.parse(req.params.email);
      approve.declineMandiri(req.params.id, req.params.token, email, res);
    });

    // Konfirmasi pelaksaan sidang
    app.get('/sidang/konfirmasi/pertama/:creds', function(req, res) {
      const creds = querystring.parse(req.params.creds);
      approve.setAcceptedPertamaSidang(creds.nim, creds.token, res);
    });
    app.get('/sidang/konfirmasi/pertama/delete/:creds', function(req, res) {
      const creds = querystring.parse(req.params.creds);
      approve.declinePertamaSidang(creds.nim, creds.token, res);
    });
    app.get('/sidang/konfirmasi/:creds', function(req, res) {
      const creds = querystring.parse(req.params.creds);
      approve.setAcceptedSidang(creds.nim, creds.token, creds.email, res);
    });
    app.get('/sidang/konfirmasi/delete/:creds', function(req, res) {
      const creds = querystring.parse(req.params.creds);
      approve.declineSidang(creds.nim, creds.token, creds.email, res);
    })
  }
};
