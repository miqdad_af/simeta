var connection = require('./connection');
var _ = require('./simak');
var fc = require('./fc');

var soap = require('soap');

var dataMahasiswa = [];
var url = 'https://iisws.ipb.ac.id/simak/deptservice.asmx?wsdl';

function test(nim) {
    for(let i = 0; i < dataMahasiswa.length; i++) {
        if(nim == dataMahasiswa[i].nim) return false;
    }
    return true;
}

var noAuth = {status: false, message: "You Can't Access This Service"};
var queryError = {status: false, message: 'Query Error'};

module.exports = {
  configure: function(app) {

    app.get('/admin/data/update', function(req, res) {
      if(fc.checkAdmin()) {
        connection.acquire(function(err, con) {
          con.query('SELECT tahunmasuk, count(*) as count FROM `mahasiswa_detail` group by tahunmasuk', function(err, result3) {
            con.query('UPDATE data_update SET date = NOW()', function(err, result1) {

              con.query('SELECT nim FROM mahasiswa_detail', function(err, result) {
                if(err) {
                    console.log(err);
                    throw err;
                } else {
                    dataMahasiswa = result;

                    soap.createClient(url, function(err, client) {
                        if(err) {
                            console.log(err);
                            throw err;
                        }
                        client.DeptService.DeptServiceSoap.ListMahasiswa({key:'272c214e2a7e46646840765e51'}, function(err, response){
                            if(err) {
                                con.release();
                                console.log(err);
                                throw err;
                            } else {

                                let dataRaw = response.ListMahasiswaResult.DataMahasiswa;
                                let dataClean = [];
                                for(let i = 0; i < dataRaw.length; i++) {
                                    if(dataRaw[i].tahunmasuk >= 2012) {
                                        dataClean.push(dataRaw[i]);
                                    }
                                }

                                let resultData = [];
                                let temp = [];

                                for(let i = 0; i < dataClean.length; i++) {
                                    if(test(dataClean[i].nim)) {
                                        temp.push(dataClean[i].nim);
                                        temp.push(dataClean[i].nama);
                                        temp.push(dataClean[i].mayor);
                                        temp.push(dataClean[i].minor);
                                        temp.push(dataClean[i].tahunmasuk);
                                        temp.push(dataClean[i].angkatan);
                                        temp.push(dataClean[i].tempatlahir);
                                        temp.push(dataClean[i].tgllahir);
                                        temp.push(dataClean[i].jeniskelamin);
                                        temp.push(dataClean[i].agama);
                                        temp.push(dataClean[i].statuskawin);
                                        temp.push(dataClean[i].alamat);
                                        temp.push(dataClean[i].kodepos);
                                        temp.push(dataClean[i].tlpbogor);
                                        temp.push(dataClean[i].hp);
                                        temp.push(dataClean[i].namaayah);
                                        temp.push(dataClean[i].namaibu);
                                        temp.push(dataClean[i].alamatortu);
                                        temp.push(dataClean[i].kotaortu);
                                        temp.push(dataClean[i].noortu);
                                        temp.push(dataClean[i].telportu);
                                        temp.push(dataClean[i].email);

                                        resultData.push(temp);
                                        temp = [];
                                    }
                                }

                                if(resultData.length > 0) {
                                    var query = 'INSERT INTO mahasiswa_2 (nim, nama, mayor, minor, tahunmasuk, angkatan, tempatlahir, tgllahir, jeniskelamin, agama, statuskawin, alamat, kodepos, tlpbogor, hp, namaayah, namaibu, alamatortu, kotaortu, noortu, telportu, email) VALUES ?'
                                    con.query(query, [resultData], function(err1, result1) {
                                        con.release();
                                        if(err1) {
                                            res.json({status: false, message: 'query Failed'});
                                            throw err1;
                                        } else {
                                            let date = new Date();
                                            res.json({status: true, message: 'query success', data: result3, total: resultData.length, date: date});
                                        }
                                    })
                                } else {
                                    con.release();
                                    let date = new Date();
                                    res.json({status: 2, data: result3, message: 'Nothing Happened', date: date});
                                }

                            }
                        });
                    });
                }
            });
            })
          })
        })
      } else {
        res.json(noAuth);
      }
    });

    app.get('/admin/data', function(req, res) {
      if(fc.checkAdmin()) {
        connection.acquire(function(err, con) {
          con.query('SELECT tahunmasuk, count(*) as count FROM `mahasiswa_detail` group by tahunmasuk', function(err, result1) {
            con.query('SELECT * FROM data_update', function(err, result) {
              con.release();
              if(err) {
                res.json(queryError);
                throw err;
              } else {
                res.json({status: true, data: result1, date: result[0].date});
              }
            })
          })
        })
      } else {
        res.json(noAuth);
      }
    })

    app.post('/admin/staff', function(req, res) {
      if(fc.checkAdmin()) {
        connection.acquire(function(err, con) {
          req = req.body;

          con.query('SELECT * FROM dosen where username = ?', [req.username], function(err, result) {
            if (err) {
              res.json({status: false, message: 'query Failed'});
              throw err;
            } else {
              if (result.length > 0) {
                var query = "UPDATE dosen SET username = ?, singkatan = ?, nama = ?, lab = ?, no_hp = ?, email = ?, nip = ?, role = ? WHERE username = ?";
                var data = [req.username, req.singkatan, req.nama, req.lab, req.no_hp, req.email, req.nip, req.role, req.username];

                con.query(query, data, function(err, result1) {
                  con.release();
                  if(err) {
                    res.json({status: false, message: 'query Failed'});
                    throw err;
                  } else {
                    res.json({status: true, message: 'Update Success'});
                  }
                })

              } else {
                var query = "INSERT INTO dosen (username, singkatan, nama, lab, no_hp, email, nip, role) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
                var data = [req.username, req.singkatan, req.nama, req.lab, req.no_hp, req.email, req.nip, req.role];

                con.query(query, data, function(err, result1) {
                  con.release();
                  if(err) {
                    res.json({status: false, message: 'query Failed'});
                    throw err;
                  } else {
                    res.json({status: true, message: 'Update Success'});
                  }
                })
              }
            }           
          })
        })
      } else {
        res.json(noAuth);
      }
    })

    app.get('/admin/kolokium', function(req, res) {
      if(fc.checkAdminDosen()) {
        connection.acquire(function(err, con) {
          var query = 'select tahun_masuk, makalah from mahasiswa_kolokium_view where tahun_masuk < YEAR(CURDATE()) - 3 and tahun_masuk > YEAR(CURDATE()) - 6 order by tahun_masuk asc';
          var data = [];
          con.query(query, data, function(err, result) {
            con.release();
            if (err) {
                res.json(queryError);
                throw err;
            }
            else {
              res.json(fc.countKolokiumPraseminar(result));
            }
          })
        })
      }
      else {
        res.json(noAuth);
      }
    });

    app.get('/admin/praseminar', function(req, res) {
      if(fc.checkAdminDosen()) {
        connection.acquire(function(err, con) {
          var query = 'select tahun_masuk, makalah from mahasiswa_praseminar_view where tahun_masuk < YEAR(CURDATE()) - 3 and tahun_masuk > YEAR(CURDATE()) - 6 order by tahun_masuk asc';
          var data = [];
          con.query(query, data, function(err, result) {
            con.release();
            if (err) {
                res.json(queryError);
                throw err;
            }
            else {
              res.json(fc.countKolokiumPraseminar(result));
            }
          })
        })
      }
      else {
        res.json(noAuth);
      }
    });

    app.get('/admin/sidang', function(req, res) {
      if(fc.checkAdminDosen()) {
        connection.acquire(function(err, con) {
          var query = 'select tahun_masuk, makalah from mahasiswa_sidang_view where tahun_masuk < YEAR(CURDATE()) - 3 and tahun_masuk > YEAR(CURDATE()) - 6 order by tahun_masuk asc';
          var data = [];
          con.query(query, data, function(err, result) {
            con.release();
            if (err) {
                res.json(queryError);
                throw err;
            }
            else {
              res.json(fc.countKolokiumPraseminar(result));
            }
          })
        })
      }
      else {
        res.json(noAuth);
      }
    });

    app.get('/admin/seminar', function(req, res) {
      if(fc.checkAdminDosen()) {
        connection.acquire(function(err, con) {
          var query = 'select * from mahasiswa_seminar_view where tahun_masuk < YEAR(CURDATE()) - 3 and tahun_masuk > YEAR(CURDATE()) - 6 order by tahun_masuk asc';
          var data = [];
          con.query(query, data, function(err, result) {
            con.release();
            if (err) {
                res.json(queryError);
                throw err;
            }
            else {
              res.json(fc.countSeminar(result));
            }
          })
        })
      }
      else {
        res.json(noAuth);
      }
    });

    app.get('/admin/skl', function(req, res) {
      if(fc.checkAdminDosen()) {
        connection.acquire(function(err, con) {
          var query = 'select tahun_masuk, berkas from mahasiswa_skl_view where tahun_masuk < YEAR(CURDATE()) - 3 and tahun_masuk > YEAR(CURDATE()) - 6 order by tahun_masuk asc';
          var data = [];
          con.query(query, data, function(err, result) {
            con.release();
            if (err) {
                res.json(queryError);
                throw err;
            }
            else {
              res.json(fc.countSKL(result));
            }
          })
        })
      }
      else {
        res.json(noAuth);
      }
    });

    app.get('/admin/summary', function(req, res) {
      if(fc.checkAdminDosen()) {
        connection.acquire(function(err, con) {
          var query = 'select * from mahasiswa_summary where tahun_masuk < YEAR(CURDATE()) - 3 and tahun_masuk > YEAR(CURDATE()) - 6 order by tahun_masuk asc';
          var data = [];
          con.query(query, data, function(err, result) {
            con.release();
            if (err) {
              res.json(queryError);
              throw err;
            }
            else {
              res.json(fc.countSummary(result));
            }
          })
        })
      }
      else {
        res.json(noAuth);
      }
    });

  }
}
