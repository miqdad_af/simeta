var nodemailer = require('nodemailer');
var _ = require('./simak');
var connection = require('./connection');
var jwt = require('./direct/generateJWT');
var ICS = require('ics');
var random = require('randomstring');
var querystring = require('querystring');
var mg = require('nodemailer-mailgun-transport');
var path = require('path');
var ics = new ICS();

// var transporter = nodemailer.createTransport('smtps://postmaster@sandboxd581378d0c504cc0b05ad6243decf5d5.mailgun.org:aead6c5060abd2250c61df0399ff37b8@smtp.mailgun.org');
  // var transporter = nodemailer.createTransport('smtps://simeta.ilkom@gmail.com:simeta.ilkom.ipb@smtp.gmail.com');
// var transporter = nodemailer.createTransport({
// 	host: 'smtp.gmail.com',
// 	port: 465,
// 	secure: true,
// 	auth: {
// 		user: 'miqdadfawwaz95@gmail.com',
// 		pass: 'bismill4h'
// 	}
// })

// var authMailGun = {
//   auth: {
//     api_key: 'key-57ca778c0d1a047f20048895e9791a6a',
//     domain: 'sandbox7a858a40d08d450c92ba9f29ef1fe51e.mailgun.org'
//   }
// }
// var transporter = nodemailer.createTransport(mg(authMailGun));

var transporter = nodemailer.createTransport({
  host: "smtp.mailtrap.io",
  port: 2525,
  auth: {
    user: "ab853562c9c22a",
    pass: "89b3723f56a4de"
  }
});


function Mail() {

  this.mailLog = function(req, id, to, nim, nama, topik, tanggal, tempat, jam, log, progress, kendala, rencana) {
    connection.acquire(function(err, con) {
      con.query('SELECT * FROM dosen_mahasiswa_view WHERE nim = ?', [nim], function(err, result) {
        con.release();
        if(err) {
          throw err;
        }
        else {
          var dosen1 = result[0].dosen1;
          var dosen2 = result[0].dosen2;

          var mailOptions = {
            from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
            to: to,
            subject: 'Log Bimbingan SIMETA - '+nama,
            html:
            'Yth. Bapak/Ibu/Sdr Pembimbing Tugas Akhir dari mahasiswa berikut :<br><br>'+
            '<b>NIM : </b>'+nim+'<br>'+
            '<b>Nama : </b>'+nama+'<br>'+
            '<b>Topik TA : </b>'+topik+'<br>'+
            '<b>Pembimbing Tugas Akhir : </b>'+dosen1+', '+dosen2+'<br><br>'+
            'Mahasiswa tersebut telah mengisi mengisi data kendali Bimbingan Tugas Akhir sebagai berikut :<br><br>'+
            '<b>Tanggal : </b>'+tanggal+'<br>'+
            '<b>Tempat : </b>'+tempat+'<br>'+
            '<b>Jam : </b>'+jam+'<br>'+
            '<b>Topik Bimbingan : </b><br>'+log+'<br><br>'+
            '<b>Progress TA : </b><br>'+progress+'<br><br>'+
            '<b>Kendala : </b><br>'+kendala+'<br><br>'+
            '<b>Rencana : </b><br>'+rencana+'<br><br>'+
            'Demikian email ini disampaikan. Silahkan akses <i>link</i> berikut untuk menyetujui log bimbingan tersebut : <br><br>'+
            '<a style="text-align:center" target="_blank" href="http://localhost:2016/log/approve/'+id+'/'+req+'/"><button style="background-color:#3498db; padding: 15px 32px; font-size: 16px; color:white">Setuju Log Bimbingan Ini</button></a><br><br>'+
            'Perlu diketahui bahwa persetujuan Ibu/Bapak Pembimbing Tugas Akhir sangat diperlukan agar data tersebut terhitung dalam evaluasi pembimbingan bagi mahasiswa.<br><br>'+
            'Terima Kasih.<br>'+
            '----------------------<br>'+
            'SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>'
          };

          transporter.sendMail(mailOptions, function(error, info){
              if(error){
                  return console.log(error);
              }
              else {
                console.log('Message sent: ' + info.response);
              }
          });

        }
      })
    })

  }

// EMAIL KONFORMASI PENGAJUAN PEMBIMBING KEDUA
// SIMETA v2
  this.setDosenPertamaMail = function(req, res, token, id) {
    connection.acquire(function(err, con) {
      con.query('SELECT * FROM dosen_mahasiswa_view WHERE nim = ?', [_.decoded.nim], function(err, result) {
        con.release();
        // var randomStr = random.generate(18);
        if (err) {
          res.json({status: 0, code: 500, message: 'Internal server error (1), please contact the developer for further informations.'})
          throw err;
        } else {
          let dosen = result[0].dosen1;
          let to = result[0].dosen1_email;
          let nama = result[0].nama;
          let topik = result[0].topik;
          let nip  = req.nip;
          // let token = jwt.mailToken(randomStr);
          let dosen2 = req.dosen_2;
          let mailOptions;
          if (req.statReq === 1) {
            mailOptions = {
              from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
              to: 'miqdad.fawwaz@gmail.com',
              subject: `Konfirmasi Pengajuan Dosen Pembimbing Kedua [PEMBIMBING PERTAMA]`,
              html: 'Yth. Bapak/Ibu/Sdr Pembimbing Tugas Akhir dari mahasiswa berikut :<br><br>'+
              '<b>NIM : </b>'+_.decoded.nim+'<br>'+
              '<b>Nama : </b>'+nama+'<br>'+
              '<b>Topik TA : </b>'+topik+'<br>'+
              '<b>Pembimbing Tugas Akhir : </b>'+dosen+'<br><br>'+
              'Mahasiswa tersebut telah melakukan pengajuan dosen pembimbing kedua sebagai berikut: <br><br>'+
              '<b>Nama :  </b>'+dosen2+'<br>'+
              '<p>Silahkan melakukan konfirmasi dengan mengklik tombol di bawah ini.</p><br><br>'+
              '<a target="_blank" style="text-align: center;" href="http://localhost:2016/pembimbing-kedua/konfirmasi/pertama/set/'+id+'/'+token+'/"><button style="background-color:#3498db; padding: 15px 32px; font-size: 16px; color:white; margin-right: 20px;">Diterima</button></a>'+
              '<a target="_blank" style="text-align: center;" href="http://localhost:2016/pembimbing-kedua/konfirmasi/delete/'+id+'/'+token+'/"><button style="background-color: red; padding: 15px 32px; font-size: 16px; color:white">Ditolak</button></a><br><br>'+
              'Terima Kasih.<br>'+
              '----------------------<br>'+
              'SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>'+
              '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>'
            };
          } else if (req.statReq === 2) {
            console.log ('masuk req.statReq 2');
            mailOptions = {
              from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
              to: 'miqdad.fawwaz@gmail.com',
              subject: `Konfirmasi Pengajuan Dosen Pembimbing Kedua [PEMBIMBING PERTAMA]`,
              html: 'Yth. Bapak/Ibu/Sdr Pembimbing Tugas Akhir dari mahasiswa berikut :<br><br>'+
              '<b>NIM : </b>'+_.decoded.nim+'<br>'+
              '<b>Nama : </b>'+nama+'<br>'+
              '<b>Topik TA : </b>'+topik+'<br>'+
              '<b>Pembimbing Tugas Akhir : </b>'+dosen+'<br><br>'+
              'Mahasiswa tersebut telah melakukan pengajuan dosen pembimbing kedua sebagai berikut: <br><br>'+
              '<b>Nama  :  </b>'+dosen2+'<br>'+
              '<b>NIP  :  </b>' + req.nip + '<br>' +
              '<b>Keahlian: </b>' + req.bidangKeahlian + '<br>' +
              '<b>Nama Instansi  :  </b>' + req.namaInstansi + '<br>' +
              '<b>Alamat Intansi  :  </b>'  + req.alamatInstansi + '<br>' +
              '<b>Nomor Telepon  :  </b>' + req.nomorTelp + '<br>' +
              '<b>Email  :  </b>' + req.email + '<br>' +
              'Silahkan melakukan konfirmasi dengan mengklik tombol di bawah ini.<br><br>'+
              '<a target="_blank" style="text-align: center;" href="http://localhost:2016/pembimbing-kedua/konfirmasi/pertama/set/'+id+'/'+token+'/"><button style="background-color:#3498db; padding: 15px 32px; font-size: 16px; color:white; margin-right: 10px;">Diterima</button></a>'+
              '<a target="_blank" style="text-align: center;" href="http://localhost:2016/pembimbing-kedua/konfirmasi/delete/'+id+'/'+token+'/"><button style="background-color: red; padding: 15px 32px; font-size: 16px; color:white">Ditolak</button></a><br><br>'+
              'Terima Kasih.<br>'+
              '----------------------<br>'+
              'SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>'+
              '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>'
            };
          } else {
            res.json({status: 0, code: 400, message: 'Bad request'});
          }
          transporter.sendMail(mailOptions, function(err, info) {
            if (err) {
              return console.log(err);
              // res.json({status: 0, code: 500, message: 'Gagal mengirimkan pesan [Internal server error. Please contact the developer for further informations. Error msg: '+ err + ']'});
              throw err;
            } else {
              console.log('Message sent: '+ info.response);
              // res.json({status: 1, code: 200, message: 'Konfirmasi pertama pembimbing kedua berhasil. Silahkan tunggu informasi selanjutnya'});
            }
          });
        }
      });
    });
  }

  this.setDosenKeduaMail = function(res, data, token, id) {
    let mailOptions;
    if (data.statusReq === 1) {
      mailOptions = {
        from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
        to: 'miqdad.fawwaz@gmail.com',
        subject: 'Konfirmasi Pengajuan Dosen Pembimbing Kedua [PEMBIMBING KEDUA]',
        html: `
          Yth. Bapak/Ibu <br>
          Dengan hormat, mahasiswa yang bertanda tangan di bawah ini: <br><br>
          <b>Nama : </b>  ${data.namaMahasiswa} <br>
          <b>NIM  : </b>  ${data.nimMahasiswa} <br>
          <b>Jurusan : </b> Ilmu Komputer, Fakultas Matematika dan Ilmu Pengetahuan Alam, Institut Pertanian Bogor <br>
          <b>Pembimbing Tugas Akhir </b> : ${data.dosen1} <br>
          <b>Topik Tugas Akhir </b> : ${data.topik} <br><br>
          Dengan ini mengajukan permohonan kepada Bapak/Ibu sebagai pembimbing kedua tugas akhir sebagai berikut: <br><br>
          <b>Nama : </b> ${data.dosen2} <br>
          <b>NIP  : </b> ${data.nip ? data.nip : '-'} <br>
          <b>Lab : </b> ${data.lab == 2 ? 'Computational Intelligence Optimization' : data.lab1 == 1 ? 'Computer Security and Networking' : data.lab1 == 3 ? 'Software Engineering and Information System' : ''} <br><br>
          Mohon sekiranya memberikan konfirmasi dengan cara mengklik tombol yang tersedia di bawah ini. <br><br>
          <a target="_blank" style="text-align: center;" href="http://localhost:2016/pembimbing-kedua/konfirmasi/kedua/set/${data.id}/${token}/"><button style="background-color:#3498db; margin-right: 20px; padding: 15px 32px; font-size: 16px; color:white">Diterima</button></a>
          <a target="_blank" style="text-align: center;" href="http://localhost:2016/pembimbing-kedua/konfirmasi/delete/${data.id}/${token}"><button style="background-color: red; padding: 15px 32px; font-size: 16px; color:white">Ditolak</button></a><br><br>
          Terima kasih. <br>
          SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
          <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
        `
      }
    } else if (data.statusReq === 2) {
        mailOptions = {
          from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
          to: 'miqdad.fawwaz@gmail.com',
          subject: 'Konfirmasi Pengajuan Dosen Pembimbing Kedua [PEMBIMBING KEDUA]',
          html: `
            Yth. Bapak/Ibu <br>
            Dengan hormat, mahasiswa yang bertanda tangan di bawah ini: <br><br>
            <b>Nama : </b>  ${data.namaMahasiswa} <br>
            <b>NIM  : </b>  ${data.nimMahasiswa} <br>
            <b>Jurusan  : </b> Ilmu Komputer, Fakultas Matematika dan Ilmu Pengetahuan Alam, Institut Pertanian Bogor <br>
            <b>Pembimbing Tugas Akhir : </b> ${data.dosen1} <br>
            <b>Topik Tugas Akhir  : </b> ${data.topik} <br><br>
            Dengan ini mengajukan permohonan kepada Bapak/Ibu sebagai pembimbing kedua tugas akhir sebagai berikut: <br><br>
            <b>Nama : </b> ${data.dosen2} <br>
            <b>NIP  : </b> ${data.nip ? data.nip : '-'} <br>
            <b>Bidang keahlian : </b> ${data.keahlian} <br>
            <b>Instansi : </b> ${data.namaInstansi} <br>
            <b>Alamat instansi  : </b> ${data.alamatInstansi} <br><br>
            Mohon sekiranya memberikan konfirmasi dengan cara mengklik tombol yang tersedia di bawah ini. <br><br>
            <a target="_blank" style="text-align: center;" href="http://localhost:2016/pembimbing-kedua/konfirmasi/kedua/set/${data.id}/${token}/"><button style="background-color:#3498db; margin-right: 20px; padding: 15px 32px; font-size: 16px; color:white">Diterima</button></a>
            <a target="_blank" style="text-align: center;" href="http://localhost:2016/pembimbing-kedua/konfirmasi/delete/${data.id}/${token}"><button style="background-color: red; padding: 15px 32px; font-size: 16px; color:white">Ditolak</button></a><br><br>
            Terima kasih. <br>
            SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
            <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
          `
        }
    } else {
        res.json({status: 0, code: 400, message: 'Bad request. Please contact the developer for further informations'});
    }
    transporter.sendMail(mailOptions, (err, info) => {
      if (err) {
        throw err;
      }
      else console.log('Berhasil mengirimkan pesan untuk dosen pembimbing kedua');
    });
  }

  this.setKepalaBagian = function(res, to, data, token, id) {
    let mailOptions;
    console.log(data.statusReq)
    if (data.statusReq === 1) {
      mailOptions = {
        from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
        // Email admin SIMETA
        to: 'miqdad.fawwaz@gmail.com',// to
        subject: 'Konfirmasi Pengajuan Dosen Pembimbing Kedua [KEPALA BAGIAN]',
        html: `
          Yth. Bapak/Ibu <br><br>
          Dengan hormat, mahasiswa yang bertanda tangan di bawah ini: <br><br>
          <b>NIM  : </b> ${data.nimMahasiswa}<br>
          <b>Nama : </b> ${data.namaMahasiswa}<br>
          <b>Topik TA : </b> ${data.topik}<br>
          <b>Pembimbing TA : </b> ${data.dosen1}<br><br>
          Mahasiswa tersebut telah melakukan pengajuan dosen pembimbing kedua sebagai berikut:<br><br>
          <b>Nama : </b> ${data.dosen2} <br>
          <b>NIP : </b> ${data.nip ? data.nip : '-'} <br>
          <b>Lab : </b> ${data.lab == 2 ? 'Computational Intelligence Optimization' : data.lab1 == 1 ? 'Computer Security and Networking' : data.lab1 == 3 ? 'Software Engineering and Information System' : ''} <br>
          <b>E-mail: </b> ${data.email} <br>
          <b>Nomor Telp: </b> ${data.nomorTelp} <br>
          Silahkan melakukan konfirmai dengan mengklik tombol di bawah ini. <br><br>
          <a  target="_blank" style="text-align: center;" href="http://localhost:2016/pembimbing-kedua/konfirmasi/kepala-bagian/set/${id}/${data.idDosen}/${token}/"><button style="background-color:#3498db; padding: 15px 32px; font-size: 16px; color:white; margin-right: 20px;">Diterima</button></a>
          <a  target="_blank" style="text-align: center;" href="http://localhost:2016/pembimbing-kedua/konfirmasi/delete/${id}/${token}"><button style="background-color: red; padding: 15px 32px; font-size: 16px; color:white">Ditolak</button></a><br><br>
          Terima kasih. <br>
          SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
          <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
        `
      }
    } else if (data.statusReq === 2) {
        console.log('masuk method admin status 2');
        mailOptions = {
          from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
          to: 'miqdad.fawwaz@gmail.com', // to,
          subject: 'Konfirmasi Pengajuan Dosen Pembimbing Kedua [KEPALA BAGIAN]',
          html: `
            Yth. Bapak/Ibu <br><br>
            <b>NIM  : </b> ${data.nimMahasiswa}<br>
            <b>Nama : </b> ${data.namaMahasiswa}<br>
            <b>Topik TA : </b> ${data.topik}<br>
            <b>Pembimbing TA : </b> ${data.dosen1}<br><br>
            Mahasiswa tersebut telah melakukan pengajuan dosen pembimbing kedua sebagai berikut:<br><br>
            <b>Nama : </b> ${data.dosen2} <br>
            <b>NIP : </b> ${data.nip ? data.nip : '-'} <br>
            <b>Bidang keahlian : </b> ${data.keahlian} <br>
            <b>Instansi: </b> ${data.namaInstansi} <br>
            <b>Alamat Instansi: </b> ${data.alamatInstansi} <br>
            <b>E-mail: </b> ${data.email} <br>
            <b>Nomor Telp: </b> ${data.nomorTelp} <br>
            Silahkan melakukan konfirmasi dengan mengklik tombol di bawah ini. <br><br>
            <a  target="_blank" style="text-align: center;" href="http://localhost:2016/pembimbing-kedua/konfirmasi/kepala-bagian/set/${data.id}/${data.idDosen}/${token}/"><button style="background-color:#3498db; padding: 15px 32px; font-size: 16px; color:white; margin-right: 20px;">Diterima</button></a>
            <a  target="_blank" style="text-align: center;" href="http://localhost:2016/pembimbing-kedua/konfirmasi/delete/${id}/${token}"><button style="background-color: red; padding: 15px 32px; font-size: 16px; color:white">Ditolak</button></a><br><br>
            Terima kasih. <br>
            SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
            <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
          `
      }
    } else {
        res.json({status: 0, code: 400, message: 'Bad request'});
    }
    transporter.sendMail(mailOptions, (err, info) => {
      if (err) {
        // res.json({status: 0, code: 500, message: 'Internal server error'});
        throw err;
      }
      else console.log('Berhasil mengirimkan pesan untuk admin');
    });
  }

  this.setMahasiswaSuccess = function(res, data) {
    let mailOptions;
    connection.acquire((err,con) => {
      if (err) {
        res.json({status: 0, code: 500, message: 'Internal server error (0). Please contact the developer for further informations.'});
        throw err;
      } else {
        con.query('SELECT email FROM mahasiswa_detail WHERE nim = ?', [data.nim], (err1, res1) => {
          if (err1) {
            res.json({status: 0, code: 500, message: 'Internal server error (1). Please contact the developer for further inforamtions.'});
            throw err1;
          } else {
            const to = res1[0].email;
            const mailOptions = {
              from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
              to: 'miqdad.fawwaz@gmail.com',
              subject: 'Konfirmasi Pengajuan Pembimbing Kedua',
              html: `
                Yth. ${data.nama} <br><br>
                Dengan surel ini kami informasikan bahwa permohonan pengajuan pembimbing kedua Anda sudah berhasil <b>diterima</b>. Berikut
                adalah detail pembimbing kedua tugas akhir Anda: <br><br>
                <b>Nama:</b> ${data.dosen2} <br>
                <b>NIP: </b> ${data.nip ? data.nip : '-'} <br>
                <b>Instansi: </b> ${data.nama_instansi ? data.nama_instansi : 'Departemen Ilmu Komputer, Fakultas Matematika dan Ilmu Pengetahuan Alam, Institut Pertanian Bogor'} <br>
                <b>Alamat instansi: </b> ${data.alamat_instansi ? data.alamat_instansi : ` Jl. Meranti Wing 20 Level V Kampus Dramaga IPB
                Bogor, Jawa Barat, Indonesia 16680`} <br> 
                <b>No Telepon: </b> ${data.no_hp} <br>
                <b>E-mail: </b> ${data.email} <br><br>
                Terima kasih. <br>
                SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
                <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
              `
            }
            transporter.sendMail(mailOptions, (err, info) => {
              if (err) {
                return console.log('Cannot send the message! Error: '+ err);
              } else {
                console.log('Message sent!');
              }
            });
          }
        });
      }
    });
  }

  this.setMahasiswaAccepted = function(res, data) {
    connection.acquire((err, con) => {
      if (err) {
        res.json({status: 0, code: 500, message: 'Internal server error (0). Please contact the developer for further informations.'});
        throw err;
      } else {
        con.query('SELECT email FROM mahasiswa_detail WHERE nim = ?', [data.nimMahasiswa], (err1, res1) => {
          if (err1) {
            res.json({status: 0, message: 'Query error'});
            throw err1;
          } else {
            const to = 'miqdad.fawwaz@gmail.com'
            const mailOptions = {
              from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
              to,
              subject: 'Konfirmasi Pengajuan Pembimbing Kedua',
              html: `
                Yth. ${data.namaMahasiswa} <br><br>
                Dengan surel ini kami informasikan bahwa ${data.statusDp === 2 ? 'dosen pembimbing pertama' : data.statusDp === 3 ? 'calon dosen pembimbing kedua' : 'kepala bagian'} telah menerima permohonan pengajuan pembimbing kedua Anda. 
                Pengajuan ini masih membutuhkan beberapa tahap verifikasi, sehingga diharapkan Anda dapat menunggu
                informasi selanjutnya. <br><br>
                Terima kasih. <br>
                SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
                <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
              `
            }
            transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                return console.log('error');
                throw error;
              } else {
                console.log('Message sent!');
              }
            });
          }
        });
      }
    });
  }

  this.setMahasiswaRejected = function(res, data) {
    const to = 'miqdad.fawwaz@gmail.com';
    const mailOptions = {
      from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
      to,
      subject: 'Konfirmasi Pengajuan Pembimbing Kedua',
      html: `
        Yth ${data.nama} <br><br>
        Dengan surel ini kami informasikan bahwa ${data.statusDp === 1 ? 'dosen pembimbing pertama' : data.statusDp === 2 ? 'calon dosen pembimbing kedua' : 'kepala bagian'}  Anda telah menolak permohonan yang
        Anda ajukan dengan alasan <b>${data.alasan ? data.alasan : "HUBUNGI PEMBIMBING ANDA"}</b>. <br><br>
        Terima kasih. <br>
        SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
        <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
      `
    };
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
        throw error;
      } else {
        console.log('Message sent! '+ info.response);
      }
    });
  }

// EMAIL MANDIRI
  this.declineMandiri = function(res, data) {
    const mailOptions = {
      from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
      to: 'miqdad.fawwaz@gmail.com',
      subject: 'Konfirmasi Seminar Mandiri',
      html: `
        Yth ${data.nama} <br><br>
        Dengan surel ini kami informasikan bahwa pengajuan seminar mandiri Anda telah ditolak oleh
        ${ data.statusDosen === 'Dosen pembimbing ketua' ? 'dosen pembimbing ketua' : data.statusDosen === 'Dosen pembimbing anggota' ? 'dosen pembimbing anggota' : data.statusDosen === 'Penguji pertama' ? 'penguji pertama' : data.statusDosen === 'Penguji kedua' ? 'penguji kedua' : 'komisi pendidikan'} 
        dengan alasan <b>"${data.alasan ? data.alasan : 'HUBUNGI PEMBIMBING'}"</b>. 
        Silahkan lakukan pengajuan kembali melalui laman <a href="http://simeta.apps.cs.ipb.ac.id">SIMETA</a>.<br><br>
        Terima kasih. <br>
        SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
        <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
      `
    };
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
        throw error;
      } else {
        console.log('Message sent! '+ info.response);
      }
    });
  }

  // KONFIRMASI SEMINAR MANDIRI
  this.setMandiriPertama = function(res, data, token) {
    const mailOptions = {
      from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
      to: 'miqdad.fawwaz@gmail.com',
      subject: 'Konfirmasi Seminar Mandiri [PEMBIMBING KETUA]',
      html: `
        Yth. Bapak/Ibu <br>
        Dengan hormat, mahasiswa yang bertanda tangan di bawah ini: <br><br>
        <b>Nama </b> : ${data.nama} <br>
        <b>NIM </b> : ${data.nim} <br>
        <b>Jurusan  : </b> Ilmu Komputer, Fakultas Matematika dan Ilmu Pengetahuan Alam, Institut Pertanian Bogor <br>
        <b>Pembimbing Tugas Akhir </b> : ${data.dosen1} <br>
        <b>Topik Tugas Akhir</b> : ${data.topik} <br><br>
        Dengan ini mengajukan pelaksanaan kegiatan seminar mandiri, yang akan dilaksanakan pada: <br><br>
        <b>Tanggal </b> : ${data.tanggal} <br>
        <b>Jam </b> : ${data.jam} <br>
        <b>Tempat </b> : ${data.tempat ? data.tempat : '(Akan ditentukan oleh komisi pendidikan)'} <br><br>
        Maka dari itu, dimohon kepada Bapak/Ibu untuk memberikan konfirmasi dengan mengeklik tombol di bawah ini. <br><br>
        <a target="_blank" style="text-align: center;" href="http://localhost:2016/seminar/mandiri/konfirmasi/pertama/${data.nim}/${token}/"><button style="background-color:#3498db; margin-right: 20px; padding: 15px 32px; font-size: 16px; color:white">Diterima</button></a>
        <a target="_blank" style="text-align: center;" href="http://localhost:2016/seminar/mandiri/delete/pertama/${data.nim}/${token}/"><button style="background-color: red; padding: 15px 32px; font-size: 16px; color:white">Ditolak</button></a><br><br>
        Terima kasih. <br>
        SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
        <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
      `
    };
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
        throw error;
      } else {
        console.log('Message sent! '+ info.response);
      }
    });
  }

  this.setMandiri = function(res, data, token) {
    const emails = data.email;
    emails.forEach((email, index) => {
      if (email) {
        let emailStr = querystring.stringify({email});
        const mailOptions = {
          from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
          to: 'miqdad.fawwaz@gmail.com', //email
          subject: `Konfirmasi Seminar Mandiri ${index === 0 ? '[PEMBIMBING ANGGOTA]' : index === 1 ? '[MODERATOR PERTAMA]' : index === 2 ? '[MODERATOR KEDUA]' : ''}`,
          html: `
            Yth. Bapak/Ibu <br>
            Dengan hormat, mahasiswa yang bertanda tangan di bawah ini: <br><br>
            <b>Nama </b> : ${data.nama} <br>
            <b>NIM </b> : ${data.nim} <br>
            <b>Jurusan </b> : Ilmu Komputer, Fakultas Matematika dan Ilmu Pengetahuan Alam, Institut Pertanian Bogor <br>
            <b>Topik Tugas Akhir</b> : ${data.topik}<br><br>
  
            <b>Pembimbing Ketua </b> : ${data.namaDosen1} <br>
            <b>Pembimbing Anggota </b> : ${data.namaDosen2 ? data.namaDosen2 : '-'} <br>
            <b>Moderator Pertama </b> : ${data.namaPenguji1 ? data.namaPenguji1 : '-'} <br>
            <b>Moderator Kedua </b> : ${data.namaPenguji2 ? data.namaPenguji2 : '-'} <br><br>
            Dengan ini mengajukan pelaksanaan kegiatan seminar mandiri, yang akan dilaksanakan pada: <br><br>
            <b>Tanggal </b> : ${data.tanggal} <br>
            <b>Jam </b> : ${data.jam} <br>
            <b>Tempat </b> : ${data.tempat ? data.tempat : '(Akan ditentukan oleh komisi pendidikan)'} <br><br>
            Maka dari itu, dimohon kepada Bapak/Ibu untuk memberikan konfirmasi dengan mengklik tombol di bawah ini. <br><br>
            <a target="_blank" style="text-align: center;" href="http://localhost:2016/seminar/mandiri/konfirmasi/${data.nim}/${emailStr}/${token}/"><button style="background-color:#3498db; margin-right: 20px; padding: 15px 32px; font-size: 16px; color:white">Diterima</button></a>
            <a target="_blank" style="text-align: center;" href="http://localhost:2016/seminar/mandiri/delete/${data.nim}/${emailStr}/${token}/"><button style="background-color: red; padding: 15px 32px; font-size: 16px; color:white">Ditolak</button></a><br><br>
            Terima kasih. <br>
            SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
            <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
          `
        };
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return console.log(error);
            throw error;
          } else {
            console.log('Message sent! '+ info.response);
          }
        });
      }
    });
  }

  this.setMandiriSukses = function(res, data, type) {
    let mailOptions;
    if (type === 'pertama') {
      mailOptions = {
        from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
        to: 'miqdad.fawwaz@gmail.com',
        subject: 'Konfirmasi Seminar Mandiri',
        html: `
          Kepada ${data.nama} <br><br>
          Dengan surel ini kami informasikan bahwa permohonan jadwal seminar mandiri Anda telah berhasil <b>diverifikasi</b>. Silahkan tentukan pembahas seminar dan unggah
          makalah seminar pada <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">SIMETA</a>, serta lengkapi persyaratan berkas-berkas seminar mandiri yang kemudian dapat dibawa ke kantor TU Depertemen Ilmu Komputer IPB.<br><br>
          Terima kasih. <br>
          SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
          <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
        `
      };
    } else {
      mailOptions = {
        from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
        to: 'miqdad.fawwaz@gmail.com',
        subject: 'Konfirmasi Seminar Mandiri',
        html: `
          Kepada ${data.nama} <br><br>
          Dengan surel ini kami informasikan bahwa permohonan pengajuan seminar mandiri Anda telah <b>diterima</b>. Berikut detail data seminar Anda: <br><br>
          <b>Tempat </b> : ${data.tempat} <br>
          <b>Tanggal </b> : ${data.tanggal} <br>
          <b>Jam </b> : ${data.jam} <br><br>
          <b>Pembahas 1 </b> : ${data.pembahas1} <br>
          <b>Pembahas 2 </b> : ${data.pembahas2} <br>
          <b>Pembahas 3 </b> : ${data.pembahas3} <br><br>
          <b>Moderator 1 </b> : ${data.penguji1 && data.konfirmasiPenguji1 === 1 ? data.penguji1 : '-'} <br>
          <b>Moderator 2 </b> : ${data.penguji2 && data.konfirmasiPenguji2 === 1 ? data.penguji2 : '-'} <br><br>
          Terima kasih. <br>
          SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
          <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
        `
      };
    }
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
        throw error;
      } else {
        console.log('Message sent! '+ info.response);
      }
    });
  }

// EMAIL SKL
  this.sklMail = function(to, nim, nama, topik, file, tanggal) {
    connection.acquire(function(err, con) {
      con.query('SELECT * FROM dosen_mahasiswa_view WHERE nim = ?', [nim], function(err, result) {
        con.release();
        if(err) {
          throw err;
        }
        else {
          var dosen1 = result[0].dosen1;
          if(result[0].dosen2) var dosen2 = result[0].dosen2;
          else dosen2 = '';
          var penguji1 = result[0].penguji1;
          if(result[0].penguji2) var penguji2 = result[0].penguji2;
          else penguji2 = '';
          var mailOptions = {
            from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
            to: to,
            subject: 'SKL TA - '+nama,
            attachments: [
                {
                    path: './uploads/fileSKL/'+file
                }
            ],
            html:
            'Yth. Bapak/Ibu/Sdr Pembimbing Tugas Akhir dari mahasiswa berikut :<br><br>'+
            '<b>NIM : </b>'+nim+'<br>'+
            '<b>Nama : </b>'+nama+'<br>'+
            '<b>Topik TA : </b>'+topik+'<br>'+
            '<b>Pembimbing Tugas Akhir : </b>'+dosen1+', '+dosen2+'<br>'+
            '<b>Penguji : </b>'+penguji1+', '+penguji2+'<br><br>'+
            'Mahasiswa tersebut telah menyelesaikan SKL pada tanggal : <b>'+tanggal+'<br><br>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id/uploads/fileSKL/'+file+'">SKL '+nama+'</a><br><br>'+
            'Terima Kasih.<br>'+
            '----------------------<br>'+
            'SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>'
          };

          transporter.sendMail(mailOptions, function(error, info){
              if(error){
                  return console.log(error);
              }
              else {
                console.log('Message sent: ' + info.response);
              }
          });

        }
      })
    })


  }

// EMAIL KOLOKIUM
  this.kolokiumMail = function(to, nim, nama, topik, file) {
    connection.acquire(function(err, con) {
      con.query('SELECT * FROM dosen_mahasiswa_view WHERE nim = ?', [nim], function(err, result) {
        con.release();
        if(err) {
          throw err;
        }
        else {
          var dosen1 = result[0].dosen1;
          var dosen2 = result[0].dosen2;

          var mailOptions = {
            from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
            to: to,
            subject: 'File Kolokium TA - '+nama,
            attachments: [
                {
                    path: './uploads/fileKolokium/'+file
                }
            ],
            html:
            'Yth. Bapak/Ibu/Sdr Pembimbing Tugas Akhir dari mahasiswa berikut :<br><br>'+
            '<b>NIM : </b>'+nim+'<br>'+
            '<b>Nama : </b>'+nama+'<br>'+
            '<b>Topik TA : </b>'+topik+'<br>'+
            '<b>Pembimbing Tugas Akhir : </b>'+dosen1+', '+dosen2+'<br><br>'+
            'Mahasiswa tersebut telah melakukan upload file makalah kolokium, dan dapat diunduh pada link berikut :<br><br>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id/uploads/fileKolokium/'+file+'">Makalah Kolokium '+nama+'</a><br><br>'+
            'Terima Kasih.<br>'+
            '----------------------<br>'+
            'SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>'
          };

          transporter.sendMail(mailOptions, function(error, info){
              if(error){
                  console.log('test'+error);
              }
              else {
                console.log('Message sent: ' + info.response);
              }
          });

        }
      })
    })



  }

// EMAIL PRASEMINAR
  this.praseminarMail = function(to, nim, nama, topik, file) {
    connection.acquire(function(err, con) {
      con.query('SELECT * FROM dosen_mahasiswa_view WHERE nim = ?', [nim], function(err, result) {
        con.release();
        if(err) {
          throw err;
        }
        else {
          var dosen1 = result[0].dosen1;
          var dosen2 = result[0].dosen2;

          var mailOptions = {
            from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
            to: to,
            subject: 'File Praseminar TA - '+nama,
            attachments: [
                {
                    path: './uploads/filePraseminar/'+file
                }
            ],
            html:
            'Yth. Bapak/Ibu/Sdr Pembimbing Tugas Akhir dari mahasiswa berikut :<br><br>'+
            '<b>NIM : </b>'+nim+'<br>'+
            '<b>Nama : </b>'+nama+'<br>'+
            '<b>Topik TA : </b>'+topik+'<br>'+
            '<b>Pembimbing Tugas Akhir : </b>'+dosen1+', '+dosen2+'<br><br>'+
            'Mahasiswa tersebut telah melakukan upload file makalah praseminar, dan dapat diunduh pada link berikut :<br><br>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id/uploads/filePraseminar/'+file+'">Makalah Seminar Konferensi '+nama+'</a><br><br>'+
            'Terima Kasih.<br>'+
            '----------------------<br>'+
            'SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>'
          };

          transporter.sendMail(mailOptions, function(error, info){
              if(error){
                  return console.log(error);
              }
              else {
                console.log('Message sent: ' + info.response);
              }
          });

        }
      })
    })
  }

// EMAIL PRASEMINAR
  this.miconMail = function(to, nim, nama, topik, file) {

    connection.acquire(function(err, con) {
      con.query('SELECT * FROM dosen_mahasiswa_view WHERE nim = ?', [nim], function(err, result) {
        con.release();
        if(err) {
          throw err;
        }
        else {
          var dosen1 = result[0].dosen1;
          if(result[0].dosen2) var dosen2 = result[0].dosen2;
          else dosen2 = '';
          var penguji1 = result[0].penguji1;
          if(result[0].penguji2) var penguji2 = result[0].penguji2;
          else penguji2 = '';

          var mailOptions = {
            from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
            to: to,
            subject: 'File Seminar Mini Conference TA - '+nama,
            attachments: [
                {
                    path: './uploads/fileSeminar/micon/'+file
                }
            ],
            html:
            'Yth. Bapak/Ibu/Sdr Pembimbing Tugas Akhir dari mahasiswa berikut :<br><br>'+
            '<b>NIM : </b>'+nim+'<br>'+
            '<b>Nama : </b>'+nama+'<br>'+
            '<b>Topik TA : </b>'+topik+'<br>'+
            '<b>Pembimbing Tugas Akhir : </b>'+dosen1+', '+dosen2+'<br>'+
            '<b>Penguji : </b>'+penguji1+', '+penguji2+'<br><br>'+
            'Mahasiswa tersebut telah melakukan upload file makalah seminar <b><i>Mini Conference</b></i>, dan dapat diunduh pada link berikut :<br><br>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id/uploads/fileSeminar/micon/'+file+'">File Seminar Mini Conference '+nama+'</a> (terdiri dari makalah dan poster) <br><br>'+
            'Terima Kasih.<br>'+
            '----------------------<br>'+
            'SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>'
          };

          transporter.sendMail(mailOptions, function(error, info){
              if(error){
                  return console.log(error);
              }
              else {
                console.log('Message sent: ' + info.response);
              }
          });

        }
      })
    })


  }

// EMAIL KONFERENSI
  this.konferensiMail = function(to, nim, nama, topik, file, namaKonferensi, judulPaper, tempat, tanggal) {

    connection.acquire(function(err, con) {
      con.query('SELECT * FROM dosen_mahasiswa_view WHERE nim = ?', [nim], function(err, result) {
        con.release();
        if(err) {
          throw err;
        }
        else {
          var dosen1 = result[0].dosen1;
          if(result[0].dosen2) var dosen2 = result[0].dosen2;
          else dosen2 = '';
          var penguji1 = result[0].penguji1;
          if(result[0].penguji2) var penguji2 = result[0].penguji2;
          else penguji2 = '';

          var mailOptions = {
            from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
            to: to,
            subject: 'Seminar Konferensi TA - '+nama,
            attachments: [
                {
                    path: './uploads/fileSeminar/konferensi/'+file
                }
            ],
            html:
            'Yth. Bapak/Ibu/Sdr Pembimbing Tugas Akhir dari mahasiswa berikut :<br><br>'+
            '<b>NIM : </b>'+nim+'<br>'+
            '<b>Nama : </b>'+nama+'<br>'+
            '<b>Topik TA : </b>'+topik+'<br>'+
            '<b>Pembimbing Tugas Akhir : </b>'+dosen1+', '+dosen2+'<br>'+
            '<b>Penguji : </b>'+penguji1+', '+penguji2+'<br><br>'+
            'Mahasiswa tersebut telah melakukan <b><i>Seminar Konferensi</b></i> dengan detail berikut :<br><br>'+
            '<b>Nama Konferensi : </b>'+namaKonferensi+'<br>'+
            '<b>Judul Paper : </b>'+judulPaper+'<br>'+
            '<b>Tempat Konferensi : </b>'+tempat+'<br><br>'+
            '<b>Tanggal : </b>'+tanggal+'<br>'+
            '<b>Berkas Konferensi (terdiri dari makalah dan sertifikat konferensi) : </b>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id/uploads/fileSeminar/konferensi/'+file+'">Berkas Konferensi '+nama+'</a><br><br>'+
            'Terima Kasih.<br>'+
            '----------------------<br>'+
            'SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>'
          };

          transporter.sendMail(mailOptions, function(error, info){
              if(error){
                  return console.log(error);
              }
              else {
                console.log('Message sent: ' + info.response);
              }
          });


        }
      })
    })



  }

// EMAIL MANDIRI
  this.mandiriMail = function(to, nim, nama, topik, pembahas1, pembahas2, pembahas3, tempat, jam, tanggal, file) {
    connection.acquire(function(err, con) {
      con.query('SELECT * FROM dosen_mahasiswa_view WHERE nim = ?', [nim], function(err, result) {
        con.release();
        if(err) {
          throw err;
        }
        else {

          var attend = [];

          var dosen1 = result[0].dosen1;
          if(result[0].dosen1_email) {
            attend.push({nama: dosen1, email: result[0].dosen1_email});
          }

          else attend.push({nama: dosen1, email: result[0].dosen1_username+'@apps.ipb.ac.id'});

          if (result[0].dosen2) {
            var dosen2 = result[0].dosen2;
            if(result[0].dosen2_email) {
              attend.push({nama: dosen2, email: result[0].dosen2_email});
            }
            else attend.push({nama: dosen2, email: result[0].dosen2_username});
          }
          else dosen2 = '';

          var penguji1 = result[0].penguji1;
          if(result[0].penguji1_email) {
            attend.push({nama: penguji1, email: result[0].penguji1_email});
          }
          else attend.push({nama: penguji1, email: result[0].penguji1_username+'@apps.ipb.ac.id'});

          if(result[0].penguji2) {
            var penguji2 = result[0].penguji2;
            if(result[0].penguji2_email) {
              attend.push({nama: penguji2, email: result[0].penguji2_email});
            }
            else attend.push({nama: penguji2, email: result[0].penguji2_username+'@apps.ipb.ac.id'});
          }
          else penguji2 = '';

          var a = ics.buildEvent({
            uid: 'simeta.ilkom',
            start: tanggal+' '+jam,
            title: 'Seminar Mandiri - '+nama,
            description: 'Seminar Mandiri - '+nama,
            location: tempat,
            url: 'http://simeta.apps.cs.ipb.ac.id/',
            status: 'confirmed',
            attendees: attend,
          });

          var mailOptions = {
            from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
            to: 'miqdad.fawwaz@gmail.com',//attend,
            cc: ['irvan.rmdn@gmail.com', 'ridwanilkom@gmail.com'],
            subject: 'Seminar Mandiri TA - '+nama,
            attachments: [
                {
                    path: './uploads/fileSeminar/undangan/undangan.pdf'
                }
            ],
            icalEvent: {
                method: 'request',
                content: a
            },
            html:
            'Yth. Bapak/Ibu/Sdr Pembimbing Tugas Akhir atau Penguji dari mahasiswa berikut :<br><br>'+
            '<b>NIM : </b>'+nim+'<br>'+
            '<b>Nama : </b>'+nama+'<br>'+
            '<b>Topik TA : </b>'+topik+'<br>'+
            '<b>Pembimbing Tugas Akhir : </b>'+dosen1+', '+dosen2+'<br>'+
            '<b>Penguji : </b>'+penguji1+', '+penguji2+'<br><br>'+
            'Mahasiswa tersebut telah mendaftar <b><i>Seminar Mandiri</b></i> dengan detail berikut :<br><br>'+
            '<b>Waktu : </b>'+jam+', '+tanggal+'<br>'+
            '<b>Tempat : </b>'+tempat+'<br>'+
            '<b>Pembahas : </b>'+pembahas1+', '+pembahas2+', '+pembahas3+'<br>'+
            '<b>Makalah Seminar Mandiri : </b>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id/uploads/fileSeminar/mandiri/'+file+'">Makalah Seminar Mandiri '+nama+'</a><br><br>'+
            'Mohon Pembimbing Tugas Akhir atau Penguji dapat menghadiri seminar sesuai dengan jadwal tersebut.<br><br>'+
            'Terima Kasih.<br>'+
            '----------------------<br>'+
            'SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>'
          };

          transporter.sendMail(mailOptions, function(error, info){
              if(error){
                  return console.log(error);
              }
              else {
                console.log('Message sent: ' + info.response);
              }
          });

        }
      })
    })


  }

// EMAIL SIDANG
  this.sidangMail = function(nim, nama, topik, file, tempat, jam, tanggal) {

    connection.acquire(function(err, con) {
      con.query('SELECT * FROM dosen_mahasiswa_view WHERE nim = ?', [nim], function(err, result) {
        con.release();
        if(err) {
          throw err;
        }
        else {

          var attend = [];

          var dosen1 = result[0].dosen1;
          if(result[0].dosen1_email) {
            attend.push({nama: dosen1, email: result[0].dosen1_email});
          }
          else attend.push({nama: dosen1, email: result[0].dosen1_username+'@apps.ipb.ac.id'});

          if (result[0].dosen2) {
            var dosen2 = result[0].dosen2;
            if(result[0].dosen2_email) {
              attend.push({nama: dosen2, email: result[0].dosen2_email});
            }
            else attend.push({nama: dosen2, email: result[0].dosen2_username+'@apps.ipb.ac.id'});
          }
          else dosen2 = '';

          var penguji1 = result[0].penguji1;
          if(result[0].penguji1_email) {
            attend.push({nama: penguji1, email: result[0].penguji1_email});
          }
          else attend.push({nama: penguji1, email: result[0].penguji1_username+'@apps.ipb.ac.id'});

          if(result[0].penguji2) {
            var penguji2 = result[0].penguji2;
            if(result[0].penguji2_email) {
              attend.push({nama: penguji2, email: result[0].penguji2_email});
            }
            else attend.push({nama: penguji2, email: result[0].penguji2_username+'@apps.ipb.ac.id'});
          }
          else penguji2 = '';

          var a = ics.buildEvent({
            uid: 'simeta.ilkom',
            start: tanggal+' '+jam,
            title: 'Sidang Tugas Akhir - '+nama,
            description: 'Sidang Tugas Akhir - '+nama,
            location: tempat,
            url: 'http://simeta.apps.cs.ipb.ac.id/',
            status: 'confirmed',
            attendees: attend,
          });

          var mailOptions = {
            from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
            to: 'miqdad.fawwaz@gmail.com',
            subject: 'Pendaftaran Sidang TA - '+nama,
            attachments: [
                {
                    path: './uploads/fileSidang/undangan/undangan.pdf'
                }
            ],
            icalEvent: {
                method: 'request',
                content: a
            },
            html:
            'Yth. Bapak/Ibu/Sdr Pembimbing Tugas Akhir atau Penguji dari mahasiswa berikut :<br><br>'+
            '<b>NIM : </b>'+nim+'<br>'+
            '<b>Nama : </b>'+nama+'<br>'+
            '<b>Topik TA : </b>'+topik+'<br>'+
            '<b>Pembimbing Tugas Akhir : </b>'+dosen1+', '+dosen2+'<br>'+
            '<b>Penguji : </b>'+penguji1+', '+penguji2+'<br><br>'+
            'Mahasiswa tersebut telah mendaftar <b><i>Sidang Tugas Akhir</b></i> dengan detail berikut :<br><br>'+
            '<b>Waktu : </b>'+jam+', '+tanggal+'<br>'+
            '<b>Tempat : </b>'+tempat+'<br>'+
            '<b>Makalah Sidang : </b>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id/uploads/fileSidang/'+file+'">Makalah Sidang '+nama+'</a><br><br>'+
            'Mohon Pembimbing Tugas Akhir atau Penguji dapat menghadiri sidang sesuai dengan jadwal tersebut.<br><br>'+
            'Terima Kasih.<br>'+
            '----------------------<br>'+
            'SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>'+
            '<a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>'
          };

          transporter.sendMail(mailOptions, function(error, info){
              if(error){
                  return console.log(error);
              }
              else {
                console.log('Message sent: ' + info.response);
              }
          });

        }
      })
    })


  }

  // KONFIRMASI SIDANG
  this.setSidangPertama = function(res, data, token) {
    const queryString = querystring.stringify({nim: data.nim, token});
    const mailOptions = {
      from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
      to: 'miqdad.fawwaz@gmail.com',
      subject: 'Konfirmasi Pelaksanaan Sidang [PEMBIMBING KETUA]',
      html: `
        Yth. Bapak/Ibu <br>
        Dengan hormat, mahasiswa yang bertanda tangan di bawah ini: <br><br>
        <b>Nama </b> : ${data.nama} <br>
        <b>NIM </b> : ${data.nim} <br>
        <b>Jurusan  : </b> Ilmu Komputer, Fakultas Matematika dan Ilmu Pengetahuan Alam, Institut Pertanian Bogor <br>
        <b>Pembimbing Tugas Akhir </b> : ${data.dosen1} <br>
        <b>Topik Tugas Akhir</b> : ${data.topik} <br><br>
        Dengan ini mengajukan pelaksanaan kegiatan sidang hasil penelitian, yang akan dilaksanakan pada: <br><br>
        <b>Tanggal </b> : ${data.tanggal} <br>
        <b>Jam </b> : ${data.jam} <br>
        <b>Tempat </b> : ${data.tempat ? data.tempat : '(Akan ditentukan oleh komisi pendidikan)'} <br><br>
        Maka dari itu, dimohon kepada Bapak/Ibu untuk memberikan konfirmasi dengan mengeklik tombol di bawah ini. <br><br>
        <a target="_blank" style="text-align: center;" href="http://localhost:2016/sidang/konfirmasi/pertama/${queryString}"><button style="background-color:#3498db; margin-right: 20px; padding: 15px 32px; font-size: 16px; color:white">Diterima</button></a>
        <a target="_blank" style="text-align: center;" href="http://localhost:2016/sidang/konfirmasi/pertama/delete/${queryString}"><button style="background-color: red; padding: 15px 32px; font-size: 16px; color:white">Ditolak</button></a><br><br>
        Terima kasih. <br>
        SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
        <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
      `
    }
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
        throw error;
      } else {
        console.log('Message sent! '+ info.response);
      }
    });
  }

  this.setSidang = function(res, data, token) {
    const emails = data.email;
    emails.forEach((email, index) => {
      if (email) {
        const queryString = querystring.stringify({nim: data.nim, token, email});
        const mailOptions = {
          from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
          to: 'miqdad.fawwaz@gmail.com', //email
          subject: `Konfirmasi Pelaksanaan Sidang ${index === 0 ? '[PEMBIMBING ANGGOTA]' : index === 1 ? '[PENGUJI PERTAMA]' : index === 2 ? '[PENGUJI KEDUA]' : ''}`,
          html: `
            Yth. Bapak/Ibu <br>
            Dengan hormat, mahasiswa yang bertanda tangan di bawah ini: <br><br>
            <b>Nama </b> : ${data.nama} <br>
            <b>NIM </b> : ${data.nim} <br>
            <b>Jurusan </b> : Ilmu Komputer, Fakultas Matematika dan Ilmu Pengetahuan Alam, Institut Pertanian Bogor <br>
            <b>Topik Tugas Akhir</b> : ${data.topik}<br><br>
            <b>Pembimbing Ketua </b> : ${data.namaDosen1} <br>
            <b>Pembimbing Anggota </b> : ${data.namaDosen2 ? data.namaDosen2 : '-'} <br>
            <b>Penguji Pertama </b> : ${data.namaPenguji1 ? data.namaPenguji1 : '-'} <br>
            <b>Penguji Kedua </b> : ${data.namaPenguji2 ? data.namaPenguji2 : '-'} <br><br>
            Dengan ini mengajukan pelaksanaan kegiatan pelaksanaan, yang akan dilaksanakan pada: <br><br>
            <b>Tanggal </b> : ${data.tanggal} <br>
            <b>Jam </b> : ${data.jam} <br>
            <b>Tempat </b> : ${data.tempat ? data.tempat : '(Akan ditentukan oleh komisi pendidikan)'} <br><br>
            Maka dari itu, dimohon kepada Bapak/Ibu untuk memberikan konfirmasi dengan mengklik tombol di bawah ini. <br><br>
            <a target="_blank" style="text-align: center;" href="http://localhost:2016/sidang/konfirmasi/${queryString}/"><button style="background-color:#3498db; margin-right: 20px; padding: 15px 32px; font-size: 16px; color:white">Diterima</button></a>
            <a target="_blank" style="text-align: center;" href="http://localhost:2016/sidang/konfirmasi/delete/${queryString}/"><button style="background-color: red; padding: 15px 32px; font-size: 16px; color:white">Ditolak</button></a><br><br>
            Terima kasih. <br>
            SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
            <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
          `
        };
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return console.log(error);
            throw error;
          } else {
            console.log('Message sent! '+ info.response);
          }
        });
      }
    });
  }

  this.declineSidang = function(res, data) {
    const mailOptions = {
      from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
      to: 'miqdad.fawwaz@gmail.com',
      subject: 'Konfirmasi Pelaksanaan Sidang',
      html: `
        Yth ${data.nama} <br><br>
        Dengan surel ini kami informasikan bahwa pengajuan pelaksanaan sidang Anda telah ditolak oleh 
        ${data.statusDosen === 'Dosen pembimbing ketua' ? 'dosen pembimbing ketua' : data.statusDosen === 'Dosen pembimbing anggota' ? 'dosen pembimbing anggota' : data.statusDosen === 'Penguji pertama' ? 'penguji pertama' : data.statusDosen === 'Penguji kedua' ? 'penguji kedua' : 'komisi pendidikan'} 
        dengan alasan <b>"${data.alasan ? data.alasan : 'HUBUNGI PEMBIMBING'}"</b>. 
        Silahkan lakukan pengajuan kembali melalui laman <a href="http://simeta.apps.cs.ipb.ac.id">SIMETA</a>.<br><br>
        Terima kasih. <br>
        SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
        <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
      `
    };
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
        throw error;
      } else {
        console.log('Message sent! '+ info.response);
      }
    });
  }

  this.setSidangSukses = function(res, data) {
    let mailOptions;
    if (data.type === 'pertama') {
      mailOptions = {
        from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
        to: 'miqdad.fawwaz@gmail.com',
        subject: 'Konfirmasi Seminar Mandiri',
        html: `
          Kepada ${data.nama} <br><br>
          Dengan surel ini kami informasikan bahwa permohonan jadwal sidang Anda telah berhasil <b>diverifikasi</b>. Silahkan unggah
          makalah sidang pada <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">SIMETA</a>, serta lengkapi persyaratan berkas-berkas seminar mandiri yang kemudian dapat dibawa ke kantor TU Depertemen Ilmu Komputer IPB.<br><br>
          Terima kasih. <br>
          SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
          <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
        `
      };
    } else {
      mailOptions = {
        from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
        to: 'miqdad.fawwaz@gmail.com',
        subject: 'Konfirmasi Pelaksanaan Sidang',
        html: `
          Kepada ${data.nama} <br><br>
          Dengan surel ini kami informasikan bahwa permohonan pengajuan pelaksanaan sidang Anda telah <b>diterima</b>. Berikut detail data pelaksanaan sidang Anda: <br><br>
          <b>Tempat </b> : ${data.tempat} <br>
          <b>Tanggal </b> : ${data.tanggal} <br>
          <b>Jam </b> : ${data.jam} <br><br>
          <b>Penguji 1 </b> : ${data.penguji1 && data.konfirmasiPenguji1 === 1 ? data.penguji1 : '-'} <br>
          <b>Penguji 2 </b> : ${data.penguji2 && data.konfirmasiPenguji2 === 1 ? data.penguji2 : '-'} <br><br>
          Terima kasih. <br>
          SIMETA-ILKOM, Dept. Ilmu Komputer, FMIPA IPB<br>
          <a target="_blank" href="http://simeta.apps.cs.ipb.ac.id">http://simeta.apps.cs.ipb.ac.id</a>
        `
      };
    }

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
        throw error;
      } else {
        console.log('Message sent! '+ info.response);
      }
    });
  }
}

module.exports = new Mail();