var express = require('express');
var bodyparser = require('body-parser');
var connection = require('./connection');
var routes = require('./routes');
var jwt = require('jsonwebtoken');
var app = express();
var uploads = require('./uploads');
var admin = require('./admin');
var dosen = require('./dosen');
var mahasiswa = require('./mahasiswa');
var cors = require('cors');
var directRoutes = require('./directRoute');

app.use(cors());
directRoutes.configure(app);

// CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.setHeader('Access-Control-Allow-Credentials', true);

    if(req.headers['authorization']) {
        var token = req.headers['authorization'];
    }

    if (!req.headers['authorization']) {
        res.json({status: false, message: 'No token provided'})
    }
    else {
        jwt.verify(token, 'yangPentingPanjang', function(err, decoded) {
            if (err){
                res.json({status: false, message: 'Token authorization failed'});
                throw err;
            }
            else {
                var decoded = jwt.verify(token, 'yangPentingPanjang');
                var nim = decoded.nim;
                var username = decoded.username;

                exports.decoded = decoded;
                next();
            }
        });
    }
}

app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
app.use(allowCrossDomain);

app.get('/', function (req, res) {
  res.send('Hello World!');
});

connection.init();
routes.configure(app);
uploads.configure(app);
admin.configure(app);
dosen.configure(app);
mahasiswa.configure(app);

var server = app.listen(2016, function() {
  console.log('Server listening on port ' + server.address().port);
});
