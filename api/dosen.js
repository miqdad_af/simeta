var connection = require('./connection');
var _ = require('./simak');
var fc = require('./fc');

var noAuth = {status: false, message: "You Can't Access This Service"};
var queryError = {status: false, message: 'Query Error'};

module.exports = {
  configure: function(app) {

    app.get('/dosen/kolokium', function(req, res) {
      if(fc.checkAdminDosen()) {
        connection.acquire(function(err, con) {
          var query = 'select tahun_masuk, makalah from mahasiswa_kolokium_view where dosen_1 = ? or dosen_2 = ? and (tahun_masuk < YEAR(CURDATE()) - 3 and tahun_masuk > YEAR(CURDATE()) - 6)  order by tahun_masuk asc';
          var data = [_.decoded.id, _.decoded.id];
          con.query(query, data, function(err, result) {
            con.release();
            if (err) {
                res.json(queryError);
                throw err;
            }
            else {
              res.json(fc.countKolokiumPraseminar(result));
            }
          })
        })
      }
      else {
        res.json(noAuth);
      }
    });

    app.get('/dosen/praseminar', function(req, res) {
      if(fc.checkAdminDosen()) {
        connection.acquire(function(err, con) {
          var query = 'select tahun_masuk, makalah from mahasiswa_praseminar_view where dosen_1 = ? or dosen_2 = ? and (tahun_masuk < YEAR(CURDATE()) - 3 and tahun_masuk > YEAR(CURDATE()) - 6) order by tahun_masuk asc';
          var data = [_.decoded.id, _.decoded.id];
          con.query(query, data, function(err, result) {
            con.release();
            if (err) {
                res.json(queryError);
                throw err;
            }
            else {
              res.json(fc.countKolokiumPraseminar(result));
            }
          })
        })
      }
      else {
        res.json(noAuth);
      }
    });

    app.get('/dosen/sidang', function(req, res) {
      if(fc.checkAdminDosen()) {
        connection.acquire(function(err, con) {
          var query = 'select tahun_masuk, makalah from mahasiswa_sidang_view where dosen_1 = ? or dosen_2 = ? and (tahun_masuk < YEAR(CURDATE()) - 3 and tahun_masuk > YEAR(CURDATE()) - 6) order by tahun_masuk asc';
          var data = [_.decoded.id, _.decoded.id];
          con.query(query, data, function(err, result) {
            con.release();
            if (err) {
                res.json(queryError);
                throw err;
            }
            else {
              res.json(fc.countKolokiumPraseminar(result));
            }
          })
        })
      }
      else {
        res.json(noAuth);
      }
    });

    app.get('/dosen/seminar', function(req, res) {
      if(fc.checkAdminDosen()) {
        connection.acquire(function(err, con) {
          var query = 'select * from mahasiswa_seminar_view where dosen_1 = ? or dosen_2 = ? and (tahun_masuk < YEAR(CURDATE()) - 3 and tahun_masuk > YEAR(CURDATE()) - 6) order by tahun_masuk asc';
          var data = [_.decoded.id, _.decoded.id];
          con.query(query, data, function(err, result) {
            con.release();
            if (err) {
                res.json(queryError);
                throw err;
            }
            else {
              res.json(fc.countSeminar(result));
            }
          })
        })
      }
      else {
        res.json(noAuth);
      }
    });

    app.get('/dosen/skl', function(req, res) {
      if(fc.checkAdminDosen()) {
        connection.acquire(function(err, con) {
          var query = 'select tahun_masuk, berkas from mahasiswa_skl_view where dosen_1 = ? or dosen_2 = ? and (tahun_masuk < YEAR(CURDATE()) - 3 and tahun_masuk > YEAR(CURDATE()) - 6) order by tahun_masuk asc';
          var data = [_.decoded.id, _.decoded.id];
          con.query(query, data, function(err, result) {
            con.release();
            if (err) {
                res.json(queryError);
                throw err;
            }
            else {
              res.json(fc.countSKL(result));
            }
          })
        })
      }
      else {
        res.json(noAuth);
      }
    });

    app.get('/dosen/summary', function(req, res) {
      if(fc.checkAdminDosen()) {
        connection.acquire(function(err, con) {
          var query = 'select * from mahasiswa_summary where dosen_1 = ? or dosen_2 = ? and (tahun_masuk < YEAR(CURDATE()) - 3 and tahun_masuk > YEAR(CURDATE()) - 6) order by tahun_masuk asc';
          var data = [_.decoded.id, _.decoded.id];
          con.query(query, data, function(err, result) {
            con.release();
            if (err) {
                res.json(queryError);
                throw err;
            }
            else {
              res.json(fc.countSummary(result));
            }
          })
        })
      }
      else {
        res.json(noAuth);
      }
    });

  }
}
