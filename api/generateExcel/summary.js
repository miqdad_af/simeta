var connection = require('../connection');
var nodeExcel = require('excel-export');

var fs = require('fs');
var jwt = require('jsonwebtoken');

function SummaryExcel(){

  this.get = function(id, res) {

    var token = id;

    if (!token){
      res.json({status: false, message: 'No token provided'})
    }
    else {

      jwt.verify(token, 'yangPentingPanjang', function(err, decoded) {
        if (err){
          res.json({status: false, message: 'Token authorization failed'});
          throw err;
        }
        else {
          var decoded = jwt.verify(token, 'yangPentingPanjang');
          var role = decoded.role;

          if (role == 1 || role == 4) {
            connection.acquire(function(err, con) {
              con.query('select * from mahasiswa_summary where tahun_masuk < YEAR(CURDATE()) - 3 order by nim asc', function(err, rows) {
                con.release();

                var conf={}
                conf.cols=[
                {
                    caption:'Tahun Masuk',
                    type:'string',
                    width:10
                },
                {
                    caption:'NIM',
                    type:'string',
                    width:10
                },
                {
                    caption:'Nama',
                    type:'string',
                    width:28.7109375
                },
                {
                    caption:'Pembimbing 1',
                    type:'string',
                    width:28.7109375
                },
                {
                    caption:'Pembimbing 2',
                    type:'string',
                    width:28.7109375
                },
                {
                    caption:'Penguji 1',
                    type:'string',
                    width:28.7109375
                },
                {
                    caption:'Penguji 2',
                    type:'string',
                    width:28.7109375
                },
                {
                    caption:'Kolokium',
                    type:'string',
                    width:10
                },
                {
                    caption:'Praseminar',
                    type:'string',
                    width:10
                },
                {
                    caption:'Seminar',
                    type:'string',
                    width:10
                },
                {
                    caption:'Sidang',
                    type:'string',
                    width:10
                },
                {
                    caption:'SKL',
                    type:'string',
                    width:10
                },
                {
                    caption:'Tahap Terakhir',
                    type:'string',
                    width:10
                }
                ];

                arr=[];
                for(i=0;i<rows.length;i++){
                    nim = rows[i].nim;
                    tahun_masuk = rows[i].tahun_masuk;
                    nama = rows[i].nama;
                    dosen1 = rows[i].dosen1;
                    dosen2 = rows[i].dosen2;
                    penguji1 = rows[i].penguji1;
                    penguji2 = rows[i].penguji2;
                    kolokium = rows[i].tanggal_kolokium;
                    praseminar = rows[i].tanggal_praseminar;
                    seminar = rows[i].tanggal_seminar;
                    sidang = rows[i].tanggal_sidang;
                    skl = rows[i].tanggal_skl;
                    topik = rows[i].topik;

                    if(skl) {
                      akhir = 'SKL - '+skl;
                    }
                    else if(sidang) {
                      akhir = 'Sidang - '+sidang;
                    }
                    else if(seminar) {
                      akhir = 'Seminar - '+seminar;
                    }
                    else if(praseminar) {
                      akhir = 'Praseminar - '+praseminar;
                    }
                    else if(kolokium) {
                      akhir = 'Kolokium - '+kolokium;
                    }
                    else if(topik){
                      akhir = 'Penentuan TA';
                    }
                    else {
                      akhir = 'Belum TA';
                    }

                    a=[tahun_masuk, nim, nama, dosen1, dosen2, penguji1, penguji2, kolokium, praseminar, seminar, sidang, skl, akhir];
                    arr.push(a);
                }
                conf.rows = arr;

                var result = nodeExcel.execute(conf);
                res.setHeader('Content-Type','application/vnd.openxmlformates');
                res.setHeader("Content-Disposition","attachment;filename="+"Summary TA.xlsx");
                res.end(result,'binary');

                res.send(rows);

              });
            });

          }

          else {
            res.json({status: false, message: "You Can't Access This Service"});
          }

        }
      });

    }

  };

  this.getDosen = function(id, res) {

    var token = id;

    if (!token){
      res.json({status: false, message: 'No token provided'})
    }
    else {

      jwt.verify(token, 'yangPentingPanjang', function(err, decoded) {
        if (err){
          res.json({status: false, message: 'Token authorization failed'});
          throw err;
        }
        else {
          var decoded = jwt.verify(token, 'yangPentingPanjang');
          var role = decoded.role;

          if (role == 4 || role == 2) {
            connection.acquire(function(err, con) {
              con.query('select * from mahasiswa_summary where tahun_masuk < YEAR(CURDATE()) - 3 and (dosen_1 = ? or dosen_2 = ?) order by nim asc', [decoded.id, decoded.id], function(err, rows) {
                con.release();

                var conf={}
                conf.cols=[
                {
                    caption:'Tahun Masuk',
                    type:'string',
                    width:10
                },
                {
                    caption:'NIM',
                    type:'string',
                    width:10
                },
                {
                    caption:'Nama',
                    type:'string',
                    width:28.7109375
                },
                {
                    caption:'Pembimbing 1',
                    type:'string',
                    width:28.7109375
                },
                {
                    caption:'Pembimbing 2',
                    type:'string',
                    width:28.7109375
                },
                {
                    caption:'Penguji 1',
                    type:'string',
                    width:28.7109375
                },
                {
                    caption:'Penguji 2',
                    type:'string',
                    width:28.7109375
                },
                {
                    caption:'Kolokium',
                    type:'string',
                    width:10
                },
                {
                    caption:'Praseminar',
                    type:'string',
                    width:10
                },
                {
                    caption:'Seminar',
                    type:'string',
                    width:10
                },
                {
                    caption:'Sidang',
                    type:'string',
                    width:10
                },
                {
                    caption:'SKL',
                    type:'string',
                    width:10
                },
                {
                    caption:'Tahap Terakhir',
                    type:'string',
                    width:10
                }
                ];

                arr=[];
                for(i=0;i<rows.length;i++){
                    nim = rows[i].nim;
                    tahun_masuk = rows[i].tahun_masuk;
                    nama = rows[i].nama;
                    dosen1 = rows[i].dosen1;
                    dosen2 = rows[i].dosen2;
                    penguji1 = rows[i].penguji1;
                    penguji2 = rows[i].penguji2;
                    kolokium = rows[i].tanggal_kolokium;
                    praseminar = rows[i].tanggal_praseminar;
                    seminar = rows[i].tanggal_seminar;
                    sidang = rows[i].tanggal_sidang;
                    skl = rows[i].tanggal_skl;
                    topik = rows[i].topik;

                    if(skl) {
                      akhir = 'SKL - '+skl;
                    }
                    else if(sidang) {
                      akhir = 'Sidang - '+sidang;
                    }
                    else if(seminar) {
                      akhir = 'Seminar - '+seminar;
                    }
                    else if(praseminar) {
                      akhir = 'Praseminar - '+praseminar;
                    }
                    else if(kolokium) {
                      akhir = 'Kolokium - '+kolokium;
                    }
                    else if(topik){
                      akhir = 'Penentuan TA';
                    }
                    else {
                      akhir = 'Belum TA';
                    }

                    a=[tahun_masuk, nim, nama, dosen1, dosen2, penguji1, penguji2, kolokium, praseminar, seminar, sidang, skl, akhir];
                    arr.push(a);
                }
                conf.rows = arr;

                var result = nodeExcel.execute(conf);
                res.setHeader('Content-Type','application/vnd.openxmlformates');
                res.setHeader("Content-Disposition","attachment;filename="+"Summary TA.xlsx");
                res.end(result,'binary');

                res.send(rows);

              });
            });

          }

          else {
            res.json({status: false, message: "You Can't Access This Service"});
          }

        }
      });

    }

  };


}

module.exports = new SummaryExcel();
