var connection = require('../connection');
var nodeExcel = require('excel-export');

var fs = require('fs');
var jwt = require('jsonwebtoken');

var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu'];
var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

function SummaryExcel2(){

  this.get = function(id, res) {

    var token = id;

    if (!token){
      res.json({status: false, message: 'No token provided'})
    }
    else {

      jwt.verify(token, 'yangPentingPanjang', function(err, decoded) {
        if (err){
          res.json({status: false, message: 'Token authorization failed'});
          throw err;
        }
        else {
          var decoded = jwt.verify(token, 'yangPentingPanjang');
          var role = decoded.role;

          if (role == 1 || role == 4) {

            var conf={};
            conf.cols=[
              {
                  caption:'No.',
                  type:'number',
                  width:10
              },
              {
                  caption:'Nama Mahasiswa',
                  type:'string',
                  width:28.7109375
              },
              {
                  caption:'NIM',
                  type:'string',
                  width:10
              },
              {
                  caption:'Hari Seminar',
                  type:'string',
                  width:15
              },
              {
                  caption:'Tanggal Seminar',
                  type:'string',
                  width:15
              },
              {
                  caption:'Waktu Seminar',
                  type:'string',
                  width:15
              },
              {
                  caption:'Judul Seminar',
                  type:'string',
                  width:28.7109375
              },
              {
                  caption:'Hari Sidang',
                  type:'string',
                  width:15
              },
              {
                  caption:'Tanggal Sidang',
                  type:'string',
                  width:15
              },
              {
                  caption:'Waktu Sidang',
                  type:'string',
                  width:15
              },
              {
                  caption:'Judul Skripsi',
                  type:'string',
                  width:28.7109375
              },
              {
                  caption:'Tempat Seminar / Sidang',
                  type:'string',
                  width:15
              },
              {
                  caption:'Pembimbing 1',
                  type:'string',
                  width:28.7109375
              },
              {
                  caption:'Pembimbing 2',
                  type:'string',
                  width:28.7109375
              },
              {
                  caption:'Penguji 1',
                  type:'string',
                  width:28.7109375
              },
              {
                  caption:'Penguji 2',
                  type:'string',
                  width:28.7109375
              },
              {
                  caption:'No. Telp / HP',
                  type:'string',
                  width:10
              },
              {
                  caption:'Email',
                  type:'string',
                  width:28.7109375
              },
              {
                  caption:'Pembahas 1',
                  type:'string',
                  width:28.7109375
              },
              {
                  caption:'Pembahas 2',
                  type:'string',
                  width:28.7109375
              },
              {
                  caption:'Pembahas 3',
                  type:'string',
                  width:28.7109375
              },
            ];

            arr = [];
            counter = 0;
            connection.acquire(function(err, con) {
              con.query('select * from seminar_sidang_view where tahun_masuk < YEAR(CURDATE()) - 3 order by nim asc', function(err, rows) {

                  for(i = 0; i < rows.length; i++) {
                    counter++;
                    nama = rows[i].nama;
                    nim = rows[i].nim;
                    email = rows[i].email;
                    hp = rows[i].hp;
                    topik = rows[i].topik;

                    dosen1 = rows[i].dosen1;
                    dosen2 = rows[i].dosen2;
                    penguji1 = rows[i].penguji1;
                    penguji2 = rows[i].penguji2;

                    if(rows[i].tanggal_seminar) {
                        d1 = new Date(rows[i].tanggal_seminar);
                        tanggalSeminar = d1.getDate()+' '+bulan[d1.getMonth()]+' '+d1.getFullYear();
                        hariSeminar = days[d1.getDay()];
                    }
                    else {
                        tanggalSeminar = '';
                        hariSeminar = '';
                    }

                    jenis = rows[i].jenis_seminar;
                    if(jenis == 1) {
                        tempatSeminar = rows[i].konferensi_tempat;
                        jamSeminar = rows[i].konferensi_jam;
                    }
                    else if(jenis == 2) {
                        tempatSeminar = rows[i].micon_tempat;
                        jamSeminar = rows[i].micon_jam;
                    }
                    else if(jenis == 3) {
                        tempatSeminar = rows[i].mandiri_tempat;
                        jamSeminar = rows[i].mandiri_jam;
                    }
                    else {
                        tempatSeminar = '';
                        jamSeminar = '';
                    }

                      if(rows[i].tanggal_sidang) {
                          d2 = new Date(rows[i].tanggal_sidang);
                          tanggalSidang = d2.getDate()+' '+bulan[d2.getMonth()]+' '+d2.getFullYear();
                          hariSidang = days[d2.getDay()];
                      }
                      else {
                          tanggalSidang = '';
                          hariSidang = '';
                      }

                    jamSidang = rows[i].jam_sidang;

                    if(rows[i].tempat_sidang) tempatSidang = ' / '+rows[i].tempat_sidang;
                    else tempatSidang = '';

                    if(rows[i].pembahas1) pembahas1 = rows[i].pembahas1+' / '+rows[i].nim1;
                    else pembahas1 = '';

                    if(rows[i].pembahas2) pembahas2 = rows[i].pembahas2+' / '+rows[i].nim2;
                    else pembahas2 = '';

                    if(rows[i].pembahas3) pembahas3 = rows[i].pembahas3+' / '+rows[i].nim3;
                    else pembahas3 = '';

                    a=[counter, nama, nim, hariSeminar, tanggalSeminar, jamSeminar, topik, hariSidang, tanggalSidang, jamSidang, topik, tempatSeminar+tempatSidang, dosen1, dosen2, penguji1, penguji2, hp, email, pembahas1, pembahas2, pembahas3];
                    arr.push(a);
                  }

                  conf.rows = arr;

                  var result = nodeExcel.execute(conf);
                  res.setHeader('Content-Type','application/vnd.openxmlformates');
                  res.setHeader("Content-Disposition","attachment;filename="+"Seminar Sidang-TA.xlsx");
                  res.end(result,'binary');

                  res.send(rows);

              });
            });

          }

          else {
            res.json({status: false, message: "You Can't Access This Service"});
          }

        }
      });

    }

  };
  //
  // this.get = function(res) {
  //
  //   var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJTSU0gSWxrb20iLCJzdWIiOiJpdmFubWF1bGFuYUBhcHBzLmlwYi5hYy5pZCIsImlkIjoiMjUiLCJ1c2VybmFtZSI6ImltYXMuc2l0YW5nZ2FuZyIsIm5pcCI6IjE5NzUwMTMwIDE5OTgwMiAyIDAwMSIsIm5hbWEiOiJEci4gSW1hcyBTdWthZXNpaCBTaXRhbmdnYW5nLCBTLlNpLCBNLktvbSIsImlhdCI6MTQ4ODk4MTQzOCwicm9sZSI6NH0.DcYiEPWAJShkCsHdw6rjy6QA1Jofid0wZ5dj-v6_Jgw';
  //
  //   if (!token){
  //     res.send({status: false, message: 'No token provided'})
  //   }
  //   else {
  //
  //     jwt.verify(token, 'yangPentingPanjang', function(err, decoded) {
  //       if (err){
  //         throw err;
  //         res.send({status: false, message: 'Token authorization failed'});
  //       }
  //       else {
  //         var decoded = jwt.verify(token, 'yangPentingPanjang');
  //         var role = decoded.role;
  //
  //         if (role == 1 || role == 4) {
  //
  //           var conf={};
  //           conf.cols=[
  //             {
  //                 caption:'No.',
  //                 type:'string',
  //                 width:10
  //             },
  //             {
  //                 caption:'Nama Mahasiswa',
  //                 type:'string',
  //                 width:28.7109375
  //             },
  //             {
  //                 caption:'NIM',
  //                 type:'string',
  //                 width:10
  //             },
  //             {
  //                 caption:'Hari Seminar',
  //                 type:'string',
  //                 width:15
  //             },
  //             {
  //                 caption:'Tanggal Seminar',
  //                 type:'string',
  //                 width:15
  //             },
  //             {
  //                 caption:'Waktu Seminar',
  //                 type:'string',
  //                 width:15
  //             },
  //             {
  //                 caption:'Judul Seminar',
  //                 type:'string',
  //                 width:28.7109375
  //             },
  //             {
  //                 caption:'Hari Sidang',
  //                 type:'string',
  //                 width:15
  //             },
  //             {
  //                 caption:'Tanggal Sidang',
  //                 type:'string',
  //                 width:15
  //             },
  //             {
  //                 caption:'Waktu Sidang',
  //                 type:'string',
  //                 width:15
  //             },
  //             {
  //                 caption:'Judul Skripsi',
  //                 type:'string',
  //                 width:28.7109375
  //             },
  //             {
  //                 caption:'Tempat Seminar / Sidang',
  //                 type:'string',
  //                 width:15
  //             },
  //             {
  //                 caption:'Pembimbing 1',
  //                 type:'string',
  //                 width:28.7109375
  //             },
  //             {
  //                 caption:'Pembimbing 2',
  //                 type:'string',
  //                 width:28.7109375
  //             },
  //             {
  //                 caption:'Penguji 1',
  //                 type:'string',
  //                 width:28.7109375
  //             },
  //             {
  //                 caption:'Penguji 2',
  //                 type:'string',
  //                 width:28.7109375
  //             },
  //             {
  //                 caption:'No. Telp / HP',
  //                 type:'string',
  //                 width:10
  //             },
  //             {
  //                 caption:'Email',
  //                 type:'string',
  //                 width:28.7109375
  //             },
  //             {
  //                 caption:'Pembahas 1',
  //                 type:'string',
  //                 width:28.7109375
  //             },
  //             {
  //                 caption:'Pembahas 2',
  //                 type:'string',
  //                 width:28.7109375
  //             },
  //             {
  //                 caption:'Pembahas 3',
  //                 type:'string',
  //                 width:28.7109375
  //             },
  //           ];
  //
  //           arr=[];
  //           var jenis;
  //           var data;
  //           var counter = 0;
  //           connection.acquire(function(err, con) {
  //             con.query('select * from mahasiswa_seminar_view where tahun_masuk < YEAR(CURDATE()) - 3 order by nim asc', function(err, rows) {
  //
  //                 for(i=0;i<rows.length;i++){
  //                     counter++;
  //
  //                     nama = rows[i].nama;
  //                     nim = rows[i].nim;
  //                     topik = rows[i].topik;
  //
  //                     dosen1 = rows[i].dosen1;
  //                     dosen2 = rows[i].dosen2;
  //                     penguji1 = rows[i].penguji1;
  //                     penguji2 = rows[i].penguji2;
  //                     jenis = rows[i].jenis_seminar;
  //
  //                         con.query('select * from mahasiswa_detail where nim = ?', [rows[i].nim], function(err, rows1)  {
  //
  //                         // console.log(rows1);
  //                         email = rows1[i].email;
  //                         // else email = '';
  //
  //                         email = '';
  //                         hp = '';
  //
  //                         // if(rows1[i].hp == '') hp = rows1[i].hp;
  //                         // else hp = '';
  //
  //
  //                         var query = [
  //                             '',
  //                             'select * from ta_konferensi where nim = ?',
  //                             'select * from ta_micon where nim = ?',
  //                             'SELECT m.nim, nim1, nim2, nim3, pembahas1, pembahas2, pembahas3, m.tempat, m.tanggal, m.jam FROM `ta_mandiri` as m, pembahas_view as p where m.nim = p.nim and m.nim = ?'
  //                         ];
  //                         data = query[jenis];
  //
  //                         if(jenis == 3) {
  //                             pembahas1 = rows1[i].pembahas1;
  //                             pembahas2 = rows1[i].pembahas2;
  //                             pembahas3 = rows1[i].pembahas3;
  //
  //                             nim1 = rows1[i].nim1;
  //                             nim2 = rows1[i].nim2;
  //                             nim3 = rows1[i].nim3;
  //                         }
  //                         else {
  //                             pembahas1 = '';
  //                             pembahas2 = '';
  //                             pembahas3 = '';
  //
  //                             nim1 = '';
  //                             nim2 = '';
  //                             nim3 = '';
  //                         }
  //
  //                         if(jenis == 'NULL') {
  //
  //                         }
  //                         else {
  //                             con.query(data, [nim], function(err, rows2) {
  //
  //                                 console.log(rows2);
  //                                 tanggalSeminar = rows2[i].tanggal;
  //                                 tempatSeminar = rows2[i].tempat;
  //                                 jamSeminar = rows2[i].jam;
  //
  //                                 d = new Date(tanggalSeminar);
  //
  //                                 hariSeminar = days[d.getDay()];
  //                                 tanggalSeminarFix = d.getDate()+' '+bulan[d.getMonth()]+' '+d.getFullYear();
  //
  //                                 con.query('select * from ta_sidang where nim = ?', [nim], function(err, rows3) {
  //
  //                                     tanggalSidang = rows3[i].tanggal;
  //                                     tempatSidang = rows3[i].tempat;
  //                                     jamSidang = rows3[i].jam;
  //
  //                                     d2 = new Date(tanggalSidang);
  //
  //                                     hariSidang = days[d2.getDay()];
  //                                     tanggalSidangFix = d2.getDate()+' '+bulan[d2.getMonth()]+' '+d2.getFullYear();
  //
  //                                     if(pembahas1) pembahas_1 = pembahas1+' / '+nim1;
  //                                     if(pembahas2) pembahas_2 = pembahas2+' / '+nim2;
  //                                     if(pembahas3) pembahas_3 = pembahas3+' / '+nim3;
  //
  //                                     a = [counter, nama, nim, hariSeminar, tanggalSeminarFix, jamSeminar, topik, hariSidang, tanggalSidangFix, jamSidang, topik, tempatSeminar+' / '+tempatSidang, dosen1, dosen2, penguji1, penguji2, hp, email, pembahas_1, pembahas_2, pembahas_3];
  //                                     arr.push(a);
  //
  //                                     if(counter == rows.length) {
  //                                         con.release();
  //
  //                                         conf.rows = arr;
  //
  //                                         var result = nodeExcel.execute(conf);
  //                                         res.setHeader('Content-Type','application/vnd.openxmlformates');
  //                                         res.setHeader("Content-Disposition","attachment;filename="+"Summary TA.xlsx");
  //                                         res.end(result,'binary');
  //
  //                                         res.send(rows);
  //                                     }
  //
  //                                 })
  //                             })
  //                         }
  //                     })
  //                 }
  //
  //
  //
  //             });
  //           });
  //
  //         }
  //
  //         else {
  //           res.send({status: false, message: "You Can't Access This Service"});
  //         }
  //
  //       }
  //     });
  //
  //   }
  //
  // };

}

module.exports = new SummaryExcel2();
