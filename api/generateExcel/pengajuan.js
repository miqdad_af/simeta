var connection = require('../connection');
var nodeExcel = require('excel-export');

var xlsx = require('node-xlsx');
var fs = require('fs');
var jwt = require('jsonwebtoken');

function PengajuanExcel(){

  this.get = function(id, res) {

    var token = id;

    if (!token){
      res.json({status: false, message: 'No token provided'})
    }
    else {

      jwt.verify(token, 'yangPentingPanjang', function(err, decoded) {
        if (err){
          res.json({status: false, message: 'Token authorization failed'});
          throw err;
        }
        else {
          var decoded = jwt.verify(token, 'yangPentingPanjang');
          var role = decoded.role;

          if (role == 1 || role == 4) {
            connection.acquire(function(err, con) {
              con.query('select * from ta_daftar_view order by nim asc', function(err, rows) {
                con.release();

                var conf={}
                conf.cols=[
                {
                    caption:'NIM',
                    type:'string',
                    width:10
                },
                {
                    caption:'Nama',
                    type:'string',
                    width:30
                },
                {
                    caption:'Topik',
                    type:'string',
                    width:40
                },
                {
                    caption:'Lab',
                    type:'string',
                    width:40
                },
                {
                    caption:'Dosen Pilihan 1',
                    type:'string',
                    width:50
                },
                {
                    caption:'Pertemuan Dosen 1',
                    type:'number',
                    width:5
                },
                {
                    caption:'Dosen Pilihan 2',
                    type:'string',
                    width:50
                },
                {
                    caption:'Pertemuan Dosen 2',
                    type:'number',
                    width:5
                },
                {
                    caption:'Belum Melakukan Apa-Apa',
                    type:'string',
                    width:3
                },
                {
                    caption:'Telah Membaca Skripsi Terkait',
                    type:'string',
                    width:3
                },
                {
                    caption:'Telah Membaca Paper & Referensi',
                    type:'string',
                    width:3
                },
                {
                    caption:'Telah Mendapatkan Data',
                    type:'string',
                    width:3
                }
                ];

                arr=[];
                for(i=0;i<rows.length;i++){
                    id = rows[i].id;
                    nim = rows[i].nim;
                    nama = rows[i].nama;
                    topik = rows[i].topik;
                    lab = rows[i].lab;
                    dosen1 = rows[i].dosen1;
                    pertemuan_1 = rows[i].pertemuan_1;
                    dosen2 = rows[i].dosen2;
                    pertemuan_2 = rows[i].pertemuan_2;
                    progress_1 = rows[i].progress_1;
                    progress_2 = rows[i].progress_2;
                    progress_3 = rows[i].progress_3;
                    progress_4 = rows[i].progress_4;

                    if (lab == 1) {
                      lab = 'NCC';
                    }
                    else if (lab == 2) {
                      lab = 'ACI';
                    }
                    else if (lab == 3) {
                      lab = 'SEIS';
                    }

                    if (progress_1 == 1) {
                      progress_1 = 'Ya';
                    }
                    else progress_1 = 'Tidak';

                    if (progress_2 == 1) {
                      progress_2 = 'Ya';
                    }
                    else progress_2 = 'Tidak';

                    if (progress_3 == 1) {
                      progress_3 = 'Ya';
                    }
                    else progress_3 = 'Tidak';

                    if (progress_4 == 1) {
                      progress_4 = 'Ya';
                    }
                    else progress_4 = 'Tidak';

                    a=[nim, nama, topik, lab, dosen1, pertemuan_1, dosen2, pertemuan_2, progress_1, progress_2, progress_3, progress_4];
                    arr.push(a);
                }
                conf.rows = arr;

                var result = nodeExcel.execute(conf);
                res.setHeader('Content-Type','application/vnd.openxmlformates');
                res.setHeader("Content-Disposition","attachment;filename="+"Pengajuan TA.xlsx");
                res.end(result,'binary');

                res.json(rows);

              });
            });

          }

          else {
            res.json({status: false, message: "You Can't Access This Service"});
          }

        }
      });

    }
  };


}

module.exports = new PengajuanExcel();
