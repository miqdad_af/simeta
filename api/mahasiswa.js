var connection = require('./connection');
var _ = require('./simak');
var fc = require('./fc');

var queryError = {status: false, message: 'Query Error'};

module.exports = {
  configure: function(app) {

    app.get('/mahasiswa/summary', function(req, res) {
      connection.acquire(function(err, con) {
        var query = 'select * from mahasiswa_summary where nim = ?';
        var data = [_.decoded.nim];
        con.query(query, data, function(err, result) {
          con.release();
          if (err) {
            res.json(queryError);
            throw err;
          }
          else {
            let kirim = {};
            let ta = 0;
            let kolokium = 0;
            let praseminar = 0;
            let seminar = 0;
            let sidang = 0;
            let skl = 0;
            //
            if (result[0].topik != null) ta =1;
            if (result[0].makalah_kolokium != null) kolokium = 1;
            if (result[0].makalah_praseminar != null) praseminar = 1;
            if (result[0].jenis_seminar != null) seminar = 1;
            if (result[0].makalah_sidang != null) sidang = 1;
            if (result[0].berkas_skl != null) skl = 1;

            kirim = {'ta': ta, 'kolokium': kolokium, 'praseminar': praseminar, 'seminar': seminar, 'sidang': sidang, 'skl': skl};

            res.json({'status': true, 'data': kirim});
          }
        })
      })
    });

  }
}
