var PDF = require('pdfkit');            //including the pdfkit module
var fs = require('fs');
var connection = require('../connection');
var jwt = require('jsonwebtoken');

var wkhtmltopdf = require('wkhtmltopdf');

var _ = require('../simak');

var date = new Date();
var month = date.getMonth();
var tahun = date.getFullYear();

if (month < 9) tahun--;
var tahunNext = tahun + 1;

function Demo(){

    this.getSeminar = function(id, res) {
        var token = id;
        if (!token){
            res.json({status: false, message: 'No token provided'})
        }
        else {
        jwt.verify(token, 'yangPentingPanjang', function(err, decoded) {
            if (err) {
                res.json({status: false, message: 'Token authorization failed'});
                throw err;
            }
            else {
                var decoded = jwt.verify(token, 'yangPentingPanjang');
                var nim = decoded.nim;

                connection.acquire(function(err, con) {
                con.query('select * from mandiri_view where nim = ?', [nim], function(err, result) {
                    con.query('select * from pembahas_view where nim = ?', [nim], function(err, result1) {

                        var nama = result[0].nama;
                        var topik = result[0].topik;
                        var dosen1 = result[0].dosen1;
                        var nip = result[0].nip1;
                        var dosen2 = result[0].dosen2;
                        var penguji1 = result[0].penguji1;
                        var penguji2 = result[0].penguji2;
                        var hp = result[0].hp;
                        var email = result[0].email;

                        var tempat = result1[0].tempat;
                        var jam = result1[0].jam;
                        var tanggal = result1[0].tanggal;
                        var nim1 = result1[0].nim1;
                        var nim2 = result1[0].nim2;
                        var nim3 = result1[0].nim3;
                        var pembahas1 = result1[0].pembahas1;
                        var pembahas2 = result1[0].pembahas2;
                        var pembahas3 = result1[0].pembahas3;

                        if(!dosen2) dosen2 = '';
                        if(!penguji2) penguji2 = '';

                        con.release();
                        var doc = new PDF();
                        res.statusCode = 200;
                        res.setHeader('Content-disposition', 'attachment; filename=Formulir Pendaftaran Seminar.pdf');

                        doc.pipe(res);

                        doc.font('fonts/tnr.ttf');


                        doc.fontSize(10)
                            .text('POB/KOM-PP/10/FRM-04-00', 400, 20);

                        doc.text(' ',50,50);

                        doc.image('img/header.png', {
                            fit: [500, 300],
                            align: 'center'
                        });

                        doc.fontSize(15)
                            .text('FORMULIR PENDAFTARAN SEMINAR (KOM498)', {
                                align: 'center'
                            }, 170, 100);

                        doc.fontSize(15)
                            .text('TAHUN AKADEMIK '+tahun+'/'+tahunNext, {
                                align: 'center'
                            });


                        // NAMA
                        doc.fontSize(11)
                            .text('Nama/NIM', 60, 230);

                        doc.text(': '+nama+' / '+nim, 200, 230);

                        // HP
                        doc.fontSize(11)
                            .text('No. Telp./HP', 60, 250);

                        doc.text(': '+hp, 200, 250);

                        // EMAIL
                        doc.fontSize(11)
                            .text('Email', 60, 270);

                        doc.text(': '+email, 200, 270);

                        // PEMBIMBING
                        doc.fontSize(11)
                            .text('Komisi Pembimbing', 60, 290);

                        doc.text(': 1. '+dosen1, 200, 290);
                        doc.text('(Ketua)', 480, 290);

                        doc.text('  2. '+dosen2, 200, 310);
                        doc.text('(Anggota)', 480, 310);


                        // PENGUJI
                        doc.fontSize(11)
                            .text('Penguji', 60, 330);

                        doc.text(': 1. '+penguji1, 200, 330);
                        doc.text('(Ketua)', 480, 330);

                        doc.text('  2. '+penguji2, 200, 350);
                        doc.text('(Anggota)', 480, 350);

                        // PEMBAHAS
                        doc.fontSize(11)
                            .text('Pembahas (Nama/NIM)', 60, 380);

                        doc.text(': 1. '+pembahas1+' / '+nim1, 200, 380);
                        doc.text('  2. '+pembahas2+' / '+nim2, 200, 400);
                        doc.text('  3. '+pembahas3+' / '+nim3, 200, 420);

                        // JUDUL
                        doc.fontSize(11)
                            .text('Judul Seminar', 60, 440);

                        doc.text(': '+topik, 200, 440);

                        // WAKTU
                        doc.fontSize(11)
                            .text('Waktu Seminar', 60, 520);

                        doc.fontSize(11)
                            .text('Hari/Tanggal', 60, 535);

                        var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu'];
                        var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

                        var d = new Date(tanggal);

                        var dayName = days[d.getDay()];
                        var bulanName = bulan[d.getMonth()];
                        var tanggalName = d.getDate();
                        var tahunName = d.getFullYear();

                        var bulanName1 = bulan[date.getMonth()];
                        var tanggalName1 = date.getDate();
                        var tahunName1 = date.getFullYear();

                        doc.text(': '+dayName+' / '+tanggalName+' '+bulanName+' '+tahunName, 200, 535);

                        doc.fontSize(11)
                            .text('Jam', 60, 550);

                        doc.text(': '+jam, 200, 550);

                        doc.fontSize(11)
                            .text('Tempat', 60, 565);

                        doc.text(': '+tempat, 200, 565);

                        // FOOTER

                        doc.text('Bogor, '+tanggalName1+' '+bulanName1+' '+tahunName1, 360, 590);

                        doc.text('Mengetahui dan menyetujui', 60, 605);
                        doc.text('Mahasiswa yang mengusulkan,', 360, 605);

                        doc.text('Ketua Komisi Pembimbing', 60, 620);

                        doc.text(dosen1, 60, 685);
                        doc.text(nama, 360, 685);

                        doc.text('NIP. '+nip, 60, 700);
                        

                        // END
                        doc.end();

                    })

                });
                });

            }
        })
        }


    };
    // SIMETA v2
    this.getBeritaAcaraMandiri = function(id, token, res) {
        let nim = id;
        if (!token){
            res.json({status: 0, message: 'No token provided'})
        } else {
            jwt.verify(token, 'yangPentingPanjang', (err, decoded) => {
                if (err) {
                    res.json({status: 0, code: 500, message: 'Token authorization failed'});
                    throw err;
                } else {
                    const decoded = jwt.verify(token, 'yangPentingPanjang'),
                        role = decoded.role;
                    if (role === 4) {
                        let doc = new PDF();
                        res.statusCode = 200;
                        // const writeStream = fs.createWriteStream('Berita Acara Seminar.pdf');
                        res.setHeader('Content-disposition', 'attachment; filename=Berita Acara Seminar.pdf');
                
                        doc.pipe(res);
                    
                        // Header
                        doc.font('fonts/tnr.ttf');
                
                        doc.fontSize(10)
                            .text('POB/KOM-PP/10/FRM-05-00', 400, 20);
                
                        doc.text(' ',50,50);
                
                        doc.image('img/header.png', {
                            fit: [500, 300],
                            align: 'center'
                        });
                
                        doc.fontSize(15)
                            .text('BERITA ACARA SEMINAR TUGAS AKHIR', {
                                align: 'center'
                            }, 170, 100);
                
                        doc.fontSize(15)
                            .text('TAHUN AKADEMIK '+tahun+'/'+tahunNext, {
                                align: 'center'
                            });
                        connection.acquire((err, con) => {
                            if (err) {
                                res.json({status: 0, code: 500, message: 'Internal server error. Please contact the developer for further informations (0)'});
                                throw err;
                            } else {
                                const query = `
                                    SELECT 
                                        ta.nama AS nama_mahasiswa,
                                        ta.nim,
                                        ta.dosen1,
                                        ta.dosen2,
                                        ta.penguji1,
                                        ta.penguji2,
                                        ta.nip1,
                                        ta.nip2,
                                        ta.nip3,
                                        ta.nip4,
                                        tas.tanggal,
                                        tam.tempat,
                                        tam.jam,
                                        p.pembahas1,
                                        p.pembahas2,
                                        p.pembahas3,
                                        p.nim1,
                                        p.nim2,
                                        p.nim3,
                                        tas.konfirmasi_pembimbing2,
                                        tas.konfirmasi_penguji1,
                                        tas.konfirmasi_penguji2
                                    FROM
                                        ta_view AS ta
                                    INNER JOIN
                                        ta_seminar AS tas ON ta.nim = tas.nim
                                    INNER JOIN
                                        mandiri_view AS m ON tas.nim = m.nim
                                    INNER JOIN
                                        pembahas_view AS p ON m.nim = p.nim
                                    INNER JOIN
                                        ta_mandiri AS tam ON p.nim = tam.nim
                                    WHERE
                                        tas.nim = ?
                                    AND
                                        ta.nim = ?
                                    AND
                                        m.nim = ?
                                    AND
                                        p.nim = ?  
                                `;
                                const queryData = [nim, nim ,nim, nim]
                                con.query(query, queryData, (err1, res1) => {
                                    if (err1) {
                                        // res.json({status: 0, code: 500, message: 'Internal server error. Please contact the developer for further informations (1)'});
                                        throw err1;
                                    } else {
                                        // console.log(res1);
                                        const days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu'],
                                            bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
                                            d = new Date(res1[0].tanggal),

                                            dayName = days[d.getDay()],
                                            bulanName = bulan[d.getMonth()],
                                            tanggalName = d.getDate(),
                                            tahunName = d.getFullYear(),

                                            bulanName1 = bulan[date.getMonth()],
                                            tanggalName1 = date.getDate(),
                                            tahunName1 = date.getFullYear();

                                            credsMhs = {
                                                nama: res1[0].nama_mahasiswa,
                                                nim: res1[0].nim,
                                                topik: res1[0].nim,
                                                dosen1: res1[0].dosen1,
                                                nip1: res1[0].nip1,
                                                tempat: res1[0].tempat,
                                                jam: res1[0].jam,
                                                pembahas1: res1[0].pembahas1,
                                                pembahas2: res1[0].pembahas2,
                                                pembahas3: res1[0].pembahas3,
                                                nim1: res1[0].nim1,
                                                nim2: res1[0].nim2,
                                                nim3: res1[0].nim3
                                            };

                                        if (res1[0].konfirmasi_pembimbing2 === 1) {
                                            credsMhs.dosen2 = res1[0].dosen2;
                                            credsMhs.nip2 = res1[0].nip2;
                                        }
                                        
                                        if (res1[0].konfirmasi_penguji1 === 1) {
                                            credsMhs.penguji1 = res1[0].penguji1;
                                            credsMhs.nip3 = res1[0].nip3;
                                        }

                                        if (res1[0].konfirmasi_penguji2 === 1) {
                                            credsMhs.penguji2 = res1[0].penguji2;
                                            credsMhs.nip4 = res1[0].nip4;
                                        }

                                        doc.fontSize(11)
                                        .text('Nama/NIM', 60, 230);

                                        doc.text(': '+credsMhs.nama+' / '+credsMhs.nim, 200, 230);

                                        doc.fontSize(11)
                                        .text('Judul Makalah', 60, 250);
                
                                        doc.text(': '+credsMhs.topik, 200, 250);

                                        doc.fontSize(11)
                                        .text('Hari, Tanggal/Jam', 60, 270);

                                        doc.text(': '+dayName+' , '+tanggalName+' '+bulanName+' '+tahunName+' / '+credsMhs.jam, 200, 270);

                                        doc.fontSize(11)
                                        .text('Tempat', 60, 290);

                                        doc.text(': '+credsMhs.tempat, 200, 290);

                                        doc.fontSize(11)
                                        .text('Dosen yang hadir', 60, 308);

                                        doc.fontSize(11)
                                        .text('Pembimbing 1', 60, 320);
                                        
                                        doc.fontSize(11)
                                        .text('Pembimbing 2', 60, 355);

                                        doc.fontSize(11)
                                        .text('Penguji 1', 60, 390);

                                        doc.fontSize(11)
                                        .text('Penguji 2', 60, 425);

                                        doc.fontSize(11)
                                        .text('TTD:', 465, 320);

                                        doc.fontSize(11)
                                        .text('TTD:', 465, 355);
                                        
                                        doc.fontSize(11)
                                        .text('TTD:', 465, 390);
                                        
                                        doc.fontSize(11)
                                        .text('TTD:', 465, 425);
                                        
                                        doc.text(': ', 200, 315)
                                        doc.lineCap('butt')
                                            .moveTo(460, 310)
                                            .lineTo(460, 450)
                                            .stroke()

                                        this.row(doc, 310);
                                        this.row(doc, 345);
                                        this.row(doc, 380);
                                        this.row(doc, 415);

                                        doc.fontSize(11)
                                            .text(credsMhs.dosen1, 210, 315)
                                        doc.text('NIP', 210, 330);
                                        doc.text(`: ${credsMhs.nip1 ? credsMhs.nip1 : '-'}`, 230, 330)

                                        doc.fontSize(11)
                                            .text(credsMhs.dosen2 ? credsMhs.dosen2 : '-', 210, 350)
                                        doc.text('NIP', 210, 365);
                                        doc.text(`: ${credsMhs.nip2 ? credsMhs.nip2 : '-'}`, 230, 365)

                                        doc.fontSize(11)
                                            .text(credsMhs.penguji1 ? credsMhs.penguji1 : '-', 210, 385)
                                        doc.text('NIP', 210, 400);
                                        doc.text(`: ${credsMhs.nip3 ? credsMhs.nip3 : '-'}`, 230, 400)


                                        doc.fontSize(11)
                                            .text(credsMhs.penguji2 ? credsMhs.penguji2 : '-', 210, 420);
                                        doc.text('NIP', 210, 435);
                                        doc.text(`: ${credsMhs.nip4 ? credsMhs.nip4 : '-'}`, 230, 435);

                                        doc.fontSize(11)
                                            .text(`Pembahas / NIM`, 60, 470);
                                        
                                        doc.text(`: ${credsMhs.pembahas1} / ${credsMhs.nim1}`, 200, 470);                                
                                        doc.text(`  ${credsMhs.pembahas2} / ${credsMhs.nim2}`, 200, 490);
                                        doc.text(`  ${credsMhs.pembahas3} / ${credsMhs.nim3}`, 200, 510);

                                        doc.fontSize(11)
                                            .text('Penilaian Seminar', 60, 535);
                                        doc.text('( ', 60, 548);
                                        doc.font('fonts/tnr-italic.ttf')
                                            .fontSize(11)
                                            .text('wajib diisi semua', 64, 548)
                                        doc.font('fonts/tnr.ttf')
                                            .fontSize(11)
                                            .text(' )', 140, 548);

                                        doc.text(':', 200, 540);

                                        doc.lineCap('butt')
                                            .moveTo(400, 530)
                                            .lineTo(400, 635)
                                            .stroke()

                                        this.row(doc, 530);
                                        this.row(doc, 565);
                                        this.row(doc, 600)

                                        doc.fontSize(11)
                                            .text('a. PenulisanMakalah (Bobot: 40%)', 210, 540);
                                        doc.text('b. PenguasaanMateri (Bobot: 30%)', 210, 575);
                                        doc.text('c. KemampuanPenyajian (Bobot: 30%)', 210, 610);

                                        doc.font('fonts/tnr-bold.ttf')
                                            .fontSize(11)
                                            .text('(A / AB / B / BC / C / D / E)', 410, 540);
                                        doc.text('(A / AB / B / BC / C / D / E)', 410, 575);
                                        doc.text('(A / AB / B / BC / C / D / E)', 410, 610);

                                        doc.font('fonts/tnr.ttf')
                                            .fontSize(11)
                                            .text('Moderator Seminar', 335, 645)
                                        
                                        doc.text('__________________________________', 335, 690);
                                        doc.text('NIP', 335, 705)
                                        
                                        doc.end();
                                    }
                                })
                            }
                        });
                    } else {
                        res.json({status: 0, code: 400, message: 'Forbidden'});
                    }
                }
            });
        }
    };

    this.getPenilaianSidang = function(id, token, res) {
        let nim = id;
        if (!token) {
            res.json({ status: 0, message: "No token provided" });
        } else {
            jwt.verify(token, "yangPentingPanjang", (err, decoded) => {
                if (err) {
                    res.json({ status: 0, code: 500, message: "Token authorization failed" });
                    throw err;
                } else {
                    const decoded = jwt.verify(token, "yangPentingPanjang");
                    const role = decoded.role;

                    if (role === 4) {
                        connection.acquire((err, con) => {
                            let creds = { 
                                    query: `
                                        SELECT 
                                            dosen1, 
                                            dosen2, 
                                            penguji1, 
                                            penguji2, 
                                            nip1, 
                                            nip2, 
                                            nip3, 
                                            nip4 
                                        FROM  
                                            mahasiswa_sidang_view 
                                        WHERE nim = ?
                                    `, 
                                    data: [nim] 
                                };
                            con.query(creds.query, creds.data, (err1, res1) => {
                                if (err1) {
                                    res.json({ status: 0, code: 500, message: "Internal server error (1)"});
                                    throw err1;
                                } else {
                                    let attend = [];

                                    if (res1[0].dosen1) {
                                        attend.push({nama: res1[0].dosen1, nip: res1[0].nip1, title: 'Pembimbing 1'});
                                    }
                                    if (res1[0].dosen2) {
                                        attend.push({nama: res1[0].dosen2, nip: res1[0].nip2, title: 'Pembimbing 2'});
                                    }
                                    if (res1[0].penguji1) {
                                        attend.push({nama: res1[0].penguji1, nip: res1[0].nip3, title: 'Penguji 1'});
                                    }
                                    if (res1[0].penguji2) {
                                        attend.push({nama: res1[0].penguji1, nip: res1[0].nip3, title: 'Penguji 1'});
                                    }

                                    let doc = new PDF();
                                    res.statusCode = 200;
                                    res.setHeader("Content-disposition", "attachment; filename=Form Penilaian Tugas Akhir.pdf");
                                    doc.pipe(res);
                                    creds = { 
                                        query: `
                                            SELECT
                                                *
                                            FROM
                                                mahasiswa_sidang_view
                                            WHERE
                                                nim = ?
                                        `, 
                                        data: [nim] 
                                    };
                                    con.query(creds.query, creds.data, (err2, res2) => {
                                        if (err2) {
                                            con.release();
                                            res.json({status: 0, code: 500, message: "Internal server error (2)"});
                                            throw err2;
                                        } else {
                                            const credsMhs = { 
                                                nama: res2[0].nama, 
                                                nim: res2[0].nim, 
                                                topik: res2[0].topik, 
                                                tempat: res2[0].tempat, 
                                                jam: res2[0].jam, 
                                                tanggal: res2[0].tanggal, 
                                            };
                                            for (let i=0; i < attend.length; i++) {
                                                // Header
                                                doc.font("fonts/tnr.ttf");
                                                doc.fontSize(18);
        
                                                doc.fontSize(10).text("POB/KOM-PP/11/FRM-06-00", 400, 20);
        
                                                doc.text(" ", 50, 50);
        
                                                doc.image("img/header.png", {
                                                    fit: [500, 300],
                                                    align: "center"
                                                });
        
                                                doc.font("fonts/tnr-bold.ttf");
                                                doc.fontSize(15)
                                                    .text(`LEMBAR PENILAIAN UJIAN TUGAS AKHIR (KOM 499)`,{
                                                        align: "center"
                                                    },160,100
                                                );
        
                                                doc.text(`TAHUN AKADEMIK ${tahun}/${tahunNext}`, {
                                                    align: "center"
                                                });
                                                doc.font("fonts/tnr.ttf", { align: "center" });
                                                doc.font("fonts/tnr.ttf").fontSize(11);
                                                const days = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"],
                                                bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                                                d = new Date(res2[0].tanggal),
                                                dayName = days[d.getDay()],
                                                bulanName = bulan[d.getMonth()],
                                                tanggalName = d.getDate(),
                                                tahunName = d.getFullYear(),
                                                bulanName1 = bulan[date.getMonth()],
                                                tanggalName1 = date.getDate(),
                                                tahunName1 = date.getFullYear();
        
        
                                                doc.fontSize(11).text("Nama/NIM", 60, 210);
        
                                                doc.text(": " + credsMhs.nama + " / " + credsMhs.nim, 200, 210);
        
                                                doc.fontSize(11).text("JudulTugasAkhir", 60, 230);
        
                                                doc.text(": " + credsMhs.topik, 200, 230);
        
                                                doc.fontSize(11).text("Hari, Tanggal/Jam", 60, 270);
        
                                                doc.text(": " + dayName + " , " + tanggalName + " " + bulanName + " " + tahunName + " / " + credsMhs.jam, 200, 270);
        
                                                doc.fontSize(11).text("Tempat", 60, 290);
        
                                                doc.text(": " + credsMhs.tempat, 200, 290);
        
                                                doc.fontSize(11).text("SidangKe", 60, 310);
                                                doc.text(": ", 200, 310);
        
                                                this.rowPenilaian(doc, 330);
        
                                                doc.fontSize(11).text("Komentarsingkat", 60, 330);
                                                doc.text(": ", 200, 330);
        
                                                doc.fontSize(11).text("Penilaian Sidang*", 60, 535);
                                                doc.text("( ", 60, 548);
                                                doc
                                                .font("fonts/tnr-italic.ttf")
                                                .fontSize(11)
                                                .text("wajib diisi semua", 64, 548);
                                                doc
                                                .font("fonts/tnr.ttf")
                                                .fontSize(11)
                                                .text(" )", 140, 548);
        
                                                doc.text(":", 200, 540);
        
                                                doc
                                                .lineCap("butt")
                                                .moveTo(400, 530)
                                                .lineTo(400, 635)
                                                .stroke();
        
                                                this.row(doc, 530);
                                                this.row(doc, 565);
                                                this.row(doc, 600);
        
                                                doc
                                                .fontSize(11)
                                                .text(
                                                    "a. Penulisan tugas akhir (Bobot: 40%)",210,540
                                                );
                                                doc.text("b. Penguasaan materi (Bobot: 40%)", 210, 575);
                                                doc.text("c. Kemampuan menjawab (Bobot: 20%)", 210, 610);
        
                                                doc
                                                .font("fonts/tnr-bold.ttf")
                                                .fontSize(11)
                                                .text("(A / AB / B / BC / C / D / E)", 410, 540);
                                                doc.text("(A / AB / B / BC / C / D / E)", 410, 575);
                                                doc.text("(A / AB / B / BC / C / D / E)", 410, 610);
        
                                                doc
                                                .font("fonts/tnr.ttf")
                                                .fontSize(11)
                                                .text(attend[i].title, 325, 645);
        
                                                doc
                                                .font("fonts/tnr.ttf")
                                                .fontSize(11)
                                                .text(attend[i].nama, 325, 690);
        
                                                doc.text("NIP." + attend[i].nip, 325, 705);
        
                                                doc
                                                .font("fonts/tnr-italic.ttf")
                                                .fontSize(9)
                                                .text("*Lingkari salah satu", 60, 708);
                                                doc.addPage();
                                            }
                                            doc.end();
                                            con.release();
                                        }
                                    });
                                }
                            });
                        });
                    }
                }
            });
        }
    };

    this.getBeritaAcaraSidang = function(id, token, res) {
        let nim = id;
        if (!token) {
            res.json({status: 0, message: 'No token provided'});
        } else {
            jwt.verify(token, 'yangPentingPanjang', (err, decoded) => {
                if (err) {
                    res.json({status: 0, code: 500, message: 'Token authorization failed'});
                    throw err;
                } else {
                    const decoded = jwt.verify(token, 'yangPentingPanjang'),
                    role = decoded.role;

                    if (role === 4) {
                        console.log ('masuk kondisi role == 4');
                        let doc = new PDF();
                        res.statusCode = 200;
                        res.setHeader('Content-disposition', 'attachment; filename=Berita Acara Ujian Tugas Akhir.pdf');
                        doc.pipe(res);

                        doc.font('fonts/tnr.ttf');

                        doc.fontSize(10)
                        .text('POB/KOM-PP/11/FRM-07-00', 400, 20);
            
                        doc.text(' ',50,50);
                
                        doc.image('img/header.png', {
                            fit: [500, 300],
                            align: 'center'
                        });

                        doc.font('fonts/tnr-bold.ttf')
                            .fontSize(15)
                            .text('BERITA ACARA UJIAN TUGAS AKHIR (KOM499)', {
                                align: 'center'
                            }, 160, 100);
                
                        doc.fontSize(15)
                            .text('TAHUN AKADEMIK '+tahun+'/'+tahunNext, {
                                align: 'center'
                            });
                        
                        connection.acquire((err, con) => {
                            if (err) {
                                con.release();
                                res.json({status: 0, code: 500, message: 'Internal server error (0)'});
                                throw err;
                            } else {
                                console.log ('masuk kondisi != error (0)');
                                const creds = {
                                    query: `
                                        SELECT 
                                            nama,
                                            nim,
                                            topik,
                                            tanggal,
                                            tempat,
                                            jam,
                                            dosen1,
                                            dosen2,
                                            penguji1,
                                            penguji2,
                                            nip1,
                                            nip2,
                                            nip3,
                                            nip4
                                        FROM  
                                            mahasiswa_sidang_view 
                                        WHERE nim = ?
                                    `,
                                    data: [nim]
                                };
                                con.query(creds.query, creds.data, (err1, res1) => {
                                    if (err1) {
                                        con.release();
                                        //res.json({status: 0, code: 500, message: 'Internal server error (1)'});
                                        throw err1;
                                    } else {
                                        console.log ('masuk kondisi != error (1)');
                                        const credsMhs = {
                                            nama: res1[0].nama,
                                            nim: res1[0].nim,
                                            topik: res1[0].topik,
                                            tanggal: res1[0].tanggal, 
                                            tempat: res1[0].tempat,
                                            jam: res1[0].jam,
                                            dosen1: res1[0].dosen1,
                                            dosen2: res1[0].dosen2,
                                            penguji1: res1[0].penguji1,
                                            penguji2: res1[0].penguji2,
                                            nip1: res1[0].nip1,
                                            nip2: res1[0].nip2,
                                            nip3: res1[0].nip3,
                                            nip4: res1[0].nip4
                                        };
                                        doc.font("fonts/tnr.ttf").fontSize(11);
                                        const 
                                            days = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"],
                                            bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                                            d = new Date(res1[0].tanggal),
                                            dayName = days[d.getDay()],
                                            bulanName = bulan[d.getMonth()],
                                            tanggalName = d.getDate(),
                                            tahunName = d.getFullYear(),
                                            bulanName1 = bulan[date.getMonth()],
                                            tanggalName1 = date.getDate(),
                                            tahunName1 = date.getFullYear();
                                        

                                        doc.fontSize(11).text("Nama/NIM", 60, 210);
                                        doc.text(": " + credsMhs.nama + " / " + credsMhs.nim, 200, 210);

                                        doc.fontSize(11).text("JudulTugasAkhir", 60, 230);
                                        doc.text(": " + credsMhs.topik, 200, 230);

                                        doc.fontSize(11).text("Hari, Tanggal/Jam", 60, 270);
                                        doc.text(": " + dayName + " , " + tanggalName + " " + bulanName + " " + tahunName + " / " + credsMhs.jam, 200, 270);

                                        doc.fontSize(11).text("Tempat", 60, 290);
                                        doc.text(": " + credsMhs.tempat, 200, 290);

                                        doc.fontSize(11).text("SidangKe", 60, 310);
                                        doc.text(": ", 200, 310);

                                        doc
                                            .font('fonts/tnr-bold.ttf')
                                            .fontSize(11)
                                            .text('Keputusan *', 220, 360)
                                        doc
                                            .lineCap('butt')
                                            .moveTo(410, 350)
                                            .lineTo(410, 385)
                                            .stroke();
                                        this.row(doc, 350);
                                        doc.text('LULUS/DIULANG', 420, 360);

                                        doc.text('PENGESAHAN', 250, 405);

                                        this.rowAbsen(doc, 420);
                                        let h = 440;
                                        let attend = [];
                                        if (credsMhs.dosen1) {
                                            attend.push({nama: credsMhs.dosen1, nip: credsMhs.nip1, title: 'Pembimbing 1'});
                                        }
                                        if (credsMhs.dosen2) {
                                            attend.push({nama: credsMhs.dosen2, nip: credsMhs.nip2, title: 'Pembimbing 2'});
                                        }
                                        if (credsMhs.penguji1) {
                                            attend.push({nama: credsMhs.penguji1, nip: credsMhs.nip3, title: 'Penguji 1'});
                                        }
                                        if (credsMhs.penguji2) {
                                            attend.push({nama: credsMhs.penguji2, nip: credsMhs.nip4, title: 'Penguji 2'});
                                        }

                                        console.log(credsMhs);
                                        console.log(attend
                                        )
                                        for (let i=0; i<attend.length; i++) {
                                            this.rowBeritaAcaraSidang(doc, h);
                                            doc
                                                .font('fonts/tnr.ttf')
                                                .text(attend[i].nama, 70, h+20)
                                                .text(attend[i].title, 310, h+20);
                                            h += 50;
                                            
                                        }
                                        
                                        doc
                                            .lineCap('butt')
                                            .moveTo(300, 420)
                                            .lineTo(300, 590)
                                            .stroke();

                                        doc
                                            .lineCap('butt')
                                            .moveTo(430, 420)
                                            .lineTo(430, 590)
                                            .stroke();
                                        
                                        doc
                                            .text('Nama/NIP', 160, 423)
                                            .text('Posisi', 353, 423)
                                            .text('TandaTangan', 460, 423);
                                        
                                        doc 
                                            .font('fonts/tnr-italic.ttf')
                                            .fontSize(9)
                                            .text('* Dicoret yang tidak perlu', 60, 705);
                                        
                                        doc.end();
                    
                                    }
                                });
                            }
                        });
                    } else {
                        res.json({status: 0, code: 403, message: 'Forbidden'});
                    }
                }
            });
        }
    }

    this.getPerbaikanSidang = function(id, token, res) {
        let nim = id;
        if (!token) {
            res.json({status: 0, message: 'No token provided'});
        } else {
            jwt.verify(token, 'yangPentingPanjang', (err, decoded) => {
                if (err) {
                    res.json({status: 0, code: 500, message: 'Token authorization failed'});
                    throw err;
                } else {
                    const 
                        decoded = jwt.verify(token, 'yangPentingPanjang'),
                        role = decoded.role;
                    if (role === 4) {
                        let doc = new PDF();
                        res.statusCode = 200;
                        res.setHeader('Content-disposition', 'attachment; filename=Lembar Perbaikan Ujian Tugas Akhir.pdf');
                        doc.pipe(res);

                        // Header
                        doc.font('fonts/tnr.ttf');

                        doc
                            .fontSize(10)
                            .text('POB/KOM-PP/11/FRM-08-00', 400, 20);
            
                        doc.text(' ',50,50);
                        doc.image('img/header.png', {
                            fit: [500, 300],
                            align: 'center'
                        });

                        doc
                            .font('fonts/tnr-bold.ttf')
                            .fontSize(15)
                            .text('LEMBAR PERBAIKAN UJIAN TUGAS AKHIR (KOM499)', {
                                align: 'center'
                            }, 160, 100);
            
                        doc
                            .fontSize(15)
                            .text('TAHUN AKADEMIK '+tahun+'/'+tahunNext, {
                                align: 'center'
                            });
                        
                        connection.acquire((err, con) => {
                            if (err) {
                                con.release();
                                res.json({status: 0, code: 500, message: 'Internal server error'});
                                throw err;
                            } else {
                                const creds = {
                                    query: `
                                        SELECT
                                            nama,
                                            nim,
                                            topik,
                                            dosen1,
                                            dosen2,
                                            penguji1,
                                            penguji2,
                                            nip1,
                                            nip2,
                                            nip3,
                                            nip4
                                        FROM
                                            mahasiswa_sidang_view 
                                        WHERE 
                                            nim = ?
                                    `,
                                    data: [nim]
                                };
                                con.query(creds.query, creds.data, (err1, res1) => {
                                    if (err1) {
                                        con.release();
                                        res.json({status: 0, code: 500, message: 'Internal server error (0)'});
                                        throw err1;
                                    } else {
                                        const credsMhs = {
                                            nama: res1[0].nama,
                                            nim: res1[0].nim,
                                            topik: res1[0].topik,
                                            dosen1: res1[0].dosen1,
                                            dosen2: res1[0].dosen2,
                                            penguji1: res1[0].penguji1,
                                            penguji2: res1[0].penguji2,
                                            nip: {
                                                dosen1: res1[0].nip1,
                                                dosen2: res1[0].nip2,
                                                penguji1: res1[0].nip3,
                                                penguji2: res1[0].nip4
                                            }
                                        };
                                        doc
                                            .font("fonts/tnr.ttf")
                                            .fontSize(11);
                                        const 
                                            days = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"],
                                            bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                                            d = new Date(res1[0].tanggal),
                                            dayName = days[d.getDay()],
                                            bulanName = bulan[d.getMonth()],
                                            tanggalName = d.getDate(),
                                            tahunName = d.getFullYear(),
                                            bulanName1 = bulan[date.getMonth()],
                                            tanggalName1 = date.getDate(),
                                            tahunName1 = date.getFullYear();

                                        doc
                                            .fontSize(11)
                                            .text("NamaMahasiswa/NIM", 60, 210);

                                        doc.text(": " + credsMhs.nama + " / " + credsMhs.nim, 200, 210);

                                        doc
                                            .fontSize(11)
                                            .text("JudulTugasAkhir", 60, 230);

                                        doc.text(": " + credsMhs.topik, 200, 230);

                                        doc
                                            .fontSize(11)
                                            .text('Batas akhir perbaikan', 60, 270);
                                        doc.text(': ', 200, 270);

                                        doc
                                            .fontSize(11)
                                            .text('Data perbaikan (', 70, 290);
                                        
                                        doc
                                            .font('fonts/tnr-italic.ttf')
                                            .fontSize(11)
                                            .text('gunakan lembar tambahan, bila tidak mencukupi', 151, 290);
                                        
                                        doc
                                            .font('fonts/tnr.ttf')
                                            .fontSize(11)
                                            .text('):', 370, 290);
                                        
                                        this.rowPerbaikan(doc, 305);
                                        this.rowPerbaikanParaf(doc, 390);

                                        doc
                                            .font('fonts/tnr-bold.ttf')
                                            .fontSize(11)
                                            .text('PARAF PEMBIMBING & PENGUJI', 350, 395);
                                            
                                            let sumHeight = 50;
                                            this.rowAbsen(doc, 420 + sumHeight);
                                            let h = 440  + sumHeight;
                                            let attend = [];
                                            if (credsMhs.dosen1) {
                                                attend.push({nama: credsMhs.dosen1, nip: credsMhs.nip1, title: 'Pembimbing 1'});
                                            }
                                            if (credsMhs.dosen2) {
                                                attend.push({nama: credsMhs.dosen2, nip: credsMhs.nip2, title: 'Pembimbing 2'});
                                            }
                                            if (credsMhs.penguji1) {
                                                attend.push({nama: credsMhs.penguji1, nip: credsMhs.nip3, title: 'Penguji 1'});
                                            }
                                            if (credsMhs.penguji2) {
                                                attend.push({nama: credsMhs.penguji2, nip: credsMhs.nip4, title: 'Penguji 2'});
                                            }
                                  
                                            console.log(credsMhs);
                                            console.log(attend
                                            )
                                            for (let i=0; i<attend.length; i++) {
                                                this.rowBeritaAcaraSidang(doc, h);
                                                doc
                                                    .font('fonts/tnr.ttf')
                                                    .text(attend[i].nama, 70, h + 20 )
                                                    .text(attend[i].title, 310, h + 20 );
                                                h += 50;
                                            }

                                            h = 510;
                                            
                                            doc
                                                .lineCap('butt')
                                                .moveTo(300, 420  + sumHeight)
                                                .lineTo(300, 590  + sumHeight)
                                                .stroke();
    
                                            doc
                                                .lineCap('butt')
                                                .moveTo(430, 420  + sumHeight)
                                                .lineTo(430, 590  + sumHeight)
                                                .stroke();
                                            
                                            doc
                                                .text('Nama/NIP', 160, 423  + sumHeight)
                                                .text('Posisi', 353, 423  + sumHeight)
                                                .text('TandaTangan*', 460, 423  + sumHeight);
                                            
                                            doc 
                                                .font('fonts/tnr-italic.ttf')
                                                .fontSize(9)
                                                .text('*) Tanda-tangani saat perbaikan telah diselesaikan', 60, 705);
                                            
                                            doc.addPage();
                                            doc.font('fonts/tnr.ttf');

                                            doc
                                                .fontSize(10)
                                                .text('POB/KOM-PP/11/FRM-08-00', 400, 20);
                                            doc
                                                .font('fonts/tnr-bold.ttf')
                                                .fontSize(11)
                                                .text('Lembar Perbaikan Tambahan:', 60, 60)
                                                .text('Nama:', 60, 80)
                                                .text('NIM: ', 60, 100)
                                                .font('fonts/tnr.ttf')
                                                .text(credsMhs.nama, 100, 80)
                                                .text(credsMhs.nim, 100, 100);
                                            
                                            this.rowPerbaikanParaf(doc, 410);

                                            doc
                                                .font('fonts/tnr-bold.ttf')
                                                .fontSize(11)
                                                .text('PARAF PEMBIMBING & PENGUJI', 350, 415);

                                            this.rowAbsen(doc, 490 );

                                            for (let i=0; i<attend.length; i++) {
                                                this.rowBeritaAcaraSidang(doc, h);
                                                doc
                                                    .font('fonts/tnr.ttf')
                                                    .text(attend[i].nama, 70, h + 20 )
                                                    .text(attend[i].title, 310, h + 20 );
                                                h += 50;
                                            }

                                            doc
                                                .lineCap('butt')
                                                .moveTo(300, 440  + sumHeight)
                                                .lineTo(300, 610  + sumHeight)
                                                .stroke();

                                            doc
                                                .lineCap('butt')
                                                .moveTo(430, 440  + sumHeight)
                                                .lineTo(430, 610  + sumHeight)
                                                .stroke();
                                            
                                            doc
                                                .text('Nama/NIP', 160, 443 + sumHeight)
                                                .text('Posisi', 353, 443  + sumHeight)
                                                .text('TandaTangan*', 460, 443  + sumHeight);
                                            
                                            doc 
                                                .font('fonts/tnr-italic.ttf')
                                                .fontSize(9)
                                                .text('*) Tanda-tangani saat perbaikan telah diselesaikan', 60, 705);
                                            
                                            doc.end();

                                    }
                                });
                            }
                        }); 
                    }
                }
            });
        }
    }

    this.getAbsenMandiri = function(id, token, res) {
        let nim = id;
        if (!token) {
            res.json({status: 0, message: 'No token provided'});
        } else {
            jwt.verify(token, 'yangPentingPanjang', (err, decoded) => {
                if (err) {
                    res.json({status: 0, code: 500, message: 'Token authorization failed'});
                    throw err;
                } else {
                    const decoded = jwt.verify(token, 'yangPentingPanjang'),
                        role = decoded.role;
                    if (role === 4) {
                        let doc = new PDF();
                        res.statusCode = 200;
                        res.setHeader('Content-disposition', 'attachment; filename=Absensi Peserta Seminar Mandiri.pdf');
                
                        doc.pipe(res);
                        // Header
                        doc.font('fonts/tnr.ttf');

                        doc.fontSize(10)
                        .text('POB/KOM-PP/10/FRM-06-00', 400, 20);
            
                        doc.text(' ',50,50);
                
                        doc.image('img/header.png', {
                            fit: [500, 300],
                            align: 'center'
                        });
                
                        doc.fontSize(15)
                            .text('DAFTAR HADIR PESERTA SEMINAR (KOM498)', {
                                align: 'center'
                            }, 170, 100);
                
                        doc.fontSize(15)
                            .text('TAHUN AKADEMIK '+tahun+'/'+tahunNext, {
                                align: 'center'
                            });
                        connection.acquire((err, con) => {
                            if (err) {
                                con.release();
                                res.json({status: 0, code: 500, message: 'Internal server error. Please contact the developer for further informations (0)'});
                                throw err;
                            } else {
                                const query = `
                                    SELECT
                                        m.nama,
                                        m.nim,
                                        m.dosen1,
                                        m.dosen2,
                                        tam.jam,
                                        tam.tempat,
                                        tam.tanggal
                                    FROM
                                        mandiri_view AS m
                                    INNER JOIN
                                        ta_mandiri AS tam ON m.nim = tam.nim
                                    WHERE
                                        m.nim = ?
                                    AND
                                        tam.nim = ?
                                `;
                                const queryData = [nim, nim]
                                con.query(query, queryData, (err1, res1) => {
                                    if (err1) {
                                        throw err1;
                                    } else {
                                        con.release();
                                        const credsMhs = {
                                            nama: res1[0].nama,
                                            nim: res1[0].nim,
                                            dosen1: res1[0].dosen1,
                                            dosen2: res1[0].dosen2,
                                            tanggal: res1[0].tanggal, 
                                            tempat: res1[0].tempat,
                                            jam: res1[0].jam
                                        };

                                        const days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu'],
                                            bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
                                            d = new Date(res1[0].tanggal),

                                            dayName = days[d.getDay()],
                                            bulanName = bulan[d.getMonth()],
                                            tanggalName = d.getDate(),
                                            tahunName = d.getFullYear(),

                                            bulanName1 = bulan[date.getMonth()],
                                            tanggalName1 = date.getDate(),
                                            tahunName1 = date.getFullYear();

                                        doc.fontSize(11)
                                            .text('Hari/Tanggal', 60, 230);
                                        doc.text(': '+dayName+' , '+tanggalName+' '+bulanName+' '+tahunName, 200, 230);
                                        doc.text('Pukul', 60, 250);
                                        doc.text(': '+credsMhs.jam, 200, 250);
                                        doc.text('Tempat', 60, 270);
                                        doc.text(': '+credsMhs.tempat, 200, 270);
                                        doc.text('Penyaji', 60, 290);
                                        doc.text(': '+credsMhs.nama+' / '+credsMhs.nim, 200, 290);
                                        doc.text('Dosen Pembimbing',60, 310);
                                        if (credsMhs.dosen2) {
                                            doc.text(': 1. ' + credsMhs.dosen1, 200, 310);
                                            doc.text('  2. ' + credsMhs.dosen2, 200, 320);
                                        }
                                        doc.text(': '+ credsMhs.dosen1, 200, 310);
                                        let sum = 0;
                                        doc.lineCap('butt')
                                            .moveTo(100, 350)
                                            .lineTo(100, 730)
                                            .stroke()
                                        doc.lineCap('butt')
                                            .moveTo(300, 350)
                                            .lineTo(300, 730)
                                            .stroke()
                                        doc.lineCap('butt')
                                            .moveTo(400, 350)
                                            .lineTo(400, 730)
                                            .stroke()
                                        // Header tabel
                                        doc.font('fonts/tnr-bold.ttf')
                                            .fontSize(11)
                                            .text('No', 72, 354)
                                        doc.fontSize(11)
                                            .text('Nama', 183, 354)
                                        doc.fontSize(11)
                                            .text('NIM', 338, 354)
                                        doc.fontSize(11)
                                            .text('Tandatangan', 443, 354)

                                        for (let i = 0; i < 19; i++) {
                                            this.rowAbsen(doc, 350 + sum);
                                            sum += 20;
                                        }
                                        sum = 0;
                                        // Page kedua
                                        doc.addPage();
                                        doc.font('fonts/tnr.ttf')
                                            .fontSize(10)
                                            .text('POB/KOM-PP/10/FRM-06-00', 400, 20);
                                        doc.lineCap('butt')
                                            .moveTo(100, 80)
                                            .lineTo(100, 560)
                                            .stroke()
                                        doc.lineCap('butt')
                                            .moveTo(300, 80)
                                            .lineTo(300, 560)
                                            .stroke()
                                        doc.lineCap('butt')
                                            .moveTo(400, 80)
                                            .lineTo(400, 560)
                                            .stroke()
                                        // Header
                                        doc.font('fonts/tnr-bold.ttf')
                                            .fontSize(11)
                                            .text('No', 72, 84)
                                        doc.fontSize(11)
                                            .text('Nama', 183, 84)
                                        doc.fontSize(11)
                                            .text('NIM', 338, 84)
                                        doc.fontSize(11)
                                            .text('Tandatangan', 443, 84)
                                        for (let i = 0; i < 24; i++) {
                                            this.rowAbsen(doc, 80 + sum);
                                            sum += 20
                                        }

                                        doc.font('fonts/tnr.ttf')
                                            .fontSize(11)
                                            .text('Bogor, '+tanggalName1+' '+bulanName1+' '+tahunName1, 380, 580);

                                        doc.text('Mengetahui,', 380, 605);
                        
                                        doc.text('Moderator Seminar', 380, 620);
                                        
                                        doc.text('___________________________', 380, 680);
                                        doc.text('NIP. ', 380, 700);
                                        doc.end();
                                    }
                                });
                            }
                        });
        
                    } else {
                        res.json({status: 0, code: 403, message: 'Forbidden'});
                    }
                }
            });
        }
    };

    this.getSiapSidang = function(id, res) {
        var token = id;
        if (!token){
            res.json({status: false, message: 'No token provided'})
        }
        else {
        jwt.verify(token, 'yangPentingPanjang', function(err, decoded) {
            if (err) {
                res.json({status: false, message: 'Token authorization failed'});
                throw err;
            }
            else {
                var decoded = jwt.verify(token, 'yangPentingPanjang');
                var nim = decoded.nim;

                connection.acquire(function(err, con) {
                con.query('select * from mahasiswa_sidang_view where nim = ?', [nim], function(err, result) {

                var nama = result[0].nama;
                var topik = result[0].topik;
                var nip1 = result[0].nip1;
                var nip2 = result[0].nip2;
                var nip3 = result[0].nip3;
                var nip4 = result[0].nip4;
                var dosen1 = result[0].dosen1;
                var dosen2 = result[0].dosen2;
                var penguji1 = result[0].penguji1;
                var penguji2 = result[0].penguji2;
                var tanggal = result[0].tanggal;
                var tempat = result[0].tempat;
                var jam = result[0].jam;

                if(!dosen2) dosen2 = '';
                if(!penguji2) penguji2 = '';
                if(!nip1) nip1 = '';
                if(!nip2) nip2 = '';
                if(!nip3) nip3 = '';
                if(!nip4) nip4 = '';

                var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu'];
                var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

                var d = new Date(tanggal);

                var dayName = days[d.getDay()];
                var bulanName = bulan[d.getMonth()];
                var tanggalName = d.getDate();
                var tahunName = d.getFullYear();

                var bulanName1 = bulan[date.getMonth()];
                var tanggalName1 = date.getDate();
                var tahunName1 = date.getFullYear();

                con.release();
                var doc = new PDF();
                res.statusCode = 200;
                res.setHeader('Content-disposition', 'attachment; filename=Formulir Kesiapan Ujian Tugas Akhir.pdf');

                doc.pipe(res);

                doc.font('fonts/tnr.ttf');


                doc.fontSize(10)
                    .text('POB/KOM-PP/11/FRM-04-00', 400, 20);

                doc.text(' ',50,50);

                doc.image('img/header.png', {
                    fit: [500, 300],
                    align: 'center'
                });

                doc.fontSize(15)
                    .text('FORMULIR KESIAPAN UJIAN TUGAS AKHIR (KOM499)', {
                        align: 'center'
                    }, 170, 100);

                doc.fontSize(15)
                    .text('TAHUN AKADEMIK '+tahun+'/'+tahunNext, {
                        align: 'center'
                    });

                doc.fontSize(11)
                    .text('Yang bertanda tangan di bawah ini, Pembimbing & Penguji Tugas Akhir mahasiswa Mayor Ilmu Komputer menyatakan bahwa:', 60, 230);


                // NAMA
                doc.fontSize(11)
                    .text('Nama Mahasiswa', 60, 270);

                doc.text(': '+nama, 200, 270);

                // NIM
                doc.fontSize(11)
                    .text('NIM', 60, 285);

                doc.text(': '+nim, 200, 285);

                // Topik
                doc.fontSize(11)
                    .text('Bidang Kajian', 60, 300);

                doc.text(': '+topik, 200, 300);

                // WAKTU
                doc.fontSize(11)
                    .text('telah SIAP DIUJI pada:', 60, 360);

                doc.fontSize(11)
                    .text('Hari, Tanggal', 60, 375);

                doc.text(': '+dayName+', '+tanggalName+' '+bulanName+' '+tahunName, 200, 375);

                doc.fontSize(11)
                    .text('Pukul', 60, 390);

                doc.text(': '+jam, 200, 390);

                doc.fontSize(11)
                    .text('Tempat', 60, 405);

                doc.text(': '+tempat, 200, 405);

                // Penguji
                doc.fontSize(11)
                    .text('dengan Dosen Penguji sebagai berikut:', 60, 420);

                // PENGUJI 1
                doc.fontSize(11)
                    .text('Penguji 1:', 60, 445);

                doc.fontSize(11)
                    .text('Nama', 60, 460);

                doc.text(': '+penguji1, 200, 460);

                doc.fontSize(11)
                    .text('NIP', 60, 475);

                doc.text(': '+nip3, 200, 475);

                doc.fontSize(11)
                    .text('Tanda Tangan', 60, 490);

                doc.text(': ', 200, 490);


                // PENGUJI 2
                doc.fontSize(11)
                    .text('Penguji 2:', 60, 520);

                doc.fontSize(11)
                    .text('Nama', 60, 535);

                doc.text(': '+penguji2, 200, 535);

                doc.fontSize(11)
                    .text('NIP', 60, 550);

                doc.text(': '+nip4, 200, 550);

                doc.fontSize(11)
                    .text('Tanda Tangan', 60, 565);

                doc.text(': ', 200, 565);

                doc.fontSize(11)
                    .text('Demikian surat keterangan ini dibuat unutk dipergunakan sebagaimana mestinya.', 60, 585);

                // TANDA TANGAN
                doc.text('Bogor, '+tanggalName1+' '+bulanName1+' '+tahunName1, 300, 615);

                doc.text('Komisi Pembimbing,', 300, 630);

                doc.text(dosen1, 300, 685);

                doc.text('NIP. '+nip1, 300, 700);

                // END
                doc.end();

                });
            });


            }
        })
        }



    };

    this.getKelengkapan = function(id, res) {
        var token = id;
        if (!token){
            res.json({status: false, message: 'No token provided'})
        }
        else {
        jwt.verify(token, 'yangPentingPanjang', function(err, decoded) {
            if (err) {
                res.json({status: false, message: 'Token authorization failed'});
                throw err;
            }
            else {
                var decoded = jwt.verify(token, 'yangPentingPanjang');
                var nim = decoded.nim;

                connection.acquire(function(err, con) {
                con.query('select * from mahasiswa_sidang_view where nim = ?', [nim], function(err, result) {

                var nama = result[0].nama;

                var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

                var bulanName1 = bulan[date.getMonth()];
                var tanggalName1 = date.getDate();
                var tahunName1 = date.getFullYear();

                con.release();
                var doc = new PDF();
                res.statusCode = 200;
                res.setHeader('Content-disposition', 'attachment; filename=Kelengkapan Ujian Tugas Akhir.pdf');

                doc.pipe(res);

                doc.font('fonts/tnr.ttf');


                doc.fontSize(10)
                    .text('POB/KOM-PP/11/FRM-05-00', 400, 20);

                doc.text(' ',50,50);

                doc.image('img/header.png', {
                    fit: [500, 300],
                    align: 'center'
                });

                doc.fontSize(12)
                    .text('KELENGKAPAN KESIAPAN UJIAN TUGAS AKHIR (KOM499)', {
                        align: 'center'
                    }, 170, 100);

                doc.fontSize(12)
                    .text('TAHUN AKADEMIK '+tahun+'/'+tahunNext, {
                        align: 'center'
                    });

                // NAMA
                doc.fontSize(11)
                    .text('Nama', 60, 220);

                doc.text(': '+nama, 200, 220);

                // NIM
                doc.fontSize(11)
                    .text('NIM', 60, 240);

                doc.text(': '+nim, 200, 240);

                doc.text('', 58,280);

                doc.image('img/tabel.png', {
                    fit: [480, 300],
                    align: 'center'
                });


                // TANDA TANGAN
                doc.text('Bogor, '+tanggalName1+' '+bulanName1+' '+tahunName1, 320, 560);

                doc.text('Komisi Pendidikan Program S1,', 320, 575);

                doc.moveTo(320, 665)
                    .lineTo(520, 665)
                    .stroke();

                doc.text('NIP. ', 320, 670);

                // END
                doc.end();

                });
            });

            }
        })
        }




    };

    this.row = function (doc, height) {
        doc.lineJoin('miter')
        .rect(205, height, 340, 35)
        .stroke()
        return doc
    }

    this.rowAbsen = function (doc, height) {
        doc.lineJoin('miter')
        .rect(60, height, 490, 20)
        .stroke()
        return doc
    }

    this.rowPerbaikan = function(doc, height) {
        doc
            .lineJoin('miter')
            .rect(60, height, 490, 150)
            .stroke()
        return doc;
    }

    this.rowPerbaikanParaf = function(doc, height) {
        doc
            .lineJoin('miter')
            .rect(330, height, 210, 55)
            .stroke()
        return doc
    } 

    this.rowPenilaian = function(doc, height) {
        doc.lineJoin('miter')
            .rect(205, height, 340, 185)
            .stroke()
        return doc;
    }

    this.rowBeritaAcaraSidang = function(doc, height) {
        doc.lineJoin('miter')
            .rect(60, height, 490, 50)
            .stroke()
        return doc
    }

}

module.exports = new Demo();
