var PDF = require('pdfkit');            //including the pdfkit module
var fs = require('fs');
var connection = require('../connection');
var jwt = require('jsonwebtoken');

var date = new Date();
var month = date.getMonth();
var tahun = date.getFullYear();

var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

if (month < 9) tahun--;
var tahunNext = tahun + 1;

function logPDF(){

  this.logpdf = function(id, res) {

      var token = id;

      if (!token){
          res.json({status: false, message: 'No token provided'})
      }
      else {
          jwt.verify(token, 'yangPentingPanjang', function(err, decoded) {
              if (err){
                  res.json({status: false, message: 'Token authorization failed'});
                  throw err;
              }
              else {
                  var decoded = jwt.verify(token, 'yangPentingPanjang');
                  var nim = decoded.nim;

                  connection.acquire(function(err, con) {
                      con.query('select * from ta_log_view where nim = ?', [nim], function(err, result1) {

                          if(result1.length > 9) {
                              con.query('select * from ta_view where nim = ?', [nim], function(err, result) {

                                  con.release();

                                  var nim = result[0].nim;
                                  var nama = result[0].nama;
                                  var topik = result[0].topik;
                                  var dosen1 = result[0].dosen1;
                                  var nip = result[0].nip1;
                                  var dosen2 = result[0].dosen2;
                                  if (!result[0].dosen2) dosen2 = '';
                                  var penguji1 = result[0].penguji1;
                                  var penguji2 = result[0].penguji2;

                                  var doc = new PDF();
                                  res.statusCode = 200;
                                  res.setHeader('Content-disposition', 'attachment; filename=Log Bimbingan Tugas Akhir.pdf');

                                  doc.pipe(res);

                                  doc.font('fonts/tnr.ttf');

                                  doc.fontSize(7)
                                      .text('LOG BIMBINGAN TUGAS AKHIR', 410, 20);

                                  doc.text(' ', 50, 50);

                                  doc.image('img/header.png', {
                                      fit: [500, 300],
                                      align: 'center'
                                  });

                                  doc.fontSize(14)
                                      .text('LOG BIMBINGAN TUGAS AKHIR', {
                                          align: 'center'
                                      }, 170, 100);

                                  // NAMA
                                  doc.fontSize(11)
                                      .text('Nama/NIM', 60, 190);

                                  doc.text(': '+nama+' / '+nim, 170, 190);

                                  // NAMA
                                  doc.fontSize(11)
                                      .text('Pembimbing 1', 60, 205);

                                  doc.text(': '+dosen1, 170, 205);

                                  // NAMA
                                  doc.fontSize(11)
                                      .text('Pembimbing 2', 60, 220);

                                  doc.text(': '+dosen2, 170, 220);

                                  // NAMA
                                  doc.fontSize(11)
                                      .text('Penguji 1', 60, 235);

                                  doc.text(': '+penguji1, 170, 235);

                                  lebih = 0;
                                  if(penguji2) {
                                      // NAMA
                                      doc.fontSize(11)
                                          .text('Penguji 2', 60, 250);

                                      doc.text(': '+penguji2, 170, 250);
                                      lebih += 15;
                                  }


                                  // TOPIK
                                  doc.fontSize(11)
                                      .text('Judul Tugas Akhir', 60, 250 + lebih);

                                  doc.text(': '+topik, 170, 250 + lebih);


                                  doc.moveTo(60, 295)
                                      .lineTo(545, 295)
                                      .stroke();

                                  // TOPIK
                                  doc.fontSize(11)
                                      .text('No.', 60, 300 + lebih);

                                  doc.fontSize(11)
                                      .text('Waktu', 85, 300 + lebih);

                                  doc.fontSize(11)
                                      .text('Topik Bimbingan', 170, 300 + lebih);

                                  doc.fontSize(11)
                                      .text('Tempat', 470, 300 + lebih);

                                  doc.moveTo(60, 320)
                                      .lineTo(545, 320)
                                      .stroke();

                                  count = 1;
                                  height = 330;
                                  for(i = 0; i < result1.length; i++) {

                                      d = new Date(result1[i].tanggal);

                                      bulanName = bulan[d.getMonth()];
                                      tanggalName = d.getDate();
                                      tahunName = d.getFullYear();

                                      doc.fontSize(11)
                                          .text(' '+count, 60, height);

                                      doc.fontSize(11)
                                          .text(tanggalName+' '+bulanName+' '+tahunName, 85, height);

                                      doc.fontSize(11)
                                          .text(result1[i].topik, 170, height, {width: 290});

                                      doc.fontSize(11)
                                          .text(result1[i].tempat, 470, height);

                                      height += 75;
                                      count++;

                                      if(height > 700) {
                                          doc.addPage();
                                          height = 80;
                                      }
                                  }

                                  bulanName1 = bulan[date.getMonth()];
                                  tanggalName1 = date.getDate();
                                  tahunName1 = date.getFullYear();

                                  if(height > 550) {
                                      doc.addPage();
                                      height = 80;
                                  }

                                  doc.text('Bogor, '+tanggalName1+' '+bulanName1+' '+tahunName1, 310, height);

                                  doc.text('Mengetahui dan menyetujui', 310, height+15);
                                  doc.text('Ketua Komisi Pembimbing', 310, height+30);

                                  doc.text(dosen1, 310, height+105);
                                  doc.text('NIP. '+nip, 310, height+125);

                                  // END
                                  doc.end();

                              });
                          }
                          else {
                              con.release();
                              res.json({status: false, message: 'Maaf, kamu belum melakukan log bimbingan minimal 10 kali. Silahkan lakukan log bimbingan minimal 10 kali terlebih dahulu.'})
                          }

                      });
                  });

              }

          });
      }


  };

}

module.exports = new logPDF();
