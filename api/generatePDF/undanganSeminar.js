var PDF = require('pdfkit');            //including the pdfkit module
var fs = require('fs');
var connection = require('../connection');
var jwt = require('jsonwebtoken');
var path = require('path');

var date = new Date();
var month = date.getMonth();
var tahun = date.getFullYear();

var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu'];

if (month < 9) tahun--;
var tahunNext = tahun + 1;

function capitalize(str) {
    return str.replace(/\w\S*/g, function(txt){
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function UndanganSeminar(){

    this.seminarUndangan = function(nim, id, res) {
        connection.acquire(function(err, con) {
            con.query('select * from seminar_sidang_view where nim = ?', [nim], function(err, result) {

                var nama = result[0].nama;
                var tanggal = result[0].tanggal_seminar;
                var jam = result[0].mandiri_jam;
                var tempat = result[0].mandiri_tempat;
                var dosen1 = result[0].dosen1;
                var dosen2 = result[0].dosen2;
                var penguji1 = result[0].penguji1;
                var penguji2 = result[0].penguji2 ;

                con.release();
                var doc = new PDF();
                // res.statusCode = 200; 
                // res.setHeader('Content-disposition', 'attachment; filename=Undangan Seminar.pdf');

                doc.pipe(fs.createWriteStream(path.join(__dirname, '/../uploads/fileSeminar/undangan/undangan.pdf')));
                doc.font('fonts/tnr.ttf');

                // NAMA
                doc.fontSize(10.5)
                .text('Nomor', 92, 160);

                doc.text(':          /IT3.7.1/PP.05/2017', 160, 160);

                doc.text(' ',50,50);

                doc.image('img/header.png', {
                    fit: [500, 300],
                    align: 'center'
                });

                bulanName1 = bulan[date.getMonth()];
                tanggalName1 = date.getDate();
                tahunName1 = date.getFullYear();
                doc.text(tanggalName1+' '+bulanName1+' '+tahunName1, 420, 160);

                // LAMPIRAN
                doc.text('Lampiran', 92, 175);

                doc.text(':   --', 160, 175);

                // PERIHAL
                doc.text('Perihal', 92, 190);

                doc.font('fonts/tnr-bold.ttf');
                doc.text(':   Undangan Seminar Tugas Akhir', 160, 190);
                doc.text('    Program Studi Ilmu Komputer', 160, 205);


                doc.font('fonts/tnr.ttf');

                // LAMPIRAN
                doc.text('Kepada Yth.', 92, 240);

                doc.text(':   '+penguji1, 160, 240);
                doc.text('(Penguji 1)', 410, 240);

                if(penguji2) {
                doc.text('    '+penguji2, 160, 255);
                doc.text('(Penguji 2)', 410, 255);

                doc.text('    '+dosen1, 160, 270);
                doc.text('(Pembimbing 1)', 410, 270);
                } else {
                doc.text('    '+dosen1, 160, 255);
                doc.text('(Pembimbing 1)', 410, 255);

                doc.text('    '+dosen2, 160, 270);
                doc.text('(Pembimbing 2)', 410, 270);
                }


                // TEXT
                doc.text('Dengan ini kami mengundang Bapak/Ibu untuk hadir dalam pelaksanaan Seminar Tugas Akhir mahasiswa Program Studi Ilmu Komputer berikut:', 92, 320);

                doc.text('Nama Mahasiswa', 170, 370);
                doc.text(':   '+capitalize(nama), 280, 370);

                doc.text('NIM', 170, 385);
                doc.text(':   '+nim, 280, 385);

                doc.text('pada:', 92, 415);


                // TANGGAL

                var d = new Date(tanggal);

                var dayName = days[d.getDay()];
                var bulanName = bulan[d.getMonth()];
                var tanggalName = d.getDate();
                var tahunName = d.getFullYear();

                doc.text('Hari, Tanggal', 170, 445);
                doc.text(':   '+dayName+', '+tanggalName+' '+bulanName+' '+tahunName, 280, 445);

                jam = jam.replace(':', '.');
                d = new Date(jam);

                doc.text('Pukul', 170, 460);
                doc.text(':   '+jam+' WIB', 280, 460);

                doc.text('Tempat', 170, 475);
                doc.text(':   '+tempat, 280, 475);

                // PENUTUP
                doc.text('Kehadiran Bapak/Ibu sangat kami harapkan. Atas perhatian dan kerja sama yang diberikan, kami ucapkan terima kasih.', 92, 505);

                // TTD
                doc.text('Ketua Program Studi S1,', 290, 545);

                doc.font('fonts/tnr-bold.ttf');
                doc.text('Dr. Imas Sukaesih Sitanggang, S.Si, M.Kom', 290, 630);

                doc.font('fonts/tnr.ttf');
                doc.text('NIP. 19750130 199802 2 001', 290, 645);


                doc.end();

            });
        });
    };

}

module.exports = new UndanganSeminar();