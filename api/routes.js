var daftar = require('./models/daftar');
var pembimbingKedua = require('./models/dosen2');
var log = require('./models/log');
var kolokium = require('./models/kolokium');
var konferensi = require('./models/konferensi');
var praseminar = require('./models/praseminar');
var micon = require('./models/micon');
var mandiri = require('./models/mandiri');
var sidang = require('./models/sidang');
var profile = require('./models/profile');
var ta = require('./models/ta');
var jadwalKolokium = require('./models/jadwalKolokium');
var jadwalPraseminar = require('./models/jadwalPraseminar');
var dosen = require('./models/dosen');
var status = require('./models/status');
var mahasiswa = require('./models/mahasiswa');
var dataSeminar = require('./models/seminar');
var konfirmasi = require('./models/konfirmasi');
var skl = require('./models/skl');
var staff = require('./models/staff');
var soap = require('./models/soap');
var url = require('url');
var querystring = require('querystring');
var demo = require('./generatePDF/demo');
// var pdfDemo = require('./generatePDF/demo');

var fc = require('./fc');
var summaryDosen = require('./models/summaryDosen');

module.exports = {
  configure: function(app) {

// SOAP SYNC
    app.get('/soap/mahasiswa', function(req, res) {
      soap.test(res);
    })

// STAFF
    app.get('/staff', function(req, res) {
      staff.get(res);
    })

// SUMMARY DOSEN
    app.get('/summary/dosen', function(req, res) {
      summaryDosen.getDosenSummary(res);
    });

// STATUS
    app.get('/status', function(req, res) {
      status.get(res);
    });

// PENDATAAN TA
    app.get('/ta/', function(req, res) {
      ta.getById(req.params.id, res);
    });

    app.put('/ta/edit/', function(req, res){
      ta.update(req.body, res);
    });

    app.put('/ta/update/', function(req, res){
      ta.updateTopik(req.body, res);
    });

    app.post('/ta/penentuan/', function(req, res) {
      ta.getPenentuanTa(req.body, res);
    });


// PENGAJUAN TA
    app.get('/ta/daftar/list', function(req, res){
      daftar.get(res);
    });

    app.get('/ta/daftar/detail', function(req, res) {
      daftar.getById(res);
    });

    app.post('/ta/pengajuan/', function(req, res){
      daftar.create(req.body, res);
    });

    app.get('/ta/daftar/detail/:id/', function(req, res){
      daftar.getByNim(req.params.id, res);
    });

// SIMETA v2
// PENGAJUAN PEMBIMBINGAN KEDUA
    app.post('/pembimbing-kedua/konfirmasi/set/', function(req, res) {
      pembimbingKedua.set(req.body, res);
    });
    app.get('/pembimbing-kedua/konfirmasi/pertama/get/', function(req, res) {
      pembimbingKedua.getDataPertama(res);
    });
    app.post('/pembimbing-kedua/konfirmasi/pertama/set/', function(req, res) {
      pembimbingKedua.setAcceptedPertama(req.body, res);
    });
    app.post('/pembimbing-kedua/konfirmasi/pertama/delete', function(req, res) {
      pembimbingKedua.delete(req.body, res);
    });

    app.get('/pembimbing-kedua/konfirmasi/kedua/get/', function(req,res) {
      pembimbingKedua.getDataKedua(res);
    });
    app.post('/pembimbing-kedua/konfirmasi/kedua/set/', function(req, res){
      pembimbingKedua.setAcceptedKedua(req.body, res);
    });
    app.get('/pembimbing-kedua/konfirmasi/kepala-bagian/get', function(req, res) {
      pembimbingKedua.getDataKepalaBagian(res);
    });
    app.post('/pembimbing-kedua/konfirmasi/kepala-bagian/set', function(req, res) {
      pembimbingKedua.setAcceptedKepalaBagian(req.body, res);
    })
    app.get('/pembimbing-kedua/konfirmasi/get/status', function(req, res) {
      pembimbingKedua.getStatus(res);
    });
    app.get('/pembimbing-kedua/admin/get/', function(req, res) {
      pembimbingKedua.getDataAdmin(res);
    });
    app.post('/pembimbing-kedua/admin/set', function(req,res) {
      pembimbingKedua.setAcceptedAdmin(req.body, res);
    })

// LOG
    app.post('/log/', function(req, res){
      log.create(req, req.body, res);
    });

    app.post('/log/balas/mahasiswa/', function(req, res){
      log.balasMahasiswa(req.body, res);
    });

    app.post('/log/balas/dosen/', function(req, res){
      log.balasDosen(req.body, res);
    });

    app.get('/log/', function(req, res){
      log.getByNim(req.params.id, res);
    });

    app.get('/log/dosen/', function(req, res){
      log.getByDosenId(req.body, res);
    });

    app.get('/log/detail/:id', function(req, res){
      log.getById(req.params.id, res);
    });

    app.get('/log/count/all', function(req, res){
      log.getCount(res);
    });

    app.get('/log/approval', function(req, res){
      log.getApprovalCount(res);
    });

    app.get('/log/jawaban/:id', function(req, res){
      log.getJawabanById(req.params.id, res);
    });

    app.post('/log/approve/', function(req, res){
      log.approve(req.body, res);
    });

    app.put('/log/delete/', function(req, res){
      log.deleteLog(req.body, res);
    });


// KOLOKIUM
    app.post('/kolokium/', function(req, res){
      kolokium.create(req.body, res);
    });

// KOLOKIUM
    app.post('/micon/', function(req, res){
      micon.create(req.body, res);
    });

// GET DATA FILE
    app.get('/kolokium/', function(req, res) {
      kolokium.getDataFile(res);
    });

    app.get('/kolokium/all', function(req, res) {
      kolokium.getAll(res);
    });

    app.get('/praseminar', function(req, res) {
      praseminar.getDataFile(res);
    });

// PRASEMINAR
    app.post('/praseminar/', function(req, res){
      praseminar.create(req.body, res);
    });

// PROFILE
    app.post('/profile/', function(req, res){
      profile.update(req.body, res);
    });

    app.get('/profile/', function(req, res){
      profile.getById(req.params.id, res);
    });


// PDF
    app.get('/pdf/', function(req, res){
      demo.get(res);
    });

// JADWAL KOLOKIUM
    app.put('/jadwalKolokium/', function(req, res){
      jadwalKolokium.update(req.body, res);
    });

    app.get('/jadwalKolokium/', function(req, res){
      jadwalKolokium.get(res);
    });

    app.get('/jadwalMicon/', function(req, res){
      micon.getData(res);
    });

    app.put('/jadwalMicon/', function(req, res){
      micon.update(req.body, res);
    });


// JADWAL PRASEMINAR
    app.get('/jadwalPraseminar/', function(req, res){
      jadwalPraseminar.get(res);
    });

    app.put('/jadwalPraseminar/', function(req, res){
      jadwalPraseminar.update(req.body, res);
    });


// DOSEN
    app.get('/dosen/', function(req, res){
      dosen.get(res);
    });
    app.get('/dosen/id', function(req, res) {
      dosen.getById(res);
    });

// SIDANG
    app.get('/sidang/all', function(req, res){
      sidang.get(res);
    });

    app.get('/sidang', function(req, res){
      sidang.getById(res);
    })

    app.post('/sidang', function(req, res) {
      sidang.set(req.body, res);
    });

    app.put('/sidang', function(req, res) {
      sidang.update(req.body, res);
    });

    app.post('/sidang/get/status', function(req, res) {
      sidang.getStatus(req.body, res);
    })

    app.post('/sidang/get/admin', function(req, res) {
      sidang.getMahasiswaAdmin(req.body, res);
    })
    
    app.get('/sidang/get/mahasiswa', function(req, res) {
      sidang.getMahasiswaDosen(res);
    });

    app.post('/sidang/konfirmasi/pertama/set', function(req, res) {
      sidang.konfirmasiPertama(req.body, res);
    });

    app.post('/sidang/konfirmasi/pertama/delete', function(req, res) {
      sidang.declinePertama(req.body, res);
    });

    app.post('/sidang/konfirmasi/set', function(req, res) {
      sidang.konfirmasi(req.body, res);
    });

    app.post('/sidang/konfirmasi/delete', function(req, res) {
      console.log('masuk url api delete');
      sidang.decline(req.body, res);
    });

    app.post('/sidang/konfirmasi/admin/set', function(req, res) {
      sidang.konfirmasiAdmin(req.body, res);
    })

// MAHASISWA
    app.get('/mahasiswa/all', function(req, res) {
      mahasiswa.getMahasiswa(res);
    });

    app.get('/mahasiswa/kolokium', function(req, res) {
      mahasiswa.getDataKolokium(res);
    });

    app.get('/mahasiswa/kolokium/dosen', function(req, res) {
      mahasiswa.getDataKolokiumDosen(res);
    });

    app.get('/mahasiswa/praseminar', function(req, res) {
      mahasiswa.getDataPraseminar(res);
    });

    app.get('/mahasiswa/praseminar/dosen', function(req, res) {
      mahasiswa.getDataPraseminarDosen(res);
    });

    app.get('/mahasiswa/sidang', function(req, res) {
      mahasiswa.getDataSidang(res);
    });

    app.get('/mahasiswa/sidang/dosen', function(req, res) {
      mahasiswa.getDataSidangDosen(res);
    });

    app.get('/mahasiswa/skl', function(req, res) {
      mahasiswa.getDataSKL(res);
    });

    app.get('/mahasiswa/skl/dosen', function(req, res) {
      mahasiswa.getDataSKLDosen(res);
    });

    app.get('/mahasiswa/bimbingan', function(req, res) {
      mahasiswa.getDataBimbingan(res);
    });

    app.get('/mahasiswa/seminar/all', function(req, res) {
      mahasiswa.getDataSeminarAll(res);
    });

    app.get('/mahasiswa/seminar/dosen', function(req, res) {
      mahasiswa.getDataSeminarDosen(res);
    });

// SEMINAR
    app.get('/seminar', function(req, res) {
      dataSeminar.get(res);
    });

    app.get('/seminar/delete', function(req, res) {
      dataSeminar.deleteSeminar(res);
    });

// KONFERENSI
    app.post('/seminar/konferensi', function(req, res) {
      konferensi.create(req.body, res);
    });

// MANDIRI
    app.post('/seminar/mandiri', function(req, res) {
      mandiri.set(req.body, res);
    });

    // SIMETA v2
    app.post('/seminar/mandiri/get', function(req, res) {
      mandiri.get(req.body, res);
    });
    app.get('/seminar/mandiri/get/mahasiswa', function(req, res) {
      mandiri.getMahasiswa(res);
    });
    app.post('/seminar/mandiri/status/get', function(req, res) {
      mandiri.getStatus(req.body, res);
    });
    app.post('/seminar/mandiri/set/pembahas', function(req, res) {
      mandiri.setPembahas(req.body, res);
    });
    app.post('/seminar/mandiri/konfirmasi/berkas', function(req, res) {
      mandiri.konfirmasiBerkas(req.body, res);
    });
    app.post('/seminar/mandiri/konfirmasi/admin', function(req, res) {
      mandiri.konfirmasiAdmin(req.body, res);
    });
    app.post('/seminar/mandiri/konfirmasi/pertama', function(req, res) {
      mandiri.konfirmasiPertama(req.body, res);
    });
    app.post('/seminar/mandiri/konfirmasi', function(req, res) {
      console.log('masuk konfirmasu');
      mandiri.konfirmasi(req.body, res);
    });
    app.post('/seminar/mandiri/delete/pertama', function(req, res) {
      mandiri.declinePertama(req.body, res);
    });
    app.post('/seminar/mandiri/delete/admin', function(req, res) {
      mandiri.declineAdmin(req.body, res);
    });
    app.post('/seminar/mandiri/delete', function(req, res) {
      mandiri.decline(req.body, res);
    });
    app.post('/seminar/mandiri/pdf/beritaacra', function(req, res) {

    });

// KONFIRMASI
  app.put('/konfirmasi/kolokium', function(req, res) {
    konfirmasi.konfirmasiKolokium(req.body, res);
  });

  app.put('/konfirmasi/praseminar', function(req, res) {
    konfirmasi.konfirmasiPraseminar(req.body, res);
  });

  app.get('/skl', function(req, res) {
    skl.getById(res);
  });

  app.post('/skl', function(req, res) {
    skl.create(req.body, res);
  })

  }
};
