var connection = require('../connection');
var _ = require('../simak');

function Ta(){

  this.update = function(todo, res) {

    if (_.decoded.role == 4 || _.decoded.role == 1) {
      connection.acquire(function(err, con) {
        var query = 'SELECT * FROM ta WHERE nim = ?';
        var data = [todo.nim];
        con.query(query, data, function(err, result) {

          if (err) {
            con.release();
            res.json({status: 0, message: 'Update TA Gagal'});
            throw err;
          }
          else {
            if (result.length > 0) {

              var queryUpdate = 'UPDATE ta SET topik = ?, lab = ?, dosen_1 = ?, penguji_1 = ?, penguji_2 = ? WHERE nim = ?';
              var dataUpdate = [todo.topik, todo.lab, todo.dosen_1, todo.penguji_1, todo.penguji_2, todo.nim];

              con.query(queryUpdate, dataUpdate, function(err, result) {
                con.release();
                if (err) {
                  res.json({status: 0, message: 'Update TA Gagal'});
                  throw err;
                }
                else {
                  res.json([{status: 1, message: 'Update TA Berhasil'}]);
                }
              })
            }
            else {
              var queryInsert = 'INSERT INTO ta (status, nim, topik, lab, dosen_1, penguji_1, penguji_2) VALUES (1, ?, ?, ?, ?, ?, ?, ?)';
              var dataInsert = [todo.nim, todo.topik, todo.lab, todo.dosen_1, todo.penguji_1, todo.penguji_2];

              con.query(queryInsert, dataInsert, function(err, result) {
                con.release();
                if (err) {
                  res.json({status: 0, message: 'Penentuan TA Gagal'});
                  throw err;
                }
                else {
                  res.json({status: 1, message: 'Penentuan TA Berhasil'});
                }
              })
            }
          }
        });
      });
    }

    else {
      con.release();
      res.json({status: false, message: "You Can't Access This Service"});
    }

  };

  this.updateTopik = function(todo, res) {
    connection.acquire(function(err, con) {
      var query = 'update ta set topik = ? where nim = ?';
      var data = [todo.topik, _.decoded.nim];
      con.query(query, data, function(err, result) {
        con.release();
        if (err) {
          res.json({status: 0, message: 'Update TA Gagal'});
          throw err;
        } else {
          res.json([{status: 1, message: 'Update TA Berhasil'}]);
        }
      });
    });
  };

  this.getById = function(id, res) {
    connection.acquire(function(err, con) {
      con.query('select * from ta_view where nim = ?', _.decoded.nim, function(err, result) {
        con.release();
        if(err) {
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          res.json(result);
        }
      });
    });
  };

  this.getPenentuanTa = function(todo, res) {

    connection.acquire(function(err, con) {
      con.query('select * from ta_view where nama = ?', [todo.nama], function(err, result) {
        if (err) {
          res.json({status: 0, message: 'Penentuan TA Gagal'});
          throw err;
        }
        else {

          if (result.length > 0) {
            res.json(result);
          }
          else {

            con.query('select * from ta_daftar_view where nama = ?', [todo.nama], function(err, result) {
              con.release();
              if (err) {
                res.json({status: 0, message: 'Penentuan TA Gagal'});
                throw err;
              }
              else {
                res.json(result);
              }
            })
          }
        }
      });
    });
    
  };
}

module.exports = new Ta();
