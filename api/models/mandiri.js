var connection = require('../connection');
var _ = require('../simak');
var fc = require('../fc');
var mail = require('../mail');
var jwt = require('../direct/generateJWT');
var undangan = require('../generatePDF/undanganSeminar');

function Mandiri() {

  // SIMETA v2
  // Mendapatkan data mahasiswa untuk admin.
  this.get = function(todo, res) {
    connection.acquire((err, con) => {
      if (err) {
        con.release();
        res.json({status: 0, code: 500, message: 'Internal server error (0)'});
        throw err;
      } else {
        const creds = {
          select: {
            query: `
              SELECT
                m.nama,
                m.nim,
                m.tanggal,
                m.topik,
                m.dosen1,
                m.dosen2,
                m.penguji1,
                m.penguji2,
                m.hp,
                m.email,
                p.tempat,
                p.jam,
                p.pembahas1,
                p.pembahas2,
                p.pembahas3
              FROM 
                mandiri_view AS m
              INNER JOIN
                pembahas_view AS p ON m.nim = p.nim
              WHERE
                m.nama = ?
            `,
            data: [
              todo.nama
            ]
          }
        };
        con.query(creds.select.query, creds.select.data, (err1, res1) => {
          con.release();
          if (err1) {
            res.json({status: 0, code: 500, message: 'Internal server error (1)'});
            throw err1;
          } else {
            res.json({status: 1, code: 200, message: 'Success', data: res1});
          }
        });
      }
    });
  }
  
  // Mendapatkan data mahasiswa untuk dosen (pembimbing dan penguji)
  this.getMahasiswa = function(res) {
    connection.acquire((err, con) => {
      if (err) {
        res.json({status: 0, code: 500, message: 'Internal server error (0).'});
        throw err;
      } else {
        const creds = {
          select: {
            query: `
              SELECT
                m.tahun_masuk,
                m.nama,
                m.nim,
                ta.topik,
                ta.dosen_1,
                ta.dosen_2,
                ta.penguji_1,
                ta.penguji_2,
                tas.konfirmasi_pembimbing1,
                tas.konfirmasi_pembimbing2,
                tas.konfirmasi_penguji1,
                tas.konfirmasi_penguji2,
                tam.tempat,
                tam.jam,
                tam.tanggal
              FROM
                mahasiswa AS m
              INNER JOIN
                ta_seminar AS tas ON m.nim = tas.nim
              INNER JOIN
                ta_mandiri AS tam ON tas.nim = tam.nim
              INNER JOIN
                ta ON tam.nim = ta.nim
              WHERE
                tam.status_formulir = 1 
              AND 
                (
                  (ta.dosen_1 = ? AND (tas.konfirmasi_pembimbing1 = 0 OR tas.konfirmasi_pembimbing1 = 2)) OR 
                  (ta.dosen_2 = ? AND tas.konfirmasi_pembimbing2 = 0) OR 
                  (ta.penguji_1 = ? AND tas.konfirmasi_penguji1 = 0) OR 
                  (ta.penguji_2 = ? AND tas.konfirmasi_penguji2 = 0)
                )
            `,
            data: [_.decoded.id, _.decoded.id, _.decoded.id, _.decoded.id]
          }
        }
        // res for result not response.
        con.query(creds.select.query, creds.select.data, (err1, res1) => {
          con.release();
          if (err1) {
            res.json({status: 0, code: 500, message: 'Internal server error(1).'});
            throw err1;
          } else {
            // Memberikan status kepada dosen dengan mengecek id masing-masing dosen
            res1.forEach(element => {
              if (element.dosen_1 == _.decoded.id) {
                element.dosen_status = 'Dosen pembimbing ketua';
              } else if (element.dosen_2 == _.decoded.id) {
                element.dosen_status = 'Dosen pembimbing anggota';
              } else if (element.penguji_1 == _.decoded.id) {
                element.dosen_status = 'Penguji pertama';
              } else if (element.penguji_2 == _.decoded.id) {
                element.dosen_status = 'Penguji kedua';
              }
            });
            res.json({status: 1, code: 200, message: 'Success', data: res1});
          }
        });
      }
    })
  }
  
  // Mendapatkan data-data status konfirmasi
  this.getStatus  = function(todo, res) {
    connection.acquire((err, con) => {
      if (todo.nim) {
        const creds = {
          select: {
            query: `
              SELECT
                tam.status_tempat, 
                tam.status_formulir,
                tam.status_admin,
                tas.konfirmasi_pembimbing1,
                tas.konfirmasi_pembimbing2,
                tas.konfirmasi_penguji1,
                tas.konfirmasi_penguji2
              FROM
                ta_mandiri AS tam
              INNER JOIN
                ta_seminar AS tas ON tam.nim = tas.nim
              WHERE
                tam.nim = ?
              AND
                tas.nim = ?
            `,
            data: [todo.nim, todo.nim]
          }
        }
        con.query(creds.select.query,creds.select.data, (err1, res1) => {
          con.release();
          if (err1) {
            res.json({status: 0, code: 500, message: 'Internal server error (1).'});
            throw err1;
          } else {
            res.json({status: 1, code: 200, message: 'Success', data: res1});
          }
        });
      } else {
        res.json({status: 0, code: 400, message: 'Bad request'});
      }
    });
  }
 
  // Mahasiswa submit pengajuan pelaksanaan seminar mandiri.
  this.set = function(todo, res){
    connection.acquire(function(err, con){
      if (err) {
        con.release();
        res.json({status: 0, code: 500, message: 'Internal server error (0)'});
        throw err;
      } else {
        const creds = {
          update: {
            first: {
              query: `
                UPDATE 
                  ta 
                SET topik = ? 
                WHERE nim = ?
              `,
              data: [todo.topik, _.decoded.nim]
            },  
            second: {
              query: `
                UPDATE
                  ta_mandiri
                SET
                  jam = ?, tanggal = ?, tempat = ?, status_formulir = ?
                WHERE
                  nim = ?
              `,
              data: [todo.jam, todo.tanggal, todo.tempat, 1, _.decoded.nim]
            },
          },
          insert: {
            query: `
              INSERT INTO 
                ta_mandiri (nim, pembahas_1, pembahas_2, pembahas_3, tempat, jam, tanggal, status_formulir) 
              VALUES (?, ?, ?, ?, ?, ?, ?, ?)
            `,
            data: [_.decoded.nim, todo.pembahas_1, todo.pembahas_2, todo.pembahas_3, todo.tempat, todo.jam, todo.tanggal, 1]
          },
          select : {
            query: `
              SELECT
                nim
              FROM
                ta_mandiri
              WHERE
                nim = ?
            `,
            data: [_.decoded.nim]
          }
        };
        const credsMhs = {
          nama: todo.nama,
          nim: todo.nim,
          dosen1: todo.dosen1,
          topik: todo.topik,
          tanggal: todo.tanggal,
          jam: todo.jam
        };
        const token = jwt.create(2, credsMhs.nim);
        con.query(creds.select.query, creds.select.data, function(err1, res1) {
          if(err1) {
            con.release();
            res.json({status: 0, code: 500, message: 'Internal server error (1).'});
            throw err1;
          }
          else {
            con.query(creds.update.first.query, creds.update.first.data, function(err2, res2) {
              if(err2) {
                con.release();
                res.json({status: 0, code: 500, message: 'Internal server error (2).'});
                throw err2;
              }
              else {
                // cek apakah sudah ada di tabel ta_mandiri
                if(res1.length > 0) {
                  con.query(creds.update.second.query, creds.update.second.data, function(err3, res3) {
                    con.release();
                    if (err3) {
                      res.json({status: 0, code: 500, message: 'Internal server error (3).'});
                      throw err3;
                    } else {
                      // assign data ke tabel ta_seminar
                      fc.createSeminar(3, res, todo.tanggal);
                      // kirim email untuk penguji/pembimbing
                      mail.setMandiriPertama(res, credsMhs, token);
                      res.json({status: 1, code: 200, message: 'Success'});
                    }
                  });
                }
                else {
                  con.query(creds.insert.query, creds.insert.data, function(err3, res3) {
                    con.release();
                    if (err3) {
                      res.json({status: 0, code: 500, message: 'Internal server error (3).'});
                      throw err3;
                    } else {
                      // assign data ke tabel ta_seminar
                      fc.createSeminar(3,res, todo.tanggal);
                       // kirim email untuk penguji/pembimbing
                      mail.setMandiriPertama(res, credsMhs, token);
                      res.json({status: 1, code: 200, message: 'Success'});
                    }
                  });
                }
              }
            });
          }
        });
      }
    });
  };

  // Mahasiswa submit pembahas ketika syarat pelaksanaan seminar mandiri sudah mencukupi
  this.setPembahas = function(todo, res) {
    console.log(todo);
    connection.acquire((err, con) => {
      if (err) {
        con.release();
        res.json({status: 0, code: 500, message: 'Internal server error (0)'});
        throw err;
      } else {
        const creds = {
          update: {
            query: `
              UPDATE
                ta_mandiri
              SET
                pembahas_1 = ?,
                pembahas_2 = ?,
                pembahas_3 = ?
              WHERE
                nim = ?
            `,
            data: [todo.pembahas_1, todo.pembahas_2, todo.pembahas_3, _.decoded.nim]
          }
        };
        con.query(creds.update.query, creds.update.data, (err1, res1) => {
          con.release();
          if (err1) {
            res.json({status: 0, code: 500, message: 'Internal server error (1).'});
            throw err1;
          } else {
            res.json({status: 1, code: 200, message: 'Success.'});
          }
        });
      }
    });
  }

  // Method penolakan dari pembimbing pertama
  this.declinePertama = function(todo, res) {
    if (todo.statusDosen === 'Dosen pembimbing ketua') {
      connection.acquire((err, con) => {
        if (err) {
          con.release();
          res.json({status: 0, code: 500, message: 'Internal server error (0)'});
          throw err;
        } else {
          const creds = {
            update: {
              query: `
                UPDATE
                  ta_mandiri AS m,
                  ta_seminar AS s
                SET
                  m.status_tempat = 0,
                  m.status_formulir = 0,
                  m.tempat = null,
                  m.jam = null,
                  m.tanggal = null,
                  s.tanggal = null,
                  s.konfirmasi_pembimbing1 = 2
                WHERE
                  m.nim = ?
                AND
                  s.nim = ?
                `,
              data: [todo.nim, todo.nim]
            },
            select: {
              query: `
                SELECT
                  nama,
                  email
                FROM
                  mahasiswa_detail
                WHERE
                  nim = ?
              `,
              data: [todo.nim]
            }
          }
          con.query(creds.update.query, creds.update.data, (err1, res1) => {
            if (err1) {
              con.release();
              res.json({status: 0, code: 500, message: 'Internal server error (1)'});
              throw err1;
            } else {
              con.query(creds.select.query, creds.select.data, (err2, res2) => {
                con.release();
                if (err2) {
                  res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                  throw err2;
                } else {
                  const credsMhs = {
                    nama: res2[0].nama,
                    email: res2[0].email,
                    alasan: todo.alasan,
                    statusDosen: todo.statusDosen
                  };
                  mail.declineMandiri(res, credsMhs);
                  res.json({status: 1, code: 200, message: 'Berhasil'});
                }
              });
            }
          });
        }
      }) 
    } else {
      res.json({status: 0, code: 403, message: 'Forbidden'});
    }
  }

  // Method penolakan dari admin
  this.declineAdmin = function(todo,res) {
    if (_.decoded.role === 4) {
      connection.acquire((err, con) => {
        if (err) {
          con.release();
          res.json({status: 0, code: 500, message: 'Internal server error (0)'});
          throw err;
        } else {
          const creds = {
            update: {
              query: `
                UPDATE
                  ta_mandiri AS m,
                  ta_seminar AS s
                SET
                  m.status_tempat = 0,
                  m.status_formulir = 0,
                  m.status_admin = 2,
                  m.tempat = null,
                  m.jam = null,
                  m.tanggal = null,
                  s.tanggal = null,
                  s.konfirmasi_pembimbing1 = 0,
                  s.konfirmasi_pembimbing2 = 0,
                  s.konfirmasi_penguji1 = 0,
                  s.konfirmasi_penguji2 = 0
                WHERE
                  m.nim = ?
                AND
                  s.nim = ?
                `,
              data: [todo.nim, todo.nim]
            },
            select: {
              query: `
                SELECT
                  nama,
                  email
                FROM
                  mahasiswa_detail
                WHERE
                  nim = ?
              `,
              data: [todo.nim]
            }
          }
          con.query(creds.update.query, creds.update.data, (err1, res1) => {
            if (err1) {
              res.json({status: 0, code: 500, message: 'Internal server error (1). Please contact the developer for further informations'});
              throw err1;
            } else {
              con.query(creds.select.query, creds.select.data, (err2, res2) => {
                con.release();
                if (err2) {
                  res.json({status: 0, code: 500, message: 'Internal server error (2). Please contact the developer for further informations'});
                  throw err2;
                } else {
                  const credsMhs = {
                    nama: res2[0].nama,
                    email: res2[0].email,
                    alasan: todo.alasan
                  };
                  mail.declineMandiri(res, credsMhs);
                  res.json({status: 1, code: 200, message: 'Berhasil'});
                }
              });
            }
          });
        }
      });
    } else {
      res.json({status: 0, code: 403, message: 'Forbidden'});
    }
  }
  
  // Method penolakan SELAIN dari pembimbing pertama
  this.decline = function(todo, res) {
    connection.acquire((err, con) => {
      if (err) {
        con.release();
        res.json({status: 0, code: 500, message: 'Internal server error (0)'});
        throw err;
      } else {
        let creds;
        let email;
        if (_.decoded.role === 4 || _.decoded.role === 2) {
          if (todo.statusDosen) {
            switch(todo.statusDosen) {
              case 'Dosen pembimbing anggota':
                creds = {
                  update: {
                    query: {
                      first: `
                        UPDATE
                          ta_seminar
                        SET
                          konfirmasi_pembimbing2 = 2
                        WHERE
                          nim = ?
                      `
                    },
                    data: {
                      first: [todo.nim]
                    }
                  },
                };
                break;
              case 'Penguji pertama' :
                creds = {
                  update: {
                    query: {
                      first: `
                        UPDATE
                          ta_seminar
                        SET
                          konfirmasi_penguji1 = 2
                        WHERE
                          nim = ?
                      `
                    }, 
                    data: {
                      first: [todo.nim]
                    }
                  }
                };
                break;
              case 'Penguji kedua':
                creds = {
                  update: {
                    query: {
                      first: `
                        UPDATE
                          ta_seminar
                        SET
                          konfirmasi_penguji2 = 2
                        WHERE
                          nim = ?
                      `
                    },
                    data: {
                      first: [todo.nim],
                    }
                  }
              };
              break;
            }
            creds.update.query.second = `
              UPDATE
                ta_seminar AS tas,
                ta_mandiri AS tam
              SET
                tas.konfirmasi_penguji2 = 0,
                tas.konfirmasi_penguji1 = 0,
                tas.konfirmasi_pembimbing1 = 0,
                tas.konfirmasi_pembimbing2 = 0,
                tam.status_tempat = 0,
                tam.status_formulir = 0,
                tam.tempat = null,
                tam.jam = null,
                tam.tanggal = null,
                tas.tanggal = null
              WHERE
                tas.nim = ? AND tam.nim = ?
            `;
            creds.update.data.second = [todo.nim, todo.nim];

            creds.select = {
              query: `
                SELECT
                  ta.penguji_2,
                  tas.konfirmasi_pembimbing1,
                  tas.konfirmasi_penguji1,
                  tas.konfirmasi_penguji2,
                  m.email,
                  m.nama
                FROM 
                  mahasiswa_detail AS m
                INNER JOIN
                  ta ON m.nim = ta.nim
                INNER JOIN
                  ta_seminar AS tas ON ta.nim = tas.nim
                WHERE
                  m.nim = ? AND
                  ta.nim = ? AND
                  tas.nim = ?
              `,
              data: [todo.nim, todo.nim, todo.nim]
            }; 
          } else {
            // Query untuk admin
              creds = {
                update: {
                  query: `
                    UPDATE
                      ta_mandiri AS m,
                      ta_seminar AS s
                    SET
                      m.status_tempat = 0,
                      m.status_admin = 2,
                      m.tempat = null,
                      m.jam = null,
                      m.tanggal = null,
                      s.konfirmasi_pembimbing1 = 0,
                      s.konfirmasi_pembimbing2 = 0,
                      s.konfirmasi_penguji1 = 0,
                      s.konfirmasi_penguji2 = 0,
                      s.
                      s.tanggal = null
                    WHERE
                      m.nim = ? AND
                      s.nim = ?
                  `,
                  data: [todo.nim, todo.nim]
                },
                select: {
                  query: `
                    SELECT 
                      email,
                      nama
                    FROM 
                      mahasiswa_detail
                    WHERE
                      nim = ?
                  `,
                  data:[todo.nim]
                }
              };
          }
          // Query untuk mengecek syarat minimal pelaksanaan mandiri
          const querySelectSyarat = `
            SELECT
              tas.konfirmasi_pembimbing1,
              tas.konfirmasi_pembimbing2,
              tas.konfirmasi_penguji1,
              tas.konfirmasi_penguji2,
              m.email,
              m.nama
            FROM
              ta_seminar AS tas
            INNER JOIN
              mahasiswa_detail AS m ON tas.nim = m.nim
            WHERE
              tas.nim = ? AND m.nim = ?  
          `;
          const queryUpdateSyarat = `
            UPDATE
              ta_mandiri
            SET
              status_formulir = 2
            WHERE
              nim = ?
          `;
          const dataSelectSyarat = [todo.nim, todo.nim];
          const dataUpdateSyarat = [todo.nim]

          connection.acquire((err, con) => {
            con.query(creds.select.query, creds.select.data, (err1, res1) => {
              if (err1) {
                con.release();
                res.json({status: 0, code: 500, message: 'Internal server error (1)'});
                throw err1;
              } else {
                res1[0].alasan = todo.alasan;
                  if (res1[0].konfirmasi_pembimbing1 === 1) {
                    if (todo.statusDosen === 'Dosen pembimbing anggota') {
                      con.query(creds.update.query.first, creds.update.data.first, (err2, res2) => {
                        if (err2) {
                          con.release();
                          res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                          throw err2;
                        } else {
                          con.query(querySelectSyarat, dataSelectSyarat, (err3, res3) => {
                          if (err3) {
                            con.release();
                            res.json({status: 0, code: 500, message: 'Internal server error (3)'});
                            throw err3;
                          } else {
                            let 
                              konfirmasiPembimbing1 = res3[0].konfirmasi_pembimbing1, 
                              konfirmasiPembimbing2 = res3[0].konfirmasi_pembimbing2,
                              konfirmasiPenguji1 = res3[0].konfirmasi_penguji1,
                              konfirmasiPenguji2 = res3[0].konfirmasi_penguji2;
                            // cek syarat minimal pelaksanaan mandiri
                            if (
                              (konfirmasiPembimbing1 === 1 && konfirmasiPembimbing2 === 1 && konfirmasiPenguji1 === 1) ||
                              (konfirmasiPembimbing1 === 1 && konfirmasiPembimbing2 === 2 && konfirmasiPenguji1 === 1) ||
                              (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 1 && konfirmasiPenguji2 === 1) ||
                              (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 1 && konfirmasiPenguji2 === 2) ||
                              (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 2 && konfirmasiPenguji2 === 1)
                            ) {
                              con.query(queryUpdateSyarat, dataUpdateSyarat, (err4, res4) => {
                                con.release();
                                if (err4) {
                                  res.json({status: 0, code: 500, message: 'Internal server error (5). Please contact the developer for further informations'});
                                  throw err4;
                                } else {
                                  const credsMhs = {
                                    nama: res3[0].nama,
                                    email: res3[0].email,
                                    statusDosen: todo.statusDosen
                                  };
                                  mail.setMandiriSukses(res, credsMhs, 'pertama');
                                  res.json({status: 1, code: 200, message: 'Success'});
                                }
                              });
                            } else {
                              con.release();
                              res.json({status: 1, code: 200, message: 'Success'});
                            }
                          }
                        });
                        }
                      });
                    } else {
                      res1[0].statusDosen = todo.statusDosen;
                      if (res1[0].penguji_2 === 0) {
                        con.query(creds.update.query.second, creds.update.data.second, (err2, res2) => {
                          if (err2) {
                            res.json({status: 0, code: 500, message: 'Internal server error (2).'});
                            throw err1;
                          } else {
                            mail.declineMandiri(res, res1[0]);
                            res.json({status: 1, code: 200, message: 'Berhasil'});
                          }
                        });
                      } else {
                        if (res1[0].konfirmasi_penguji1 === 2 || res1[0].konfirmasi_penguji2 === 2) {
                          con.query(creds.update.query.second, creds.update.data.second, (err2, res2) => {
                            con.release();
                            if (err2) {
                              res.json({status: 0, code: 500, message: 'Internal server error (2). '});
                              throw err2;
                            } else {
                              mail.declineMandiri(res, res1[0]);
                              res.json({status: 1, code: 200, message: 'Berhasil'});
                            }
                          });
                        } else {
                          con.query(creds.update.query.first, creds.update.data.first, (err2, res2) =>{
                            if (err2) {
                              res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                              throw err2;
                            } else {
                              con.query(querySelectSyarat, dataSelectSyarat, (err3, res3) => {
                                if (err3) {
                                  res.json({status: 0, code: 500, message: 'Please contact the developer (3)'});
                                  throw err3;
                                } else {
                                  let 
                                    konfirmasiPembimbing1 = res3[0].konfirmasi_pembimbing1, 
                                    konfirmasiPembimbing2 = res3[0].konfirmasi_pembimbing2,
                                    konfirmasiPenguji1 = res3[0].konfirmasi_penguji1,
                                    konfirmasiPenguji2 = res3[0].konfirmasi_penguji2;
                                  // Cek syarat
                                  if (
                                    (konfirmasiPembimbing1 === 1 && konfirmasiPembimbing2 === 1 && konfirmasiPenguji1 === 1) ||
                                    (konfirmasiPembimbing1 === 1 && konfirmasiPembimbing2 === 2 && konfirmasiPenguji1 === 1) ||
                                    (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 1 && konfirmasiPenguji2 === 1) ||
                                    (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 1 && konfirmasiPenguji2 === 2) ||
                                    (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 2 && konfirmasiPenguji2 === 1 )
                                  ) {
                                    con.query(queryUpdateSyarat, dataUpdateSyarat, (err4, res4) => {
                                      con.release();
                                      if (err4) {
                                        res.json({status: 0, code: 500, message: 'Internal server error (4). Please contact the developer for further informations'});
                                        throw err4;
                                      } else {
                                        const credsMhs = {
                                          nama: res3[0].nama,
                                          email: res3[0].email,
                                          statusDosen: todo.statusDosen
                                        };
                                        mail.setMandiriSukses(res, credsMhs, 'pertama');
                                        res.json({status: 1, code: 200, message: 'Berhasil'});
                                      }
                                    });
                                  } else {
                                    con.release();
                                    res.json({status: 1, code: 200, message: 'Berhasil'});
                                  }
                                }
                              });
                            }
                          });
                        }
                      }
                    }
                  } else {
                    res.json({status: 0, message: 'Dosen pembimbing ketua  menolak atau belum melakukan konfirmasi'});
                  }
                }
              });
          })
        } else {
            res.json({status: 0, code: 403, message: 'Forbidden'});
        }
      }
    });
  }

  // Method untuk admin
  this.konfirmasiAdmin = function(todo, res) {
    if (_.decoded.role === 4) {
      connection.acquire((err, con) => {
        if (err) {
          con.release();
          res.json({status: 0, code: 500, message: 'Internal server error (0).'});
          throw err;
        } else {
          const creds = {
            update: {
              query: `
                UPDATE
                  ta_mandiri
                SET
                  status_tempat = 1,
                  status_admin = 1,
                  tempat  = ?
                WHERE
                  nim = ?
              `,
              data: [todo.tempat, todo.nim]
            },
            select: {
              query: `
                SELECT
                  m.nama,
                  m.nim,
                  m.tanggal,
                  m.topik,
                  m.dosen1,
                  m.dosen2,
                  m.penguji1,
                  m.penguji2,
                  p.tempat,
                  p.jam,
                  p.pembahas1,
                  p.pembahas2,
                  p.pembahas3,
                  tas.konfirmasi_pembimbing2,
                  tas.konfirmasi_penguji1,
                  tas.konfirmasi_penguji2,
                  d.dosen1_email,
                  d.dosen2_email,
                  d.penguji1_email,
                  d.penguji2_email,
                  tam.makalah
                FROM 
                  mandiri_view AS m
                INNER JOIN
                  pembahas_view AS p ON m.nim = p.nim
                INNER JOIN
                  ta_seminar AS tas ON p.nim = tas.nim
                INNER JOIN
                  dosen_mahasiswa_view AS d ON tas.nim = d.nim
                INNER JOIN
                  ta_mandiri AS tam ON d.nim = tam.nim
                WHERE
                  m.nim = ? AND p.nim = ? AND tas.nim = ? AND d.nim = ? AND tam.nim = ?
              `,
              data: [todo.nim, todo.nim, todo.nim, todo.nim, todo.nim]
            }
          }
          con.query(creds.select.query, creds.select.data, (err1, res1) => {
            if (err1) {
              con.release();
              res.json({status: 0, code: 500, message: 'Internal server error (1).'});
              throw err1;
            } else {
              // console.log(res1);
              con.query(creds.update.query, creds.update.data, (err2, res2) => {
                con.release();
                if (err2) {
                  res.json({status: 0, code: 500, message: 'Internal server error (2).'});
                  throw err2;
                } else {
                  const credsMhs = {
                    nama: res1[0].nama,
                    nim: res1[0].nim,
                    topik: res1[0].topik,
                    tempat: todo.tempat,
                    jam: res1[0].jam,
                    tanggal: res1[0].tanggal,
                    tahunMasuk: res1[0].tahun_masuk,
                    namaDosen1: res1[0].dosen1,
                    namaDosen2: res1[0].dosen2,
                    penguji1: res1[0].penguji1,
                    penguji2: res1[0].penguji2,
                    pembahas1: res1[0].pembahas1,
                    pembahas2: res1[0].pembahas2,
                    pembahas3: res1[0].pembahas3,
                    konfirmasiPembimbing2: res1[0].konfirmasi_pembimbing2,
                    konfirmasiPenguji1: res1[0].konfirmasi_penguji1,
                    konfirmasiPenguji2: res1[0].konfirmasi_penguji2,
                    emailDosen1: res1[0].dosen1_email,
                    file: res1[0].makalah
                  };
                  const token = jwt.create(2, credsMhs.nim);
                  undangan.seminarUndangan(credsMhs.nim);
                  mail.setMandiriSukses(res, credsMhs, 'kedua');

                  if (credsMhs.konfirmasiPembimbing2 === 1) {
                    credsMhs.emailDosen2 = res1[0].dosen2_email
                  } 
                  
                  if (credsMhs.konfirmasiPenguji1 === 1) {
                    credsMhs.emailPenguji1 = res1[0].penguji1_email
                  }

                  if (credsMhs.konfirmasiPenguji2 === 1) {
                    credsMhs.emailPenguji2 = res1[0].penguji2_email;
                  }
                  
                  mail.mandiriMail(
                    [credsMhs.emailDosen1, credsMhs.emailDosen2, credsMhs.penguji1, credsMhs.penguji2],
                    credsMhs.nim,
                    credsMhs.nama,
                    credsMhs.topik,
                    credsMhs.pembahas1,
                    credsMhs.pembahas2,
                    credsMhs.pembahas3,
                    credsMhs.tempat,
                    credsMhs.jam,
                    credsMhs.tanggal,
                    credsMhs.file
                  );
                  res.json({status: 1, code: 200, message: 'Berhasil'});
                }
              });
            }
          });
        }
      });
    } else {
      res.json({status: 0, code: 403, message: 'Forbidden'});
    }
  }

  // Method untuk konfirmasi pembimbing ketua
  this.konfirmasiPertama = function(todo, res) {
    connection.acquire((err, con) => {
      if (err) {
        con.release();
        res.json({status: 0, code: 500, message: 'Internal server error (0). Please contact the developer for further informations.'});
        throw err;
      } else {
        console.log(todo);
        const creds = {
          update: {
            query: `
              UPDATE
                ta_seminar
              SET
                konfirmasi_pembimbing1 = 1
              WHERE
                nim = ?
            `,
            data: [todo.nim]
          },
          select: {
            query: {
              first: `
                SELECT
                  m.nama,
                  m.nim,
                  m.tanggal,
                  m.topik,
                  m.dosen1,
                  m.dosen2,
                  m.penguji1,
                  m.penguji2,
                  p.tempat,
                  p.jam,
                  p.pembahas1,
                  p.pembahas2,
                  p.pembahas3
                FROM 
                  mandiri_view AS m
                INNER JOIN
                  pembahas_view AS p ON m.nim = p.nim
                WHERE
                  m.nim = ? AND p.nim = ?
              `,
              second: `
                SELECT
                  dosen1,
                  dosen2,
                  penguji1,
                  penguji2,
                  dosen1_email,
                  dosen2_email,
                  penguji1_email,
                  penguji2_email
                FROM
                  dosen_mahasiswa_view
                WHERE
                  nim = ?
              `,
            },
            data: {
              first: [todo.nim, todo.nim],
              second: [todo.nim]
            }
          }
        };
        con.query(creds.select.query.first, creds.select.data.first, (err1, res1) => {
          if (err1) {
            con.release();
            res.json({status: 0, code: 500, message: 'Internal server error (1)'});
            throw err1;
          } else {
            con.query(creds.select.query.second, creds.select.data.second, (err2, res2) => {
              if (err2) {
                con.release();
                res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                throw err2;
              } else {
                con.query(creds.update.query, creds.update.data, (err3, res3) => {
                  con.release();
                  if (err3) {
                    res.json({status: 0, code: 500, message: 'Internal server error (3). Please contact the developer for further informations.'});
                    throw err3;
                  } else {
                    const credsMhs = {
                      nama: res1[0].nama,
                      nim: res1[0].nim,
                      topik: res1[0].topik,
                      tempat: res1[0].tempat,
                      jam: res1[0].jam,
                      tanggal: res1[0].tanggal,
                      tahun_masuk: res1[0].tahun_masuk,
                      namaDosen1: res1[0].dosen1,
                      namaDosen2: res2[0].dosen2,
                      namaPenguji1: res2[0].penguji1,
                      namaPenguji2: res2[0].penguji2,
                      email: [
                        res2[0].dosen2_email,
                        res2[0].penguji1_email,
                        res2[0].penguji2_email,
                      ],
                      statusDosen: todo.statusDosen,
                      role: 2
                    };
                    const token = jwt.mailToken(credsMhs);
                    mail.setMandiri(res, credsMhs, token);
                    res.json({status: 1, code: 200, message: 'Berhasil'});
                  }
                });
              }
            });
          }
        });
      }
    });
  }

  // Method untuk konfirmasi SELAIN dosen pembimbing ketua
  this.konfirmasi = function(todo, res) {
    connection.acquire((err, con) => {
      if (err) {
        res.json({status: 0, code: 500, message: 'Internal server error (0)'});
        throw err;
      } else {
        let creds;
        switch(todo.status_dosen) {
          case 'Dosen pembimbing anggota':
            creds = {
              update: {
                query: `
                  UPDATE
                    ta_seminar
                  SET
                    konfirmasi_pembimbing2 = 1
                  WHERE
                    nim = ?
                `,
                data: [todo.nim]
              },
            };
            break;
          case 'Penguji pertama':
            creds = {
              update: {
                query: `
                  UPDATE
                    ta_seminar
                  SET
                    konfirmasi_penguji1 = 1
                  WHERE
                    nim = ?
                `,
                data: [todo.nim]
              },
            }
            break;
          case 'Penguji kedua':
            creds = {
              update: {
                query: `
                  UPDATE
                    ta_seminar
                  SET
                    konfirmasi_penguji2 = 1
                  WHERE
                    nim = ?
                `,
                data: [todo.nim]
              },
            }
            break;
          default:
            res.json({status: 0, code: 400, message: 'Bad request'});
            break;
        };

        creds.select = {
          query: `
            SELECT
              konfirmasi_pembimbing1
            FROM 
              ta_seminar
            WHERE
              nim = ?
          `,
          data: [todo.nim]
        }

        const querySelectSyarat = `
          SELECT
            tas.konfirmasi_pembimbing1,
            tas.konfirmasi_pembimbing2,
            tas.konfirmasi_penguji1,
            tas.konfirmasi_penguji2,
            m.email,
            m.nama
          FROM
            ta_seminar AS tas
          INNER JOIN
            mahasiswa_detail AS m ON tas.nim = m.nim
          WHERE
            tas.nim = ? AND m.nim = ? 
        `;
        const queryUpdateSyarat = `
          UPDATE
            ta_mandiri
          SET
            status_formulir = 2
          WHERE
            nim = ?
        `;
        const dataSelectSyarat = [todo.nim, todo.nim];
        const dataUpdateSyarat = [todo.nim];

        con.query(creds.select.query, creds.select.data, (err1, res1) => {
          if (err1) {
            con.release();
            res.json({status: 0, code: 500, message: 'Internal server error (1)'});
            throw err1;
          } else if (res1[0].konfirmasi_pembimbing1 != 1) {
            con.release();
            res.json({status: 0, message: 'Pembimbing ketua menolak atau belum memberikan konfirmasi'});
          } else {
            con.query(creds.update.query, creds.update.data, (err2, res2) => {
              if (err2) {
                res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                throw err2;
              } else {
                // res.json({status: 1, code: 200, message: 'Berhasil'});
                con.query(querySelectSyarat, dataSelectSyarat, (err3, res3) => {
                  if (err3) {
                    res.json({status: 0, code: 500, message: 'Internal server error (3)'});
                    throw err3;
                  } else {
                    let 
                      konfirmasiPembimbing1 = res3[0].konfirmasi_pembimbing1, 
                      konfirmasiPembimbing2 = res3[0].konfirmasi_pembimbing2,
                      konfirmasiPenguji1 = res3[0].konfirmasi_penguji1,
                      konfirmasiPenguji2 = res3[0].konfirmasi_penguji2;
                    // Cek syarat minimum pelaksanaan seminar
                    if (
                      (konfirmasiPembimbing1 === 1 && konfirmasiPembimbing2 === 1 && konfirmasiPenguji1 === 1) ||
                      (konfirmasiPembimbing1 === 1 && konfirmasiPembimbing2 === 2 && konfirmasiPenguji1 === 1)||
                      (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 1 && konfirmasiPenguji2 === 1) ||
                      (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 1 && konfirmasiPenguji2 === 2) ||
                      (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 2 && konfirmasiPenguji2 === 1)
                    ) {
                      con.query(queryUpdateSyarat, dataUpdateSyarat, (err4, res4) => {
                        con.release();
                        if (err4) {
                          res.json({status: 0, code: 500, message: 'Internal server error (4). Please contact the developer for further informations'});
                          throw err4;
                        } else {
                          const credsMhs = {
                            nama: res3[0].nama,
                            email: res3[0].email
                          };
                          mail.setMandiriSukses(res, credsMhs, 'pertama');
                          res.json({status: 1, code: 200, message: 'Berhasil'});
                        }
                      });
                    } else {
                      res.json({status: 1, code: 200, message: 'Berhasil'});
                    }
                  }
                 
                });
              }
            });
          }
        });
      }
    });
  }
}

module.exports = new Mandiri();
