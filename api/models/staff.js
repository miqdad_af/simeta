var connection = require('../connection');
var fc = require('../fc');
var noAuth = {status: false, message: "You Can't Access This Service"};

function Staff() {

  this.get = function(res) {
    if(fc.checkAdminDosen()) {
      connection.acquire(function(err, con) {
        con.query('select * from dosen where role != 0 order by role desc', function(err, result) {
          con.release();
          if(err) {
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            for(let i = 0; i < result.length; i++) {
              if(result[i].role == 1) {
                result[i].role = 'Admin';
              }
              else if(result[i].role == 2) {
                result[i].role = 'Dosen';
              }
              else if(result[i].role == 4) {
                result[i].role = 'Komisi Pendidikan';
              }

            }

            res.json(result);
          }
        });
      });
    } else {
      res.json(noAuth);
    }
  };

}

module.exports = new Staff();
