var connection = require('../connection');
var _ = require('../simak');

function Profile() {

  // GET MAHASISWA DETAIL BY NIM
  this.getById = function(id, res) {
    connection.acquire(function(err, con) {
      con.query('select * from mahasiswa_detail where nim = ?', _.decoded.nim, function(err, result) {
        con.release();
        if(err) {
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          res.json(result);
        }
      });
    });
  };

  this.create = function(todo, res){
    connection.acquire(function(err, con){

      var querySelect = 'SELECT * FROM mahasiswa_detail WHERE nim = ?';
      var dataSelect = [_.decoded.nim];
      con.query(querySelect, dataSelect, function(err, result) {
        //IF QUERY SELECT ERROR
        if (err) {
          con.release();
          res.json({status: false, message: 'Pembaruan Profile Gagal'});
          throw err;
        }
        else {
          // UPDATE IF NIM EXIST
          if (result.length > 0) {
            var queryUpdate = 'UPDATE mahasiswa_detail SET alamat = ?, no_hp = ?, email = ?, nama_ayah = ?, nama_ibu = ?, no_ortu = ?, alamat_ortu = ? WHERE nim = ?';
            var dataUpdate = [todo.alamat, todo.no_hp, todo.email, todo.nama_ayah, todo.nama_ibu, todo.no_ortu, todo.alamat_ortu, _.decoded.nim];
            con.query(queryUpdate, dataUpdate, function(err, result) {
              con.release();
              if (err) {
                res.json({status: 0, message: 'Pembaruan Profile Gagal'});
                throw err;
              } else {
                res.json({status: 1, message: 'Pembaruan Profile Berhasil'});
              };
            });

          }
          // UPDATE IF NIM NOT EXIST
          else {

            var queryInsert = 'INSERT INTO mahasiswa_detail (nim, alamat, no_hp, email, nama_ayah, nama_ibu, no_ortu, alamat_ortu) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
            var dataInsert = [_.decoded.nim, todo.alamat, todo.no_hp, todo.email, todo.nama_ayah, todo.nama_ibu, todo.no_ortu, todo.alamat_ortu];
            con.query(queryInsert, dataInsert, function(err, result) {
              con.release();
              if (err) {
                res.json({status: 0, message: 'Insert Profile Gagal'});
                throw err;
              } else {
                res.json({status: 1, message: 'Insert Profile Berhasil'});
              };
            });

          };

        }
      });

    });
  };

  // UPDATE DETAIL
  this.update = function(todo, res) {
    connection.acquire(function(err, con) {

      var queryUpdate = `
        UPDATE mahasiswa_detail SET alamat = ?, hp = ?, email = ?, namaayah = ?, namaibu = ?, noortu = ?, telportu = ?, alamatortu = ?, status = 1 WHERE nim = ?
      `;

      var dataUpdate = [todo.alamat, todo.hp, todo.email, todo.namaayah, todo.namaibu, todo.noortu, todo.telportu, todo.alamatortu, _.decoded.nim];

      con.query(queryUpdate, dataUpdate, function(err, result) {
        con.release();
        if (err) {
          res.json({status: 0, message: 'Insert Profile Gagal'});
          throw err;
        } else {
          res.json({status: 1, message: 'Update Profile Berhasil'});
        };
      });
    });
  };

}

module.exports = new Profile();
