// SIMETA v2
// Model dan controller untuk modul pengajuan pembimbing kedua
// Note for further developer/s: please avoid callback hell by using async/await functions.
// or you can read the guide here : http://callbackhell.com/

var connection = require('../connection');
var _ = require('../simak');
var mail = require('../mail');
var notifier = require('node-notifier');
var path = require('path');
var random = require('randomstring');
var jwt = require('../direct/generateJWT');

function PembimbingKedua(req, res) {

    // Method ketika mahasiswa inisialisasi mengirimkan pengajuan.
    // status_dp2 status u/ tracking pengajuan. 
    // status_dp2:
    // null = belum mengajukan pengajuan.
    // 1 = sudah mengajukan pengajuan namun belum dikonfirmasi oleh pembimbing pertama.
    // 2 = sudah dikonfirmasi oleh pembimbing pertama namum belum oleh calon pembimbing kedua.
    // 3 = sudah dikonfirmasi oleh calon pembimbing kedua namun belum dikonfirmasi oleh kepala bagian.
    // 4 = sudah dikonfirmasi oleh kepala bagian namun belum dikonfirmasi oleh admin
    // 5 = sudah dikonfimasi oleh admin.
    // status_req status apakah untuk dosen dari ilkom (1) atau non-ilkom (2).
    this.set = function (req, res) {
        const nim = _.decoded.nim;
        if (req.statReq === 1 && !req.id) res.json({status: 0, code: 500, message: 'Dosen terkait tidak ada di dalam basis data SIMETA. Silahkan coba lagi'});
        else {
            connection.acquire((err, con) => {
                // q = query, err = error, res = result. 
                // Setiap iterasi callback bertambah angka sesuai iterasi.
                const q = 'SELECT * FROM ta WHERE nim = ?';
                con.query(q, nim, (err1, res1) => {
                    if (err) {
                        res.json({status: 0, code: 500, message: 'Internal server error (1).'});
                        throw err1;
                    } else {
                        // dosen1 = dosen pertama
                        // dosen2 = dosen kedua
                        // Token dibuat untuk token reminder pada url api untuk approval via email
                        const dosen1 = res1[0].dosen_1;
                        const dosen2 = res1[0].dosen_2;
                        const token = jwt.create(2, dosen1);
                        // Cek jika sudah memiliki dosen kedua atau belum.
                        if (dosen2 != 0) {
                            res.json({status: 0, code: 409, message: 'Anda telah memiliki dosen pembimbing kedua.'});
                            con.release();
                        } else {
                            const q2 = 'SELECT * FROM ta_dosbing_kedua WHERE nim = ?';
                            con.query(q2, nim, (err2, res2) => {
                                if (err2) {
                                    res.json({status: 0, code: 500, message: 'Internal server error (2).'});
                                    con.release();
                                    throw err2;
                                } else {
                                    // Cek jika sudah ada pada tabel ta_dosen_kedua
                                    if (res2.length > 0) {
                                        const status = res2[0].status_dp2;
                                        if (req.statReq === 1 && status === null) {
                                            if (dosen1 == req.id) {
                                                con.release();
                                                res.json({status: 0, code: 400, message: 'Calon pembimbing kedua tidak boleh sama dengan pembimbing kedua.'});
                                            } else {
                                                const q3 = 
                                                'UPDATE ta_daftar AS t, ta_dosbing_kedua AS s SET t.dosen_2 = ?, s.status_dp2 = ?, s.status_req = ?, s.dosen_2 = ? WHERE t.nim = ? AND s.nim = ?';
                                                const creds = [req.id, 1, req.statReq, req.id, nim, nim];
                                                con.query(q3, creds, (err3, res3) => {
                                                    if (err3) {
                                                        res.json({status: 0, code: 500 ,message: 'Internal server error (3). Please contact the developer for further informations. '});
                                                        con.release();
                                                        throw err3;
                                                    } else  {
                                                        q4 = 'SELECT id FROM ta_dosbing_kedua WHERE nim= ?';
                                                        con.query(q4, [_.decoded.nim], (err4, res4) => {
                                                            // con.release();
                                                            if (err4) {
                                                                res.json({status: 0, code: 500, message: 'Internal server error (4). Please contact the developer for further informations.'});
                                                                con.release();
                                                                throw err4;
                                                            } else {
                                                                const idMhs = res4[0].id;
                                                                mail.setDosenPertamaMail(req, res, token, idMhs);
                                                                res.json({status: 1, code: 200, message: 'Berhasil mengajukan permohonan pembimbing kedua tugas akhir.'});
                                                                con.release();
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        } else if (req.statReq === 2 && status === null) {
                                            const q3 = 
                                            'INSERT dosen (nama, keahlian, nama_instansi, alamat_instansi, no_hp, email, nip, role) VALUES (?,?,?,?,?,?,?, ?)';
                                            const creds = [req.dosen_2, req.bidangKeahlian, req.namaInstansi, req.alamatInstansi, req.nomorTelp, req.email, req.nip, 2];
                                            con.query(q3, creds, (err3, res3) => {
                                                if (err3) {
                                                    res.json({status: 0, code: 500, message: 'Internal server error (3). Please contact the developer for further informations.'});
                                                    con.release();
                                                    throw err3;
                                                } else {
                                                    const q4 = 'SELECT d.id AS id_dosen, t.id AS id_mhs FROM dosen AS d, ta_dosbing_kedua AS t WHERE d.nama = ? AND t.nim = ?';
                                                    con.query(q4, [req.dosen_2, _.decoded.nim], (err4, res4) => {
                                                        if (err4) {
                                                            res.json({status: 0, code: 500 ,message: 'Internal server error (4). Please contact the developer for further informations.'});
                                                            con.release();
                                                            throw err4;
                                                        } else { 
                                                            const idDosenDua = res4[0].id_dosen;
                                                            const idMhs = res4[0].id_mhs;
                                                            const q5 = 
                                                            'UPDATE ta_daftar AS t, ta_dosbing_kedua AS s SET t.dosen_2 = ?, s.status_dp2 = ? , s.status_req = ? , s.dosen_2 = ? WHERE t.nim = ? AND s.nim =?';
                                                            const creds2 = [idDosenDua, 1, req.statReq, idDosenDua, nim, nim];
                                                            if (idDosenDua == dosen1) {
                                                                con.release();
                                                                res.json({status: 0, code: 400, message: 'Calon pembimbing kedua tidak boleh sama dengan pembimbing kedua.'})
                                                            } else {
                                                                con.query(q5, creds2, (err5, res5) => {
                                                                    // con.release();
                                                                    if (err5) {
                                                                        res.json({status: 0, code: 500, message: 'Internal server error (5). Please contact the developer for further informations.'});
                                                                        con.release();
                                                                        throw err5;
                                                                    } else {
                                                                        mail.setDosenPertamaMail(req, res, token, idMhs);
                                                                        res.json({status: 1, code: 200, message: 'Berhasil mengajukan permohonan pembimbing kedua tugas akhir'});
                                                                        con.release();
                                                                    }
                                                                }); 
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        } else if (status === 1) {
                                            res.json({status: 0, code: 409, message: 'Anda telah memberikan pengajuan.'});
                                            con.release();
                                            throw err2;
                                        }
                                    } else {
                                        if (req.statReq === 1) {
                                            if (dosen1 == req.id) {
                                                con.release();
                                                res.json({status: 0, code: 400, message: 'Calon pembimbing kedua tidak boleh sama dengan pembimbing kedua.'});
                                            } else {
                                                const q3 = 
                                                'INSERT ta_dosbing_kedua (nim, dosen_1, dosen_2, status_dp2, status_req) VALUE (?,?,?,?,?)';
                                                const creds = [nim, dosen1, req.id, 1, req.statReq];
                                                con.query(q3, creds, (err3, res3) => {
                                                    if (err3) {
                                                        con.release();
                                                        res.json({status: 0, code: 500, message: 'Internal server error (3). Pelase contact the developer for further informations.'});
                                                        throw err3;
                                                    } else {
                                                        const q4 =
                                                        'UPDATE ta_daftar SET dosen_2 = ? WHERE nim = ?';
                                                        const creds2 = [req.id, nim];
                                                        con.query(q4, creds2, (err4, res4) => {
                                                            if (err4) {
                                                                con.release();
                                                                res.json({status: 0, code: 500, message: 'Internal server error (4). Please contact the developer for further informations.'});
                                                                throw err4;
                                                            } else {
                                                                const q5 = 'SELECT id FROM ta_dosbing_kedua WHERE nim = ?';
                                                                con.query(q5, [_.decoded.nim], (err5, res5) => {
                                                                    if (err5) {
                                                                        con.release();
                                                                        throw err5;
                                                                        res.json({status: 0, code: 500, message: 'Internal server error (5). Please contact the developer for further informations.'});
                                                                    } else {
                                                                        con.release();
                                                                        const idMhs = res5[0].id;
                                                                        mail.setDosenPertamaMail(req, res, token, idMhs);
                                                                        res.json({status: 1, code: 200, message: 'Berhasil mengajukan permohonan pembimbing kedua tugas akhir.'});
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        } else if (req.statReq === 2) {
                                            const q3 = 
                                            `
                                                INSERT 
                                                    dosen (nama, keahlian, nama_instansi, alamat_instansi, no_hp, email, nip, role) 
                                                VALUES (?,?,?,?,?,?,?,?)
                                            `;
                                            const creds = [req.dosen_2, req.bidangKeahlian, req.namaInstansi, req.alamatInstansi, req.nomorTelp, req.email, req.nip, 2];
                                            con.query(q3, creds, (err3, res3) => {
                                               if(err3) {
                                                    con.release();
                                                    res.json({status: 0, code: 500, message: 'Internal server error (3). Please contact the developer for further informations.'});
                                                    throw err3;
                                               } else {
                                                    const q4 = 'SELECT id FROM dosen WHERE nama = ?';
                                                    const creds2 = [req.dosen_2];
                                                    con.query(q4, creds2, (err4, res4) => {
                                                        if (err4) {
                                                            con.release();
                                                            res.json({status: 0, code: 500, message: 'Internal server error (4). Please contact the developer for further informations.'});
                                                            throw err4;
                                                        } else {
                                                            const idDosenDua = res4[0].id;
                                                            const q5 = 'INSERT ta_dosbing_kedua (nim, dosen_1, dosen_2, status_dp2, status_req) VALUE (?,?,?,?,?)';
                                                            const creds3 = [nim, dosen1, idDosenDua, 1, req.statReq];
                                                            if (idDosenDua == dosen1) {
                                                                con.release();
                                                                res.json({status: 0, code: 400, message: 'Calon pembimbing kedua tidak boleh sama dengan pembimbing kedua.'});
                                                            } else {
                                                                con.query(q5, creds3, (err5, res5) => {
                                                                    if (err5) {
                                                                        con.release();
                                                                        res.json({status: 0, code: 500, message: 'Internal server error (5). Please contact the developer for further informations.'});
                                                                        throw err5;
                                                                    } else {
                                                                        const q6 = 'SELECT d.id AS dosen_id, t.id AS mhs_id FROM dosen AS d, ta_dosbing_kedua AS t WHERE d.nama = ? AND t.nim = ?';
                                                                        con.query(q6, [req.dosen_2, _.decoded.nim], (err6, res6) => {
                                                                            if (err6) {
                                                                                con.release();
                                                                                res.json({status: 0, code: 500, message: 'Internal server error (6). Please contact the developer for further informations.'});
                                                                                throw err6;
                                                                            } else {
                                                                                const idMhs = res6[0].mhs_id;
                                                                                const q7 = 'UPDATE ta_daftar SET dosen_2 = ? WHERE nim = ?';
                                                                                const creds4 = [idDosenDua, nim];
                                                                                con.query(q7, creds4, (err7, res7) => {
                                                                                    if (err7) {
                                                                                        con.release();
                                                                                        res.json({status: 0, code: 500, message: 'Internal server error (7). Please contact the developer for further informations.'});
                                                                                        throw err7;
                                                                                    } else {
                                                                                        mail.setDosenPertamaMail(req, res, token, idMhs);
                                                                                        res.json({status: 1, code: 200, message: 'Berhasil mengajukan permohonan pembimbing kedua.'});
                                                                                        con.release();
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        } else  {
                                            con.release();
                                            res.json({status: 0, code: 409, message: 'Anda telah memberikan pengajuan.'});
                                            throw err2;
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            });
        }

    };

    // Method untuk get data untuk keperluan konfirmasi dosen pembimbing pertama.
    this.getDataPertama = function(res) {
        connection.acquire(function(err, con) {
            const creds = {
                select: {
                    query: `
                        SELECT DISTINCT
                            s.id,
                            s.status_req,
                            s.status_dp2,
                            m.nama,
                            m.nim,
                            m.topik,
                            d.nama AS nama_dosen,
                            d.nip, 
                            d.email,
                            d.lab,
                            d.keahlian, 
                            d.no_hp, 
                            d.lab,
                            d.alamat_instansi, 
                            d.nama_instansi
                        FROM 
                            dosen_mahasiswa_view AS m
                        INNER JOIN
                            ta_dosbing_kedua AS s ON s.nim = m.nim
                        INNER JOIN
                            dosen AS d ON d.id = s.dosen_2
                        WHERE 
                            d.id IN (SELECT dosen_2 FROM ta_dosbing_kedua WHERE dosen_1 = ?)
                            AND m.nim IN (SELECT nim FROM ta_dosbing_kedua WHERE dosen_2 IN (SELECT dosen_2 FROM ta_dosbing_kedua WHERE dosen_1 = ?))
                            AND s.status_dp2 = 1
                    `,
                    data: [_.decoded.id, _.decoded.id]
                },
            }
            con.query(creds.select.query, creds.select.data, (err1, res1) => {
                con.release();
                if (err1) {
                    res.json({status: 0, code: 500, message: 'Internal server error (1). Please contact the developer for further informations.'});
                    throw err1;
                } else {
                    console.log(res1[0]);
                    if (res1.length > 0) {
                        res.json({status: 1, code: 200, message: 'Berhasil', data: res1});
                    }
                    else {
                        res.json({status: 0, code: 404, message: 'Tidak ada data mahahasiswa pengajuan'});
                    }
                }
            });
        })
    }

     // Method untuk get data untuk keperluan konfirmasi dosen pembimbing kedua.
    this.getDataKedua = function(res) {
        connection.acquire((err, con) => {
            const creds = {
                pertama: {
                    select: {
                        query: `
                            SELECT DISTINCT
                                s.id,
                                s.status_req,
                                s.status_dp2,
                                m.nama,
                                m.nim,
                                m.topik,
                                d.nama AS nama_dosen,
                                d.nip, 
                                d.email,
                                d.lab, 
                                d.no_hp, 
                                d.lab,
                                d.alamat_instansi, 
                                d.nama_instansi
                            FROM 
                                dosen_mahasiswa_view AS m
                            INNER JOIN
                                ta_dosbing_kedua AS s ON s.nim = m.nim
                            INNER JOIN
                                dosen AS d ON d.id = s.dosen_2
                            WHERE 
                                d.id = ?
                                AND m.nim IN (SELECT nim FROM ta_dosbing_kedua WHERE dosen_2 = ?)
                                AND s.status_dp2 = 2
                            ORDER BY
                                m.nim
                        `,
                        data: [_.decoded.id, _.decoded.id]
                    },            
                },
                kedua: {
                    select: {
                        query: `
                            SELECT
                                dosen1
                            FROM
                                ta_daftar_view
                            WHERE
                                nim IN (SELECT nim FROM ta_dosbing_kedua WHERE dosen_2 = ? AND status_dp2 = ?)
                            ORDER BY
                                nim
                        `,
                        data: [_.decoded.id, 2]
                    }
                }
            };
 
            con.query(creds.pertama.select.query, creds.pertama.select.data, (err1, res1) => {
                if (err1) {
                    res.json({status: 0, code: 500, message: 'Internal server error(1), please contact the developer for further informations'});
                    throw err1;
                } else {
                    // con.release();
                    con.query(creds.kedua.select.query, creds.kedua.select.data, (err2, res2) => {
                        if (err2) {
                            con.release();
                            res.json({status: 0, code: 500, message: 'Internal server error(2)'});
                            throw err2;
                        } else {
                            con.release();
                            res1.map((element, index) => {
                                element.dosen1 = res2[index].dosen1;
                            });
                            if (res1.length > 0) {    
                                res.json({status: 1, code: 200, message: 'Berhasil get data', data: res1});
                            } else {
                                res.json({status: 0, code: 404, message: 'Tidak ada data mahahasiswa pengajuan'});
                            }
                        }
                    });
            
                }
            });
        });
    }

    this.getDataKepalaBagian = function(res) {
        // console.log('masuk data admin');
        if (_.decoded.id == 27 /*Pak Irman*/ || _.decoded.id == 12 /*Pak Heru*/ || _.decoded.id == 5 /*Pak Wisnu */ || _.decoded.id == 25 /*Untuk test ya*/) {
            connection.acquire((err, con) => {
                if (err) {
                    res.json({status: 0, code: 500, message: 'Internal server error (0)'});
                    throw err;
                } else {
                    const creds = {
                        select: {
                            pertama: {
                                query: `
                                    SELECT DISTINCT
                                        tad.id,
                                        tad.status_req,
                                        tad.status_dp2,
                                        m.nama,
                                        m.nim,
                                        m.topik,
                                        d.nama AS nama_dosen,
                                        d.nip, 
                                        d.email,
                                        d.lab, 
                                        d.keahlian,
                                        d.no_hp, 
                                        d.alamat_instansi, 
                                        d.nama_instansi
                                    FROM
                                        ta_dosbing_kedua AS tad
                                    INNER JOIN
                                        dosen_mahasiswa_view AS m ON m.nim = tad.nim
                                    INNER JOIN
                                        dosen AS d ON tad.dosen_2 = d.id
                                    WHERE
                                        tad.status_dp2 = 3
                                    AND
                                        tad.nim IN (SELECT nim FROM ta_dosbing_kedua WHERE status_dp2 = 3)
                                    AND
                                        m.nim IN (SELECT nim FROM ta_dosbing_kedua WHERE status_dp2 = 3)
                                    ORDER BY
                                        tad.nim
                                `,
                            },
                           kedua: {
                               query: `
                                    SELECT
                                        d.nama AS dosen1,
                                        d.lab AS lab1
                                    FROM
                                        dosen AS d
                                    INNER JOIN
                                        ta_dosbing_kedua AS tad ON tad.dosen_1 = d.id
                                    WHERE
                                        d.id IN (SELECT dosen_1 FROM ta_dosbing_kedua WHERE status_dp2 = 3)
                                    AND
                                        tad.status_dp2 = 3
                                    ORDER BY
                                        tad.nim
                               `
                           }
                        },
                    };
                    con.query(creds.select.pertama.query, (err1, res1) => {
                        if (err1) {
                            res.json({status: 0, code: 500, message: 'Internal server error (1)'});
                            throw err1;
                        } else {
                            con.query(creds.select.kedua.query, (err2, res2) => {
                                if (err2) {
                                    res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                                    throw err2;
                                } else {
                                    res1.map((item, index) => {
                                      item.dosen1 = res2[index].dosen1;
                                      item.lab1 = res2[index].lab1 
                                    })
                                    console.log(res1);
                                    res.json({status: 1, code: 200, data: res1});
                                }
                             
                            })
                        }
                    })
                }
            })
        } else {
            console.log('masuk gagal');
            res.json({status: 1, code: 200, data: []});
        }
    }

    // Method u/ mendapatkan data pada modul Admin
    this.getDataAdmin = function(res) {
        connection.acquire((err, con) => {
            const creds = {
                select: {
                    query: `
                        SELECT DISTINCT
                            s.id,
                            s.status_req,
                            s.status_dp2,
                            m.nama,
                            m.nim,
                            m.topik,
                            m.dosen1,
                            d.id As id_dosen,
                            d.nama AS dosen2,
                            d.nip, 
                            d.email,
                            d.lab, 
                            d.no_hp, 
                            d.lab,
                            d.alamat_instansi, 
                            d.nama_instansi,
                            d.keahlian
                        FROM 
                            dosen_mahasiswa_view AS m
                        INNER JOIN
                            ta_dosbing_kedua AS s ON s.nim = m.nim
                        INNER JOIN
                            dosen AS d ON d.id = s.dosen_2
                        WHERE 
                            s.status_dp2 = ?
                        ORDER BY
                            s.updated_at DESC
                    `,
                    data: [4]
                }
            };
            con.query(creds.select.query, creds.select.data, (err1, res1) => {
                if (err1) {
                    con.release();
                    res.json({status: 0, code: 500, message: 'Internal server error(1). Please contact the developer for further informations.'});
                    throw err1;
                } else {
                    console.log(res1);
                    con.release();
                    if (res1.length > 0) {
                        res.json({status: 1, code: 200, message: 'Berhasil mendapatkan data mahasiswa u/ admin', data: res1});
                    } else {
                        res.json({status: 0, code: 404, message: 'Data mahasiswa tida ditemukan'});
                    }
                }
            });
        });
    }

    // Method u/ get status tracking pengajuan
    this.getStatus = function(res) {
        connection.acquire(function(err, con) {
            const query = 'SELECT status_dp2, dosen_1, dosen_2 FROM ta_dosbing_kedua WHERE dosen_1 = ? OR dosen_2 = ? OR nim = ?';
            con.query(query, [_.decoded.id, _.decoded.id, _.decoded.nim], function(error, result) {
                con.release();
                if (!error) {
                    if (result.length > 0) {
                        let status = [0,0,0,0,0,0];
                        for(let i=0; i<result.length; i++) {
                            if(_.decoded.nim != null) {
                                if(result[i].status_dp2 == null) {
                                    status[0]++;
                                } else if (result[i].status_dp2 == 5) {
                                    status[5]++;
                                }
                            } else {
                                if(_.decoded.id == result[i].dosen_1 && result[i].status_dp2 == 1) {
                                    status[1]++
                                } else if(_.decoded.id == result[i].dosen_2 && result[i].status_dp2 == 2) {
                                    status[2]++
                                } else if(result[i].status_dp2 == 3) {
                                    status[3]++
                                } else if(result[i].status_dp2 == 4) {
                                    status[4]++
                                }
                            }
                        }
                        res.json({status: 1, code: 200, message: 'Berhasil', data: status});
                    } else {
                        res.json({status: 0, code: 409, message: 'Anda tidak memiliki konfimasi pengajuan pembimbing kedua.', data: null});
                    }
                } else {
                    res.json({status: 0, code: 500, message: 'Internal server error (0). Please contact the developer for further informations.'});
                }
            });
        })
    }

    // Method untuk acc dosen pembimbing pertama
    this.setAcceptedPertama = function(req, res) {
        if (_.decoded.role === 2 || _.decoded.role === 4) {
            connection.acquire((err, con) => {
                const creds = {
                    select: {
                        query: `
                            SELECT DISTINCT
                                s.id,
                                s.status_req,
                                s.status_dp2,
                                s.dosen_2 AS id_dosen2,
                                m.nama,
                                m.nim,
                                m.topik,
                                m.dosen1 AS dosen1,
                                d.nip, 
                                d.email,
                                d.lab, 
                                d.keahlian,
                                d.no_hp, 
                                d.lab,
                                d.alamat_instansi, 
                                d.nama_instansi AS nama_instansi
                            FROM 
                                dosen_mahasiswa_view AS m
                            INNER JOIN
                                ta_dosbing_kedua AS s ON s.nim = m.nim
                            INNER JOIN
                                dosen AS d ON d.id = s.dosen_2
                            WHERE 
                                d.id IN (SELECT dosen_2 FROM ta_dosbing_kedua WHERE dosen_1 = ?)
                                AND m.nim = ?
                                AND s.status_dp2 = 2
                            ORDER BY
                                s.updated_at DESC
                        `,
                        data: [_.decoded.id, req.nim]
                    },
                    update: {
                        query: `
                            UPDATE ta_dosbing_kedua SET status_dp2 = ? WHERE dosen_1 = ? AND nim = ?
                        `,
                        data: [2, _.decoded.id, req.nim]
                    }
                };
                con.query(creds.update.query, creds.update.data, (err1, res1) => {
                    if (err1) {
                        res.json({status: 0, message: 'Internal server error (1). Please contact the developer for further informations.'});
                        throw err1;
                    } else {
                        // Untuk keperluan data yang ditampilkan pada email.
                        con.query(creds.select.query, creds.select.data, (err2, res2) => {
                            if (err2) {
                                con.release();
                                res.json({status: 0, code: 500, message: 'Internal server error (2). Please contact the developer for further informations.'});
                                throw err2;
                            } else {
                                const credsMhs = {
                                    id: req.id,
                                    topik: res2[0].topik,
                                    namaMahasiswa: res2[0].nama,
                                    nimMahasiswa: res2[0].nim,
                                    dosen1: res2[0].dosen1,
                                    dosen2: req.nama_dosen,
                                    idDosen: res2[0].id_dosen2,
                                    nip: res2[0].nip,
                                    to: res2[0].email,
                                    lab: res2[0].lab,
                                    keahlian: res2[0].keahlian,
                                    nomorTelp: res2[0].no_hp,
                                    lab: res2[0].lab,
                                    alamatInstansi: res2[0].alamat_instansi,
                                    namaInstansi: res2[0].nama_instansi,
                                    statusReq: req.status_req,
                                    statusDp: res2[0].status_dp2
                                };
                                const token = jwt.create(2, credsMhs.idDosen);
                                mail.setDosenKeduaMail(res, credsMhs, token, req.id);
                                mail.setMahasiswaAccepted(res, credsMhs);
                                res.json({status: 1, code: 200, message: 'Konfirmasi berhasil'});
                                con.release();
                            }
                        });
                    }
                });
            });
        }  
    }

    // Method untuk konfirmasi kedua (konfirmasi oleh calon pembimbing kedua)
    this.setAcceptedKedua = function(req, res) {
        if (_.decoded.role === 2 || _.decoded.role === 4) {
            const creds = {
                update: {
                    query: `
                        UPDATE 
                            ta_dosbing_kedua
                        SET 
                            status_dp2 = ?
                        WHERE 
                            id = ?
                    `,
                    data: [3, req.id]
                },
                select: {
                    pertama: {
                        query: `
                            SELECT DISTINCT
                                tas.id,
                                tas.status_req,
                                tas.status_dp2,
                                tas.dosen_2 AS id_dosen2,
                                m.nama,
                                m.nim,
                                m.topik,
                                m.dosen1 AS dosen1,
                                d.nip, 
                                d.lab, 
                                d.no_hp, 
                                d.lab,
                                d.keahlian,
                                d.alamat_instansi, 
                                d.nama_instansi
                            FROM 
                                dosen_mahasiswa_view AS m
                            INNER JOIN
                                ta_dosbing_kedua AS tas ON tas.nim = m.nim
                            INNER JOIN
                                dosen AS d ON d.id = tas.dosen_2
                            WHERE
                                d.id = ?
                                AND m.nim = ?
                                AND tas.status_dp2 = 3
                            ORDER BY
                                tas.updated_at DESC
                            `,
                        data: [_.decoded.id, req.nim]
                    },
                    kedua: {
                        query: `
                            SELECT
                                nama,
                                email
                            FROM
                                dosen
                            WHERE
                                id = ?
                        `
                    }
                }
            }
            connection.acquire((err, con) => {
                con.query(creds.update.query, creds.update.data, (err1, res1) => {
                    if (err1) {
                        res.json({status: 0, code: 500, message: 'Internal server error (1), please contact the developer for further information.'})
                        throw err1;
                    } else {
                        con.query(creds.select.pertama.query, creds.select.pertama.data, (err2, res2) => {
                            if (err2) {
                                con.release();
                                res.json({status: 0, code: 500, message: 'Internal server error (2), please contact the developer for further information.'});
                                throw err2;
                            } else {
                                const credsMhs = {
                                    id: req.id,
                                    topik: res2[0].topik,
                                    namaMahasiswa: res2[0].nama,
                                    nimMahasiswa: res2[0].nim,
                                    dosen1: res2[0].dosen1,
                                    dosen2: req.nama_dosen,
                                    idDosen: res2[0].id_dosen2,
                                    nip: res2[0].nip,
                                    email: req.email,
                                    lab: res2[0].lab,
                                    nomorTelp: res2[0].no_hp,
                                    lab: res2[0].lab,
                                    keahlian: res2[0].keahlian,
                                    alamatInstansi: res2[0].alamat_instansi,
                                    namaInstansi: res2[0].namaInstansi,
                                    statusReq: req.status_req,
                                    statusDp: res2[0].status_dp2
                                };
                                switch(credsMhs.lab) {
                                    case '1':
                                        creds.select.kedua.data = [12];
                                    case '2':
                                        creds.select.kedua.data = [5];
                                    case '3':
                                        creds.select.kedua.data = [27];
                                };
                                con.query(creds.select.kedua.query, creds.select.kedua.data, (err3, res3) => {
                                    if (err3) {
                                        res.json({status: 0, code: 500, message: 'Internal server error(3)'});
                                        throw err3;
                                    } else  {
                                        const token = jwt.create(2, credsMhs.dosen2);
                                        const to = res3[0].email;
                                        mail.setKepalaBagian(res,to, credsMhs, token, req.id);
                                        mail.setMahasiswaAccepted(res, credsMhs);
                                        res.json({status: 1, code: 200, message: 'Konfirmasi berhasil'});
                                        con.release();
                                    }
                                });
                              
                            }
                        });
                    }
                });
            });
        } else {
            res.json({status: 0, code: 403, message: 'You are not allowed to use this service'});
        }
    }

    this.setAcceptedKepalaBagian = function(req, res) {
        if (_.decoded.role === 4 || _.decoded.role === 2) {
            if (_.decoded.id == 27 /*Pak Irman*/ || _.decoded.id == 12 /*Pak Heru*/ || _.decoded.id == 5 /*Pak Wisnu */ || _.decoded.id == 25 /*Untuk test ya*/) {
                const creds = {
                    update: {
                        query: `
                            UPDATE
                                ta_dosbing_kedua
                            SET
                                status_dp2 = 4
                            WHERE
                                nim = ?
                        `,
                        data: [req.nim]
                    },
                    select: {
                        query: `
                            SELECT
                                m.nama,
                                m.nim,
                                ta_dosbing_kedua.status_dp2
                            FROM
                                mahasiswa_detail AS m
                            INNER JOIN
                                ta_dosbing_kedua ON m.nim = ta_dosbing_kedua.nim
                            WHERE
                                m.nim = ?
                            AND
                                ta_dosbing_kedua.nim = ?
                        `,
                        data: [req.nim, req.nim]
                    }
                };
                connection.acquire((err, con) => {
                    if (err) {
                        res.json({status: 0, code: 500, message: 'Internal server error (0)'});
                        throw err;
                    } else {
                        con.query(creds.update.query, creds.update.data, (err1, res1) => {
                            if (err1) {
                                res.json({status: 0, code: 500, message: 'Internal server error (1)'});
                                throw err1;
                            } else {
                                con.query(creds.select.query, creds.select.data, (err2, res2) => {
                                    con.release();
                                    if (err2) {
                                        res.json({status: 0, code: 500, message: 'Internal server error (1)'});
                                        throw err2;
                                    } else {
                                        console.log(res2);
                                        const credsMhs = {
                                            namaMahasiswa: res2[0].nama,
                                            nimMahasiswa: res2[0].nim,
                                            statusDp: res2[0].status_dp2
                                        };
                                        mail.setMahasiswaAccepted(res, credsMhs);
                                        res.json({status: 1, code: 200, message: 'Berhasil'});
                                    }
                                });
                                
                            }
                        });
                    }
                })
            } else {
                res.json({status: 0, code: 403, message: 'Anda bukan kepala bagian'});
            }
          
        } else {
            res.json({status: 0, code: 403, message: 'Forbidden'});
        }
    }

    this.setAcceptedAdmin = function(req, res) {
        console.log(req.id);
        if (_.decoded.role === 4 ) {
            const creds = {
                update: {
                    query:`
                        UPDATE
                            ta_dosbing_kedua AS t,
                            ta
                        SET
                            t.status_dp2 = ?,
                            ta.dosen_2 = ?
                        WHERE
                            t.id = ? AND
                            ta.nim = ?
                    `,
                    data: [5, req.id_dosen, req.id, req.nim]
                },
                select: {
                    query: `
                        SELECT DISTINCT
                            s.id,
                            s.status_req AS status,
                            s.dosen_2 AS id_dosen2,
                            m.nama,
                            m.nim,
                            m.dosen2_email,
                            m.topik,
                            m.dosen1 AS dosen1,
                            m.dosen2 AS dosen2,
                            d.nip, 
                            d.lab, 
                            d.no_hp, 
                            d.lab,
                            d.alamat_instansi, 
                            d.nama_instansi,
                            mhs.email
                        FROM 
                            dosen_mahasiswa_view AS m
                        INNER JOIN
                            ta_dosbing_kedua AS s ON s.nim = m.nim
                        INNER JOIN
                            dosen AS d ON d.id = s.dosen_1
                        INNER JOIN
                            mahasiswa_detail AS mhs ON m.nim = mhs.nim
                        WHERE
                            s.status_dp2 = ?
                        AND
                            m.nim = ?
                        AND
                            s.nim = ?
                        AND
                            mhs.nim = ?
                        ORDER BY
                            s.updated_at DESC
                        `,
                    data: [4, req.nim, req.nim, req.nim, req.nim]
                }
            };
            connection.acquire((err, con) => {
                con.query(creds.update.query, creds.update.data, (err1, res1) =>{
                    if (err1) {
                        res.json({status: 0, code: 500, message: 'Internal server error (1). Please contact the developer for further informations.'});
                        throw err1;
                    } else {
                        con.query(creds.select.query, creds.select.data, (err2, res2) => {
                            console.log(res2);
                            con.release();
                            if (err2) {  
                                res.json({status: 0, code: 500, message: 'Internal server error (2). Please contact the developer for further informations.'});
                                throw err2;
                            } else {
                                mail.setMahasiswaSuccess(res, req);
                                res.json({status: 1, code: 200, message: 'Konfirmasi berhasil'});
                            }
                        });
                    }
                });
            });
        }
    }

    // Method untuk reject konfirmasi
    this.delete = function(req,res) {
        if (_.decoded.role === 2 || _.decoded.role === 4 || _.decoded.role === 1) {
            connection.acquire((err, con) => {
                if (err) {
                    res.json({status: 0, code: 500, message: 'Internal server error (0). Please contact the developer for further informations'});
                    throw err;
                } else {
                    const creds = {
                        select: {
                            query: `
                                SELECT
                                    t.dosen_2 AS t_dosen2,
                                    ta.dosen_2 AS ta_dosen2,
                                    t.status_dp2
                                FROM
                                    ta_dosbing_kedua AS t, ta
                                WHERE
                                    t.nim = ? AND ta.nim = ?
                                `,
                            data: [req.nim, req.nim]
                        },
                        update: {
                            query: `
                                UPDATE 
                                    ta_dosbing_kedua AS t,
                                    ta
                                SET
                                    t.dosen_2 = '0',
                                    t.status_dp2 = null,
                                    ta.dosen_2 = '0',
                                    t.status_req = null
                                WHERE
                                    t.nim = ?
                                    AND ta.nim =?
                                `,
                            data: [req.nim, req.nim]
                        },
                        delete: {
                            query: `
                                DELETE FROM
                                    dosen
                                WHERE
                                    id IN (SELECT dosen_2 FROM ta_dosbing_kedua WHERE nim = ?)
                                `,
                            data: [req.nim]
                        }
                    };
                    con.query(creds.select.query, creds.select.data, (err1, res1) => {
                        if (err1) {
                            res.json({status: 0, code: 500, message: 'Internal server error (1)'});
                            throw err1;
                        } else {
                            const dosen2 = {
                                t: res1[0].t_dosen2,
                                ta: res1[0].ta_dosen2
                            };
                            req.statusDp = res1[0].status_dp2;
                            mail.setMahasiswaRejected(res, req);
                            if (dosen2.t !== 0 && dosen2.ta !== 0) {
                                if (req.status_req === 1) {
                                    con.query(creds.update.query, creds.update.data, (err2, res2) => {
                                        if (err2) {
                                            res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                                            throw err2;
                                        } else {
                                            res.json({status: 1, code: 200, message: 'Berhasil'});
                                        }
                                    });
                                } else if (req.status_req === 2) {
                                    console.log('masuk kondisi kedua');
                                    con.query(creds.delete.query, creds.delete.data, (err2, res2) => {
                                        if (err2) {
                                            res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                                            throw err2;
                                        } else {
                                            con.query(creds.update.query, creds.update.data, (err3, res3) => {
                                                if (err3) {
                                                    res.json({status: 0, code: 500, message: 'Internal server error (3)'});
                                                    throw err3;
                                                } else {
                                                    res.json({status: 1, code: 200, message: 'Berhasil'});
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    res.json({status: 0, code: 204, message: 'Konten tidak ditemukan'});
                                }
                            } else if (dosen2.t === 0 && dosen2.ta === 0) {
                                res.json({status: 1, code: '200', message: 'Mahasiswa terkait tidak memiliki pembimbing kedua'});
                            } else {
                                res.json({status: 0, code: '500', message: 'Internal server error (1). Please contact the developer for further informations.'});
                            }
                        }
                    });
                }
            });
        } else {
            res.json({status: 0, code: 403, message: "You cannot access this service"});
        }
    }
}

module.exports = new PembimbingKedua();