var connection = require('../connection');
var _ = require('../simak');

function Kolokium() {

  // INFO KOLOKIUM
  this.getDataFile = function(res) {
    connection.acquire(function(err, con) {

      var query = "SELECT * FROM `ta_kolokium` WHERE nim = ?";
      var data = [_.decoded.nim];

      con.query(query, data, function(err, result) {
        con.release();
        if (err) {
          res.json({status: 0, message: 'Fungsi Kolokium Gagal'});
          throw err;
        } else {
          res.json(result);
        };
      })
    })
  }

  this.getAll = function(res) {
    if (_.decoded.role == 4 || _.decoded.role == 1 || _.decoded.role == 2) {
      connection.acquire(function(err, con) {

        var query = "SELECT * FROM `ta_kolokium`";
        var data = [_.decoded.nim];

        con.query(query, data, function(err, result) {
          con.release();
          if (err) {
            res.json({status: 0, message: 'Fungsi Kolokium Gagal'});
            throw err;
          } else {
            res.json(result);
          };
        })
      })
    }
    else {
      res.json({status: false, message: "You Can't Access This Service"});
    }
  }

  this.create = function(req, res) {
    connection.acquire(function(err, con) {

      var query = 'UPDATE ta SET topik = ? WHERE nim = ?';
      var data = [req.topik, _.decoded.nim];
      con.query(query, data, function(err, result) {
        if (err) {
          con.release();
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          con.query('select * from ta_kolokium where nim = ?', [_.decoded.nim] ,function(err, result1) {
            if(err) {
              con.release();
              res.json({status: 0, message: 'API Failed'});
              throw err;
            }
            else {

              if(result1.length > 0) {

                query = 'UPDATE ta_kolokium SET tanggal = ? WHERE nim = ?';
                data = [req.tanggal, _.decoded.nim];
                con.query(query, data, function(err, result) {
                  con.release();
                  if(err) {
                    res.json({status: 0, message: 'API Failed'});
                    throw err;
                  }
                  else {
                    res.json({status: 1, message: 'Fungsi Kolokium Berhasil'});
                  }
                })

              }
              else {
                query = 'INSERT ta_kolokium (tanggal, nim, konfirmasi) VALUES (?, ?, ?)';
                data = [req.tanggal, _.decoded.nim, 1];
                con.query(query, data, function(err, result) {
                  con.release();
                  if(err) {
                    res.json({status: 0, message: 'API Failed'});
                    throw err;
                  }
                  else {
                    res.json({status: 1, message: 'Fungsi Kolokium Berhasil'});
                  }
                })

              }

            }
          })
        }
      })
    })
  }

}

module.exports = new Kolokium();
