var connection = require('../connection');
var _ = require('../simak');

function Seminar() {

  this.deleteSeminar = function(res) {
    connection.acquire(function(err, con) {
      con.query('DELETE FROM `ta_seminar` WHERE nim = ?', [_.decoded.nim], function(err, result) {
        if (err) {
          con.release();
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          con.query('DELETE FROM `ta_mandiri` WHERE nim = ?', [_.decoded.nim], function(err, result) {
            if (err) {
              con.release();
              res.json({status: 0, message: 'API Failed'});
              throw err;
            }
            else {
              con.query('DELETE FROM `ta_micon` WHERE nim = ?', [_.decoded.nim], function(err, result) {
                if (err) {
                  con.release();
                  res.json({status: 0, message: 'API Failed'});
                  throw err;
                }
                else {
                  con.query('DELETE FROM `ta_konferensi` WHERE nim = ?', [_.decoded.nim], function(err, result) {
                    con.release();
                    if (err) {
                      res.json({status: 0, message: 'API Failed'});
                      throw err;
                    }
                    else {
                      res.json({status: true, message: 'Berhasil Delete Seminar'});
                    }
                  })
                }
              })
            }
          })
        }
      })
    })
  }

  this.get = function(res) {
    connection.acquire(function(err, con) {
      con.query('select * from ta_seminar where nim = ?',[_.decoded.nim], function(err, result) {
        if(err) {
          con.release();
            res.json({status: 0, message: 'API Failed'});
            throw err;
        }
        else {

          if(result.length > 0) {
            var queryData;
            if(result[0].jenis_seminar == 1) {
              queryData = 'select * from ta_konferensi where nim = ?';
            }
            else if(result[0].jenis_seminar == 2) {
              queryData = 'select * from ta_micon where nim = ?';
            }
            else if(result[0].jenis_seminar == 3) {
              queryData = 'select * from ta_mandiri where nim = ?';
            }

            con.query(queryData, [_.decoded.nim], function(err, result2) {
              if(err) {
                  con.release();
                  res.json({status: 0, message: 'API Failed'});
                  throw err;
              }
              else {
                con.query('select * from ta_log where nim = ?', [_.decoded.nim], function(err, result3) {
                    con.release();
                    count = 0;
                    for(i = 0; i < result3.length; i++) {
                      if(result3[i].approval) count++;
                    }
                    if(err) {
                        res.json({status: 0, message: 'API Failed'});
                        throw err;
                    }
                    else {
                        res.json({'seminar': result[0], 'data': result2[0], 'log': count});
                    }
                })
              }
            })
          }
          else {
              con.query('select * from ta_log where nim = ?', [_.decoded.nim], function(err, result3) {
                  con.release();
                  count = 0;
                  for(i = 0; i < result3.length; i++) {
                      if(result3[i].approval) count++;
                  }
                  if(err) {
                      res.json({status: 0, message: 'API Failed'});
                      throw err;
                  }
                  else {
                      res.json({'seminar': false, 'message': 'Belum Mengisi Seminar', 'log': count});
                  }
              })
          }
        }
      });
    });
  };
}

module.exports = new Seminar();
