var connection = require('../connection');

function JadwalKolokium() {

  this.get = function(res) {
    connection.acquire(function(err, con) {
      con.query('select * from jadwal_kolokium', function(err, result) {
        con.release();
        if(err) {
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          res.json(result);
        }
      });
    });
  };

  this.update = function(todo, res){
    connection.acquire(function(err, con){

      var query = 'UPDATE jadwal_kolokium SET active = ?, jadwal_kolokium = ?, deadline = ?';
      var data = [todo.active, todo.jadwal_kolokium, todo.deadline];
      con.query(query, data, function(err, result) {
        con.release();
        if (err) {
          res.json([{status: 0, message: 'Pembaruan Jadwal Kolokium Gagal'}]);
          throw err;
        } else {
          res.json([{status: 1, message: 'Pembaruan Jadwal Kolokium Berhasil'}]);
        };
      });

    });
  };

}

module.exports = new JadwalKolokium();
