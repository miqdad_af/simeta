var connection = require('../connection');
var _ = require('../simak');
var dosen;

function Mahasiswa() {

  this.getMahasiswa = function(res) {
      if(_.decoded.role == 4 || _.decoded.role == 1 || _.decoded.role == 2) {
        connection.acquire(function(err, con) {
          con.query('SELECT * FROM `mahasiswa_detail` where tahunmasuk < YEAR(CURDATE())', function(err, result) {
              con.release();
              if(err) {
                res.json({status: 0, message: 'API Failed'});
                throw err;
              }
              else {
                res.json(result);
              }
          })
        })
      }
      else {
        res.json({status: false, message: "You Can't Access This Service"});
      }
  }

  // -------------------------
  // KOLOKIUM VIEW
  this.getDataKolokium = function(res) {
    if (_.decoded.role == 4 || _.decoded.role == 1 || _.decoded.role == 2) {
      connection.acquire(function(err, con) {
        con.query('SELECT * FROM mahasiswa_kolokium_view where tahun_masuk < YEAR(CURDATE()) order by timestamp desc', function(err, result) {
          con.release();
          if(err) {
              res.json({status: 0, message: 'API Failed'});
              throw err;
          }
          else {
            for(let i = 0; i < result.length; i++) {
              if (result[i].makalah != null) {
                var link = result[i].makalah;
                result[i].status = "<a target='_blank' href='http://simeta.apps.cs.ipb.ac.id/uploads/fileKolokium/"+link+"'>Lihat Makalah</a>";
                if(result[i].konfirmasi) {
                  result[i].konfirmasi = `<span class='text-success'>Sudah Dikonfirmasi</span>`;
                }
                else {
                  result[i].konfirmasi = `<span class='text-danger'>Belum Dikonfirmasi</span>`;
                }
              }
              else {
                var link = result[i].makalah;
                result[i].status = "<span class='text-danger'>Belum Upload</span>";
              }
            }
            res.json(result);
          }
        });
      });
    }
    else {
      res.json({status: false, message: "You Can't Access This Service"});
    }
  };

  this.getDataKolokiumDosen = function(res) {
    if (_.decoded.role == 4 || _.decoded.role == 1 || _.decoded.role == 2) {
      connection.acquire(function(err, con) {
        con.query('SELECT * FROM mahasiswa_kolokium_view where tahun_masuk < YEAR(CURDATE()) - 3 and (dosen_1 = ? OR dosen_2 = ?) order by timestamp desc', [_.decoded.id, _.decoded.id], function(err, result) {
          con.release();
          if(err) {
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            for(let i = 0; i < result.length; i++) {
              if (result[i].makalah != null) {
                var link = result[i].makalah;
                result[i].status = "<span class='text-success'>Sudah Upload</span>";
                result[i].makalah = "<a target='_blank' href='http://simeta.apps.cs.ipb.ac.id/uploads/fileKolokium/"+link+"'>Lihat Makalah</a>";
              }
              else {
                var link = result[i].makalah;
                result[i].status = "<span class='text-danger'>Belum</span>";
              }
            }
            res.json(result);
          }
        });
      });
    }
    else {
      con.release();
      res.json({status: false, message: "You Can't Access This Service"});
    }
  };

  // -------------------------
  // PRASEMINAR VIEW

  this.getDataSeminarAll = function(res) {
    if (_.decoded.role == 4 || _.decoded.role == 1 || _.decoded.role == 2) {
      connection.acquire(function(err, con) {
        var query = `
          SELECT 
            topik, tahun_masuk, nim, nama, jenis_seminar, DATE_FORMAT(tanggal, "%d/%m/%Y") as tanggal, makalah_mandiri, berkas_micon, berkas_konferensi, dosen1, timestamp, dosen2, penguji1, penguji2 
          FROM 
            mahasiswa_seminar_view 
          WHERE 
            tahun_masuk < YEAR(CURDATE()) - 3 order by timestamp desc
          `;
        con.query(query, function(err, result) {
          con.release();
          if(err) {
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            for(let i = 0; i < result.length; i++) {

              if(result[i].jenis_seminar == 1) {
                result[i].jenis_seminar = "<span class='text-success'>Konferensi</span><br><a target='_blank' href='http://simeta.apps.cs.ipb.ac.id/uploads/fileSeminar/konferensi/"+result[i].berkas_konferensi+"'>Lihat File</a>";
              }
              else if(result[i].jenis_seminar == 2) {
                result[i].jenis_seminar = "<span class='text-info'>Mini Conference</span><br><a target='_blank' href='http://simeta.apps.cs.ipb.ac.id/uploads/fileSeminar/micon/"+result[i].berkas_micon+"'>Lihat File</a>";
              }
              else if(result[i].jenis_seminar == 3) {
                result[i].jenis_seminar = "<span class='text-primary'>Seminar Mandiri</span><br><a target='_blank' href='http://simeta.apps.cs.ipb.ac.id/uploads/fileSeminar/mandiri/"+result[i].makalah_mandiri+"'>Lihat File</a>";
              }
              else {
                result[i].jenis_seminar = "<span class='text-danger'>Belum Seminar</span>";
              }
            }
            res.json(result);
          }
        });
      });
    }
    else {
      con.release();
      res.json({status: false, message: "You Can't Access This Service"});
    }
  };

  this.getDataSeminarDosen = function(res) {
    if (_.decoded.role == 4 || _.decoded.role == 1 || _.decoded.role == 2) {
      connection.acquire(function(err, con) {
        var query = 'SELECT tahun_masuk, nim, nama, jenis_seminar, DATE_FORMAT(tanggal, "%d/%m/%Y") as tanggal, makalah_mandiri, berkas_micon, berkas_konferensi, dosen1, dosen2, timestamp, penguji1, penguji2, dosen_1, dosen_2 FROM `mahasiswa_seminar_view`  where tahun_masuk < YEAR(CURDATE()) - 3 and (dosen_1 = ? or dosen_2 = ?) order by timestamp desc';
        var data = [_.decoded.id, _.decoded.id]
        con.query(query,data, function(err, result) {
          con.release();
          if(err) {
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            for(let i = 0; i < result.length; i++) {

              if(result[i].jenis_seminar == 1) {
                result[i].jenis_seminar = "<span class='text-success'>Konferensi</span><br><a target='_blank' href='http://simeta.apps.cs.ipb.ac.id/uploads/fileSeminar/konferensi/"+result[i].berkas_konferensi+"'>Lihat File</a>";
              }
              else if(result[i].jenis_seminar == 2) {
                result[i].jenis_seminar = "<span class='text-info'>Mini Conference</span><br><a target='_blank' href='http://simeta.apps.cs.ipb.ac.id/uploads/fileSeminar/micon/"+result[i].berkas_micon+"'>Lihat File</a>";
              }
              else if(result[i].jenis_seminar == 3) {
                result[i].jenis_seminar = "<span class='text-primary'>Seminar Mandiri</span><br><a target='_blank' href='http://simeta.apps.cs.ipb.ac.id/uploads/fileSeminar/mandiri/"+result[i].makalah_mandiri+"'>Lihat File</a>";
              }
              else {
                result[i].jenis_seminar = "<span class='text-danger'>Belum Seminar</span>";
              }
            }
            res.json(result);
          }
        });
      });
    }
    else {
      con.release();
      res.json({status: false, message: "You Can't Access This Service"});
    }
  };

  // -------------------------
  // PRASEMINAR VIEW

  this.getDataPraseminar = function(res) {
    if (_.decoded.role == 4 || _.decoded.role == 1 || _.decoded.role == 2) {
      connection.acquire(function(err, con) {
        con.query('SELECT * FROM mahasiswa_praseminar_view where tahun_masuk < YEAR(CURDATE())  order by timestamp desc', function(err, result) {
          con.release();
          if(err) {
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            for(let i = 0; i < result.length; i++) {
              if (result[i].makalah != null) {
                var link = result[i].makalah;
                result[i].status = "<a target='_blank' href='http://simeta.apps.cs.ipb.ac.id/uploads/filePraseminar/"+link+"'>Lihat Makalah</a>";
                if(result[i].konfirmasi) {
                  result[i].konfirmasi = `<span class='text-success'>Sudah Dikonfirmasi</span>`;
                }
                else {
                  result[i].konfirmasi = `<span class='text-danger'>Belum Dikonfirmasi</span>`;
                }
              }
              else {
                var link = result[i].makalah;
                result[i].status = "<span class='text-danger'>Belum</span>";
              }
            }
            res.json(result);
          }
        });
      });
    }
    else {
      con.release();
      res.json({status: false, message: "You Can't Access This Service"});
    }
  };

  this.getDataPraseminarDosen = function(res) {
    if (_.decoded.role == 4 || _.decoded.role == 1 || _.decoded.role == 2) {
      connection.acquire(function(err, con) {
        con.query('SELECT * FROM mahasiswa_praseminar_view  where tahun_masuk < YEAR(CURDATE()) - 3 and (dosen_1 = ? OR dosen_2 = ?) order by timestamp desc',[_.decoded.id, _.decoded.id], function(err, result) {
          con.release();
          if(err) {
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            for(let i = 0; i < result.length; i++) {
              if (result[i].makalah != null) {
                var link = result[i].makalah;
                result[i].status = "<span class='text-success'>Sudah Upload</span>";
                result[i].makalah = "<a target='_blank' href='http://simeta.apps.cs.ipb.ac.id/uploads/filePraseminar/"+link+"'>Lihat Makalah</a>";
              }
              else {
                var link = result[i].makalah;
                result[i].status = "<span class='text-danger'>Belum</span>";
              }
            }
            res.json(result);
          }
        });
      });
    }
    else {
      con.release();
      res.json({status: false, message: "You Can't Access This Service"});
    }
  };


  // -------------------------
  // SIDANG VIEW

  this.getDataSidang = function(res) {
    if (_.decoded.role == 4 || _.decoded.role == 1 || _.decoded.role == 2) {
      connection.acquire(function(err, con) {

        con.query('SELECT * FROM dosen', function(err, result1) {
          dosen = result1;

          con.query('SELECT * FROM mahasiswa_sidang_view where tahun_masuk < YEAR(CURDATE()) - 3 order by timestamp desc', function(err, result) {
            con.release();
            if(err) {
              res.json({status: 0, message: 'API Failed'});
              throw err;
            }
            else {
              for(let i = 0; i < result.length; i++) {
                if (result[i].makalah != null) {
                  var link = result[i].makalah;
                  result[i].status = "<a target='_blank' href='http://simeta.apps.cs.ipb.ac.id/uploads/fileSidang/"+link+"'>Lihat Makalah</a>";
                }
                else {
                  var link = result[i].makalah;
                  result[i].status = "<span class='text-danger'>Belum</span>";
                }
              }
              res.json(result);
            }
          });
        })
      });
    }
    else {
      res.json({status: false, message: "You Can't Access This Service"});
    }
  };

  this.getDataSidangDosen = function(res) {
    if (_.decoded.role == 4 || _.decoded.role == 1 || _.decoded.role == 2) {
      connection.acquire(function(err, con) {

        con.query('SELECT * FROM dosen', function(err, result1) {
          dosen = result1;

          con.query('SELECT * FROM mahasiswa_sidang_view where tahun_masuk < YEAR(CURDATE()) - 3 and (dosen_1 = ? OR dosen_2 = ?) order by timestamp desc',[_.decoded.id, _.decoded.id], function(err, result) {
            con.release();
            if(err) {
              res.json({status: 0, message: 'API Failed'});
              throw err;
            }
            else {
              for(let i = 0; i < result.length; i++) {
                if (result[i].makalah != null) {
                  var link = result[i].makalah;
                  result[i].status = "<a target='_blank' href='http://simeta.apps.cs.ipb.ac.id/uploads/fileSidang/"+link+"'>Lihat Makalah</a>";
                }
                else {
                  var link = result[i].makalah;
                  result[i].status = "<span class='text-danger'>Belum</span>";
                }
              }
              res.json(result);
            }
          });
        })
      });
    }
    else {
      res.json({status: false, message: "You Can't Access This Service"});
    }
  };

  // -------------------------
  // SKL VIEW

  this.getDataSKL = function(res) {
    if (_.decoded.role == 4 || _.decoded.role == 1 || _.decoded.role == 2) {
      connection.acquire(function(err, con) {

        con.query('SELECT * FROM mahasiswa_skl_view where tahun_masuk < YEAR(CURDATE()) - 3 order by timestamp desc', function(err, result) {
          con.release();
          if(err) {
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            for(let i = 0; i < result.length; i++) {
              if (result[i].berkas != null) {
                var link = result[i].berkas;
                result[i].status = "<span class='text-success'>Sudah Upload</span>";
                result[i].berkas = "<a target='_blank' href='http://simeta.apps.cs.ipb.ac.id/uploads/fileSKL/"+link+"'>Lihat SKL</a>";
              }
              else {
                var link = result[i].berkas;
                result[i].status = "<span class='text-danger'>Belum</span>";
              }
            }
            res.json(result);
          }
        });

      });
    }
    else {
      res.json({status: false, message: "You Can't Access This Service"});
    }
  };

  this.getDataSKLDosen = function(res) {
    if (_.decoded.role == 4 || _.decoded.role == 1 || _.decoded.role == 2) {
      connection.acquire(function(err, con) {

        con.query('SELECT * FROM mahasiswa_skl_view where tahun_masuk < YEAR(CURDATE()) - 3 and (dosen_1 = ? OR dosen_2 = ?) order by timestamp desc',[_.decoded.id, _.decoded.id], function(err, result) {
          con.release();
          if(err) {
              res.json({status: 0, message: 'API Failed'});
              throw err;
          }
          else {
            for(let i = 0; i < result.length; i++) {
              if (result[i].berkas != null) {
                var link = result[i].berkas;
                result[i].status = "<span class='text-success'>Sudah Upload</span>";
                result[i].berkas = "<a target='_blank' href='http://simeta.apps.cs.ipb.ac.id/uploads/fileSKL/"+link+"'>Lihat SKL</a>";
              }
              else {
                var link = result[i].berkas;
                result[i].status = "<span class='text-danger'>Belum</span>";
              }
            }
            res.json(result);
          }
        });

      });
    }
    else {
      res.json({status: false, message: "You Can't Access This Service"});
    }
  };


  // ------------------------
  // MAHASISWA BIMBINGAN

  this.getDataBimbingan = function(res) {
    if (_.decoded.role == 4 || _.decoded.role == 2) {
      connection.acquire(function(err, con) {

        con.query('SELECT * FROM ta_view where tahun_masuk < YEAR(CURDATE()) - 3 and (dosen_1 = ? OR dosen_2 = ?)', [_.decoded.id, _.decoded.id], function(err, result) {
          con.release();
          if(err) {
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            res.json(result);
          }
        });

      });
    }
    else {
      res.json({status: false, message: "You Can't Access This Service"});
    }
  };

}

module.exports = new Mahasiswa();
