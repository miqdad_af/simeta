var connection = require('../connection');
var _ = require('../simak');

function Daftar() {

  this.getById = function(res) {
    connection.acquire(function(err, con) {
      con.query('select * from ta_daftar_view where nim = ?', _.decoded.nim, function(err, result) {
        con.release();
        if(err) {
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          res.json(result);
        }
      });
    });
  };

  this.get = function(res) {
    connection.acquire(function(err, con) {

      if (_.decoded.role == 4 || _.decoded.role == 1) {
        con.query('select * from ta_daftar_view', function(err, result) {
          con.release();
          if(err) {
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            res.json(result);
          }
        });
      }

      else {
        con.release();
        res.json({status: false, message: "You Can't Access This Service"});
      }

    });
  };

  // GET MAHASISWA DETAIL BY NIM
  this.getByNim = function(id, res) {
    connection.acquire(function(err, con) {

      if (_.decoded.role == 4 || _.decoded.role == 1) {
        con.query('select * from ta_daftar_view where nim = ?', id, function(err, result) {
          con.release();
          if(err) {
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            res.json(result);
          }
        });
      }

      else {
        con.release();
        res.json({status: false, message: "You Can't Access This Service"});
      }


    });
  };

  this.create = function(todo, res){
    connection.acquire(function(err, con){

      var querySelect = 'SELECT * FROM ta_daftar WHERE nim = ?';
      var dataSelect = [_.decoded.nim];
      con.query(querySelect, dataSelect, function(err, result) {
        if (err) {
          con.release();
          res.json({status: false, message: 'Pembaruan Profile Gagal'});
          throw err;
        }

        else {

          if (result.length > 0){

            var queryUpdate = `UPDATE ta_daftar SET topik = ?, lab = ?, dosen_1 = ?, konsultasi_1 = ?, pertemuan_1 = ?, dosen_2 = ?, konsultasi_2 = ?, pertemuan_2 = ?, progress_1 = ?, progress_2 = ?, progress_3 = ?, progress_4 = ? WHERE nim = ?`;
            var dataUpdate = [todo.topik, todo.lab, todo.dosen_1, todo.konsultasi_1, todo.pertemuan_1, todo.dosen_2, todo.konsultasi_2, todo.pertemuan_2, todo.progress_1, todo.progress_2, todo.progress_3, todo.progress_4, _.decoded.nim];

            con.query(queryUpdate, dataUpdate, function(err, result) {
              con.release();
              if (err) {
                res.json([{status: 0, message: 'Pengajuan Topik Gagal'}]);
                throw err;
              }
              else {
                res.json([{status: 1, message: 'update gan'}]);
              }

            })
          }

          else {
            // res.json({status: true, message: 'ada orang nih'});
            var queryInsert = `INSERT INTO ta_daftar (nim, topik, lab, dosen_1, konsultasi_1, pertemuan_1, dosen_2, konsultasi_2, pertemuan_2, progress_1, progress_2, progress_3, progress_4) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
            var dataInsert = [_.decoded.nim, todo.topik, todo.lab, todo.dosen_1, todo.konsultasi_1, todo.pertemuan_1, todo.dosen_2, todo.konsultasi_2, todo.pertemuan_2, todo.progress_1, todo.progress_2, todo.progress_3, todo.progress_4];
            con.query(queryInsert, dataInsert, function(err, result){
              con.release();
              if (err) {
                res.json([{status: 0, message: 'Pengajuan Topik Gagal'}]);
                throw err;
              }
              else {
                res.json([{status: 1, message: 'Pengajuan Topik Berhasil'}]);
              }

            })

          };
        }
      })
    });
  };

}

module.exports = new Daftar();
