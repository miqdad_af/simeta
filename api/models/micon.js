var connection = require('../connection');
var _ = require('../simak');
var fc = require('../fc');

function Micon() {

  this.getData = function(res) {
    connection.acquire(function(err, con) {
      con.query('SELECT * from jadwal_micon', function(err, result) {
        con.release();
        if(err) {
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          res.json(result);
        }
      })
    })
  }

  this.update = function(todo, res){
    connection.acquire(function(err, con){

      var query = 'UPDATE jadwal_micon SET active = ?, jadwal_micon = ?, deadline = ?';
      var data = [todo.active, todo.jadwal_micon, todo.deadline];
      con.query(query, data, function(err, result) {
        con.release();
        if (err) {
          res.json({status: 0, message: 'Pembaruan Jadwal Kolokium Gagal'});
          throw err;
        } else {
          res.json({status: 1, message: 'Pembaruan Jadwal Kolokium Berhasil'});
        };
      });

    });
  };


    this.create = function(req, res) {
        connection.acquire(function(err, con) {

            var query = 'UPDATE ta SET topik = ? WHERE nim = ?';
            var data = [req.topik, _.decoded.nim];
            con.query(query, data, function(err, result) {
                if (err) {
                    con.release();
                    res.json({status: 0, message: 'API Failed'});
                    throw err;
                }
                else {
                    con.query('select * from ta_micon where nim = ?', [_.decoded.nim] ,function(err, result1) {
                        if(err) {
                            con.release();
                            res.json({status: 0, message: 'API Failed'});
                            throw err;
                        }
                        else {
                            fc.createSeminar(2, req.tanggal);
                            if(result1.length > 0) {

                                query = 'UPDATE ta_micon SET tanggal = ?, jam = ? WHERE nim = ?';
                                data = [req.tanggal, req.jam, _.decoded.nim];
                                con.query(query, data, function(err, result) {
                                    con.release();
                                    if(err) {
                                        res.json({status: 0, message: 'API Failed'});
                                        throw err;
                                    }
                                    else {
                                        res.json({status: 1, message: 'Fungsi Kolokium Berhasil'});
                                    }
                                })

                            }
                            else {
                                query = 'INSERT ta_micon (tanggal, nim, konfirmasi, jam) VALUES (?, ?, ?, ?)';
                                data = [req.tanggal, _.decoded.nim, 1, req.jam];
                                con.query(query, data, function(err, result) {
                                    con.release();
                                    if(err) {
                                        res.json({status: 0, message: 'API Failed'});
                                        throw err;
                                    }
                                    else {
                                        res.json({status: 1, message: 'Fungsi Kolokium Berhasil'});
                                    }
                                })

                            }

                        }
                    })
                }
            })
        })
    }

}

module.exports = new Micon();
