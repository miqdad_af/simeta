var connection = require('../connection');

function JadwalPraseminar() {

  this.get = function(res) {
    connection.acquire(function(err, con) {
      con.query('select * from jadwal_praseminar', function(err, result) {
        con.release();
        if(err) {
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          res.json(result);
        }
      });
    });
  };

  this.update = function(todo, res){
    connection.acquire(function(err, con){

      var query = 'UPDATE jadwal_praseminar SET active = ?, jadwal_praseminar = ?, deadline = ?, file = ?';
      var data = [todo.active, todo.jadwal_praseminar, todo.deadline, todo.file];
      con.query(query, data, function(err, result) {
        con.release();
        if (err) {
          res.json([{status: 0, message: 'Pembaruan Jadwal Praseminar Gagal'}]);
          throw err;
        } else {
          res.json([{status: 1, message: 'Pembaruan Jadwal Praseminar Berhasil'}]);
        };
      });

    });
  };

}

module.exports = new JadwalPraseminar();
