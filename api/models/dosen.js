var connection = require('../connection');
var _ = require('../simak');

function Dosen() {

  this.get = function(res) {
    connection.acquire(function(err, con) {
      con.query('select id, nama, singkatan from dosen where role = 2 or role = 4 group by id desc', function(err, result) {
        con.release();
        if(err) {
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          res.json(result);
        }
      });
    });
  };

  // SIMETA v2
  this.getById = function(res) {
    connection.acquire((err, con) => {
      con.query('SELECT id, nama, singkatan from dosen WHERE id = ?', [_.decoded.id], (err1, res1) => {
        con.release();
        if (err) {
          res.json({status: 0, code: 500, message: 'Internal server error (1). Please contact the developer for further informations.'});
          throw err;
        } else {
          res.json({status: 1, code: 200, data: res1[0]});
        }
      });
    })
  }

  this.update = function(todo, res){
    connection.acquire(function(err, con){

      var query = 'UPDATE jadwal_kolokium SET active = ?, jadwal_kolokium = ?, deadline = ?';
      var data = [todo.active, todo.jadwal_kolokium, todo.deadline];
      con.query(query, data, function(err, result) {
        con.release();
        if (err) {
          res.json([{status: 0, message: 'Pembaruan Jadwal Kolokium Gagal'}]);
          throw err;
        } else {
          res.json([{status: 1, message: 'Pembaruan Jadwal Kolokium Berhasil'}]);
        };
      });

    });
  };

}

module.exports = new Dosen();
