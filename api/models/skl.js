var connection = require('../connection');
var _ = require('../simak');

function SKL() {

  this.getById = function(res) {
    connection.acquire(function(err, con) {
      con.query('SELECT * FROM ta_skl WHERE nim = ?', [_.decoded.nim], function(err, result) {
        con.release();
        if (err) {
          res.json({status: 0, message: 'Pendaftaran SKL Gagal'});
          throw err;
        } else {
          res.json(result);
        };
      })
    })
  }

  this.create = function(todo, res) {
    connection.acquire(function(err, con) {
      con.query('SELECT * FROM ta_skl WHERE nim = ?', [_.decoded.nim], function(err, result) {
        if(result.length > 0) {
          var query = 'UPDATE ta_skl SET tanggal = ? WHERE nim = ?';
          var data = [todo.tanggal, _.decoded.nim];
          con.query(query, data, function(err, result) {
            if (err) {
              con.release();
              res.json({status: 0, message: 'Pendaftaran SKL Gagal'});
              throw err;
            } else {
              con.query('UPDATE ta SET topik = ? WHERE nim = ?', [todo.topik, _.decoded.nim], function(err1, result) {
                con.release();
                if (err1) {
                  res.json({status: 0, message: 'Pendaftaran SKL Gagal'});
                  throw err1;
                } else {
                  res.json({status: 1, message: 'Pendaftaran SKL Berhasil'});
                };
              })
            };
          });
        }
        else {
          var query = 'INSERT INTO ta_skl (nim, tanggal) VALUES (?, ?)';
          var data = [_.decoded.nim, todo.tanggal];
          con.query(query, data, function(err, result) {
            if (err) {
              con.release();
              res.json({status: 0, message: 'Pendaftaran SKL Gagal'});
              throw err;
            } else {
              con.query('UPDATE ta SET topik = ? WHERE nim = ?', [todo.topik, _.decoded.nim], function(err1, result) {
                con.release();
                if (err1) {
                  res.json({status: 0, message: 'Pendaftaran SKL Gagal'});
                  throw err1;
                } else {
                  res.json({status: 1, message: 'Pendaftaran SKL Berhasil'});
                };
              })
            };
          });
        }
      })
    });
  };

}

module.exports = new SKL();
