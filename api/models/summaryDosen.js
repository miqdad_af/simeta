var connection = require('../connection');
var _ = require('../simak');

var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

function SummaryDosen(){

  this.getDosenSummary = function(res) {

      if (_.decoded.role == 4 ||_.decoded.role == 2) {
        connection.acquire(function(err, con) {
          con.query('SELECT * FROM mahasiswa_summary WHERE tahun_masuk < YEAR(CURDATE()) - 3 and (dosen_1 = ? or dosen_2 = ?) order by nim desc', [_.decoded.id, _.decoded.id], function(err, result) {
            con.release();
            if(err) {
                res.json({status: 0, message: 'API Failed'});
                throw err;
            }
            else {

                for(i = 0; i < result.length; i++) {

                    if(result[i].tanggal_kolokium) {
                        d = new Date(result[i].tanggal_kolokium);
                        result[i].tanggal_kolokium = d.getDate()+' '+bulan[d.getMonth()]+' '+d.getFullYear()
                    }

                    if(result[i].tanggal_praseminar) {
                        d = new Date(result[i].tanggal_praseminar);
                        result[i].tanggal_praseminar = d.getDate()+' '+bulan[d.getMonth()]+' '+d.getFullYear()
                    }

                    if(result[i].tanggal_sidang) {
                        d = new Date(result[i].tanggal_sidang);
                        result[i].tanggal_sidang = d.getDate()+' '+bulan[d.getMonth()]+' '+d.getFullYear()
                    }

                    if(result[i].tanggal_seminar) {
                        d = new Date(result[i].tanggal_seminar);
                        result[i].tanggal_seminar = d.getDate()+' '+bulan[d.getMonth()]+' '+d.getFullYear()
                    }

                    if(result[i].tanggal_sidang) {
                        d = new Date(result[i].tanggal_sidang);
                        result[i].tanggal_sidang = d.getDate()+' '+bulan[d.getMonth()]+' '+d.getFullYear()
                    }

                    if(result[i].tanggal_skl) {
                        d = new Date(result[i].tanggal_skl);
                        result[i].tanggal_skl = d.getDate()+' '+bulan[d.getMonth()]+' '+d.getFullYear()
                    }

                }

                res.json(result);
            }
          });
        });
      }

      else {
        res.json({status: false, message: "You Can't Access This Service"});
      }

    }

};

module.exports = new SummaryDosen();
