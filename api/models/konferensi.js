var connection = require('../connection');
var _ = require('../simak');
var fc = require('../fc');

function Konferensi() {

  this.create = function(todo, res){
    connection.acquire(function(err, con){

      con.query('select nim from ta_konferensi where nim = ?', [_.decoded.nim], function(err, result) {
        if(err) {
          con.release();
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          if(result.length > 0) {
            var query = 'UPDATE ta_konferensi SET nama_konferensi = ?, judul_paper = ?, jam = ?, tempat = ?, tanggal = ? WHERE nim = ?';
            var data = [todo.nama_konferensi, todo.judul_paper, todo.jam, todo.tempat, todo.tanggal, _.decoded.nim];
            con.query(query, data, function(err, result) {
              con.release();
              if (err) {
                res.json({status: 0, message: 'Update Konferensi Gagal'});
                throw err;
              } else {
                fc.createSeminar(1, todo.tanggal);
                res.json({status: 1, message: 'Update Konferensi Berhasil'});
              };
            });
          }
          else {
            var query = 'INSERT INTO ta_konferensi (nim, nama_konferensi, judul_paper, tempat, tanggal, jam) VALUES (?, ?, ?, ?, ?, ?)';
            var data = [_.decoded.nim, todo.nama_konferensi, todo.judul_paper, todo.tempat, todo.tanggal, todo.jam];
            con.query(query, data, function(err, result) {
              con.release();
              if (err) {
                res.json({status: 0, message: 'Pendaftaran Konferensi Gagal'});
                throw err;
              } else {
                fc.createSeminar(1, todo.tanggal);
                res.json({status: 1, message: 'Pendaftaran Konferensi Berhasil'});
              };
            });
          }
        }
      })
    });
  };

}

module.exports = new Konferensi();
