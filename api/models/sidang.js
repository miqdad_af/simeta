var connection = require('../connection');
var _ = require('../simak');
var jwt = require('../direct/generateJWT');
var mail = require('../mail');
var undangan = require('../generatePDF/undanganSidang');

function Sidang() {

  this.get = function(res) {
    connection.acquire(function(err, con) {
      con.query('select * from ta_sidang', function(err, result) {
        con.release();
        if(err) {
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          res.json(result);
        }
      });
    });
  };

  this.getById = function(res) {
    connection.acquire(function(err, con) {
      con.query('select * from ta_sidang where nim = ?', _.decoded.nim, function(err, result) {
        con.release();
        if(err) {
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          res.json(result);
        }
      });
    });
  };

  // Method untuk mendapatkan data-data mahasiswa untuk admin
  this.getMahasiswaAdmin = function(todo, res) {
    if (_.decoded.role === 4) {
      connection.acquire(function(err, con) {
        const creds = {
          select: {
            query: `
              SELECT
                *
              FROM
                mahasiswa_sidang_view
              WHERE
                nama = ?
            `,
            data: [todo.nama]
          }
        };
        con.query(creds.select.query, creds.select.data, (err1, res1) => {
          con.release();
          if (err1) {
            res.json({status: 0, code: 500, message: 'Internal server error (1)'});
            throw err1;
          } else {
            res.json({status: 1, code: 200, data: res1});
          }
        });
      });
    }
  }

  // Method untuk mendapatkan data-data mahasiswa untuk dosen (penguji dan pembimbing)
  this.getMahasiswaDosen = function(res) {
    connection.acquire((err, con) => {
      if (err) {
        con.release();
        res.json({status: 0, code: 500, message: 'Internal server error (0)'});
        throw err;
      } else {
        const creds = {
          select: {
            query: `
              SELECT
                m.tahun_masuk,
                m.nama,
                m.nim,
                ta.topik,
                ta.dosen_1,
                ta.dosen_2,
                ta.penguji_1,
                ta.penguji_2,
                tas.konfirmasi_pembimbing1,
                tas.konfirmasi_pembimbing2,
                tas.konfirmasi_penguji1,
                tas.konfirmasi_penguji2,
                tas.tempat,
                tas.jam,
                tas.tanggal
              FROM
                mahasiswa AS m
              INNER JOIN
                ta ON m.nim = ta.nim
              INNER JOIN
                ta_sidang AS tas ON ta.nim = tas.nim
              WHERE
                tas.status_formulir = 1
              AND
                (
                  (ta.dosen_1 = ? AND (tas.konfirmasi_pembimbing1 = 0 || tas.konfirmasi_pembimbing1 = 2)) OR 
                  (ta.dosen_2 = ? AND tas.konfirmasi_pembimbing2 = 0) OR 
                  (ta.penguji_1 = ? AND tas.konfirmasi_penguji1 = 0) OR 
                  (ta.penguji_2 = ? AND tas.konfirmasi_penguji2 = 0)
                )
            `,
            data: [_.decoded.id, _.decoded.id, _.decoded.id, _.decoded.id]
          }
        };
        con.query(creds.select.query, creds.select.data, (err1, res1) => {
          con.release();
          if (err1) {
            res.json({status: 0, code: 500, message: 'Internal server error (1)'});
            throw err1;
          } else {
            res1.forEach(element => {
              if (element.dosen_1 == _.decoded.id) {
                element.dosen_status = 'Dosen pembimbing ketua';
              } else if (element.dosen_2 == _.decoded.id) {
                element.dosen_status = 'Dosen pembimbing anggota';
              } else if (element.penguji_1 == _.decoded.id) {
                element.dosen_status = 'Penguji pertama';
              } else if (element.penguji_2 == _.decoded.id) {
                element.dosen_status = 'Penguji kedua';
              }
            });
            res.json({status: 1, code: 200, message: 'Success', data: res1});
          }
        });
      }
    });
  };

  // Method untuk mendapatkan status konfirmasi pelaksanaan sidang (ex: status_tempat)
  this.getStatus = function(todo, res) {
    connection.acquire((err, con) => {
      if (todo.nim) {
        const creds = {
          select: {
            query: `
              SELECT
                status_tempat, 
                status_formulir,
                status_admin,
                konfirmasi_pembimbing1,
                konfirmasi_pembimbing2,
                konfirmasi_penguji1,
                konfirmasi_penguji2
              FROM
                ta_sidang
              WHERE
                nim = ?
            `,
            data: [todo.nim]
          }
        }
        con.query(creds.select.query,creds.select.data, (err1, res1) => {
          con.release();
          if (err1) {
            res.json({status: 0, code: 500, message: 'Internal server error (1)'});
            throw err1;
          } else {
            res.json({status: 1, code: 200, message: 'Success', data: res1});
          }
        });
      } else {
        res.json({status: 0, code: 400, message: 'Bad request'});
      }
    });
  }

  // Method untuk assign pengajuan pelaksaan sidang dari mahasiswa
  this.set = function(todo, res) {
    connection.acquire(function(err, con) {
      var query = "select * from `ta_sidang` where nim = ?";
      var data = [_.decoded.nim];
      const credsMhs = {
        nama: todo.nama,
        nim: todo.nim,
        dosen1: todo.dosen1,
        topik: todo.topik,
        tanggal: todo.tanggal,
        jam: todo.jam
      };
      const token = jwt.create(2, credsMhs.nim);
      con.query(query, data, function(err, result) {
        if(err) {
          con.release();
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          if (result.length > 0) {
            const token = jwt.create(2, todo.nim);
            var queryUpdate = "UPDATE `ta_sidang` SET tempat = ?, tanggal = ?, jam = ?, status_formulir = ? where nim = ?";
            var dataUpdate = [todo.tempat, todo.tanggal, todo.jam, 1, _.decoded.nim];
            con.query(queryUpdate, dataUpdate, function(err, resultUpdate) {
              if (err) {
                con.release();
                res.json({status: false, message: 'Pendaftaran Sidang Gagal'});
                throw err;
              }
              else {
                var updateTa1 = "UPDATE `ta` SET topik = ? where nim = ?";
                var dataTa1 = [todo.topik, _.decoded.nim];
                con.query(updateTa1, dataTa1, function(err, resultTa1) {
                  con.release();
                  if (err) {
                    res.json({status: false, message: 'Pendaftaran Sidang gagal'});
                    throw err;
                  }
                  else {
                    mail.setSidangPertama(res, credsMhs, token);
                    res.json({status: true, message: 'Pendaftaran Sidang berhasil'});
                  }
                })
              }
            })
          }
          else {
            var queryInsert = "INSERT INTO ta_sidang (nim, tanggal, jam, tempat, status_formulir, konfirmasi_pembimbing1, konfirmasi_pembimbing2, konfirmasi_penguji1, konfirmasi_penguji2) VALUES (?, ?, ?, ?, 1, 0, 0, 0, 0)";
            var dataInsert = [_.decoded.nim, todo.tanggal, todo.jam, todo.tempat];
            con.query(queryInsert, dataInsert, function(err, resultInsert) {
              con.release();
              if(err) {
                res.json({status: 0, message: 'API Failed'});
                throw err;
              }
              else {
                mail.setSidangPertama(res, credsMhs, token);
                res.json({status: true, message: 'Pendaftaran Sidang berhasil'});
              }
            });
          }
        }
      });
    });
  }

  // Method untuk konfirmasi pembimbing ketua/pertama
  this.konfirmasiPertama = function(todo, res) {
    if ((_.decoded.role === 2 || _.decoded.role === 4) && todo.statusDosen === 'Dosen pembimbing ketua') {
      connection.acquire((err, con) => {
        if (err) {
          con.release();
          res.json({status: 0, code: 500, message: 'Internal server error (0)'});
          throw err;
        } else {
          const creds = {
            update: {
              query: `
                UPDATE
                  ta_sidang
                SET
                  konfirmasi_pembimbing1 = 1
                WHERE
                  nim = ?
              `,
              data: [todo.nim]
            },
            select: {
              query: `
                SELECT
                  d.nama,
                  d.nim,
                  d.topik,
                  d.dosen1,
                  d.dosen2,
                  d.penguji1,
                  d.penguji2,
                  d.dosen2_email,
                  d.penguji1_email,
                  d.penguji2_email,
                  m.tanggal,
                  m.tempat,
                  m.jam
                FROM
                  dosen_mahasiswa_view AS d
                INNER JOIN
                  mahasiswa_sidang_view AS m ON d.nim = m.nim
                WHERE
                  d.nim = ? AND m.nim = ?
              `,
              data: [todo.nim, todo.nim]
            }
          };
          con.query(creds.select.query, creds.select.data, (err1, res1) => {
            if (err1) {
              con.release();
              res.json({status: 0, code: 500, message: 'Internal server error (1)'});
              throw err1;
            } else {
              con.query(creds.update.query, creds.update.data, (err2, res2) => {
                con.release();
                if (err2) {
                  res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                  throw err2;
                } else {
                  const credsMhs = {
                    nama: res1[0].nama,
                    nim: res1[0].nim,
                    topik: res1[0].topik,
                    tempat: res1[0].tempat,
                    jam: res1[0].jam,
                    tanggal: res1[0].tanggal,
                    namaDosen1: res1[0].dosen1,
                    namaDosen2: res1[0].dosen2,
                    namaPenguji1: res1[0].penguji1,
                    namaPenguji2: res1[0].penguji2,
                    email: [
                      res1[0].dosen2_email,
                      res1[0].penguji1_email,
                      res1[0].penguji2_email,
                    ],
                    statusDosen: todo.statusDosen,
                  };
                  const token = jwt.mailToken(credsMhs);
                  // mail
                  mail.setSidang(res, credsMhs, token);
                  res.json({status: 1, code: 200, message: 'Berhasil'});
                }
              });
            }
          });
        }
      })
    } else {
      res.json({status: 0, code: 403, message: 'Forbidden'});
    }
  }

  // Mehthod untuk konfirmasi SELAIN pembimbing ketua
  this.konfirmasi = function(todo, res) {
    if ((_.decoded.role === 2 || _.decoded.role === 4)) {
      connection.acquire((err, con) => {
        if (err) {
          con.release();
          res.json({status: 0, code: 500, message: 'Internal server error (0)'});
          throw err;
        } else {
          let creds;
          switch(todo.statusDosen) {
            case 'Dosen pembimbing anggota':
              creds = {
                update: {
                  query: `
                    UPDATE
                      ta_sidang
                    SET
                      konfirmasi_pembimbing2 = 1
                    WHERE
                      nim = ?
                  `,
                  data: [todo.nim]
                }
              };
              break;
            case 'Penguji pertama':
              creds = {
                update: {
                  query: `
                    UPDATE
                      ta_sidang
                    SET
                      konfirmasi_penguji1 = 1
                    WHERE
                      nim = ?
                  `,
                  data: [todo.nim]
                }
              };
              break;
            case 'Penguji kedua':
              console.log('masuk penguji kedua');
              creds = {
                update: {
                  query: `
                    UPDATE
                      ta_sidang
                    SET
                      konfirmasi_penguji2 = 1
                    WHERE nim = ?
                  `,
                  data: [todo.nim]
                }
              };
              break;
            default:
              res.json({status: 0, code: 400, message: 'Bad request'});
              break;
          }
          creds.select = {
            query: `
              SELECT
                konfirmasi_pembimbing1
              FROM 
                ta_sidang
              WHERE
                nim = ?
            `,
            data: [todo.nim]
          }
          const credsSyarat = {
            select: {
              query: `
                SELECT
                  tas.konfirmasi_pembimbing1,
                  tas.konfirmasi_pembimbing2,
                  tas.konfirmasi_penguji1,
                  tas.konfirmasi_penguji2,
                  m.email,
                  m.nama
                FROM
                  ta_sidang AS tas
                INNER JOIN
                  mahasiswa_detail AS m ON tas.nim = m.nim
                WHERE
                  tas.nim = ? AND m.nim = ? 
            `,
              data: [todo.nim, todo.nim]
            },
            update: {
              query: `
                UPDATE
                  ta_sidang
                SET
                  status_formulir = 2
                WHERE
                  nim = ?
              `,
              data: [todo.nim]
            }
          };
          con.query(creds.select.query, creds.select.data, (err1, res1) => {
            if (err1) {
              con.release();
              res.json({status: 0, code: 500, message: 'Internal server error (1)'});
              throw err1;
            } else if (res1[0].konfirmasi_pembimbing1 !== 1) {
              res.json({status: 0, message: 'Pembimbing ketua menolak atau belum memberikan konfirmasi'});
            } else {
              con.query(creds.update.query, creds.update.data, (err2, res2) => {
                if (err2) {
                  con.release();
                  res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                  throw err2;
                } else {
                  con.query(credsSyarat.select.query, credsSyarat.select.data, (err3, res3) => {
                    if (err3) {
                      con.release();
                      res.json({status: 0, code: 500, message: 'Internal server error (3)'});
                      throw err3;
                    } else {
                      let 
                        konfirmasiPembimbing1 = res3[0].konfirmasi_pembimbing1, 
                        konfirmasiPembimbing2 = res3[0].konfirmasi_pembimbing2,
                        konfirmasiPenguji1 = res3[0].konfirmasi_penguji1,
                        konfirmasiPenguji2 = res3[0].konfirmasi_penguji2;
                        
                      // Cek syarat minimum sidang apakah sudah mencukupi
                      if (
                        (konfirmasiPembimbing1 === 1 && konfirmasiPembimbing2 === 1 && konfirmasiPenguji1 === 1) || 
                        (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 1 && konfirmasiPenguji2 === 1)
                      ) {
                        con.query(credsSyarat.update.query, credsSyarat.update.data, (err4, res4) => {
                          con.release();
                          if (err4) {
                            res.json({status: 0, code: 500, message: 'Internal server error (4)'});
                            throw err4;
                          } else {
                            const credsMhs = {
                              nama: res3[0].nama,
                              email: res3[0].email,
                              type : 'pertama'
                            };
                            // Mail
                            mail.setSidangSukses(res, credsMhs);
                            res.json({status: 1, code: 200, message: 'Berhasil'});
                          }
                        });
                      } else {
                        con.release();
                        res.json({status: 1, code: 200, message: 'Berhasil'});
                      }
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  } 

  // Method untuk konfirmasi admin
  this.konfirmasiAdmin = function(todo, res) {
    if (_.decoded.role === 4) {
      connection.acquire((err, con) => {
        if (err) {
          con.release();
          res.json({status: 0, code: 500, message: 'Internal server error(0)'});
          throw err;
        } else {
          const creds = {
            update: {
              query: `
                UPDATE
                  ta_sidang
                SET
                  status_admin = 1,
                  status_tempat = 1,
                  tempat = ?
                WHERE
                  nim = ?
              `,
              data: [todo.tempat, todo.nim]
            },
            select: {
              query: `
                SELECT
                  s.tanggal,
                  s.jam,
                  s.tempat,
                  s.konfirmasi_pembimbing2,
                  s.konfirmasi_penguji1,
                  s.konfirmasi_penguji2,
                  s.makalah,
                  d.dosen1,
                  d.dosen2,
                  d.penguji1,
                  d.penguji2,
                  d.penguji1_email,
                  d.penguji2_email,
                  d.dosen2_email,
                  m.tahunmasuk,
                  m.email,
                  m.nama,
                  m.nim,
                  d.topik
                FROM
                  ta_sidang AS s
                INNER JOIN
                  mahasiswa_detail AS m ON s.nim = m.nim
                INNER JOIN
                  dosen_mahasiswa_view AS d ON m.nim = d.nim
                WHERE
                  s.nim = ? AND m.nim = ? AND d.nim = ?
              `,
              data: [todo.nim, todo.nim, todo.nim]
            }
          };
          con.query(creds.update.query, creds.update.data, (err1, res1) => {
            if (err1) {
              con.release();
              res.json({status: 0, code: 500, message: 'Internal server error (1)'});
              throw err1;
            } else {
              con.query(creds.select.query, creds.select.data, (err2,res2) => {
                if (err2) {
                  con.release();
                  res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                  throw err2;
                } else {
                  const credsMhs = {
                    nama: res2[0].nama,
                    nim: res2[0].nim,
                    topik: res2[0].topik,
                    tempat: todo.tempat,
                    jam: res2[0].jam,
                    tanggal: res2[0].tanggal,
                    tahunMasuk: res2[0].tahunmasuk,
                    namaDosen1: res2[0].dosen1,
                    namaDosen2: res2[0].dosen2,
                    penguji1: res2[0].penguji1,
                    penguji2: res2[0].penguji2,
                    emailDosen1: res2[0].dosen1_email,
                    emailDosen2: res2[0].dosen2_email,
                    emailPenguji1: res2[0].penguji1_email,
                    emailPenguji2: res2[0].penguji2_email,
                    konfirmasiPembimbing2: res2[0].konfirmasi_pembimbing2,
                    konfirmasiPenguji1: res2[0].konfirmasi_penguji1,
                    konfirmasiPenguji2: res2[0].konfirmasi_penguji2,
                    file: res2[0].makalah,
                    type: 'kedua'
                  };
                  const token = jwt.create(2, credsMhs.nim);
                  // email
                  undangan.sidangUndangan(credsMhs.nim, token, res);
                  mail.sidangMail(credsMhs.nim, credsMhs.nama, credsMhs.topik, credsMhs.file, credsMhs.tempat, credsMhs.jam, credsMhs.tanggal);
                  mail.setSidangSukses(res, credsMhs);
                  res.json({status: 1, code: 200, message: 'Berhasil'});
                }
              });
            }
          })
        }
      });
    } else {
      res.json({status: 0, code: 403, message: 'Forbidden'});
    }
  }

  // Method untuk penolakan pembimbing ketua/pertama
  this.declinePertama = function(todo, res) {
    if ((_.decoded.role === 2 || _.decoded.role === 4) && todo.statusDosen === 'Dosen pembimbing ketua') {
      connection.acquire((err, con) => {
        if (err) {
          con.release();
          res.json({status: 0, code: 500, message: 'Internal server error (0)'});
          throw err;
        } else {
          const creds = {
            delete: {
              // query: `
              //   UPDATE
              //     ta_sidang
              //   SET
              //     status_tempat = 0,
              //     status_formulir = 0,
              //     tempat = null,
              //     jam = null,
              //     tanggal = null,
              //     konfirmasi_pembimbing1 = 2
              //   WHERE
              //     nim = ?
              // `,
              query: `
                DELETE FROM
                  ta_sidang
                WHERE
                  nim = ?
              `,
              data: [todo.nim]
            },
            select: {
              query: `
                SELECT
                  nama,
                  email
                FROM
                  mahasiswa_detail
                WHERE
                  nim = ?
              `,
              data: [todo.nim]
            }
          };
          con.query(creds.select.query, creds.select.data, (err1, res1) => {
            if (err1) {
              con.release();
              res.json({status: 0, code: 500, message: 'Internal server error (0)'});
              throw err1;
            } else {
              con.query(creds.delete.query, creds.delete.data, (err2, res2) => {
                con.release();
                if (err2) {
                  res.json({status: 0, code: 500, message: 'Internal server error (1)'});
                  throw err2;
                } else {
                  const credsMhs = {
                    nama: res1[0].nama,
                    email: res1[0].email,
                    alasan: todo.alasan,
                    statusDosen: todo.statusDosen
                  };
                  // Mail
                  mail.declineSidang(res, credsMhs);
                  res.json({status: 1, code: 200, message: 'Berhasil'});
                }
              });
            }
          });
        }
      })
    } else {
      res.json({status: 0, code: 403, message: 'Forbidden'});
    }
  }

  // Method untuk penolakan SELAIN pembimbing ketua/pertama
  this.decline = function(todo, res) {
    if (_.decoded.role === 2 || _.decoded.role === 4) {
      connection.acquire((err, con) => {
        if (err) {
          con.release();
          res.json({status: 0, code: 500, message: 'Internal server error (0)'});
          throw err;
        } else {
          const creds = {
            delete: {
              // query: `
              //   UPDATE
              //     ta_sidang
              //   SET
              //     status_formulir = 0,
              //     status_tempat = 0,
              //     status_admin = 0,
              //     konfirmasi_penguji2 = 0,
              //     konfirmasi_penguji1 = 0,
              //     konfirmasi_pembimbing1 = 0,
              //     konfirmasi_pembimbing2 = 0,
              //     tempat = null,
              //     jam = null,
              //     tanggal = null
              //   WHERE
              //     nim = ?
              // `,
              query: `
                DELETE FROM
                  ta_sidang
                WHERE
                  nim = ?
              `,
              data: [todo.nim]
            },
            select: {
              query: `
                SELECT
                  nama,
                  email
                FROM
                  mahasiswa_detail
                WHERE
                  nim = ?
              `,
              data: [todo.nim]
            }
          };
          con.query(creds.delete.query, creds.delete.data, (err1, res1) => {
            if (err1) {
              con.release();
              res.json({status: 0, code: 500, message: 'Internal server error (1)'});
              throw err1;
            } else {
              con.query(creds.select.query, creds.select.data, (err2, res2) => {
                con.release();
                if (err2) {
                  res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                  throw err2;
                } else {
                  // Mail penolakan
                  const credsMhs = {
                    nama: res2[0].nama,
                    email: res2[0].email,
                    alasan: todo.alasan,
                    statusDosen: todo.statusDosen
                  };
                  mail.declineSidang(res, credsMhs);
                  res.json({status: 1, code: 200, message: 'Berhasil'}); 
                }
              });
            }
          });
        }
      });
    } else {
      res.json({status: 0, code: 403, message: 'Forbidden'});
    }
  }

}

module.exports = new Sidang();
