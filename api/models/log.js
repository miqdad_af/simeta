var connection = require('../connection');
var _ = require('../simak');
var mail = require('../mail');
var generateJWT = require('../direct/generateJWT');

function Log() {

  this.deleteLog = function(req, res) {
    connection.acquire(function(err, con) {
      con.query('select id, nim from ta_log where id = ?', [req.id], function(err, result) {
        if(err) {
          con.release();
          res.json({status: 0, message: 'Query Error'});
        }
        else {
          if(result.length > 0) {

            if(result[0].nim == _.decoded.nim) {
              con.query('DELETE from ta_log where id = ?', [req.id], function(err, result1) {
                if(err) {
                  con.release();
                  res.json({status: 0, message: 'Query Error'});
                  throw err;
                }
                else {
                  con.query('DELETE from ta_log_jawaban where topikId = ?', [req.id], function(err, result2) {
                    con.release();
                    if(err) {
                      res.json({status: 0, message: 'Query Error'});
                      throw err;
                    }
                    else {
                      res.json({status: 1, message: 'Delete Log Successful'});
                    }
                  })
                }
              })
            }
            else {
              con.release();
              res.json({status: 0, message: "You Don't Have Access"});
            }

          }
          else {
            con.release();
            res.json({status: 0, message: 'Log Not Fount'});
          }
        }
      })
    })
  }

  this.getByNim = function(id, res) {
    connection.acquire(function(err, con) {
      con.query('select * from ta_log_view where nim = ? group by id desc', _.decoded.nim, function(err, result) {
        con.release();
        if(err) {
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          res.json(result);
        }
      });
    });
  };

  this.getById = function(id, res) {
    connection.acquire(function(err, con) {
      con.query('select * from ta_log_view where id = ?', id, function(err, result) {
        con.release();
        if(err) {
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          res.json(result);
        }
      });
    });
  };

  this.getByDosenId = function(id, res) {
    connection.acquire(function(err, con) {
      if (_.decoded.role == 4 || _.decoded.role == 2) {
        con.query('select * from ta_log_view where dosen_1 = ? OR dosen_2 = ? group by id desc', [_.decoded.id, _.decoded.id], function(err, result) {
          con.release();
          if(err) {
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            res.json(result);
          }
        });
      }
      else {
        con.release();
        res.json({status: false, message: "You Can't Access This Service"});
      }
    });
  };

  this.getJawabanById = function(id, res) {
    connection.acquire(function(err, con) {
      con.query('select * from ta_log_jawaban_view where topikId = ? ORDER BY timestamp ASC', id, function(err, result) {
        con.release();
        if(err) {
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          res.json(result);
        }
      });
    });
  };

  this.approve = function(req, res) {
    if (_.decoded.role == 4 || _.decoded.role == 2 || _.decoded.role == 1) {
      connection.acquire(function(err, con) {
        var query = 'UPDATE ta_log SET approval = ? WHERE id = ?';
        var data = [req.status, req.id];
        con.query(query, data, function(err, result) {
          if(err) {
            con.release();
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            var querySearch = 'SELECT * from ta_log_view where id = ?';
            var dataSearch = [req.id];
            con.query(querySearch, dataSearch, function(err1, result1) {
              con.release();
              if (err1) {
                res.json({status: 0, message: 'API Failed'});
                throw err1;
              }
              else {
                res.json({status: 1, message: 'Tulis Log Berhasil'});
              }
            })
          }
        });
      });
    }
    else {
      res.json({status: false, message: "You Can't Access This Service"});
    }
  };

  this.getCount = function(res) {
    connection.acquire(function(err, con) {
      con.query('select * from ta_log_jawaban_count', function(err, result) {
        con.release();
        if(err) {
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          res.json(result);
        }
      });
    });
  };

  this.getApprovalCount = function(res) {
    connection.acquire(function(err, con) {
      var query = "select approval, count(*) as count from ta_log where nim = ? and approval = 1 union all  select approval, count(*) as count from ta_log where nim = ? and approval = 0";
      var data = [_.decoded.nim, _.decoded.nim];
      con.query(query, data, function(err, result) {
        con.release();
        if(err) {
          res.json({status: 0, message: 'API Failed'});
          throw err;
        }
        else {
          res.json(result);
        }
      });

    });
  };

  this.create = function(header, req, res){
    connection.acquire(function(err, con){
      var query = 'INSERT INTO ta_log (nim, dosen_1, dosen_2, tanggal, jam, tempat, topik, progress, kendala, rencana) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
      var data = [_.decoded.nim, req.dosen_1, req.dosen_2, req.tanggal, req.jam, req.tempat, req.topik, req.progress, req.kendala, req.rencana];
      con.query(query, data, function(err, result) {
        if (err) {
          con.release();
          res.json({status: 0, message: 'Tulis Log Gagal'});
          throw err;
        }
        else {
          var querySearch = 'SELECT (select d.username from ta as t, dosen as d where nim = ? and t.dosen_1 = d.id) as dosen1_username, (select d.email from ta as t, dosen as d where nim = ? and t.dosen_1 = d.id) as dosen1_email, (select d.username from ta as t, dosen as d where nim = ? and t.dosen_2 = d.id) as dosen2_username, (select d.email from ta as t, dosen as d where nim = ? and t.dosen_2 = d.id) as dosen2_email, (select t.topik from ta as t, dosen as d where nim = ? and t.dosen_2 = d.id) as topik';
          var dataSearch = [_.decoded.nim, _.decoded.nim, _.decoded.nim, _.decoded.nim, _.decoded.nim];
          con.query(querySearch, dataSearch, function(err1, result1) {
            if (err1) {
              con.release();
              res.json({status: 0, message: 'API Failed'});
              throw err1;
            }
            else {
              con.query('SELECT id, dosen_1, dosen_2 FROM ta_log WHERE nim = ? order by id desc limit 1', [_.decoded.nim], function(err, result2) {
                var idLog = result2[0].id;
                var dosen_1 = result2[0].dosen_1;
                var header1 = generateJWT.create(2, dosen_1);
                con.release();
                if (err) {
                  res.json({status: 0, message: 'API Failed'});
                  throw err;
                }
                else {
                  mail.mailLog(header1, idLog, result1[0].dosen1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result1[0].topik, req.tanggal, req.tempat, req.jam, req.topik, req.progress, req.kendala, req.rencana);
                  if(result1[0].dosen2_username) {
                    mail.mailLog(header1, idLog, result1[0].dosen2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result1[0].topik, req.tanggal, req.tempat, req.jam, req.topik, req.progress, req.kendala, req.rencana);
                  }
                  mail.mailLog(header1, idLog, result1[0].dosen1_email, _.decoded.nim, _.decoded.nama, result1[0].topik, req.tanggal, req.tempat, req.jam, req.topik, req.progress, req.kendala, req.rencana);
                  if(result1[0].dosen2_email) {
                    mail.mailLog(header1, idLog, result1[0].dosen2_email, _.decoded.nim, _.decoded.nama, result1[0].topik, req.tanggal, req.tempat, req.jam, req.topik, req.progress, req.kendala, req.rencana);
                  }
                  res.json({status: 1, message: 'Tulis Log Berhasil'});
                }
              })
            }
          })
        };
      });
    });
  };

  this.balasMahasiswa = function(req, res){
    connection.acquire(function(err, con){
      var query = 'INSERT INTO ta_log_jawaban (topikId, nim, jawaban) VALUES (?, ?, ?)';
      var data = [req.topikId, req.nim, req.jawaban];
      con.query(query, data, function(err, result) {
        con.release();
        if (err) {
          res.json({status: 0, message: 'Balas Log Gagal'});
          throw err;
        } else {
          res.json({status: 1, message: 'Balas Log Berhasil'});
        };
      });

    });
  };

  this.balasDosen = function(req, res){
    connection.acquire(function(err, con){
      if (_.decoded.role == 4 || _.decoded.role == 2) {
        var query = 'INSERT INTO ta_log_jawaban (topikId, dosen_id, jawaban) VALUES (?, ?, ?)';
        var data = [req.topikId, _.decoded.id, req.jawaban];
        con.query(query, data, function(err, result) {
          con.release();
          if (err) {
            res.json({status: 0, message: 'Balas Log Gagal'});
            throw err;
          } else {
            res.json({status: 1, message: 'Balas Log Berhasil'});
          };
        });
      }
      else {
        con.release();
        res.json({status: false, message: "You Can't Access This Service"});
      }
    });
  };

}

module.exports = new Log();

// imas.sitanggang@gmail.com
// muhammad.abrari@gmail.com
