var connection = require('../connection');
var _ = require('../simak');

function Status() {

  this.get = function(res){
    connection.acquire(function(err, con){

      var query = `
      SELECT
        ( SELECT status
            FROM mahasiswa_detail
            WHERE nim =  ?) AS statusProfile,

        ( SELECT EXISTS (
            SELECT *
            FROM ta_daftar
            WHERE nim =  ?)
          ) AS statusDaftar,

        ( SELECT EXISTS (
            SELECT *
            FROM ta
            WHERE nim =  ?)
          ) AS statusTa,

        ( SELECT EXISTS (
            SELECT *
            FROM ta_kolokium
            WHERE nim =  ?)
          ) AS statusKolokium,

        ( SELECT EXISTS (
            SELECT *
            FROM ta_praseminar
            WHERE nim =  ?)
          ) AS statusPrasminar,

        ( SELECT EXISTS (
            SELECT *
            FROM ta_seminar
            WHERE nim =  ?)
          ) AS statusSeminar,

        ( SELECT EXISTS (
            SELECT *
            FROM ta_sidang
            WHERE nim =  ?)
          ) AS statusSidang,

        ( SELECT EXISTS (
            SELECT *
            FROM ta_skl
            WHERE nim =  ?)
          ) AS statusSkl
      `;
      var data = [_.decoded.nim, _.decoded.nim, _.decoded.nim, _.decoded.nim, _.decoded.nim, _.decoded.nim, _.decoded.nim, _.decoded.nim];
      con.query(query, data, function(err, result) {
        con.release();
        if (err) {
          res.json({status: 0, message: 'Query Error'});
          throw err;
        } else {
          res.json(result);
        };
      });

    });
  };

}

module.exports = new Status();
