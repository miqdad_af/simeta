var soap = require('soap');
var connection = require('../connection');
var fc = require('../fc');

var dataMahasiswa = [];
var url = 'https://iisws.ipb.ac.id/simak/deptservice.asmx?wsdl';

function test(nim) {
    for(let i = 0; i < dataMahasiswa.length; i++) {
        if(nim == dataMahasiswa[i].nim) return false;
    }
    return true;
}

function TestSoap(){

    this.test = function(res) {
        if(fc.checkAdmin()) {
            connection.acquire(function(err, con) {
                con.query('SELECT nim FROM mahasiswa_2', function(err, result) {
                    if(err) {
                        console.log(err);
                        throw err;
                    } else {
                        dataMahasiswa = result;

                        soap.createClient(url, function(err, client) {
                            if(err) {
                                console.log(err);
                                throw err;
                            }
                            client.DeptService.DeptServiceSoap.ListMahasiswa({key:'272c214e2a7e46646840765e51'}, function(err, response){
                                if(err) {
                                    con.release();
                                    console.log(err);
                                    throw err;
                                } else {

                                    let dataRaw = response.ListMahasiswaResult.DataMahasiswa;
                                    let dataClean = [];
                                    for(let i = 0; i < dataRaw.length; i++) {
                                        if(dataRaw[i].tahunmasuk >= 2012) {
                                            dataClean.push(dataRaw[i]);
                                        }
                                    }

                                    let resultData = [];
                                    let temp = [];

                                    for(let i = 0; i < dataClean.length; i++) {
                                        if(test(dataClean[i].nim)) {
                                            temp.push(dataClean[i].nim);
                                            temp.push(dataClean[i].nama);
                                            temp.push(dataClean[i].mayor);
                                            temp.push(dataClean[i].minor);
                                            temp.push(dataClean[i].tahunmasuk);
                                            temp.push(dataClean[i].angkatan);
                                            temp.push(dataClean[i].tempatlahir);
                                            temp.push(dataClean[i].tgllahir);
                                            temp.push(dataClean[i].jeniskelamin);
                                            temp.push(dataClean[i].agama);
                                            temp.push(dataClean[i].statuskawin);
                                            temp.push(dataClean[i].alamat);
                                            temp.push(dataClean[i].kodepos);
                                            temp.push(dataClean[i].tlpbogor);
                                            temp.push(dataClean[i].hp);
                                            temp.push(dataClean[i].namaayah);
                                            temp.push(dataClean[i].namaibu);
                                            temp.push(dataClean[i].alamatortu);
                                            temp.push(dataClean[i].kotaortu);
                                            temp.push(dataClean[i].noortu);
                                            temp.push(dataClean[i].telportu);
                                            temp.push(dataClean[i].email);

                                            resultData.push(temp);
                                            temp = [];
                                        }
                                    }

                                    if(resultData.length > 0) {
                                        var query = 'INSERT INTO mahasiswa_2 (nim, nama, mayor, minor, tahunmasuk, angkatan, tempatlahir, tgllahir, jeniskelamin, agama, statuskawin, alamat, kodepos, tlpbogor, hp, namaayah, namaibu, alamatortu, kotaortu, noortu, telportu, email) VALUES ?'
                                        con.query(query, [resultData], function(err1, result1) {
                                            con.release();
                                            if(err1) {
                                                res.json({status: false, message: 'query Failed'});
                                                throw err1;
                                            } else {
                                                let date = new Date();
                                                res.json({status: true, message: 'query success', total: resultData.length, date: date});
                                            }
                                        })
                                    } else {
                                        con.release();
                                        res.json({status: true, message: 'Nothing Happened'});
                                    }

                                }
                            });
                        });
                    }
                })
            })
        } else {
            res.json({status: false, message: "You Don't Have Authorized Access"});
        }
    }
}

module.exports = new TestSoap();