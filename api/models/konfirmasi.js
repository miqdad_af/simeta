var connection = require('../connection');
var _ = require('../simak');
var fc = require('../fc');

var noAuth = {status: false, message: "You Can't Access This Service"};

function Test() {

  this.konfirmasiKolokium = function(todo, res) {
    if(fc.checkAdminDosen()) {
      connection.acquire(function(err, con) {
        var query = 'UPDATE ta_kolokium SET konfirmasi = ? WHERE nim = ?';
        var data = [todo.data, todo.nim];
        con.query(query, data, function(err, result) {
          con.release()
          if(err) {
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            res.json({status: 1, message: 'Konfirmasi Kolokium Berhasil'});
          }
        })

      })
    }
    else {
      res.json(noAuth);
    }
  }

  // SIMETA v2
  this.konfirmasiSeminar  = function(todo, res) {

  }

  this.konfirmasiPraseminar = function(todo, res) {
    if(fc.checkAdminDosen()) {
      connection.acquire(function(err, con) {
        var query = 'UPDATE ta_praseminar SET konfirmasi = ? WHERE nim = ?';
        var data = [todo.data, todo.nim];
        con.query(query, data, function(err, result) {
          con.release()
          if(err) {
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            res.json({status: 1, message: 'Konfirmasi Kolokium Berhasil'});
          }
        })

      })
    }
    else {
      res.json(noAuth);
    }
  }

  this.get = function(res) {
    res.json({status: 1, message: 'Connection Established'});
  };

}

module.exports = new Test();
