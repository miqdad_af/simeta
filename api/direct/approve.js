var connection = require('../connection');
var jwt = require('jsonwebtoken');
var generateJwt = require('../direct/generateJWT');
var mail = require('../mail');
var mandiri = require('../models/mandiri');

function DirectApprove(){

  this.get = function(id, token, res) {

    if (!token){
      res.json({status: false, message: 'No token provided'})
    }
    else {
      jwt.verify(token, 'yangPentingPanjang', function(err, decoded) {
        if (err){
          res.json({status: false, message: 'Token authorization failed'});
          throw err;
        }
        else {
          var decoded = jwt.verify(token, 'yangPentingPanjang');
          var role = decoded.role;

          if (role == 4 || role == 2) {
            connection.acquire(function(err, con) {

              var query = 'SELECT dosen_1, dosen_2, id FROM ta_log WHERE (dosen_1 = ? OR dosen_2 = ?) AND id = ?';
              var data = [decoded.id, decoded.id, id];
              con.query(query, data, function(err, result) {
                if(err) {
                  con.release();
                  res.json({status: 0, message: 'API Failed'});
                  throw err;
                }
                else {
                  if(result.length > 0) {
                    var query = 'UPDATE ta_log SET approval = 1 WHERE id = ?';
                    var data = [id];
                    con.query(query, data, function(err, result) {
                      con.release();
                      if(err) {
                        res.json({status: 0, message: 'API Failed'});
                        throw err;
                      }
                      else {
                        res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                      }
                    });
                  }
                  else {
                    res.json({status: 0, message: "You Can't Access This Service"});
                  }
                }
              })
            });
          }
          else {
            res.json({status: false, message: "You Can't Access This Service"});
          }
        }
      });
    }
  };

  // SIMETA v2
  // MODUL PENGAJUAN PEMBIMBING KEDUA
  this.setAcceptedPertama = function(id, token, res){
    if (!token) res.json({status: 1, code: 401, message: 'No token provided.'});
    else {
      jwt.verify(token, 'yangPentingPanjang', (err, decoded) => {
        if (err){
          res.json({status: 0, code: 500, message: 'Token authorization failed'});
          throw err;
        } else {
            const decoded = jwt.verify(token, 'yangPentingPanjang');
            const role = decoded.role;
            if (role === 2 || role === 4) {
              connection.acquire((err, con) => {
                const creds = {
                  select:{
                    satu: {
                      query: `
                        SELECT
                          d.nama,
                          d.nip,
                          d.email,
                          d.lab,
                          d.keahlian,
                          d.no_hp,
                          d.lab,
                          d.alamat_instansi,
                          d.keahlian,
                          d.nama_instansi
                        FROM
                          dosen AS d, ta_dosbing_kedua AS t
                        WHERE
                          d.id IN (SELECT dosen_2 FROM ta_dosbing_kedua WHERE id = ?)
                          AND t.status_dp2 = 2
                      `,
                      data: [id]
                    },
                    dua: {
                      query: `
                        SELECT
                          d.nama
                        FROM
                          dosen AS d, ta_dosbing_kedua AS t
                        WHERE
                          d.id IN (SELECT dosen_1 FROM ta_dosbing_kedua WHERE id = ?)
                          AND t.status_dp2 = 2
                      `,
                      data: [id]
                    },
                    tiga: {
                      query: `
                        SELECT DISTINCT
                          s.id,
                          s.status_req,
                          s.status_dp2,
                          m.nama,
                          m.nim,
                          m.topik
                        FROM 
                          dosen_mahasiswa_view AS m
                        INNER JOIN
                          ta_dosbing_kedua AS s ON s.nim = m.nim
                        WHERE 
                          m.nim IN (SELECT nim FROM ta_dosbing_kedua WHERE id = ?)
                          AND s.status_dp2 = 2
                        ORDER BY
                          s.updated_at DESC
                      `,
                      data: [id]
                    },
                  },
                  update: {
                    query: `
                      UPDATE 
                        ta_dosbing_kedua 
                      SET 
                        status_dp2 = ? 
                      WHERE
                        (status_dp2 = ? AND dosen_1 = ?) 
                      AND 
                        id = ?
                    `,
                    data: [2, 1, decoded.id, id]
                  }
                }
                con.query(creds.update.query, creds.update.data, (err1, res1) => {
                  if (err1) {
                    res.json({status: 0, code: 500 , message: 'Internal server error (1). Please contact the developer for further informations.'});
                    throw err1;
                  } else {
                      con.query(creds.select.satu.query, creds.select.satu.data, (err2, res2) => {
                          if (err2) {
                            res.json({status: 0, code: 500, message: 'Internal server error (2). Please contact the developer for further informations.'});
                            throw err2;
                          } else {
                            con.query(creds.select.dua.query, creds.select.dua.data, (err3, res3) => {
                              if (err3) {
                                res.json({status: 0, code: 500, message: 'Internal server error (3). Please contact the developer for further informations.'});
                                throw err3;
                              } else {
                                con.query(creds.select.tiga.query, creds.select.tiga.data, (err4, res4) => {
                                  if (err4) {
                                    con.release();
                                    res.json({status: 0, code: 500, message: 'Internal server error (4). Please contact the developer for further informations.'})
                                    throw err4;
                                  } else {
                                    const credsMhs = {
                                      id,
                                      topik: res4[0].topik,
                                      namaMahasiswa: res4[0].nama,
                                      nimMahasiswa: res4[0].nim,
                                      dosen1: res3[0].nama,
                                      dosen2: res2[0].nama,
                                      nip: res2[0].nip,
                                      email: res2[0].email,
                                      lab: res2[0].lab,
                                      keahlian: res2[0].keahlian,
                                      nomorTelp: res2[0].no_hp,
                                      lab: res2[0].lab,
                                      alamatInstansi: res2[0].alamat_instansi,
                                      namaInstansi: res2[0].nama_instansi,
                                      statusReq: res4[0].status_req,
                                      statusDp: res4[0].status_dp2
                                    }
                                    mail.setDosenKeduaMail(res, credsMhs, token);
                                    mail.setMahasiswaAccepted(res, credsMhs); 
                                    res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                                    con.release();
                                  }
                                });
                              }
                            }); 
                          }
                      });
                    }
                });
              });
          } else {
              res.json({status: 0, code: 403, message: "You Can't Access This Service"});
          }
        }
      });
    } 
  }

  this.setAcceptedKedua = function(id, token, res) {
    if (!token) res.json({status: 0, code: 401, message: 'No token provided'});
    else {
      jwt.verify(token, 'yangPentingPanjang', (err, decoded) => {
        if (err) {
          res.json({status: 0, code: 500, message: 'Token authorization failed'});
          throw err;
        } else {
          const decoded = jwt.verify(token, 'yangPentingPanjang');
          const role = decoded.role;
          if (role === 2 || role === 4) {
            connection.acquire((err, con) => {
              if (err) {
                res.json({ status: 0, code: 500, message: 'Internal server error(0). Please contact the develoeper for forther informations.'});
                throw err;
              } else {
                  const creds = {
                    select:{
                      satu: {
                        query: `
                          SELECT
                            d.nama,
                            d.nip,
                            d.email,
                            d.lab,
                            d.keahlian,
                            d.no_hp,
                            d.lab,
                            d.alamat_instansi,
                            d.nama_instansi
                          FROM
                            dosen AS d, ta_dosbing_kedua AS t
                          WHERE
                            d.id IN (SELECT dosen_2 FROM ta_dosbing_kedua WHERE id = ?)
                            AND t.status_dp2 = 3
                        `,
                        data: [id]
                      },
                      dua: {
                        query: `
                          SELECT
                            d.nama
                          FROM
                            dosen AS d, ta_dosbing_kedua AS t
                          WHERE
                            d.id IN (SELECT dosen_1 FROM ta_dosbing_kedua WHERE id = ?)
                            AND t.status_dp2 = 3
                        `,
                        data: [id]
                      },
                      tiga: {
                        query: `
                          SELECT DISTINCT
                            s.id,
                            s.status_req,
                            s.status_dp2,
                            m.nama,
                            m.nim,
                            m.topik
                          FROM 
                            dosen_mahasiswa_view AS m
                          INNER JOIN
                            ta_dosbing_kedua AS s ON s.nim = m.nim
                          WHERE 
                            m.nim IN (SELECT nim FROM ta_dosbing_kedua WHERE id = ?)
                            AND s.status_dp2 = 3
                          ORDER BY
                            s.updated_at DESC
                        `,
                        data: [id]
                      },
                      empat: {
                        query: `
                            SELECT
                                nama,
                                email
                            FROM
                                dosen
                            WHERE
                                id = ?
                        `,
                        data: [id]
                      }
                    },
                    update: {
                      query: `
                        UPDATE 
                          ta_dosbing_kedua 
                        SET 
                          status_dp2 = ? 
                        WHERE
                          status_dp2 = ?
                        AND 
                          id = ?
                      `,
                      data: [3, 2, id]
                    }
                  };

                  con.query(creds.update.query, creds.update.data, (err1, res1) => {
                    if (err1) {
                      res.json({status: 0, code: 500, message: 'Internal server error (1). Please contact the developer for further informations.'});
                      throw err1;
                    } else {
                      con.query(creds.select.satu.query, creds.select.satu.data, (err2, res2) => {
                        if (err2) {
                          res.json({status: 0, code: 500, message: 'Internal server error (2). Please contact the developer for further informations.'});
                          throw err2;
                        } else {
                          con.query(creds.select.dua.query, creds.select.dua.data, (err3, res3) => {
                            if (err3) {
                              res.json({status: 0, code: 500, message: 'Internal server error (3). Please contact the developer for further informations.'});
                              throw err3;
                            } else {
                              console.log(decoded);
                              con.query(creds.select.tiga.query, creds.select.tiga.data, (err4, res4) => {
                                con.release();
                                if (err4) {
                                  res.json({status: 0, code: 500, message: 'Internal server error (4). Please contact the developer for further informations.'});
                                  throw err4;
                                } else {
                                  const credsMhs = {
                                    id,
                                    topik: res4[0].topik,
                                    namaMahasiswa: res4[0].nama,
                                    nimMahasiswa: res4[0].nim,
                                    dosen1: res3[0].nama,
                                    dosen2: res2[0].nama,
                                    nip: res2[0].nip,
                                    email: res2[0].email,
                                    lab: res2[0].lab,
                                    keahlian: res2[0].keahlian,
                                    nomorTelp: res2[0].no_hp,
                                    lab: res2[0].lab,
                                    alamatInstansi: res2[0].alamat_instansi,
                                    namaInstansi: res2[0].nama_instansi,
                                    statusReq: res4[0].status_req,
                                    statusDp: res4[0].status_dp2
                                  };
                                  console.log(credsMhs);
                                  switch(credsMhs.lab) {
                                    case '1':
                                        creds.select.empat.data = [12];
                                    case '2':
                                        creds.select.empat.data = [5];
                                    case '3':
                                        creds.select.empat.data = [27];
                                  };
                                  con.query(creds.select.empat.query, creds.select.empat.data, (err5, res5) => {
                                    if (err5) {
                                      res.json({status: 0, code: 500, message: 'Internal server error (0)'});
                                      throw err5;
                                    } else {
                                      const to = res5[0].email;
                                      mail.setKepalaBagian(res, to, credsMhs, token, id);
                                      mail.setMahasiswaAccepted(res, credsMhs);
                                      res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                                    }
                                  }); 
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
              }
            });
          }
        }
      });
    }
  }

  this.setAcceptedKepalaBagian = function(id, idDosen, token, res) {
    if (!token) res.json({status: 0, code: 401, message: 'No token provided.'});
    else {
      jwt.verify(token, 'yangPentingPanjang', (error, decoded) => {
        if (error) {
          res.json({status: 0, code: 500, message: 'Token authorization failed'});
          throw err;
        } else {
          const decoded = jwt.verify(token, 'yangPentingPanjang');
          const role = decoded.role;
          if (role === 2 || role === 4) {
            connection.acquire((err, con) => {
              if (err) {
                res.json({status: 0, code: 500, message: 'Internal server error (1). Please contact the developer for further informations.'});
                throw err;
              } else {
                const creds = {
                  select: {
                    satu: {
                      query: `
                        SELECT
                          nim
                        FROM 
                          ta_dosbing_kedua
                        WHERE
                          id = ?
                      `,
                      data: [id]
                    },
                    dua: {
                      query: `
                        SELECT
                          d.nama,
                          d.nip,
                          d.email,
                          d.lab,
                          d.no_hp,
                          d.lab,
                          d.keahlian,
                          d.alamat_instansi,
                          d.nama_instansi
                        FROM
                          dosen AS d, ta_dosbing_kedua AS t
                        WHERE
                          d.id IN (SELECT dosen_2 FROM ta_dosbing_kedua WHERE id = ?)
                          AND t.status_dp2 = 4
                      `,
                      data: [id]
                    },
                    tiga: {
                      query: `
                        SELECT
                          d.nama
                        FROM
                          dosen AS d, ta_dosbing_kedua AS t
                        WHERE
                          d.id IN (SELECT dosen_1 FROM ta_dosbing_kedua WHERE id = ?)
                          AND t.status_dp2 = 4
                      `,
                      data: [id]
                    },
                    empat: {
                      query: `
                        SELECT DISTINCT
                          s.id,
                          s.status_req,
                          s.status_dp2,
                          m.nama,
                          m.nim,
                          m.topik
                        FROM 
                          dosen_mahasiswa_view AS m
                        INNER JOIN
                          ta_dosbing_kedua AS s ON s.nim = m.nim
                        WHERE 
                          m.nim IN (SELECT nim FROM ta_dosbing_kedua WHERE id = ?)
                          AND s.status_dp2 = 4
                        ORDER BY
                          s.updated_at DESC
                      `,
                      data: [id]
                    }
                  },
                  update: {
                    query: `
                      UPDATE
                        ta_dosbing_kedua AS t
                      SET
                        t.status_dp2 = ?
                      WHERE
                        t.id = ? 
                    `,
                    data: [4, id]
                  }
                }
                con.query(creds.select.satu.query, creds.select.satu.data, (err1, res1) => {
                  if (err1) {
                    con.release();
                    res.json({status: 0, code: 500, message: 'Internal server error (1). Please contact the developer for further informations.'});
                    throw err1;
                  } else {
                    con.query(creds.update.query, creds.update.data, (err2, res2) => {
                      if (err2) {
                        con.release();
                        res.json({status: 0, code: 500, message: 'Internal server error (2). Please contact the developer for further informations.'});
                        throw err2;
                      } else {
                        con.query(creds.select.dua.query, creds.select.dua.data, (err3, res3) => {
                          if (err3) {
                            res.json({status: 0, code: 500, message: 'Internal server error (3). Please contact the developer for further informations.'});
                            throw err3;
                          } else {
                            con.query(creds.select.tiga.query, creds.select.tiga.data, (err4, res4) => {
                              if (err3) {
                                res.json({status: 0, code: 500, message: 'Internal server error (4). Please contact the developer for further informations.'});
                                throw err4;
                              } else {
                                con.query(creds.select.empat.query, creds.select.empat.data, (err5, res5) => {
                                  if (err4) {
                                    con.release();
                                    res.json({status: 0, code: 500, message: 'Internal server error (5). Please contact the developer for further informations.'});
                                    throw err5;
                                  } else {
                                      const credsMhs = {
                                        id,
                                        topik: res5[0].topik,
                                        namaMahasiswa: res5[0].nama,
                                        nim: res5[0].nim,
                                        dosen1: res4[0].nama,
                                        dosen2: res3[0].nama,
                                        nip: res3[0].nip,
                                        email: res3[0].email,
                                        lab: res3[0].lab,
                                        keahlian: res3[0].keahlian,
                                        no_hp: res3[0].no_hp,
                                        lab: res3[0].lab,
                                        alamatInstansi: res3[0].alamat_instansi,
                                        namaInstansi: res3[0].nama_instansi,
                                        statusReq: res5[0].status_req,
                                        statusDp: res5[0].status_dp2
                                      };
                                      console.log(credsMhs);
                                      mail.setMahasiswaAccepted(res, credsMhs);
                                      res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                                      con.release();
                                  }
                                });
                              }
                            });
                          }
                        });
                      }
                    });    
                  }
                })
              }
            });
          } else {
            res.json({status: 0, code: 403, message: 'Forbidden'});
          }
        }
      });
    }
  }

  this.setRejected = function(id, token, res) {
    if (!token) res.json({status: 0, message: 'No token provided.'});
    else {
      jwt.verify(token, 'yangPentingPanjang', (err, decoded) => {
        if (err){
          res.json({status: false, message: 'Token authorization failed'});
          throw err;
        } else {
            const decoded = jwt.verify(token, 'yangPentingPanjang');
            const role = decoded.role;
            if (role === 2 || role === 4 || role === 1) {
              connection.acquire((err, con) => {
                const queryNIM = `SELECT nim, status_req FROM ta_dosbing_kedua WHERE id = ?`
                con.query(queryNIM, [id], (err1, res1) => {
                  if (err1) {
                    res.json({status: 0, code: 500 , message: 'Internal server error (1) '});
                    throw err1;
                  } else {
                    const nim = res1[0].nim;
                    const statusReq = res1[0].status_req;
                    const creds = {
                      select: {
                          query: `
                              SELECT
                                  m.nama,
                                  t.dosen_2 AS t_dosen2,
                                  ta.dosen_2 AS ta_dosen2,
                                  t.status_dp2
                              FROM
                                  ta_dosbing_kedua AS t, ta, mahasiswa_detail AS m
                              WHERE
                                  t.nim = ? AND ta.nim = ? AND m.nim = ?
                              `,
                          data: [nim, nim, nim]
                      },
                      update: {
                          query: `
                              UPDATE 
                                  ta_dosbing_kedua AS td,
                                  ta
                              SET
                                  td.dosen_2 = '0',
                                  td.status_dp2 = null,
                                  ta.dosen_2 = '0',
                                  td.status_req = null
                              WHERE
                                  td.nim = ?
                                  AND ta.nim =?
                              `,
                          data: [nim, nim]
                      },
                      delete: {
                          query: `
                              DELETE FROM
                                  dosen
                              WHERE
                                  id IN (SELECT dosen_2 FROM ta_dosbing_kedua WHERE nim = ?)
                              `,
                          data: [nim]
                      }
                    };
                    con.query(creds.select.query, creds.select.data, (err2, res2) => {
                      if (err2) {
                        con.release();
                        res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                        throw err2;
                      } else {
                        const credsMhs = {
                          nama: res2[0].nama,
                          dosen2: {
                            t: res2[0].t_dosen2,
                            ta: res2[0].ta_dosen2
                          },
                          statusDp: res2[0].status_dp2,
                          alasan: null
                        };
                        console.log(credsMhs);
                        mail.setMahasiswaRejected(res, credsMhs);
                        if (credsMhs.dosen2.t !== 0 && credsMhs.dosen2.ta !== 0) {
                          if (statusReq === 1) {
                            console.log('masuk kondisi pertama')
                            con.query(creds.update.query, creds.update.data, (err3, res3) => {
                              if (err3) {
                                con.release();
                                res.json({status: 0, code: 500, message: 'Gagal update data, please contact the developer [Internal server error(1)]'});
                                throw err3;
                              } else {
                                const data = {};    
                                res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                                con.release();
                              }
                            });
                          } else if (statusReq === 2) {
                            console.log('masuk kondisi kedua');
                            con.query(creds.delete.query, creds.delete.data, (err3, res3) => {
                              if (err3) {
                                con.release();
                                res.json({status: 0, code: 500, message: 'Gagal menghapus data, please contact the developer [Internal server error(1)]'});
                                throw err3;
                              } else {
                                  con.query(creds.update.query, creds.update.data, (err4, res4) => {
                                    if (err4) {
                                      con.release();
                                      res.json({status: 0, code: 500, message: 'Gagal update data, please contact the developer [Internal server error(2)]'});
                                      throw err4;
                                    } else {
                                      res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                                      con.release();
                                    }
                                  });
                              }
                            });
                          } else {
                              res.json({status: 0, code: 204, message: 'Konten tidak ditemukan'});
                          }
                        } else if (credsMhs.dosen2.t === 0 && credsMhs.dosen2.ta === 0){
                            res.json({status: 1, code: 200, message: 'Mahasiswa terkait tidak memiliki pembimbing kedua'});
                        } else {
                            res.json({status: 0, code: 500, message: 'Gagal menghapus data, please contact the developer [Internal server error(3)]'});
                        }
                      }
                    });
                  }
                });
              })
            } else {
                res.json({status: 0, code: 403, message: "You cannot access this service"});
            }
        }
      });
    }
  }

  // MODUL KONFIRMASI PELAKSANAAN SEMINAR MANDIRI
  // Method untuk konfirmasi pembimbing ketua/pertama
  this.setAcceptedPertamaMandiri = function(nim, token, res) {
    if (!token) res.json({status: 0, code: 401, message: 'No token provided'});
    else {
      const decoded = jwt.verify(token, 'yangPentingPanjang');
      const role = decoded.role;
      if (role === 2 || role === 4) {
        connection.acquire((err, con) => {
          if (err) {
            res.json({status: 0, code: 500, message: 'Internal server error (0)'});
            throw err;
          } else {
            const creds = {
              update: {
                query: `
                  UPDATE
                    ta_seminar
                  SET
                    konfirmasi_pembimbing1 = 1
                  WHERE
                    nim = ?
                `,
                data: [nim]
              },
              select: {
                query: {
                  pertama: `
                    SELECT
                      m.nama,
                      m.nim,
                      m.tanggal,
                      m.topik,
                      m.dosen1,
                      m.dosen2,
                      m.penguji1,
                      m.penguji2,
                      p.tempat,
                      p.jam,
                      p.pembahas1,
                      p.pembahas2,
                      p.pembahas3
                    FROM 
                      mandiri_view AS m
                    INNER JOIN
                      pembahas_view AS p ON m.nim = p.nim
                    WHERE
                      m.nim = ? 
                  `,
                  kedua: `
                    SELECT
                      d.dosen1,
                      dosen2,
                      penguji1,
                      penguji2,
                      dosen1_email,
                      dosen2_email,
                      penguji1_email,
                      penguji2_email
                    FROM
                      dosen_mahasiswa_view AS d
                    WHERE
                      nim = ?
                  `,
                },
                data: [nim] 
              }
            };
            con.query(creds.select.query.pertama, creds.select.data, (err1, res1) => {
              if (err1) {
                res.json({status: 0, code: 500, message: 'Internal server error (1)'});
                throw err1;
              } else {
                con.query(creds.select.query.kedua, creds.select.data, (err2, res2) => {
                  if (err2) {
                    res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                    throw err2;
                  } else {
                    con.query(creds.update.query, creds.update.data, (err3, res3) => {
                      if (err3) {
                        res.json({status: 0, code: 500, message: 'Internal server error (3)'});
                        throw err3;
                      } else {
                        const credsMhs = {
                          nama: res1[0].nama,
                          nim: res1[0].nim,
                          topik: res1[0].topik,
                          tempat: res1[0].tempat,
                          jam: res1[0].jam,
                          tanggal: res1[0].tanggal,
                          namaDosen1: res1[0].dosen1,
                          namaDosen2: res2[0].dosen2,
                          namaPenguji1: res2[0].penguji1,
                          namaPenguji2: res2[0].penguji2,
                          email: [
                            res2[0].dosen2_email,
                            res2[0].penguji1_email,
                            res2[0].penguji2_email,
                          ],
                          role: 2
                        };
                        const token = generateJwt.mailToken(credsMhs);
                        mail.setMandiri(res, credsMhs, token);
                        res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    }
  }

  // Method untuk konfirmasi SELAIN pembimbing ketua/pertama
  this.setAcceptedMandiri = function(nim, token, email, res) {
    if (!token) res.json({status: 0, code: 401, message: 'No token provided'});
    else {  
      const decoded = jwt.verify(token, 'yangPentingPanjang');
      const role = decoded.role;
      if (role === 2 || role === 4) {
        connection.acquire((err, con) => {
          if (err) {
            con.release();
            res.json({status: 0, code: 500, message: 'Internal server error (0)'});
            throw err;
          } else {
            let statusDosen;
            let creds;
            const querySelect = 'SELECT dosen1_email, dosen2_email, penguji1_email, penguji2_email FROM dosen_mahasiswa_view WHERE nim = ?';
            const dataSelect = [nim]
            con.query(querySelect, dataSelect, (err1, res1) => {
              if (err1) {
                res.json({status: 0, code: 500, message: 'Internal server error (1)'});
                throw err1;
              } else {
                // Membuat status dosen dengan mencocokkan masing2 email
                if (email.email) {
                  if (res1[0].dosen1_email === email.email) {
                    statusDosen = 'Dosen pembimbing ketua';
                  } else if (res1[0].dosen2_email === email.email) {
                    statusDosen = 'Dosen pembimbing anggota';
                  } else if (res1[0].penguji1_email === email.email) {
                    statusDosen = 'Penguji pertama';
                  } else if (res1[0].penguji2_email === email.email) {
                    statusDosen = 'Penguji kedua';
                  }
                }
                switch(statusDosen) {
                  case 'Dosen pembimbing anggota':
                    creds = {
                      update: {
                        query: `
                          UPDATE
                            ta_seminar
                          SET
                            konfirmasi_pembimbing2 = 1
                          WHERE
                            nim = ?
                        `,
                        data: [nim]
                      }
                    };
                    break;
                  case 'Penguji pertama':
                    creds = {
                      update: {
                        query: `
                          UPDATE
                            ta_seminar
                          SET
                            konfirmasi_penguji1 = 1
                          WHERE
                            nim = ?
                        `,
                        data: [nim]
                      }
                    }
                    break;
                  case 'Penguji kedua':
                    creds = {
                      update: {
                        query: `
                          UPDATE
                            ta_seminar
                          SET
                            konfirmasi_penguji2 = 1
                          WHERE
                            nim = ?
                        `,
                        data: [nim]
                      }
                    }
                    break;
                  default:
                    res.json({status: 0, code: 400, message: 'Bad request'});
                    break;
                };
                creds.select = {
                  query: `
                    SELECT
                      konfirmasi_pembimbing1
                    FROM 
                      ta_seminar
                    WHERE
                      nim = ?
                  `,
                  data: [nim]
                }
                const querySelectSyarat = `
                  SELECT
                    tas.konfirmasi_pembimbing1,
                    tas.konfirmasi_pembimbing2,
                    tas.konfirmasi_penguji1,
                    tas.konfirmasi_penguji2,
                    m.email,
                    m.nama
                  FROM
                    ta_seminar AS tas
                  INNER JOIN
                    mahasiswa_detail AS m ON tas.nim = m.nim
                  WHERE
                    tas.nim = ? AND m.nim = ?  
                `;
                const queryUpdateSyarat = `
                  UPDATE
                    ta_mandiri
                  SET
                    status_formulir = 2
                  WHERE
                    nim = ?
                `;
                const dataSelectSyarat = [nim, nim]; 
                const dataUpdateSyarat = [nim];
                con.query(creds.select.query, creds.select.data, (err2, res2) => {
                  if (err2) {
                    con.release();
                    res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                    throw err2;
                  } else if (res2[0].konfirmasi_pembimbing1 != 1){
                    con.release();
                    res.json({status: 0, message: 'Pembimbing ketua menolak atau belum memberikan konfirmasi'});
                  } else {
                    con.query(creds.update.query, creds.update.data, (err3, res3) => {
                      if (err3) {
                        con.release();
                        res.json({status: 0, code: 500, message: 'Internal server error (3)'});
                        throw err3;
                      } else {
                        con.query(querySelectSyarat, dataSelectSyarat, (err4, res4) => {
                          if (err4) {
                            con.release();
                            res.json({status: 0, code: 500, message: 'Internal server error (4)'});
                            throw err4;
                          } else {
                            let 
                              konfirmasiPembimbing1 = res4[0].konfirmasi_pembimbing1, 
                              konfirmasiPembimbing2 = res4[0].konfirmasi_pembimbing2,
                              konfirmasiPenguji1 = res4[0].konfirmasi_penguji1,
                              konfirmasiPenguji2 = res4[0].konfirmasi_penguji2;
                            // Cek apakah sudah memenuhi syarat untuk bisa seminar.
                            if (
                              (konfirmasiPembimbing1 === 1 && konfirmasiPembimbing2 === 1 && konfirmasiPenguji1 === 1) ||
                              (konfirmasiPembimbing1 === 1 && konfirmasiPembimbing2 === 2 && konfirmasiPenguji1 === 1) ||
                              (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 1 && konfirmasiPenguji2 === 1) ||
                              (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 1 && konfirmasiPenguji2 === 2) ||
                              (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 2 && konfirmasiPenguji2 === 1)
                            ) {
                              con.query(queryUpdateSyarat, dataUpdateSyarat, (err5, res5) => {
                                con.release();
                                if (err5) {
                                  res.json({status: 0, code: 500, message: 'Internal server error (5)'});
                                  throw err5;
                                } else {
                                  const credsMhs = {
                                    nama: res4[0].nama,
                                    email: res4[0].email,
                                    statusDosen
                                  }
                                  mail.setMandiriSukses(res, credsMhs, 'pertama');
                                  res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                                }
                              });
                            } else {
                              con.release();
                              res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                            }
                          }
                        });
                      }
                    });
                  }
                });
              }
            }); 
          }  
        });
      }
    }
  };

  // Method penolakan dari pembimbing pertama
  this.declinePertamaMandiri = function(nim, token, res) {
    if (!token) res.json({status: 0, code: 401, message: 'No token provided'});
    else {
      const decoded = jwt.verify(token, 'yangPentingPanjang');
      const role = decoded.role;
      if (role === 2 || role === 4 ){
        connection.acquire((err, con) => {
          if (err) {
            con.release();
            res.json({status: 0, code: 500, message: 'Internal server error (0)'});
            throw err;
          } else {
            const creds = {
              update: {
                query: `
                  UPDATE
                    ta_mandiri AS m,
                    ta_seminar AS s
                  SET
                    m.status_tempat = 0,
                    m.status_formulir = 0,
                    m.tempat = null,
                    m.jam = null,
                    m.tanggal = null,
                    s.tanggal = null,
                    s.konfirmasi_pembimbing1 = 2
                  WHERE
                    m.nim = ?
                  AND
                    s.nim = ?
                `,
                data: [nim, nim]
              },
              select: {
                query: `
                  SELECT
                    nama,
                    email
                  FROM
                    mahasiswa_detail
                  WHERE
                    nim = ?
                `,
                data: [nim]
              }
            }
            con.query(creds.update.query, creds.update.data, (err1, res1) => {
              if (err1) {
                con.release();
                res.json({status: 0, code: 500, message: 'Internal server error (1)'});
                throw err1;
              } else {
                con.query(creds.select.query, creds.select.data, (err2, res2) => {
                  con.release();
                  if (err2) {
                    res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                    throw err2;
                  } else {
                    const credsMhs = {
                      nama: res2[0].nama,
                      email: res2[0].email,
                      statusDosen: 'Dosen pembimbing ketua'
                    };
                    mail.declineMandiri(res, credsMhs);
                    res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                  }
                })
              }
            });
          }
        }); 
      } else {
        res.json({status: 0, code: 403, message: 'Forbidden'});
      }
    }
  }

  // Method penolakan dari SELAIN pembimbing pertama
  this.declineMandiri = function(nim, token, email, res) {
    if (!token) res.json({status: 0, code: 401, message: 'No token provided'});
    else {
      const decoded = jwt.verify(token, 'yangPentingPanjang');
      const role = decoded.role;
      if (role === 2 || role === 4) {
        connection.acquire((err, con) => {
          if (err) {
            con.release();
            res.json({status: 0, code: 500, message: 'Internal server error (0)'});
            throw err;
          } else {
            let statusDosen;
            con.query('SELECT dosen1_email, dosen2_email, penguji1_email, penguji2_email FROM dosen_mahasiswa_view WHERE nim = ?', [nim], (err1, res1) => {
              if (err1) {
                res.json({status: 0, code: 500, message: 'Internal server error (1)'});
                throw err1;
              } else {
                if (email.email) {
                  if (res1[0].dosen1_email === email.email) {
                    statusDosen = 'Dosen pembimbing ketua';
                  } else if (res1[0].dosen2_email === email.email) {
                    statusDosen = 'Dosen pembimbing anggota';
                  } else if (res1[0].penguji1_email === email.email) {
                    statusDosen = 'Penguji pertama';
                  } else if (res1[0].penguji2_email === email.email) {
                    statusDosen = 'Penguji kedua';
                  }
                  console.log(statusDosen);
                  switch(statusDosen) {
                    case 'Dosen pembimbing anggota':
                      creds = {
                        update: {
                          query: {
                            first: `
                              UPDATE
                                ta_seminar
                              SET
                                konfirmasi_pembimbing2 = 2
                              WHERE
                                nim = ?
                            `
                          },
                          data: {
                            first: [nim]
                          }
                        }
                      };
                      break;
                    case 'Penguji pertama':
                      creds = {
                        update: {
                          query: {
                            first: `
                              UPDATE
                                ta_seminar
                              SET
                                konfirmasi_penguji1 = 2
                              WHERE
                                nim = ?
                            `
                          }, 
                          data: {
                            first: [nim]
                          }
                        },
                     
                      };
                      break;
                    case 'Penguji kedua': 
                      creds = {
                        update: {
                          query: {
                            first: `
                              UPDATE
                                ta_seminar
                              SET
                                konfirmasi_penguji2 = 2
                              WHERE
                                nim = ?
                            `
                          },
                          data: {
                            first: [nim]
                          }
                        }
                      };
                    break;
                  }

                  creds.update.query.second = `
                    UPDATE
                      ta_seminar AS tas,
                      ta_mandiri AS tam
                    SET
                      tas.konfirmasi_penguji2 = 0,
                      tas.konfirmasi_penguji1 = 0,
                      tas.konfirmasi_pembimbing1 = 0,
                      tas.konfirmasi_pembimbing2 = 0,
                      tam.status_tempat = 0,
                      tam.status_formulir = 0,
                      tam.tempat = null,
                      tam.jam = null,
                      tam.tanggal = null,
                      tas.tanggal = null
                    WHERE
                      tas.nim = ? AND tam.nim = ?
                  `;
                  creds.update.data.second = [nim, nim];

                  creds.select = {
                    query: `
                      SELECT
                        ta.penguji_2,
                        tas.konfirmasi_pembimbing1,
                        tas.konfirmasi_penguji1,
                        tas.konfirmasi_penguji2,
                        m.email,
                        m.nama
                      FROM 
                        mahasiswa_detail AS m
                      INNER JOIN
                        ta ON m.nim = ta.nim
                      INNER JOIN
                        ta_seminar AS tas ON ta.nim = tas.nim
                      WHERE
                        m.nim = ? AND
                        ta.nim = ? AND
                        tas.nim = ?
                    `,
                    data: [nim, nim, nim]
                  }; 
                  const querySelectSyarat = `
                    SELECT
                      tas.konfirmasi_pembimbing1,
                      tas.konfirmasi_pembimbing2,
                      tas.konfirmasi_penguji1,
                      tas.konfirmasi_penguji2,
                      m.email,
                      m.nama
                    FROM
                      ta_seminar AS tas
                    INNER JOIN
                      mahasiswa_detail AS m ON tas.nim = m.nim
                    WHERE
                      tas.nim = ? AND m.nim = ?  
                  `;
                  const queryUpdateSyarat = `
                    UPDATE
                      ta_mandiri
                    SET
                      status_formulir = 2
                    WHERE
                      nim = ?
                  `
                  const dataSelectSyarat = [nim, nim];
                  const dataUpdateSyarat = [nim]
                  con.query(creds.select.query, creds.select.data, (err2, res2) => {
                    if (err2) {
                      con.release();
                      res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                      throw err2;
                    } else {
                      if (res2[0].konfirmasi_pembimbing1 === 1) {
                        if (statusDosen === 'Dosen pembimbing anggota') {
                          con.query(creds.update.query.first, creds.update.data.first, (err3, res3) => {
                            if (err3) {
                              con.release();
                              res.json({status: 0, code: 500, message: 'Internal server error (3)'});
                              throw err3;
                            } else {
                              con.query(querySelectSyarat, dataSelectSyarat, (err4, res4) => {
                                if (err4) {
                                  con.release();
                                  res.json({status: 0, code: 500, message: 'Internal server error (4)'});
                                  throw err4;
                                } else {
                                  let konfirmasiPembimbing1 = res4[0].konfirmasi_pembimbing1, 
                                  konfirmasiPembimbing2 = res4[0].konfirmasi_pembimbing2,
                                  konfirmasiPenguji1 = res4[0].konfirmasi_penguji1,
                                  konfirmasiPenguji2 = res4[0].konfirmasi_penguji2;
                                  if (
                                    (konfirmasiPembimbing1 === 1 && konfirmasiPembimbing2 === 1 && konfirmasiPenguji1 === 1) ||
                                    (konfirmasiPembimbing1 === 1 && konfirmasiPembimbing2 === 2 && konfirmasiPenguji1 === 1) ||
                                    (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 1 && konfirmasiPenguji2 === 1) ||
                                    (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 1 && konfirmasiPenguji2 === 2) ||
                                    (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 2 && konfirmasiPenguji2 === 1 )
                                  ) {
                                    con.query(queryUpdateSyarat, dataUpdateSyarat, (err5, res5) => {
                                      con.release();
                                      if (err5) {
                                        res.json({status: 0, code: 500, message: 'Internal server error (5)'});
                                        throw err5;
                                      } else {
                                        const credsMhs = {
                                          nama: res4[0].nama,
                                          email: res4[0].email,
                                          statusDosen
                                        }
                                        mail.setMandiriSukses(res, credsMhs, 'pertama');
                                        res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                                      }
                                    });
                                  } else {
                                    con.release();
                                    res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                                  }
                                }
                              });
                            }
                          });
                        } else {
                          res2[0].statusDosen = statusDosen;
                          if (res2[0].penguji_2 === 0) {
                            con.query(creds.update.query.second, creds.update.data.second, (err3, res3) => {
                              con.release();
                              if (err3) {
                                res.json({status: 0, code: 500, message: 'Internal server error (3)'});
                                throw err3;
                              } else {
                                mail.declineMandiri(res, res2[0]);
                                res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                              }
                            });
                          } else {
                            if (res2[0].konfirmasi_penguji1 === 2 || res2[0].konfirmasi_penguji2 === 2) {
                              con.query(creds.update.query.second, creds.update.data.second, (err3, res3) => {
                                con.release();
                                if (err3) {
                                  res.json({status: 0, code: 500, message: 'Internal server error (3)'});
                                  throw err3;
                                } else {
                                  mail.declineMandiri(res, res2[0]);
                                  res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                                }
                              });
                            } else {
                              con.query(creds.update.query.first, creds.update.data.first, (err3, res3) =>{
                                if (err3) {
                                  con.release();
                                  res.json({status: 0, code: 500, message: 'Internal server error (3)'});
                                  throw err3;
                                } else {
                                  con.query(querySelectSyarat, dataSelectSyarat, (err4, res4) => {
                                    if (err4) {
                                      con.release();
                                      res.json({status: 0, code: 500, message: 'Internal server error (4)'});
                                      throw err4;
                                    } else {
                                      let 
                                        konfirmasiPembimbing1 = res4[0].konfirmasi_pembimbing1, 
                                        konfirmasiPembimbing2 = res4[0].konfirmasi_pembimbing2,
                                        konfirmasiPenguji1 = res4[0].konfirmasi_penguji1,
                                        konfirmasiPenguji2 = res4[0].konfirmasi_penguji2;
                                      if ((konfirmasiPembimbing1 === 1 && konfirmasiPembimbing2 === 1 && konfirmasiPenguji1 === 1) ||
                                      (konfirmasiPembimbing1 === 1 && konfirmasiPembimbing2 === 2 && konfirmasiPenguji1 === 1 ||
                                      (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 1 && konfirmasiPenguji2 === 1 ||
                                      (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 1 && konfirmasiPenguji2 === 2 ||
                                      (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 2 && konfirmasiPenguji2 === 1 ))))) {
                                        con.query(queryUpdateSyarat, dataUpdateSyarat, (err5, res5) => {
                                          con.release();
                                          if (err5) {
                                            res.json({status: 0, code: 500, message: 'Internal server error (5)'});
                                            throw err5;
                                          } else {
                                            const credsMhs = {
                                              nama: res4[0].nama,
                                              email: res4[0].email
                                            }
                                            mail.setMandiriSukses(res, credsMhs, 'pertama');
                                            res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                                          }
                                        });
                                      } else {
                                        con.release();
                                        res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                                      }
                                    }
                                  });
                                }
                              });
                            }
                          }
                        }
                      } else {
                        res.json({status: 0, message: 'Dosen pembimbing ketua  menolak atau belum melakukan konfirmasi'});
                      }
                    } 
                  })
                }
              }
            });
          }
        
        });
      } else {
        res.json({status: 0, code: 403, message: 'Forbidden'});
      }
    }
  }

  // MODUL KONFIRMASI PELAKSANAAN SIDANG
  // Method konfirmasi pengajuan pelaksanaan sidang u/ dosen ketua
  this.setAcceptedPertamaSidang = function(nim, token, res) {
    if (!token) res.json({status: 0, code: 401, message: 'No token provided'});
    else {
      const decoded = jwt.verify(token, 'yangPentingPanjang');
      const role = decoded.role;
      if (role === 2 || role === 4) {
        connection.acquire((err, con) => {
          if (err) {
            res.json({status: 0, code: 500, message: 'Internal server error (0)'});
            throw err;
          } else {
            console.log(nim);
            const creds = {
              update: {
                query: `
                  UPDATE
                    ta_sidang
                  SET
                    konfirmasi_pembimbing1 = 1
                  WHERE
                    nim = ?
                `,
                data: [nim]
              },
              select: {
                query: `
                  SELECT
                    d.nama,
                    d.nim,
                    d.topik,
                    d.dosen1,
                    d.dosen2,
                    d.penguji1,
                    d.penguji2,
                    d.dosen2_email,
                    d.penguji1_email,
                    d.penguji2_email,
                    m.tanggal,
                    m.tempat,
                    m.jam
                  FROM
                    dosen_mahasiswa_view AS d
                  INNER JOIN
                    mahasiswa_sidang_view AS m ON d.nim = m.nim
                  WHERE
                    d.nim = ? AND m.nim = ?
                `,
                data: [nim, nim]
              }
            };
            con.query(creds.select.query, creds.select.data, (err1, res1) => {
              if (err1) {
                res.json({status: 0, code: 500, message: 'Internal server error (1)'});
                throw err1;
              } else {
                con.query(creds.update.query, creds.update.data, (err2, res2) => {
                  con.release();
                  if (err2) {
                    res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                    throw err2;
                  } else {
                    const credsMhs = {
                      nama: res1[0].nama,
                      nim: res1[0].nim,
                      topik: res1[0].topik,
                      tempat: res1[0].tempat,
                      jam: res1[0].jam,
                      tanggal: res1[0].tanggal,
                      namaDosen1: res1[0].dosen1,
                      namaDosen2: res1[0].dosen2,
                      namaPenguji1: res1[0].penguji1,
                      namaPenguji2: res1[0].penguji2,
                      email: [
                        res1[0].dosen2_email,
                        res1[0].penguji1_email,
                        res1[0].penguji2_email,
                      ],
                    };
                    mail.setSidang(res, credsMhs, token);
                    res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                  }
                });
              }
            });
          }
        })
      } else {
        res.json({status: 0, code: 403, message: 'Forbidden'});
      }
    }
  }

  this.setAcceptedSidang = function(nim, token, email, res) {
    if (!token) res.json({status: 0, code: 401, message: 'No token provided'});
    else {
      const decoded = jwt.verify(token, 'yangPentingPanjang');
      const role = decoded.role;
      if (role === 2 || role === 4) {
        connection.acquire((err, con) => {
          if (err) {
            res.json({status: 0, code: 500, message: 'Internal server error (0)'});
            throw err;
          } else {
            let statusDosen;
            let creds;
            const querySelect = 'SELECT dosen1_email, dosen2_email, penguji1_email, penguji2_email FROM dosen_mahasiswa_view WHERE nim = ?';
            const dataSelect = [nim];
            con.query(querySelect, dataSelect, (err1, res1) => {
              if (err1) {
                res.json({status: 0, code: 500, message: 'Internal server errror (1)'});
                throw err1;
              } else {
                if (email) {
                  if (res1[0].dosen1_email === email) {
                    statusDosen = 'Dosen pembimbing ketua';
                  } else if (res1[0].dosen2_email === email) {
                    statusDosen = 'Dosen pembimbing anggota';
                  } else if (res1[0].penguji1_email === email) {
                    statusDosen = 'Penguji pertama';
                  } else if (res1[0].penguji2_email === email) {
                    statusDosen = 'Penguji kedua';
                  }
                  switch(statusDosen) {
                    case 'Dosen pembimbing anggota':
                      creds = {
                        update: {
                          query: `
                            UPDATE
                              ta_sidang
                            SET
                              konfirmasi_pembimbing2 = 1
                            WHERE
                              nim = ?
                          `,
                          data: [nim]
                        },
                        select: {
                          query: `
                            SELECT
                              konfirmasi_pembimbing1
                            FROM 
                              ta_sidang
                            WHERE
                              nim = ?
                          `,
                          data: [nim]
                        }
                      };
                      break;
                    case 'Penguji pertama':
                      creds = {
                        update: {
                          query: `
                            UPDATE
                              ta_sidang
                            SET
                              konfirmasi_penguji1 = 1
                            WHERE
                              nim = ?
                          `,
                          data: [nim]
                        },
                        select: {
                          query: `
                            SELECT
                              konfirmasi_pembimbing1
                            FROM 
                              ta_sidang
                            WHERE
                              nim = ?
                          `,
                          data: [nim]
                        }
                      };
                      break;
                    case 'Penguji kedua':
                      console.log('masuk penguji kedua');
                      creds = {
                        update: {
                          query: `
                            UPDATE
                              ta_sidang
                            SET
                              konfirmasi_penguji2 = 1
                            WHERE nim = ?
                          `,
                          data: [nim]
                        },
                        select: {
                          query: `
                            SELECT
                              konfirmasi_pembimbing1
                            FROM 
                              ta_sidang
                            WHERE
                              nim = ?
                          `,
                          data: [nim]
                        }
                      };
                      break;
                    default:
                      res.json({status: 0, code: 400, message: 'Bad request'});
                      break;
                  };
                  const credsSyarat = {
                    select: {
                      query: `
                        SELECT
                          tas.konfirmasi_pembimbing1,
                          tas.konfirmasi_pembimbing2,
                          tas.konfirmasi_penguji1,
                          tas.konfirmasi_penguji2,
                          m.email,
                          m.nama
                        FROM
                          ta_sidang AS tas
                        INNER JOIN
                          mahasiswa_detail AS m ON tas.nim = m.nim
                        WHERE
                          tas.nim = ? AND m.nim = ? 
                    `,
                      data: [nim, nim]
                    },
                    update: {
                      query: `
                        UPDATE
                          ta_sidang
                        SET
                          status_formulir = 2
                        WHERE
                          nim = ?
                      `,
                      data: [nim]
                    }
                  };
                  con.query(creds.select.query, creds.select.data, (err2, res2) => {
                    if (err2) {
                      res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                      throw err2;
                    } else if (res2[0].konfirmasi_pembimbing1 != 1) {
                      res.json({status: 0, message: 'Pembimbing ketua menolak atau belum memberikan konfirmasi'})
                    } else {
                      con.query(creds.update.query, creds.update.data, (err3, res3) => {
                        if (err3) {
                          res.json({status: 0, code: 500, message: 'Internal server error (3)'});
                          throw err3;
                        } else {
                          con.query(credsSyarat.select.query, credsSyarat.select.data, (err4, res4) => {
                            if (err4) {
                              res.json({status: 0, code: 500, message: 'Internal server error (4)'});
                              throw err4;
                            } else {
                              let 
                                konfirmasiPembimbing1 = res4[0].konfirmasi_pembimbing1, 
                                konfirmasiPembimbing2 = res4[0].konfirmasi_pembimbing2,
                                konfirmasiPenguji1 = res4[0].konfirmasi_penguji1,
                                konfirmasiPenguji2 = res4[0].konfirmasi_penguji2;
                              if (
                                (konfirmasiPembimbing1 === 1 && konfirmasiPembimbing2 === 1 && konfirmasiPenguji1 === 1) || 
                                (konfirmasiPembimbing1 === 1 && konfirmasiPenguji1 === 1 && konfirmasiPenguji2 === 1)
                              ) {
                                con.query(credsSyarat.update.query, credsSyarat.update.data, (err5, res5) => {
                                  if (err5) {
                                    res.json({status: 0, code: 500, message: 'Internal server error (5)'});
                                    throw err5;
                                  } else {
                                    con.release();
                                    const credsMhs = {
                                      nama: res4[0].nama,
                                      email: res4[0].email,
                                      type: 'pertama'
                                    };
                                    // Email here
                                    mail.setSidangSukses(res, credsMhs);
                                    res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                                  }
                                });
                              } else {
                                res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                              }
                            }
                          });
                        }
                      });
                    }
                  })
                }
              }
            });
          }
        });
      } else {
        res.json({status: 0, code: 403, message: 'Forbidden'});
      }
    }
  }
  // Method penolakan pelaksanaan sidang u/ pembimbing kedua
  this.declinePertamaSidang = function(nim, token, res) {
    if (!token) res.json({status: 0, code: 401, message: 'No token provided'});
    else {
      const decoded = jwt.verify(token, 'yangPentingPanjang');
      const role = decoded.role;
      if (role === 2 || role === 4) {
        connection.acquire((err, con) => {
          if (err) {
            res.json({status: 0, code: 500, message: 'Internal server error (0)'});
            throw err;
          } else {
            const creds = {
              delete: {
                // query: `
                //   UPDATE
                //     ta_sidang
                //   SET
                //     status_tempat = 0,
                //     status_formulir = 0,
                //     tempat = null,
                //     jam = null,
                //     tanggal = null,
                //     tanggal = null,
                //     konfirmasi_pembimbing1 = 2
                //   WHERE
                //     nim = ?
                // `,
                query: `
                  DELETE FROM
                    ta_sidang
                  WHERE
                    nim = ?
                `,
                data: [nim]
              },
              select: {
                query: `
                  SELECT
                    nama,
                    email
                  FROM
                    mahasiswa_detail
                  WHERE
                    nim = ?
                `,
                data: [nim]
              }
            };
            con.query(creds.select.query, creds.select.data, (err1, res1) => {
              if (err1) {
                res.json({status: 0, code: 500, message: 'Internal server error (1)'});
                throw err1;
              } else {
                con.query(creds.delete.query, creds.delete.data, (err2, res2) => {
                  con.release();
                  if (err2) {
                    res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                    throw err2;
                  } else {
                    const credsMhs = {
                      nama: res1[0].nama,
                      email: res1[0].email,
                      statusDosen: 'Dosen pembimbing ketua'
                    };
                    mail.declineSidang(res, credsMhs);
                    res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                  }
                })
              }
            });
          }
        });
      } else {
        res.json({status: 0, code: 403, message: 'Forbidden'});
      }
    }
  }

  // Method penolakan pelaksanaan sidang u/ seleain pembimbing kedua
  this.declineSidang = function(nim, token,email, res) {
    if (!token) res.json({status: 0, code: 401, message: 'No token provided'});
    else {
      const decoded = jwt.verify(token, 'yangPentingPanjang');
      const role = decoded.role;
      if (role === 2 || role === 4) {
        connection.acquire((err, con) => {
          if (err) {
            res.json({status: 0, code: 500, message: 'Internal server error'});
            throw err;
          } else {
            const creds = {
              delete: {
                // query: `
                //   UPDATE
                //     ta_sidang
                //   SET
                //     status_formulir = 0,
                //     status_tempat = 0,
                //     konfirmasi_penguji2 = null,
                //     konfirmasi_penguji1 = null,
                //     konfirmasi_pembimbing1 = null,
                //     konfirmasi_pembimbing2 = null,
                //     konfirmasi_admin = null,
                //     tempat = null,
                //     jam = null,
                //     tanggal = null
                //   WHERE
                //     nim = ?
                // `,
                query: `
                  DELETE FROM
                    ta_sidang
                  WHERE
                    nim = ?
                `,
                data: [nim]
              },
              select: {
                query: `
                  SELECT
                    nama,
                    email
                  FROM
                    mahasiswa_detail
                  WHERE
                    nim = ?
                `,
                data: [nim]
              }
            };
            con.query('SELECT dosen1_email, dosen2_email, penguji1_email, penguji2_email FROM dosen_mahasiswa_view WHERE nim = ?', [nim], (err1, res1) => {
              if (err1) {
                con.release();
                res.json({status: 0, code: 500, message: 'Internal server error (1)'})
                throw err1;
              } else {
                if (email) {
                  if (res1[0].dosen1_email === email) {
                    statusDosen = 'Dosen pembimbing ketua';
                  } else if (res1[0].dosen2_email === email) {
                    statusDosen = 'Dosen pembimbing anggota';
                  } else if (res1[0].penguji1_email === email) {
                    statusDosen = 'Penguji pertama';
                  } else if (res1[0].penguji2_email === email) {
                    statusDosen = 'Penguji kedua';
                  }
                  con.query(creds.delete.query, creds.delete.data, (err1, res1) => {
                    if (err1) {
                      con.release();
                      res.json({status: 0, code: 500, message: 'Internal server error (1)'});
                      throw err1;
                    } else {
                      const queryStatusDosen = ''
                      con.query(creds.select.query, creds.select.data, (err2, res2) => {
                        con.release();
                        if (err2) {
                          res.json({status: 0, code: 500, message: 'Internal server error (2)'});
                          throw err2;
                        } else {
                          // Mail penolakan
                          const credsMhs = {
                            nama: res2[0].nama,
                            email: res2[0].email,
                            statusDosen
                          };
                          mail.declineSidang(res, credsMhs);
                          res.send(`<iframe src='http://simeta.apps.cs.ipb.ac.id/success' width='100%' height='100%'  frameBorder="0"></iframe>`);
                        }
                      });
                    }
                  });
                } else {
                  res.json({status: 0, code: 400, message: 'Email is not defined'});
                }
              }
            });
         
          }
        });
      } else {
        res.json({status: 0, code: 403, message: 'Forbidden'});
      }
    }
  }
}

module.exports = new DirectApprove();
