var nodemailer = require('nodemailer');
var ICS = require('ics');
var ics = new ICS();

// var transporter = nodemailer.createTransport('smtps://postmaster@sandboxd581378d0c504cc0b05ad6243decf5d5.mailgun.org:aead6c5060abd2250c61df0399ff37b8@smtp.mailgun.org');
var transporter = nodemailer.createTransport('smtps://simeta.ilkom@gmail.com:simeta.ilkom.ipb@smtp.gmail.com');

function TestMail(){

    this.test = function(res) {

        let a = ics.buildEvent({
            uid: 'simeta.ilkom',
            start: '2016-05-30 06:50',
            title: 'Seminar',
            description: 'Seminar',
            location: 'Folsom Field, University of Colorado (finish line)',
            url: 'http://simeta.apps.cs.ipb.ac.id/',
            status: 'confirmed',
            attendees: [
                { name: 'Adam Gibbons', email: 'adam@example.com' },
                { name: 'Brittany Seaton', email: 'brittany@example2.org' }
            ],
            categories: ['10k races', 'Memorial Day Weekend', 'Boulder CO']
        });

        var mailOptions = {
            from: '"[SIMETA-ILKOM]" <simeta@apps.cs.ipb.ac.id>',
            to: ['ivanmaulana@apps.ipb.ac.id', "ivanmaulanaputra@gmail.com"],
            subject: 'test email 2dwad',
            attachments: [
                {   // file on disk as an attachment
                    // path: 'https://www.tutorialspoint.com/angularjs/angularjs_tutorial.pdf' // stream this file
                    path: './uploads/jadwal_kolokium.pdf' // stream this file
                }
            ],
            icalEvent: {
                method: 'request',
                content: a
            },
            html:
                'test kirim'
        };

        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                res.send(ics);
                return console.log(error);
            }
            else {
                res.send('success');
                console.log(ics);
                console.log('Message sent: ' + info.response);
            }
        });

    }
}

module.exports = new TestMail();