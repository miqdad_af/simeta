var connection = require('../connection');
var jwt = require('jsonwebtoken');

function GenerateJWT(){

  this.create = function(role, id) {
    var token = jwt.sign({role: role, id: id}, 'yangPentingPanjang');
    return token;
  };

  // SIMETA v2
  this.mailToken = function(data) {
    return jwt.sign(data, 'yangPentingPanjang');
  }

  this.checkToken = function(data, res, next) {
    jwt.verify(data, 'yangPentingPanjang', function(err, decode){
      console.log('Masuk err', err);
      console.log('Masuk decode', decode);
      if (err != null) res.json({status: false, message: "Sorry It doesn't my token, please make new request"});
    });
  }

  this.decodeToken = function(data) {
    return jwt.decode(data);
  }

}

module.exports = new GenerateJWT();
