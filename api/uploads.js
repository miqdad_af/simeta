const multer = require('multer');
const path = require('path');
var connection = require('./connection');
var fs = require('fs');
var fc = require('./fc');
var mail = require('./mail');

var d = new Date();
var time = d.getDate()  + "-" + (d.getMonth()+1) + "-" + d.getFullYear() + " " +d.getHours() + ":" + d.getMinutes();

var _ = require('./simak');

function fileFilter (req, file, cb) {
  if (file.originalname.search('.pdf') != -1) {
    cb(null, true);
  }
}

function fileFilterJPG (req, file, cb) {
  if (file.originalname.search('.jpg') != -1) {
    cb(null, true);
  }
}

function fileFilterZIP (req, file, cb) {
  if (file.originalname.search('.zip') != -1) {
    cb(null, true);
  }
}

const uploadSidang = multer({
  storage: multer.diskStorage({
    filename: (req, file, cb) => {
      let ext = path.extname(file.originalname);
      cb(null, `${'Sidang_'+_.decoded.nim+'_'+time}${ext}`);
    },
    destination: function (req, file, cb) {
      cb(null, './uploads/fileSidang/')
    },
  }),
  fileFilter: fileFilter
});

const uploadSeminarMicon = multer({
  storage: multer.diskStorage({
    filename: (req, file, cb) => {
      let ext = path.extname(file.originalname);
      cb(null, `${'Seminar_Micon_'+_.decoded.nim+'_'+time}${ext}`);
    },
    destination: function (req, file, cb) {
      cb(null, './uploads/fileSeminar/micon/')
    },
  }),
  fileFilter: fileFilterZIP
});

const uploadSeminarKonferensi = multer({
  storage: multer.diskStorage({
    filename: (req, file, cb) => {
      let ext = path.extname(file.originalname);
      cb(null, `${'Seminar_Konferensi_'+_.decoded.nim+'_'+time}${ext}`);
    },
    destination: function (req, file, cb) {
      cb(null, './uploads/fileSeminar/konferensi/')
    },
  }),
  fileFilter: fileFilterZIP
});

const uploadSeminarMandiri = multer({
  storage: multer.diskStorage({
    filename: (req, file, cb) => {
      let ext = path.extname(file.originalname);
      cb(null, `${'Seminar_Mandiri_'+_.decoded.nim+'_'+time}${ext}`);
    },
    destination: function (req, file, cb) {
      cb(null, './uploads/fileSeminar/mandiri/')
    },
  }),
  fileFilter: fileFilter
});

const uploadKolokium = multer({
  storage: multer.diskStorage({
    filename: (req, file, cb) => {
      let ext = path.extname(file.originalname);
      cb(null, `${'kolokium_'+_.decoded.nim+'_'+time}${ext}`);
    },
    destination: function (req, file, cb) {
      cb(null, './uploads/fileKolokium')
    },
  }),
  fileFilter: fileFilter
});

const uploadPraseminar = multer({
  storage: multer.diskStorage({
    filename: (req, file, cb) => {
      let ext = path.extname(file.originalname);
      cb(null, `${'praseminar_'+_.decoded.nim+'_'+time}${ext}`);
    },
    destination: function (req, file, cb) {
      cb(null, './uploads/filePraseminar')
    },
  }),
  fileFilter: fileFilter
});

const uploadSeminar = multer({
  storage: multer.diskStorage({
    filename: (req, file, cb) => {
      let ext = path.extname(file.originalname);
      cb(null, `${'seminar_'+_.decoded.nim}${ext}`);
    },
    destination: function (req, file, cb) {
      cb(null, './uploads/fileSeminar')
    },
  }),
  fileFilter: fileFilter
});

const uploadFinal = multer({
  storage: multer.diskStorage({
    filename: (req, file, cb) => {
      let ext = path.extname(file.originalname);
      cb(null, `${'final_'+_.decoded.nim}${ext}`);
    },
    destination: function (req, file, cb) {
      cb(null, './uploads/fileTAFinal')
    },
  }),
  fileFilter: fileFilter
});

const uploadSKL = multer({
  storage: multer.diskStorage({
    filename: (req, file, cb) => {
      let ext = path.extname(file.originalname);
      cb(null, `${'SKL_'+_.decoded.nim+'_'+time}${ext}`);
    },
    destination: function (req, file, cb) {
      cb(null, './uploads/fileSKL')
    },
  }),
  fileFilter: fileFilter
})

const uploadPhoto = multer({
  storage: multer.diskStorage({
    filename: (req, file, cb) => {
      let ext = path.extname(file.originalname);
      cb(null, `${'foto-'+_.decoded.nim+'-'+_.decoded.nama}${ext}`);
    },
    destination: function (req, file, cb) {
      cb(null, './uploads/filePhoto')
    },
  }),
  fileFilter: fileFilterJPG
})

const uploadJadwalKolokium = multer({
  storage: multer.diskStorage({
    filename: (req, file, cb) => {
      let ext = path.extname(file.originalname);
      cb(null, `${'jadwal_kolokium'}${ext}`);
    },
    destination: function (req, file, cb) {
      cb(null, './uploads/')
    },
  }),
  fileFilter: fileFilter
});

const uploadJadwalPraseminar = multer({
  storage: multer.diskStorage({
    filename: (req, file, cb) => {
      let ext = path.extname(file.originalname);
      cb(null, `${'jadwal_praseminar'}${ext}`);
    },
    destination: function (req, file, cb) {
      cb(null, './uploads/')
    },
  }),
  fileFilter: fileFilter
});

const uploadJadwalMicon = multer({
  storage: multer.diskStorage({
    filename: (req, file, cb) => {
      let ext = path.extname(file.originalname);
      cb(null, `${'jadwal_micon'}${ext}`);
    },
    destination: function (req, file, cb) {
      cb(null, './uploads/')
    },
  }),
  fileFilter: fileFilter
});

module.exports = {
  configure: function(app) {

    // ----------------------------
    // UPLOAD KOLOKIUM
    app.post('/upload/seminar/micon', uploadSeminarMicon.any(), (req, res) => {
      connection.acquire(function(err, con) {

        var jadwal;
        con.query('select * from jadwal_micon',function(err, resultJadwal) {
          if(err) {
            con.release();
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            jadwal = resultJadwal[0].jadwal_micon;
          }
        })

        // NO FILE
        if (!req.files) {
          con.release();
          res.json({status: false, message: 'Tidak ada file'});
        }

        // FILE
        else {

          if (req.files[0].originalname.search('.zip') != -1) {
            var queryFile = "SELECT * FROM `ta_seminar` WHERE nim = ?";
            var dataFile = [_.decoded.nim];
            con.query(queryFile, dataFile, function(err, result) {

              if(result.length > 0) {
                var query1 = 'UPDATE ta_micon SET berkas = ? WHERE nim = ?';
                var data1 = ['Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip',  _.decoded.nim];
                con.query(query1, data1, function(err, result1) {

                  if (err) {
                    con.release();
                    res.json({status: 0, message: 'Upload File Gagal'});
                    throw err;
                  }
                  else {
                    var query2 = 'select * from dosen_mahasiswa_view where nim = ?';
                    var data2 = [_.decoded.nim];
                    con.query(query2, data2, function(err2, result2) {
                      con.release();
                      if (err2) {
                        res.json({status: 0, message: 'Upload File Gagal'});
                        throw err2;
                      }
                      else {
                        fc.createSeminar(2, jadwal);
                        if(result2[0].dosen1_email) {
                            mail.miconMail(result2[0].dosen1_email, _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');
                        }
                        mail.miconMail(result2[0].dosen1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');
                       
                        if(result2[0].penguji1_email) {
                            mail.miconMail(result2[0].penguji1_email, _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');
                        }
                        mail.miconMail(result2[0].penguji1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');
                       
                        if (result2[0].dosen2_username) {
                          mail.miconMail(result2[0].dosen2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');
                        }
                        if(result2[0].dosen2_email) {
                          mail.miconMail(result2[0].dosen2_email, _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');
                        }
                       
                        if (result2[0].penguji22_username) {
                          mail.miconMail(result2[0].penguji2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');
                        }
                        if(result2[0].penguji2_email) {
                          mail.miconMail(result2[0].penguji2_email, _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');
                        }

                        // EMAIL TU
                         mail.miconMail('irvan.rmdn@gmail.com', _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');
                         mail.miconMail('ridwanilkom@gmail.com', _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');

                        res.json(req.files.map(file => {

                          let ext = path.extname(file.originalname);
                          return {
                            status: true,
                            detail: 'update',
                            originalName: file.originalname,
                            filename: file.filename
                          }
                        }));
                      }
                    })
                  }
                })
              }

              else {
                var query1 = 'INSERT ta_micon (nim, berkas) VALUES (?, ?)';
                var data1 = [_.decoded.nim, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip'];
                con.query(query1, data1, function(err, result1) {
                  if (err) {
                    con.release();
                    res.json({status: 0, message: 'Upload File Gagal'});
                    throw err;
                  }
                  else {
                    var query2 = 'select * from dosen_mahasiswa_view where nim = ?';
                    var data2 = [_.decoded.nim];
                    con.query(query2, data2, function(err2, result2) {
                      con.release();
                      if (err2) {
                        res.json({status: 0, message: 'Upload File Gagal'});
                        throw err2;
                      }
                      else {
                        if(result2[0].dosen1_email) {
                            mail.miconMail(result2[0].dosen1_email, _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');
                        }
                        mail.miconMail(result2[0].dosen1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');
                        if(result2[0].dosen2_username) {
                          mail.miconMail(result2[0].dosen2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');
                        }
                        if(result2[0].dosen2_email) {
                          mail.miconMail(result2[0].dosen2_email, _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');
                        }
                        // EMAIL TU
                         mail.miconMail('irvan.rmdn@gmail.com', _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');
                         mail.miconMail('ridwanilkom@gmail.com', _.decoded.nim, _.decoded.nama, result2[0].topik, 'Seminar_Micon_'+_.decoded.nim+'_'+time+'.zip');

                        fc.createSeminar(2, jadwal);
                        res.json(req.files.map(file => {
                          let ext = path.extname(file.originalname);
                          return {
                            status: true,
                            detail: 'update',
                            originalName: file.originalname,
                            filename: file.filename
                          }
                        }));
                      }
                    })
                  }
                })
              }

            })

          }
          else {
            con.release();
            res.json({status: false, message: 'File bukan pdf'});
          }

        }

      })
    })
    // ----------------------------

    // ----------------------------
    // UPLOAD SIDANG
    app.post('/upload/sidang', uploadSidang.any(), (req, res) => {
      connection.acquire(function(err, con) {

        // NO FILE
        if (!req.files) {
          con.release();
          res.json({status: false, message: 'Tidak ada file'});
        }

        // FILE
        else {
          if (req.files[0].originalname.search('.pdf') != -1) {

              var query1 = 'UPDATE ta_sidang SET makalah = ? WHERE nim = ?';
              var data1 = ['Sidang_'+_.decoded.nim+'_'+time+'.pdf', _.decoded.nim];
              con.query(query1, data1, function(err, result1) {

                if (err) {
                  con.release();
                  res.json({status: 0, message: 'Upload File Gagal'});
                  throw err;
                }
                else {

                  var query2 = 'select * from ta_sidang where nim = ?';
                  var data2 = [_.decoded.nim];
                  con.query(query2, data2, function(err, result2) {
                    if (err) {
                      con.release();
                      res.json({status: 0, message: 'Upload File Gagal'});
                      throw err;
                    }
                    else {
                      var tempat = result2[0].tempat;
                      var jam = result2[0].jam;
                      var tanggal = result2[0].tanggal;

                      var query3 = 'select * from dosen_mahasiswa_view where nim = ?';
                      var data3 = [_.decoded.nim];
                      con.query(query3, data3, function(err3, result3) {
                        con.release();
                        if (err3) {
                          res.json({status: 0, message: 'Upload File Gagal'});
                          throw err3;
                        }
                        else {
                          // if(result3[0].dosen1_email) {
                          //     mail.sidangMail(result3[0].dosen1_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'Sidang_'+_.decoded.nim+'_'+time+'.pdf', tempat, jam, tanggal);
                          // }
                          // mail.sidangMail(result3[0].dosen1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Sidang_'+_.decoded.nim+'_'+time+'.pdf',tempat, jam, tanggal);
                          
                          // if(result3[0].penguji1_email) {
                          //     mail.sidangMail(result3[0].penguji1_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'Sidang_'+_.decoded.nim+'_'+time+'.pdf', tempat, jam, tanggal);
                          // }
                          // mail.sidangMail(result3[0].penguji1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Sidang_'+_.decoded.nim+'_'+time+'.pdf',tempat, jam, tanggal);
                          
                          // if(result3[0].dosen2_username) {
                          //   mail.sidangMail(result3[0].dosen2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Sidang_'+_.decoded.nim+'_'+time+'.pdf', tempat, jam, tanggal);
                          // }
                          // if(result3[0].dosen2_email) {
                          //   mail.sidangMail(result3[0].dosen2_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'Sidang_'+_.decoded.nim+'_'+time+'.pdf', tempat, jam, tanggal);
                          // }
                          
                          // if(result3[0].penguji2_username) {
                          //   mail.sidangMail(result3[0].penguji2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Sidang_'+_.decoded.nim+'_'+time+'.pdf', tempat, jam, tanggal);
                          // }
                          // if(result3[0].penguji2_email) {
                          //   mail.sidangMail(result3[0].penguji2_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'Sidang_'+_.decoded.nim+'_'+time+'.pdf', tempat, jam, tanggal);
                          // }

                          // // EMAIL TU
                          //  mail.sidangMail('irvan.rmdn@gmail.com', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Sidang_'+_.decoded.nim+'_'+time+'.pdf', tempat, jam, tanggal);
                          //  mail.sidangMail('ridwanilkom@gmail.com', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Sidang_'+_.decoded.nim+'_'+time+'.pdf', tempat, jam, tanggal);

                          res.json(req.files.map(file => {
                            let ext = path.extname(file.originalname);
                            return {
                              status: true,
                              detail: 'update',
                              originalName: file.originalname,
                              filename: file.filename
                            }
                          }));
                        }
                      })

                    }
                  })
                }
              })

          }
          else {
            con.release();
            res.json({status: false, message: 'File bukan pdf'});
          }
        }
      })
    })
    // ----------------------------


    // ----------------------------

    // ----------------------------
    // UPLOAD KOLOKIUM
    app.post('/upload/seminar/konferensi', uploadSeminarKonferensi.any(), (req, res) => {
      connection.acquire(function(err, con) {

        // NO FILE
        if (!req.files) {
          con.release();
          res.json({status: false, message: 'Tidak ada file'});
        }

        // FILE
        else {

          if (req.files[0].originalname.search('zip') != -1) {
            var queryFile = "SELECT * FROM `ta_konferensi` WHERE nim = ?";
            var dataFile = [_.decoded.nim];
            con.query(queryFile, dataFile, function(err, result) {

              var query1 = 'UPDATE ta_konferensi SET berkas = ? WHERE nim = ?';
              var data1 = ['Seminar_Konferensi_'+_.decoded.nim+'_'+time+'.zip', _.decoded.nim];
              con.query(query1, data1, function(err, result1) {

                if (err) {
                  con.release();
                  res.json({status: 0, message: 'Upload File Gagal'});
                  throw err;
                }
                else {
                  var query2 = 'select * from ta_konferensi where nim = ?';
                  var data2 = [_.decoded.nim];
                  con.query(query2, data2, function(err, result2) {
                    if (err) {
                      con.release();
                      res.json({status: 0, message: 'Upload File Gagal'});
                      throw err;
                    }
                    else {
                      var namaKonferensi = result2[0].nama_konferensi;
                      var judulPaper = result2[0].judul_paper;
                      var tempat = result2[0].tempat;
                      var tanggal = result2[0].tanggal;

                      var query3 = 'select * from dosen_mahasiswa_view where nim = ?';
                      var data3 = [_.decoded.nim];
                      con.query(query3, data3, function(err3, result3) {
                        con.release();
                        if (err3) {
                          res.json({status: 0, message: 'Upload File Gagal'});
                          throw err3;
                        }
                        else {
                          if(result3[0].dosen1_email) {
                              mail.konferensiMail(result3[0].dosen1_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Konferensi_'+_.decoded.nim+'_'+time+'.zip', namaKonferensi, judulPaper, tempat, tanggal);
                          }
                          mail.konferensiMail(result3[0].dosen1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Konferensi_'+_.decoded.nim+'_'+time+'.zip', namaKonferensi, judulPaper, tempat, tanggal);
                          
                          if(result3[0].penguji1_email) {
                              mail.konferensiMail(result3[0].penguji1_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Konferensi_'+_.decoded.nim+'_'+time+'.zip', namaKonferensi, judulPaper, tempat, tanggal);
                          }
                          mail.konferensiMail(result3[0].penguji1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Konferensi_'+_.decoded.nim+'_'+time+'.zip', namaKonferensi, judulPaper, tempat, tanggal);
                          
                          if(result3[0].dosen2_username) {
                            mail.konferensiMail(result3[0].dosen2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Konferensi_'+_.decoded.nim+'_'+time+'.zip', namaKonferensi, judulPaper, tempat, tanggal);
                          }
                          if(result3[0].dosen2_email) {
                            mail.konferensiMail(result3[0].dosen2_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Konferensi_'+_.decoded.nim+'_'+time+'.zip', namaKonferensi, judulPaper, tempat, tanggal);
                          }
                          
                          if(result3[0].penguji2_username) {
                            mail.konferensiMail(result3[0].penguji2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Konferensi_'+_.decoded.nim+'_'+time+'.zip', namaKonferensi, judulPaper, tempat, tanggal);
                          }
                          if(result3[0].penguji2_email) {
                            mail.konferensiMail(result3[0].penguji2_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Konferensi_'+_.decoded.nim+'_'+time+'.zip', namaKonferensi, judulPaper, tempat, tanggal);
                          }

                          // EMAIL TU
                           mail.konferensiMail('irvan.rmdn@gmail.com', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Konferensi_'+_.decoded.nim+'_'+time+'.zip', namaKonferensi, judulPaper, tempat, tanggal);
                           mail.konferensiMail('ridwanilkom@gmail.com', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Konferensi_'+_.decoded.nim+'_'+time+'.zip', namaKonferensi, judulPaper, tempat, tanggal);


                          res.json(req.files.map(file => {
                            let ext = path.extname(file.originalname);
                            return {
                              status: true,
                              detail: 'update',
                              originalName: file.originalname,
                              filename: file.filename
                            }
                          }));
                        }
                      })

                    }
                  })

                }
              })

            })

          }
          else {
            con.release();
            res.json({status: false, message: 'File bukan zip'});
          }

        }

      })
    })


    // ----------------------------

    // ----------------------------
    // UPLOAD KOLOKIUM
    app.post('/upload/seminar/mandiri', uploadSeminarMandiri.any(), (req, res) => {
      connection.acquire(function(err, con) {

        // NO FILE
        if (!req.files) {
          con.release();
          res.json({status: false, message: 'Tidak ada file'});
        }

        // FILE
        else {

          if (req.files[0].originalname.search('.pdf') != -1) {
            var queryFile = "SELECT * FROM `ta_mandiri` WHERE nim = ?";
            var dataFile = [_.decoded.nim];
            con.query(queryFile, dataFile, function(err, result) {

              if(result.length > 0) {
                var query1 = 'UPDATE ta_mandiri SET makalah = ? WHERE nim = ?';
                var data1 = ['Seminar_Mandiri_'+_.decoded.nim+'_'+time+'.pdf', _.decoded.nim];
                con.query(query1, data1, function(err, result1) {
                  if (err) {
                    con.release();
                    res.json({status: 0, message: 'Upload File Gagal'});
                    throw err;
                  }
                  else {

                    var query2 = 'select * from ta_mandiri where nim = ?';
                    var data2 = [_.decoded.nim];
                    con.query(query2, data2, function(err, result2) {
                      if (err) {
                        con.release();
                        res.json({status: 0, message: 'Upload File Gagal'});
                        throw err;
                      }
                      else {
                        var pembahas1 = result2[0].pembahas_1;
                        var pembahas2 = result2[0].pembahas_2;
                        var pembahas3 = result2[0].pembahas_3;

                        var tempat = result2[0].tempat;
                        var jam = result2[0].jam;
                        var tanggal = result2[0].tanggal;

                        var query3 = 'select * from dosen_mahasiswa_view where nim = ?';
                        var data3 = [_.decoded.nim];
                        con.query(query3, data3, function(err3, result3) {
                          con.release();
                          if (err3) {
                            res.json({status: 0, message: 'Upload File Gagal'});
                            throw err3;
                          }
                          else {
                            // if(result3[0].dosen1_email) {
                            //     mail(result3[0].dosen1_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Mandiri_'+_.decoded.nim+'_'+time+'.pdf', pembahas1, pembahas2, pembahas3, tempat, jam, tanggal);
                            // }
                            // mail.mandiriMail(result3[0].dosen1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Mandiri_'+_.decoded.nim+'_'+time+'.pdf', pembahas1, pembahas2, pembahas3, tempat, jam, tanggal);
                            
                            // if(result3[0].penguji1_email) {
                            //     mail.mandiriMail(result3[0].penguji1_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Mandiri_'+_.decoded.nim+'_'+time+'.pdf', pembahas1, pembahas2, pembahas3, tempat, jam, tanggal);
                            // }
                            // mail.mandiriMail(result3[0].penguji1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Mandiri_'+_.decoded.nim+'_'+time+'.pdf', pembahas1, pembahas2, pembahas3, tempat, jam, tanggal);
                            
                            // if(result3[0].dosen2_username) {
                            //   mail.mandiriMail(result3[0].dosen2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Mandiri_'+_.decoded.nim+'_'+time+'.pdf', pembahas1, pembahas2, pembahas3, tempat, jam, tanggal);
                            // }
                            // if(result3[0].dosen2_email) {
                            //   mail.mandiriMail(result3[0].dosen2_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Mandiri_'+_.decoded.nim+'_'+time+'.pdf', pembahas1, pembahas2, pembahas3, tempat, jam, tanggal);
                            // }
                            
                            // if(result3[0].penguji2_username) {
                            //   mail.mandiriMail(result3[0].dosen2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Mandiri_'+_.decoded.nim+'_'+time+'.pdf', pembahas1, pembahas2, pembahas3, tempat, jam, tanggal);
                            // }
                            // if(result3[0].penguji2_email) {
                            //   mail.mandiriMail(result3[0].dosen2_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Mandiri_'+_.decoded.nim+'_'+time+'.pdf', pembahas1, pembahas2, pembahas3, tempat, jam, tanggal);
                            // }

                            // EMAIL ILKOM
                            //  mail.mandiriMail('irvan.rmdn@gmail.com', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Mandiri_'+_.decoded.nim+'_'+time+'.pdf', pembahas1, pembahas2, pembahas3, tempat, jam, tanggal);
                            //  mail.mandiriMail('ridwanilkom@gmail.com', _.decoded.nim, _.decoded.nama, result3[0].topik, 'Seminar_Mandiri_'+_.decoded.nim+'_'+time+'.pdf', pembahas1, pembahas2, pembahas3, tempat, jam, tanggal);

                            res.json(req.files.map(file => {
                              let ext = path.extname(file.originalname);
                              return {
                                status: true,
                                detail: 'update',
                                originalName: file.originalname,
                                filename: file.filename
                              }
                            }));
                          }
                        })

                      }
                    })

                  }
                })
              }

            })

          }
          else {
            con.release();
            res.json({status: false, message: 'File bukan pdf'});
          }

        }

      })
    })


    // ----------------------------

    // ----------------------------
    // UPLOAD KOLOKIUM
    app.post('/upload/kolokium', uploadKolokium.any(), (req, res) => {
      connection.acquire(function(err, con) {

        // NO FILE
        if (!req.files) {
          con.release();
          res.json({status: false, message: 'Tidak ada file'});
        }

        // FILE
        else {

          if (req.files[0].originalname.search('.pdf') != -1) {
            var queryFile = "SELECT * FROM `ta_kolokium` WHERE nim = ?";
            var dataFile = [_.decoded.nim];
            con.query(queryFile, dataFile, function(err, result) {

              if(result.length > 0) {
                var query1 = 'UPDATE ta_kolokium SET makalah = ? WHERE nim = ?';
                var data1 = ['kolokium_'+_.decoded.nim+'_'+time+'.pdf', _.decoded.nim];
                con.query(query1, data1, function(err, result1) {
                  if (err) {
                    con.release();
                    res.json({status: 0, message: 'Upload File Gagal'});
                    throw err;
                  }
                  else {
                    var query2 = 'select * from dosen_mahasiswa_view where nim = ?';
                    var data2 = [_.decoded.nim];
                    con.query(query2, data2, function(err2, result2) {
                      con.release();
                      if (err2) {
                        res.json({status: 0, message: 'Upload File Gagal'});
                        throw err2;
                      }
                      else {
                          if(result2[0].dosen1_email) {
                              mail.kolokiumMail(result2[0].dosen1_email, _.decoded.nim, _.decoded.nama, result2[0].topik, 'kolokium_'+_.decoded.nim+'_'+time+'.pdf');
                          }
                        mail.kolokiumMail(result2[0].dosen1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result2[0].topik, 'kolokium_'+_.decoded.nim+'_'+time+'.pdf');
                        if(result2[0].dosen2_username) {
                          mail.kolokiumMail(result2[0].dosen2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result2[0].topik, 'kolokium_'+_.decoded.nim+'_'+time+'.pdf');
                        }
                        if(result2[0].dosen2_email) {
                          mail.kolokiumMail(result2[0].dosen2_email, _.decoded.nim, _.decoded.nama, result2[0].topik, 'kolokium_'+_.decoded.nim+'_'+time+'.pdf');
                        }

                        res.json(req.files.map(file => {
                          let ext = path.extname(file.originalname);
                          return {
                            status: true,
                            detail: 'update',
                            originalName: file.originalname,
                            filename: file.filename
                          }
                        }));
                      }
                    })
                  }
                })
              }

              else {
                var query2 = 'INSERT ta_kolokium (nim, makalah) VALUES (?, ?, ?)';
                var data2 = [_.decoded.nim, 'kolokium_'+_.decoded.nim+'_'+time+'.pdf'];
                con.query(query2, data2, function(err, result2) {
                  if (err) {
                    con.release();
                    res.json({status: 0, message: 'Upload File Gagal'});
                    throw err;
                  }
                  else {
                    var query2 = 'select * from dosen_mahasiswa_view where nim = ?';
                    var data2 = [_.decoded.nim];
                    con.query(query2, data2, function(err2, result2) {
                      con.release();
                      if (err2) {
                        res.json({status: 0, message: 'Upload File Gagal'});
                        throw err2;
                      }
                      else {

                          if(result2[0].dosen1_email) {
                              mail.kolokiumMail(result2[0].dosen1_email, _.decoded.nim, _.decoded.nama, result2[0].topik, 'kolokium_'+_.decoded.nim+'_'+time+'.pdf');
                          }
                        mail.kolokiumMail(result2[0].dosen1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result2[0].topik, 'kolokium_'+_.decoded.nim+'_'+time+'.pdf');
                        if(result2[0].dosen2_username) {
                          mail.kolokiumMail(result2[0].dosen2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result2[0].topik, 'kolokium_'+_.decoded.nim+'_'+time+'.pdf');
                        }
                        if(result2[0].dosen2_email) {
                          mail.kolokiumMail(result2[0].dosen2_email, _.decoded.nim, _.decoded.nama, result2[0].topik, 'kolokium_'+_.decoded.nim+'_'+time+'.pdf');
                        }

                        res.json(req.files.map(file => {
                          let ext = path.extname(file.originalname);
                          return {
                            status: true,
                            detail: 'update',
                            originalName: file.originalname,
                            filename: file.filename
                          }
                        }));
                      }
                    })
                  }
                })
              }

            })

          }
          else {
            con.release();
            res.json({status: false, message: 'File bukan pdf'});
          }

        }

      })
    })


    // ----------------------------



    // UPLOAD PRASEMINAR
    app.post('/upload/praseminar', uploadPraseminar.any(), (req, res) => {
      connection.acquire(function(err, con) {

        var jadwal;
        con.query('select * from jadwal_praseminar', function(err, resultJadwal) {
          if(err) {
            con.release();
            res.json({status: 0, message: 'API Failed'});
            throw err;
          }
          else {
            jadwal = resultJadwal[0].jadwal_praseminar;
          }

        })

        // NO FILE
        if (!req.files) {
          con.release();
          res.json({status: false, message: 'Tidak ada file'});
        }

        // FILE
        else {

          if (req.files[0].originalname.search('.pdf') != -1) {
            var queryFile = "SELECT * FROM `ta_praseminar` WHERE nim = ?";
            var dataFile = [_.decoded.nim];
            con.query(queryFile, dataFile, function(err, result) {

              var fileTemp = result[0].makalah;
              // fs.unlink('./uploads/filePraseminar/'+fileTemp);

              if(result.length > 0) {
                var query1 = 'UPDATE ta_praseminar SET makalah = ? WHERE nim = ?';
                var data1 = ['praseminar_'+_.decoded.nim+'_'+time+'.pdf', _.decoded.nim];
                con.query(query1, data1, function(err, result1) {
                  if (err) {
                    con.release();
                    res.json({status: 0, message: 'Upload File Gagal'});
                    throw err;
                  }
                  else {
                    var query2 = 'select * from dosen_mahasiswa_view where nim = ?';
                    var data2 = [_.decoded.nim];
                    con.query(query2, data2, function(err2, result2) {
                      con.release();
                      if (err2) {
                        res.json({status: 0, message: 'Upload File Gagal'});
                        throw err2;
                      }
                      else {

                          if(result2[0].dosen1_email) {
                              mail.praseminarMail(result2[0].dosen1_email, _.decoded.nim, _.decoded.nama, result2[0].topik, 'praseminar_'+_.decoded.nim+'_'+time+'.pdf');
                          }
                        mail.praseminarMail(result2[0].dosen1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result2[0].topik, 'praseminar_'+_.decoded.nim+'_'+time+'.pdf');
                        if(result2[0].dosen2_username) {
                          mail.praseminarMail(result2[0].dosen2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result2[0].topik, 'praseminar_'+_.decoded.nim+'_'+time+'.pdf');
                        }
                        if(result2[0].dosen2_email) {
                          mail.praseminarMail(result2[0].dosen2_email, _.decoded.nim, _.decoded.nama, result2[0].topik, 'praseminar_'+_.decoded.nim+'_'+time+'.pdf');
                        }
                        res.json(req.files.map(file => {
                          let ext = path.extname(file.originalname);
                          return {
                            status: true,
                            detail: 'update',
                            originalName: file.originalname,
                            filename: file.filename
                          }
                        }));
                      }
                    })
                  }
                })
              }

              else {
                var query1 = 'INSERT ta_praseminar (nim, makalah) VALUES (?, ?)';
                var data1 = [_.decoded.nim, 'praseminar_'+_.decoded.nim+'_'+time+'.pdf'];
                con.query(query1, data1, function(err, result1) {
                  if (err) {
                    con.release();
                    res.json({status: 0, message: 'Upload File Gagal'});
                    throw err;
                  }
                  else {
                    var query2 = 'select * from dosen_mahasiswa_view where nim = ?';
                    var data2 = [_.decoded.nim];
                    con.query(query2, data2, function(err2, result2) {
                      con.release();
                      if (err2) {
                        res.json({status: 0, message: 'Upload File Gagal'});
                        throw err2;
                      }
                      else {
                          if(result2[0].dosen1_email) {
                              mail.praseminarMail(result2[0].dosen1_email, _.decoded.nim, _.decoded.nama, result2[0].topik, 'praseminar_'+_.decoded.nim+'_'+time+'.pdf');
                          }

                        mail.praseminarMail(result2[0].dosen1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result2[0].topik, 'praseminar_'+_.decoded.nim+'_'+time+'.pdf');
                        if(result2[0].dosen2_username) {
                          mail.praseminarMail(result2[0].dosen2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result2[0].topik, 'praseminar_'+_.decoded.nim+'_'+time+'.pdf');
                        }
                        if(result2[0].dosen2_email) {
                          mail.praseminarMail(result2[0].dosen2_email, _.decoded.nim, _.decoded.nama, result2[0].topik, 'praseminar_'+_.decoded.nim+'_'+time+'.pdf');
                        }
                        res.json(req.files.map(file => {

                          let ext = path.extname(file.originalname);
                          return {
                            status: true,
                            detail: 'update',
                            originalName: file.originalname,
                            filename: file.filename
                          }
                        }));
                      }
                    })
                  }
                })
              }

            })

          }
          else {
            con.release();
            res.json({status: false, message: 'File bukan pdf'});
          }

        }

      })
    })

    // ---------------------------

    // ----------------------------
    // UPLOAD SKL
    app.post('/upload/skl', uploadSKL.any(), (req, res) => {
      connection.acquire(function(err, con) {

        // NO FILE
        if (!req.files) {
          con.release();
          res.json({status: false, message: 'Tidak ada file'});
        }

        // FILE
        else {

          if (req.files[0].originalname.search('.pdf') != -1) {
            var queryFile = "SELECT * FROM `ta_skl` WHERE nim = ?";
            var dataFile = [_.decoded.nim];
            con.query(queryFile, dataFile, function(err, result) {

              var query1 = 'UPDATE ta_skl SET berkas = ? WHERE nim = ?';
              var data1 = ['SKL_'+_.decoded.nim+'_'+time+'.pdf', _.decoded.nim];
              con.query(query1, data1, function(err, result1) {
                if (err) {
                  con.release();
                  res.json({status: 0, message: 'Upload File Gagal'});
                  throw err;
                }
                else {

                  var query2 = 'select * from ta_sidang where nim = ?';
                  var data2 = [_.decoded.nim];
                  con.query(query2, data2, function(err, result2) {
                    if (err) {
                      con.release();
                      res.json({status: 0, message: 'Upload File Gagal'});
                      throw err;
                    }
                    else {
                      var tanggal = result2[0].tanggal;

                      var query3 = 'select * from dosen_mahasiswa_view where nim = ?';
                      var data3 = [_.decoded.nim];
                      con.query(query3, data3, function(err3, result3) {
                        con.release();
                        if (err3) {
                          res.json({status: 0, message: 'Upload File Gagal'});
                          throw err3;
                        }
                        else {
                          if(result3[0].dosen1_email) {
                              mail.sklMail(result3[0].dosen1_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'SKL_'+_.decoded.nim+'_'+time+'.pdf', tanggal);
                          }
                          mail.sklMail(result3[0].dosen1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'SKL_'+_.decoded.nim+'_'+time+'.pdf', tanggal);
                          
                          if(result3[0].penguji1_email) {
                              mail.sklMail(result3[0].penguji1_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'SKL_'+_.decoded.nim+'_'+time+'.pdf', tanggal);
                          }
                          mail.sklMail(result3[0].penguji1_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'SKL_'+_.decoded.nim+'_'+time+'.pdf', tanggal);
                          
                          if(result3[0].dosen2_username) {
                            mail.sklMail(result3[0].dosen2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'SKL_'+_.decoded.nim+'_'+time+'.pdf', tanggal);
                          }
                          if(result3[0].dosen2_email) {
                            mail.sklMail(result3[0].dosen2_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'SKL_'+_.decoded.nim+'_'+time+'.pdf', tanggal);
                          }
                          
                          if(result3[0].penguji2_username) {
                            mail.sklMail(result3[0].penguji2_username+'@apps.ipb.ac.id', _.decoded.nim, _.decoded.nama, result3[0].topik, 'SKL_'+_.decoded.nim+'_'+time+'.pdf', tanggal);
                          }
                          if(result3[0].penguji2_email) {
                            mail.sklMail(result3[0].penguji2_email, _.decoded.nim, _.decoded.nama, result3[0].topik, 'SKL_'+_.decoded.nim+'_'+time+'.pdf', tanggal);
                          }

                          // EMAIL TU
                           mail.sklMail('irvan.rmdn@gmail.com', _.decoded.nim, _.decoded.nama, result3[0].topik, 'SKL_'+_.decoded.nim+'_'+time+'.pdf', tanggal);
                           mail.sklMail('ridwanilkom@gmail.com', _.decoded.nim, _.decoded.nama, result3[0].topik, 'SKL_'+_.decoded.nim+'_'+time+'.pdf', tanggal);

                          res.json(req.files.map(file => {
                            let ext = path.extname(file.originalname);
                            return {
                              status: true,
                              detail: 'update',
                              originalName: file.originalname,
                              filename: file.filename
                            }
                          }));
                        }
                      })

                    }
                  })

                }
              })

            })

          }
          else {
            con.release();
            res.json({status: false, message: 'File bukan pdf'});
          }

        }

      })
    })

    // ---------------------------


    // ----------------------------
    // UPLOAD FINAL
    app.post('/upload/final', uploadFinal.any(), (req, res) => {
      connection.acquire(function(err, con) {

        // NO FILE
        if (!req.files) {
          con.release();
          res.json({status: false, message: 'Tidak ada file'});
        }

        // FILE
        else {

          if (req.files[0].originalname.search('.pdf') != -1) {
            var queryFile = "SELECT * FROM `ta_final` WHERE nim = ?";
            var dataFile = [_.decoded.nim];
            con.query(queryFile, dataFile, function(err, result) {

              if(result.length > 0) {
                var query1 = 'UPDATE ta_final SET berkas = ? WHERE nim = ?';
                var data1 = ['TA final_'+_.decoded.nim+'.pdf', _.decoded.nim];
                con.query(query1, data1, function(err, result1) {
                  con.release();
                  if (err) {
                    res.json({status: 0, message: 'Upload File Gagal'});
                    throw err;
                  }
                  else {
                    res.json(req.files.map(file => {

                      let ext = path.extname(file.originalname);
                      return {
                        status: true,
                        detail: 'update',
                        originalName: file.originalname,
                        filename: file.filename
                      }
                    }));
                  }
                })
              }

              else {
                var query2 = 'INSERT ta_final (nim, berkas) VALUES (?, ?)';
                var data2 = [_.decoded.nim, 'TA final_'+_.decoded.nim+'.pdf'];
                con.query(query2, data2, function(err, result2) {
                  con.release();
                  if (err) {
                    res.json({status: 0, message: 'Upload File Gagal'});
                    throw err;
                  }
                  else {
                    res.json(req.files.map(file => {

                      let ext = path.extname(file.originalname);
                      return {
                        status: true,
                        detail: 'insert',
                        originalName: file.originalname,
                        filename: file.filename
                      }
                    }));
                  }
                })
              }

            })

          }
          else {
            con.release();
            res.json({status: false, message: 'File bukan pdf'});
          }

        }

      })
    })

    // ---------------------------

    // ----------------------------
    // UPLOAD PHOTO
    app.post('/upload/photo', uploadPhoto.any(), (req, res) => {

      res.json(req.files.map(file => {

        let ext = path.extname(file.originalname);
        return {
          status: true,
          originalName: file.originalname,
          filename: file.filename
        }
      }));

    })

    // ----------------------------
    // UPLOAD FILE KOLOKIUM
    app.post('/upload/jadwal/kolokium', uploadJadwalKolokium.any(), (req, res) => {

      res.json(req.files.map(file => {

        let ext = path.extname(file.originalname);
        return {
          status: true,
          originalName: file.originalname,
          filename: file.filename
        }
      }));

    })

    // ----------------------------
    // UPLOAD FILE KOLOKIUM
    app.post('/upload/jadwal/micon', uploadJadwalMicon.any(), (req, res) => {

      res.json(req.files.map(file => {

        let ext = path.extname(file.originalname);
        return {
          status: true,
          originalName: file.originalname,
          filename: file.filename
        }
      }));

    })

    // ----------------------------
    // UPLOAD FILE PRASEMINAR
    app.post('/upload/jadwal/praseminar', uploadJadwalPraseminar.any(), (req, res) => {

      res.json(req.files.map(file => {

        let ext = path.extname(file.originalname);
        return {
          status: true,
          originalName: file.originalname,
          filename: file.filename
        }
      }));

    })

  }
};
